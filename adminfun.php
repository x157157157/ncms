<?


function test_cores_list(){
	$out='';
	foreach ( glob ( 'core_*' ) as $ob ) {
		$sufix = str_replace('core_', '', $ob);
		$out.= "<li>
					<a onclick=\"test_toggle('$sufix')\"><span>&nbsp;</span>$sufix</a></li>";
	}
	return $out;
}

function get_files_folder($dir, $sub,$mode=false,$repository=false)
{
    if ($dir)
    {
        $directory=$repository.$dir.'/';
        $pref='/';
    }
    else
    {
        $directory=$repository;
    }

    $out=[];

    if ($handle = opendir($directory))
    {
        if ( $mode!=='files' ) $out[]=  $dir ;

        while (false !== ($file = readdir($handle)))
        {


            if ( is_file($directory.$file) and $mode!=='dirs' )
            {
                $out[]= $dir.$pref.$file ;
                //v($file);
            }
            elseif ($file != '.' and $file != '..' and is_dir($directory.$file)  )
            {
                if ($sub>=2)
                {

                    $out = array_merge( $out , ( get_files_folder( ($dir.$pref.$file ) , $sub-1, $mode, $repository) ) ) ;
                }
            }
        }
    }
    closedir($handle);
    return $out;
}



function fgetcsv2($f_handle, $delimiter=',', $enclosure='"' ) {

    if (!$f_handle || feof($f_handle))
        return false;

    if (strlen($delimiter) > 1)
        $delimiter = substr($delimiter, 0, 1);
    elseif (!strlen($delimiter))
        return false;

    $line =(  fgets($f_handle) ) ;

    if (!$line)
        return false;

    $first_char = true;
    //номер столбца
    $col_num = 0;
    $length = strlen($line);
    for($b = 0; $b < $length; $b++)
    {
        //переменная $skip_char определяет обрабатывать ли данный символ
        if($skip_char != true)
        {
            //определяет обрабатывать/не обрабатывать строку
            ///print $line[$b];
            $process = true;
            //определяем маркер окончания столбца по первому символу
            if($first_char == true)
            {
                if($line[$b] == '"')
                {
                    $terminator = '",';
                    $process = false;
                }
                else
                    $terminator = ',';
                $first_char = false;
            }

            //просматриваем парные кавычки, опредляем их природу
            if($line[$b] == '"')
            {
                $next_char = $line[$b + 1];
                //удвоенные кавычки
                if($next_char == '"')
                    $skip_char = true;
                //маркер конца столбца
                elseif($next_char == ',')
                {
                    if($terminator == '",')
                    {
                        $first_char = true;
                        $process = false;
                        $skip_char = true;
                    }
                }
            }

            //определяем природу точки с запятой
            if($process == true)
            {
                if($line[$b] == ',')
                {
                    if($terminator == ',')
                    {

                        $first_char = true;
                        $process = false;
                    }
                }
            }

            if($process == true)
                $column .= $line[$b];

            if($b == ($length - 1))
            {
                $first_char = true;
            }

            if($first_char == true)
            {

                $csvline_arr[$col_num] = $column;
                $column = '';
                $col_num++;
            }
        }
        else
            $skip_char = false;
    }

    //var_dump($csvline_arr);

    return ($csvline_arr);
}

function folder2array_asort($folder , $types=  ' gif jpg png' )
{
    if ($handle = opendir($folder) )
    {
        while (false !== ($file = readdir($handle)))
        {
            if(  strpos(  $types ,fileending($file)  ) )  $fileindir[$file]=$file;
        }
    }
    closedir($handle);

    $fileindir_lower=(array_map('strtolower', $fileindir));


    asort( $fileindir_lower);

    return $fileindir_lower;
}

function resizer_kw($in_file_image,$out_file_image, $limw, $xmid, $ymid, $_rounder)
{

    $wc   = 1;
    $hc    = 1;
    if ( @intval($limw)==0) $limw=3500;
    list($w_o, $h_o) = getimagesize($in_file_image);
    if ($h_o>$w_o) {$x1 = $h_o / 150 * $xmid;} else {$x1 = $w_o / 150 * $xmid;}
    if ($w_o>$h_o) {$y1 = $w_o / 150 * $ymid;} else {$y1 = $h_o / 150 * $ymid;}
    if ($w_o >= $h_o) {
        $x1N = ($wc/2) + $x1 - ($h_o/2);
        $x2N = $h_o;
        $y1N = 0;
        $y2N = $h_o;
        if (($x1N + $h_o) > $w_o) {
            $x1N = $w_o - $h_o;	}
        if ($x1N < 0) {
            $x1N = 0;	}
    } else {
        $y1N = ($hc/2) + $y1 - ($w_o/2);
        $y2N = $w_o;
        $x1N = 0;
        $x2N = $w_o;
        if (($y1N + $w_o) > $h_o) {
            $y1N = $h_o - $w_o;	}
        if ($y1N < 0) {
            $y1N = 0;	}
    }

    $new = imagecreatetruecolor( $limw, $limw);
    $ink = imagecreatefromjpeg($in_file_image);

    // 	dst_im, 	src_im, 	dstX, 	dstY, 	srcX, 	srcY, 	dstW, 	dstH, 	srcW, 	srcH
    imagecopyresampled(	$new, 	$ink, 	$sdvig, 	0, 		$x1N, 	$y1N, 	$limw, 	$limw, 	$x2N, 	$y2N);
    if  ($_rounder) rounder_set($new);
    imagejpeg($new, $out_file_image, 95);
}

function resizer_kn($in_file_image,$out_file_image, $limw, $limh, $xmid, $ymid, $_rounder){

    $wc   = 1;
    $hc    = 1;
    if ( @intval($limh)==0) $limh=3500;
    if ( @intval($limw)==0) $limw=3500;
    list($w_o, $h_o) = getimagesize($in_file_image);
    if ($h_o>$w_o) {$x1 = $h_o / 150 * $xmid;} else {$x1 = $w_o / 150 * $xmid;}
    $y1 = $h_o / $limh * $ymid;
    $y1N = 0;
    $y2N = $h_o;
    $x2N = $limw * $h_o / $limh;
    $x1N = $x1 - ($x2N - $wc) / 2;
    if ($x1N < 0) {
        $x1N = 0;}
    if ($wc > $x2N) {
        $x2N = $wc;
        $y2N = $limh * $wc / $limw;
        $x1N = $x1 + $wc/2 - $x2N/2;}
    if (($x1N + $x2N) > $w_o)
    { $x1N = $w_o - $x2N;}
    if (($h_o/$w_o)>(intval($limh)/intval($limw))){
        $x2N = $w_o;
        $y2N = $w_o*$limh/$limw;
        $y1 = $h_o * $ymid / 150;
        $y1N = $y1 - $y2N / 2;
        if (($y1N + $y2N) > $h_o)
        { $y1N = $h_o - $y2N;}
        if ($y1N<0) {$y1N=0;}
    }
    //$sdvig = ($limh-($h_o*$limw/$x2N))/2;
    if ($x1N < 0) { 	$x1N = 0;}
    if ($w_o < intval($limw)) {
        $y2N = $w_o*$y2N/$x2N;
        $x2N = $w_o;
        if ($h_o>$w_o) {
            $y1 = $h_o * $ymid / 150;
            $y1N = $y1 - $y2N / 2;
        }
        if ($y1N<0) {$y1N=0;}
        if (($y1N + $y2N) > $h_o) { $y1N = $h_o - $y2N;}
    }
    $new = imagecreatetruecolor($limw, $limh);
    $ink = imagecreatefromjpeg($in_file_image);
    imagecopyresampled($new, $ink, 0, $sdvig, $x1N, $y1N, $limw, $limh, $x2N, $y2N);
    if  ($_rounder) rounder_set($new);
    imagejpeg($new, $out_file_image, 95);

}


function resizer_al($in_file_image,$out_file_image, $limw, $limh, $xmid, $ymid, $_rounder)
{


    $wc   = 1;
    $hc    = 1;
    if ( @intval($limh)==0) $limh=3500;
    if ( @intval($limw)==0) $limw=3500;
    list($w_o, $h_o) = getimagesize($in_file_image);
    //$x1 = $w_o / $limw * $xmid;
    if ($w_o>$h_o) {$y1 = $w_o / 150 * $ymid;} else {$y1 = $h_o / 150 * $ymid;}
    $x1N = 0;
    $x2N = $w_o;
    $y2N = $limh * $w_o /  $limw;
    $y1N = $y1 - ($y2N - $hc) / 2;
    if ($y1N < 0) {
        $y1N = 0;}
    if ($hc > $y2N) {
        $y2N = $hc;
        $x2N =  $limw * $hc / $limh;
        $y1N = $y1 + $hc/2 - $y2N/2;}

    if (($y1N + $y2N) > $h_o)
    { $y1N = $h_o - $y2N;}
    if (($w_o/$h_o)>(intval($limw)/intval($limh))){
        $y2N = $h_o;
        $x2N = $h_o*$limw/$limh;
        $x1 = $w_o * $xmid / 150;
        $x1N = $x1 - $x2N / 2;
        if (($x1N + $x2N) > $w_o)
        { $x1N = $w_o - $x2N;}
        if ($x1N<0) {$x1N=0;}
    }
    //$sdvig = ( $limw-($w_o*$limh/$y2N))/2;
    if ($y1N < 0) { 	$y1N = 0;}
    if ($h_o < intval($limh)) {
        $x2N = $h_o*$x2N/$y2N;
        $y2N = $h_o;
        if ($w_o>$h_o) {
            $x1 = $w_o * $xmid / 150;
            $x1N = $x1 - $x2N / 2;
        }
        if ($x1N<0) {$x1N=0;}
        if (($x1N + $x2N) > $w_o) { $x1N = $w_o - $x2N;}
    }
    //v(get_defined_vars());
    $new = imagecreatetruecolor( $limw, $limh);
    $ink = imagecreatefromjpeg($in_file_image);
    imagecopyresampled($new, $ink, $sdvig, 0, $x1N, $y1N,  $limw, $limh, $x2N, $y2N);
    if  ($_rounder) rounder_set($new);
    imagejpeg($new, $out_file_image, 95);
}


function resizer_smart($in_file_image, $out_file_image, $limw, $limh, $imgcoord=false, $_rounder=true )
{

    $_filetype=fileending($out_file_image);
    //$GLOBALS['v']=$imgcoord;
    $coord = explode(',',  $imgcoord );
    $xmid = floatval($coord[0])+10;
    $ymid = floatval($coord[1])+10;

    if (intval($limw) == intval($limh) )
    {
        resizer_kw($in_file_image,$out_file_image, $limw ,$xmid, $ymid, $_rounder);

    }
    else
    {
        if (intval($limw) > intval($limh) )
        {

            resizer_al($in_file_image,$out_file_image, $limw, $limh, $xmid, $ymid, $_rounder);
        }
        else
        {
            resizer_kn($in_file_image,$out_file_image, $limw, $limh, $xmid, $ymid, $_rounder);
        }
    }

}



function rounder_set($thumb,$_rounder=TRUE)
{
    if ( o('system-rounder') and $_rounder )
    {
        //Читаем картинку
        $s=getimagesize('cont/files/rounder-img.png');
        $rw=o('rounder-w-repeat');// ширина повторяшки
        $w=$s[0];
        $h=$s[1];
        $cw= ($w-$rw)/2;
        $ch= ($h-$rw)/2;

        $rounder=imagecreatefrompng('cont/files/rounder-img.png');
        imagealphablending($rounder,TRUE);

        // накладываем поверх полоски часть закруглатора
        //Копирует часть изображения src_im в dst_im, начиная с x,y-координат src_x, src_y, с шириной src_w и высотой src_h.Определённая часть будет скопирована в x,y-координаты dst_x и dst_y.

        $newwidth=imagesx($thumb);
        $newheight=imagesy($thumb);
        imagealphablending($thumb,TRUE);
        imagecopy ($thumb, $rounder, 0, 0, 0, 0, $cw, $ch); // 1
        imagecopy ($thumb, $rounder, $newwidth-$cw, 0, $w-$cw, 0, $cw, $ch); // 3
        imagecopy ($thumb, $rounder, 0, $newheight-$ch, 0, $h=$ch, $cw, $ch); // 7
        imagecopy ($thumb, $rounder, $newwidth-$cw, $newheight-$ch, $w-$cw, $h=$ch, $cw, $ch); // 9

        // накладываем углы

    }
}



function check_view_admin($ident)
{


    //echo $mm['ident']." - ".$mm['rod'].'<br/>';
    $qu = 'SELECT `rod`, `ident`, `d100` FROM '.pref_db.'content WHERE `ident`='.xs($ident).'  limit 1' ;
    //$qu);
    $pr=my_mysql_query( $qu );
    $mm=mysqli_fetch_assoc($pr);


    if ( ($mm) )
    {
        if ( rg($mm['d100']) )
        {
            return check_view_admin($mm['rod']);
        }
        else
        {
            return false;
        }

    }
    else
    {
        return true;
    }

}


function automenu() //2
{

    $qu = 'SELECT `ident`,`d1`,`d3`,`d6`,`d4`,`type` FROM '.pref_db.'content WHERE `prio`=\'1\' AND `d5`<>\'\' AND (`type`="page" or `type`="rootopros")' ;
    //v($qu);
    $pr=my_mysql_query( $qu );

    $rand=range(0,mysqli_num_rows($pr) - 1);
    shuffle($rand);

    foreach ( $rand as $rd)
    {
        $mm['ident'] = mysql_result($pr, $rd,0);
        $mm['d1'] = mysql_result($pr, $rd,1);
        $mm['d3'] = mysql_result($pr, $rd,2);
        $mm['d6'] = mysql_result($pr, $rd,3);
        $mm['d4'] = mysql_result($pr, $rd,4);
        $mm['type'] = mysql_result($pr, $rd,5);



        if ( file_exists(cont.'/automenu/'.$mm['ident'].'.jpg' ) )
        {
            $img='<img src="'.cont.'/automenu/'.$mm['ident'].'.jpg" />';
            $ok= true and (bool)$mm['d3'] ;
        }
        if ($mm['d1'])
        {
            $hr=hrpref.$mm['d1'].hrsuf.lngsuf;
        }
        else
        {
            $hr=hrpref.$mm['ident'].hrsuf.lngsuf;
        }

        $h=$h+$mm['d4'];


        if ($h>$GLOBALS['longpage'][1])
        {
            $out.= " [automenu]"; //
            $left=1;
            $h=$mm['d4'];
        }

        //$out.= "{$h}-{$GLOBALS['longpage'][1]}-{$GLOBALS['longpage'][2]}<br/>";

        if( ($h>($GLOBALS['longpage'][1]-$GLOBALS['longpage'][2]) ) and ((bool)$left) )
        {

            break;
        }



        if ($mm['type']=='rootopros' )
        {

            $out.="<div class='autom'><div class='autom-1'></div><div class='autom-2'>".imgex(img.'/autoimg/'.$mm['ident'].'.jpg').opros($mm['ident'])."</div><div class='autom-3'></div></div>";
        }
        else
        {

            $out.="<div class='autom'><div class='autom-1'></div><div class='autom-2'><a href='{$hr}'>".imgex(img.'/autoimg/'.$mm['ident'].'.jpg')."{$mm['d3']}</a></div><div class='autom-3'></div></div>";
        }
        //$out.= "<div><a href ='$hr'>$img {$mm['d3']}</a></div>";




    }
    //$out = iconv("windows-1251", "UTF-8", $out);



    echo $out;
    //v($out);
    $bd='title~title
		
		ident~ident
		ok~ok
		d1~link
		d3~autoname
		d5~okautolen
		d6~heightlen
		prio~prioritet';

}

function filename_ext_nofull($filename)
{
    $filename=array_reverse(explode('/',$filename));
    return $filename[0];
}




function curl_file_get_contents($url,$ref='auto',$caching=false)
{
    if( is_array($url) ) extract( $url );

    if ( CURLVERSION_NOW === 'CURLVERSION_NOW' )
    {
        return file_get_contents($url);
    }
    else
    {
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c, CURLOPT_USERAGENT, "Mozila/4.0(compatible; MSIE 6.0; Windows NT 5.1)");
        if ($ref=='auto' or $ref==TRUE )
        {
            $cat=explode('/',$url);
            array_pop($cat);
            $ref=implode('/',$cat);
        }
        //v($ref);
        if ($ref) curl_setopt($c,CURLOPT_REFERER, $ref);
        //v(CURLOPT_REFERER);
        if (!$caching) curl_setopt($c, CURLOPT_FRESH_CONNECT, 1); // не использовать cache
        $contents = curl_exec($c);
        curl_close($c);

        if ($contents) return $contents;
        else return FALSE;
    }


}





function upload_remoute_img($html_text,$upload_dir,$del_w_h_img)
{
    include_once('jscripts/lib/simple_html_dom.php');
    $html = str_get_html($html_text);
    foreach ($html->find('img') as $img )
    {
        $src=$img->src;
        if (substr($src,0,4)=='http')
        {
            $file=curl_file_get_contents($src,'auto',TRUE);
            $fullname=filename_ext_nofull($src);
            // hазделяем на имя и расширение
            $filename=filename($fullname);
            $ext=fileending($fullname);
            // Добавляем к имени случайную цифру
            $filename=$filename.'-'.rand(1,999);
            $outfile=$upload_dir.'/'.$filename.'.'.$ext;
            // записываем куда надо
            file_put_contents($outfile,$file);
            //echo '<img src="'.$outfile.'">';

            $html_text=str_replace($src,$outfile,$html_text);
        }

    }
    return $html_text;

}

//curl_mypars_contents('http://netnarkotik.ru/',TRUE,TRUE,'#right img/1','src',FALSE,'cont/img/1.jpg',FALSE,'http://netnarkotik.ru/');

function curl_mypars_contents ($url, $ref, $caching, $selector,$content_type, $is_charset_utf8, $file_save_path,$save_name_img,$base_href_for_img,$ident)
{
    $file=curl_file_get_contents($url,$ref,$caching);

    $dom = new DOMDocument();//создаем объект DOM

    if ($is_charset_utf8) 	{$charset='utf-8';			}
    else	{	$charset='windows-1251';		}

    //v('<textarea cols="50" rows="10">'.$file.'</textarea>');
    $file = prepareForDOM($file, $charset); // pакрываем все теги

    @$dom->loadHTML($file );
    $file=$dom->saveHTML();
    unset($dom);

    //парсим:
    include_once('jscripts/lib/simple_html_dom.php');

    //v('<textarea cols="50" rows="10">'.$file.'</textarea>');
    $html = str_get_html($file);

    $selec=explode(' ',$selector);
    foreach ($selec as $v)
    {
        $s=explode('/',$v);
        $sel[]='find(\''.$s[0].'\','.intval($s[1]).')';
    }

    $act='$elem=$html->'.implode('->',$sel).';';
    //$elem=$html->find('table',0)->find('a',1);
    //v($act);
    eval($act);

    // #podrob_table td/2
    // find('#podrob_table',0)->find('td',2)


    if ($content_type=='html')
    {
        $out=trim($elem->innertext);
    }
    elseif ($content_type=='text')
    {
        $out=trim($elem->plaintext);
    }
    elseif($content_type=='href')
    {
        $out=trim($elem->href);
    }
    elseif($content_type=='alt')
    {
        $out=trim($elem->alt);
    }
    elseif($content_type=='title')
    {
        $out=trim($elem->title);
    }
    elseif($content_type=='src')
    {
        $out=trim($elem->src);
        if ( $out)
        {
            $filenamenoful=filename_ext_nofull($out);
            $imgdata=curl_file_get_contents($base_href_for_img.$out,'auto',TRUE);

            if ($imgdata)
            {
                file_put_contents($file_save_path,$imgdata);
                creat_cat_img($file_save_path,$ident);
            }

            echo "-<img width='100' src='/{$file_save_path}'>-<br> ";
            //usleep($GLOBALS['zad']*1000);
        }
    }

    $out=strip_tags(iconv('utf-8','windows-1251', strtr($out, array( "\t"=>'', "\v"=>'', "\0"=>'',"\n"=>'',"\r"=>'','	'=>'' )) ), o('importtags') );

    if ($file_save)
    {
        file_put_contents($file_save_path,$out);
    }
    else
    {
        return $out;
    }






}


function extractor($start, $end, $text, $mode)  //2
{

    preg_match_all("!{$start}(.*?){$end}!si",$text,$ok);

    return $ok[1][0];

}



function truebase()  //2
{

    $prebase=my_mysql_query( 'SELECT * FROM '.pref_db.'content limit 1,1 ' ) ;
    $mm=mysqli_fetch_array($prebase);

    $keys=explode(' ',$GLOBALS['field_exist']);

    foreach ($keys as $k)
    {

        if ( !(isset($mm[trim($k)] )) & ( (bool)trim($k) )  )
        {
            //v(trim($k));
            $qu='ALTER TABLE '.pref_db.'content ADD `'.trim($k).'` TEXT NOT NULL AFTER `'.$after.'` ';
            //echo "insert :$k<br/>";
            v($qu);
            my_mysql_query($qu);

        }
        else
        {
            $after=trim($k);
        }
    }

}

function mycron($num)
{
    while ($num>=1)
    {
        $pr=my_mysql_query( 'SELECT * FROM `'.pref_db.'cron` ORDER BY `id` desc LIMIT 1' ) ;
        @$mm=mysqli_fetch_assoc($pr);
        my_mysql_query('DELETE FROM `'.pref_db.'cron` WHERE `id`='.xs($mm['id']).' ');

        eval ($mm['event']);
        $num--;

    }

    die();
}

function incron($event)
{
    my_mysql_query( 'INSERT INTO `'.pref_db.'cron` (`event`) VALUES ('.xs($event).')');
}



function get_index_structure($table_name ,$index_name)
{
    $qu='SHOW INDEX FROM '.pref_db.$table_name;

    $pr=my_mysql_query( $qu ) ;

    while ($mm=mysqli_fetch_assoc($pr) )
    {
        $out[$mm['Key_name']]=$mm;
    }
    //v($out);
    if ($index_name)
    {
        return $out[$index_name];
    }
    else
    {
        return $out;
    }
}



function create_mysql_table ($table_name)
{
    $pr=my_mysql_query( 'SHOW COLUMNS FROM '.pref_db.$table_name ) ;
    @$mm=mysqli_fetch_assoc($pr);
    if (!$mm)
    {
        $qu='CREATE TABLE IF NOT EXISTS `'.pref_db.$table_name.'` 
						(
						`id` int(11) NOT NULL auto_increment,						  
						PRIMARY KEY  (`id`)
						)
						ENGINE=InnoDB  
						DEFAULT CHARSET=utf8 
						DEFAULT COLLATE utf8_general_ci 
						AUTO_INCREMENT=2 ';

        if ( my_mysql_query($qu) )  echo "таблица {$table_name} создана<br/>" ;
    }
}

function mydb_field_move ( $table_name, $field_name_old , $field_name ) {
    $fields = mydb_get_fields($table_name);
    if ( $fields[$field_name_old] AND $fields[$field_name]   ) {
        $qu='UPDATE `'.pref_db.$table_name.'` set '.$field_name.'='.$field_name_old.' WHERE 1 ';
        my_mysql_query($qu);
        $qu='ALTER TABLE `'.pref_db.$table_name.'` DROP '.$field_name_old;
        my_mysql_query($qu);
        echo 'Переименовано поле '.$field_name_old.' на '.$field_name.'<br/>';

    }


}

function mydb_field_create ($table_name, $field_left, $field_name, $type, $lenght, $keyed, $exist_key )
{
    /* ALTER TABLE `ncms__proba` ADD `uid` TEXT NOT NULL AFTER `id` ;*/
    // ALTER TABLE `ncms__proba` CHANGE `pass` `pass` LONGTEXT CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL
    if ($lenght) $lenght="($lenght)";
    if ($field_left) $after='AFTER `'.$field_left.'`';

    if ($exist_key)
    {
        $act=' CHANGE `'.$field_name.'`';
        $mesage='изменено';
    }
    else
    {
        $act=' ADD ';
        $mesage='добавлено';
    }
    $oldfield=implode('',get_field_structure($table_name,$field_name));

    if  (!$exist_key or $GLOBALS['dropkey'] )
    {
        $qu='ALTER TABLE `'.pref_db.$table_name.'` '.$act.' `'.$field_name.'` '.$type.$lenght.' NOT NULL '.$after;

        if ( my_mysql_query($qu) )
        {
            if ( $oldfield!==implode('',get_field_structure($table_name,$field_name)) )
                echo " В таблице $table_name $mesage поле $field_name <br> ";
        }
        else
        {
            echo 'Плохой запрос: '.$qu;
        }
    }

}

function create_or_destroy_index($table_name, $mm)
{

    @ $oldindex=implode('', get_index_structure($table_name,$mm['d1']) );
    //v($oldindex);
    if ( (!$mm['d5'] or $GLOBALS['dropkey'] ) and $oldindex )
    {

        // ALTER TABLE `ncms__proba` DROP INDEX `logpass`
        $qu='ALTER TABLE `'.pref_db.$table_name.'` DROP INDEX `'.$mm['d1'].'`';
        if ( my_mysql_query($qu) )
        { $del=1; }
        else
        {
            //echo 'Плохой запрос: '.$qu;
        }

    }

    if ($mm['d5'])
    {
        //ALTER TABLE `ncms__proba` ADD INDEX `status_reg` ( `status_reg` ( 5 ) )
        //v($mm);
        if ($mm['d3']) $lenght='('.$mm['d3'].')';
        if ($mm['d6'])
        {
            /* ALTER TABLE `ncms__proba` ADD FULLTEXT `ww` (
								`login` ( 4 ) ,
								`pass` ( 4 )
								)  */
            //v(1);
            for ($x=11; $x<=16; $x++)
            {
                if ($mm['d'.$x])
                {
                    $dpk=explode('/',$mm['d'.$x]);
                    if ($dpk[1]) $lenghtdpk='('.$dpk[1].')';
                    $dopkey.=', `'.trim($dpk[0]).'`'.$lenghtdpk;
                }
            }
        }

        $qu='ALTER TABLE `'.pref_db.$table_name.'` ADD '.$mm['d5'].' `'.$mm['d1'].'` ( `'.$mm['d1'].'`'.$lenght.$dopkey.')';
        //v($qu);
        if ( my_mysql_query($qu) )
        { $add=1; }
        else
        {
            //echo 'Плохой запрос: '.$qu;
        }
    }
    @ $newindex=implode('', get_index_structure($table_name,$mm['d1']) );

    if ( $add and  $del  )
    {
        if ( $oldindex!==$newindex )
        {
            $act= 'изменён';
        }
    }
    elseif ($add)
    {
        $act='добавлен';

    }
    elseif ( $del )
    {
        $act='удалён';
    }

    if ($act) echo " В таблице $table_name $act индекс {$mm['d1']} <br> ";

}


/**
 * @param $table_name
 * @property array  $field {
 * 		@type string $fieldname
 * 		@type string $index_type
 * 		@type string $name
 *   	@type string $field_old




 *
 * }
 *
 *
 * 'type'=>$mm2['d2'],
'lenght'=>$mm2['d4'],
'index_type'=>$mm2['d5'],
'index_lenght'=>$mm2['d3'],
'num'=>$mm2['d8'],
'fun_in'=>$mm2['d9'],
'fun_out'=>$mm2['d10'],
'dopindex'=>$mm2['d6'],
'dopindex1'=>str_replace( '/' , '.' , $mm2['d11']),
'dopindex2'=>str_replace( '/' , '.' , $mm2['d12']),
'dopindex3'=>str_replace( '/' , '.' , $mm2['d13']),
'dopindex4'=>str_replace( '/' , '.' , $mm2['d14']),
'dopindex5'=>str_replace( '/' , '.' , $mm2['d15']),
'dopindex6'=>str_replace( '/' , '.' , $mm2['d16']),
 */
function create_or_destroy_index2($table_name, $field)
{
    if (0) {
        $field = [
            'name'=>$mm2['name'],
            'field'=>$mm2['d1'],
            'field_old'=>$mm2['sub']['d1_old'],
            'type'=>$mm2['d2'],
            'lenght'=>$mm2['d4'],
            'index_type'=>$mm2['d5'],
            'index_lenght'=>$mm2['d3'],
            'num'=>$mm2['d8'],
            'fun_in'=>$mm2['d9'],
            'fun_out'=>$mm2['d10'],
            'dopindex'=>$mm2['d6'],
            'dopindex1'=>str_replace( '/' , '.' , $mm2['d11']),
            'dopindex2'=>str_replace( '/' , '.' , $mm2['d12']),
            'dopindex3'=>str_replace( '/' , '.' , $mm2['d13']),
            'dopindex4'=>str_replace( '/' , '.' , $mm2['d14']),
            'dopindex5'=>str_replace( '/' , '.' , $mm2['d15']),
            'dopindex6'=>str_replace( '/' , '.' , $mm2['d16']),


        ];
    }

    @ $oldindex=implode('', get_index_structure($table_name,$field['fieldname']) );
    //v($oldindex);
    if ( (!$field['index_type'] or $GLOBALS['dropkey'] ) and $oldindex )
    {

        // ALTER TABLE `ncms__proba` DROP INDEX `logpass`
        $qu='ALTER TABLE `'.pref_db.$table_name.'` DROP INDEX `'.$field['fieldname'].'`';
        if ( my_mysql_query($qu) )
        { $del=1; }
        else
        {
            //echo 'Плохой запрос: '.$qu;
        }

    }

    if ($field['index_type'])
    {
        //ALTER TABLE `ncms__proba` ADD INDEX `status_reg` ( `status_reg` ( 5 ) )
        //v($mm);
        if ($field['index_lenght']) $lenght='('.$field['index_lenght'].')';
        if ($field['dopindex'])
        {
            /* ALTER TABLE `ncms__proba` ADD FULLTEXT `ww` (
								`login` ( 4 ) ,
								`pass` ( 4 )
								)  */
            //v(1);
            for ($x=1; $x<=6; $x++)
            {
                if ($field['dopindex'.$x])
                {
                    $dpk=explode('.',$field['dopindex'.$x]);
                    if ($dpk[1]) $lenghtdpk='('.$dpk[1].')';
                    $dopkey.=', `'.trim($dpk[0]).'`'.$lenghtdpk;
                }
            }
        }

        $qu='ALTER TABLE `'.pref_db.$table_name.'` ADD '.$field['index_type'].' `'.$field['fieldname'].'` ( `'.$field['fieldname'].'`'.$lenght.$dopkey.')';
        //v($qu);
        if ( my_mysql_query($qu) )
        { $add=1; }
        else
        {
            //echo 'Плохой запрос: '.$qu;
        }
    }
    @ $newindex=implode('', get_index_structure($table_name,$field['fieldname']) );

    if ( $add and  $del  )
    {
        if ( $oldindex!==$newindex )
        {
            $act= 'изменён';
        }
    }
    elseif ($add)
    {
        $act='добавлен';

    }
    elseif ( $del )
    {
        $act='удалён';
    }

    if ($act) echo " В таблице $table_name $act индекс ".$field['fieldname']." <br> ";

}



function newbase($go=false)  //2
{
    patch::newbase($go);
}


function hidden()
{
    $field2=qqu("`type`='contenttype' and `d1`='{$GLOBALS['m']['id']}'",'d2');
    if ($field2) $field2=' ('.$m[$field2].')';
    $hhtitle=$GLOBALS['m']['name'].$field2;
    echo '<input  type="hidden" name="ruscode" value="рускод" />';
    echo '<input  type="hidden" name="id" value="'.$GLOBALS['m']['id'].'" id="editid" />
	<input  type="hidden" name="view" value="pages" />
	<input  type="hidden" name="edit" value="'.$GLOBALS['m']['num'].'" />
	<input  type="hidden" name="mode" value="'.$GLOBALS['mode'].'" />
	<input  type="hidden" name="htitle" id="htitle" value="'.$hhtitle.'" />
	<input  type="hidden" name="identedit" id="identedit" value="'.$GLOBALS['m']['ident'].'" />
	<input  type="hidden" name="typeedit" id="typeedit" value="'.$GLOBALS['m']['type'].'" />
	<input  type="hidden" name="lim" value="'.$GLOBALS['lim'].'" />
	';
}

function imresize($in_file_image, $out_file_image=false, $limw=false, $limh=false, $_qual=false, $_mode=false , $_rounder=true, $_mark=false )

{


    if (is_array($in_file_image)) extract($in_file_image);

    $_filetype=( fileending($out_file_image) );

    $qual=$_qual;
    $mode=($_mode);//(__LINE__);

    $size_im = (getimagesize($in_file_image));

    $limh = $limh ?: 1500;
    $limw = $limw ?: 1500;

    $k0 = $size_im[0]/intval($limw);
    $k1 = $size_im[1]/intval($limh);

    if (  $k0>1 or $k1>1  )
    {
        if ( ($k0 > $k1) xor $mode=='crop' )
        {
            $k=$k0;
        }
        else
        {
            $k=$k1;
        }
    }
    else
    { $k=1; }


    $newwidth = intval($size_im[0]/ $k);
    $newheight = intval($size_im[1] / $k);

    if ($size_im['mime']=="image/jpeg")
    {
        $im = imagecreatefromjpeg ( $in_file_image );
    }
    elseif ($size_im['mime']=="image/png")
    {
        $im = imagecreatefrompng ( $in_file_image );
        imagesavealpha($im,true);
    }
    elseif ($size_im['mime']=="image/gif")
    {
        $im = imagecreatefromgif ( $in_file_image );
        //imagetruecolorimagepalettetotruecolortopalette($im, false, 255*255*255 );
        //($im);
        imagejpeg($im, img.'/cat/big/tmp');
        $im = imagecreatefromjpeg( img.'/cat/big/tmp' );
    }

    $thumb =  ( $mode=='crop' ? imagecreatetruecolor($limw, $limh) : imagecreatetruecolor( ($newwidth), ($newheight)) ) ;



    imagesavealpha($thumb,true);
    imagealphablending($thumb, false);
    imagesavealpha($im,true);
    imagealphablending($im, false);

    $transparent = imagecolorallocatealpha($thumb, 255, 255, 255, 127);
    imagefilledrectangle($thumb, 0, 0,  $newwidth, $newheight, $transparent);
    if ($mode=='crop')
    {
        imagefilledrectangle($thumb, 0, 0,  $limw, $limh, $transparent);
    }
    else
    {
        imagefilledrectangle($thumb, 0, 0,  $newwidth, $newheight, $transparent);
    }

    if ($mode=='crop')
    {
        if ($newwidth>$limw)
        {
            $ofset_x=round( ($newwidth-$limw)/2);
        }

        if ($newheight>$limh)
        {
            $ofset_y=round( ($newheight-$limh)/2);
        }

        imagecopyresampled($thumb, $im, -$ofset_x, -$ofset_y, 0, 0, $newwidth, $newheight, $size_im[0], $size_im[1]);
    }
    else
    {
        imagecopyresampled($thumb, $im, 0, 0, 0, 0, $newwidth, $newheight, $size_im[0], $size_im[1]);
    }



    imageinterlace($thumb, true);


    rounder_set($thumb,$_rounder);


    if ($_filetype=="jpg")
    {
        if ( !( (bool)$qual) )
        {
            $qual=90;
        }

        imagejpeg ( $thumb, $out_file_image , $qual );
    }
    elseif ($_filetype=="png")
    {
        imagepng ( $thumb, $out_file_image );
    }
    elseif ($_filetype=="gif")
    {
        imagegif ( $thumb, $out_file_image );
    }

}



/*function imagepalettetotruecolor(&$img)
     {
         if (!imageistruecolor($img))
         {
             $w = imagesx($img);
             $h = imagesy($img);
             $img1 = imagecreatetruecolor($w,$h);
             imagecopy($img1,$img,0,0,0,0,$w,$h);
             $img = $img1;
         }
     }*/

if(!function_exists('imagepalettetotruecolor'))
{
    function imagepalettetotruecolor(&$src)
    {
        if(imageistruecolor($src))
        {
            return(true);
        }

        $dst = imagecreatetruecolor(imagesx($src), imagesy($src));

        imagecopy($dst, $src, 0, 0, 0, 0, imagesx($src), imagesy($src));
        imagedestroy($src);

        $src = $dst;

        return(true);
    }
}


function uploadcontent($ident,$content)
{
    file_put_contents(cont.'/htm/'.$ident.'.htm',$content);
}

function uploadcatimg($ident, $url)
{
    if ($url)
    {
        if (  substr($url,0,5)=='http:' and  !strpos( ' '.$url, basehref )  )
        {
            $fe=curl_file_get_contents($url,1,1);
            file_put_contents(img.'/cat/big/tmp',	$fe );
            $_FILES['img']['tmp_name']=img.'/cat/big/tmp';
            unset($url);
            usleep($GLOBALS['comlagimg']*200000);
        }
        else if ( !substr($url,0,5)=='http:' )
        {
            $_FILES['img']['tmp_name']=img.'/cat/'.$url;
        }
        else
        {
            $url=explode('?',$url);
            $_FILES['img']['tmp_name']= str_replace(basehref.'/','',$url[0]);
        }




        creat_cat_img( ($_FILES['img']['tmp_name']) , $ident );

    }




    /*
		if (  $_FILES['img']['tmp_name']   )
			{
			//v( $_FILES['img']['tmp_name'] );
			imresize($_FILES['img']['tmp_name'], img.'/cat/min/'.$ident.'.jpg' , 60, 60 );
			imresize($_FILES['img']['tmp_name'], img.'/cat/mid/'.$ident.'.jpg' , o('tov_img_w'), o('tov_img_h') );
			imresize($_FILES['img']['tmp_name'], img.'/cat/big/'.$ident.'.jpg' , 1000, 700 );

			echo "-<img alt='{$ident}.jpg' src='".img.'/cat/min/'.$ident.'.jpg'."' />-<br/>";
			}
		*/
}

function update_rod_info()
{
    $qu1='UPDATE `'.pref_db."content` SET `nadcat`='0' 
		WHERE `type`='page' or `type`='category' ";
    //v($qu1);
    my_mysql_query($qu1);

    $qu='SELECT `rod` FROM `'.pref_db."content` 
			WHERE 
			(`type`='page' or `type`='category') and `ok`<>'' " ;
    $prebase=my_mysql_query($qu);
    while ( $mm=mysqli_fetch_array($prebase) )
    {
        if (!$uprod[$mm['rod']])
        {
            $qu2='UPDATE `'.pref_db."content` SET `nadcat`='1' WHERE `ident`=".xs($mm['rod']);
            my_mysql_query($qu2);
            $uprod[$mm['rod']]=1;
        }

    }
}



function edit_sismenu()
{

    global $submit1, $m, $elm1, $mode, $name, $ident,$id;
    $form='sismenu-1';
    $vars= select_fields($form);

    if (isset($submit1))
    {

        // newident
        $newident=newident($id,$ident);
        $ident=$newident;

        if ($GLOBALS['url'])
        {
            $fe=curl_file_get_contents($GLOBALS['url'],'auto', FALSE);

            file_put_contents(img.'/cat/big/tmp',	$fe );
            $_FILES['img']['tmp_name']=img.'/cat/big/tmp';
            unset($GLOBALS['url']);
        }

        if ( $_FILES['img']['tmp_name'] )
        {
            //v($_FILES['img']['tmp_name']);

            move_uploaded_file($_FILES['img']['tmp_name'], jscripts.'/tpladmin/'.$ident.'.png' );


        }

        update($vars);

    }
    else
    {

    }
    select($vars) ;
    if ($GLOBALS['multisubmit']) return; menu_edit();;
    //v(intval($ident));
    ?>


    <form class='ajaxform' method="post" action="myadmin.php" enctype="multipart/form-data">
        <input type="hidden" name="ajax" value="1" />

        <?=hidden(); ?><br />

        <input class="admbut" type="submit" name="submit1" value="Сохранить!" id="submit_tn" />
        <?=form($form); ?>
        <br />
        <input class="admbut" type="submit" name="submit1" value="Сохранить!" id="submit_tn" /></form>
    <br /><br /><div id='response'><div></div><img src='/<?=jscripts.'/tpladmin/'.$ident.'.png?'.rand(); ?>' /><div></div></div>
    <br/>
    <?

}



function edit_zadacha()
{

    global $submit1, $m, $elm1, $mode, $name, $ident,$id;
    $form='zadacha-1';
    $vars= select_fields($form);

    if (isset($submit1))
    {

        // newident
        $newident=newident($id,$ident);
        $ident=$newident;

        if ($GLOBALS['url'])
        {
            $fe=curl_file_get_contents($GLOBALS['url'],'auto', FALSE);

            file_put_contents(img.'/cat/big/tmp',	$fe );
            $_FILES['img']['tmp_name']=img.'/cat/big/tmp';
            unset($GLOBALS['url']);
        }

        if ( $_FILES['img']['tmp_name'] )
        {
            //v($_FILES['img']['tmp_name']);

            move_uploaded_file($_FILES['img']['tmp_name'], jscripts.'/tpladmin/'.$ident.'.png' );


        }

        update($vars);

    }
    else
    {

    }
    select($vars) ;
    if ($GLOBALS['multisubmit']) return; menu_edit();;
    //v(intval($ident));
    ?>

    <h2>Задача</h2>
    <form class='ajaxform' method="post" action="myadmin.php" enctype="multipart/form-data">
        <input type="hidden" name="ajax" value="1" />

        <?=hidden(); ?><br />


        <?=form($form); ?>
        <br />
        <input class="admbut" type="submit" name="submit1" value="Сохранить!" id="submit_tn" /></form>
    <br /><br /><div id='response'><div></div><img src='/<?=jscripts.'/tpladmin/'.$ident.'.png?'.rand(); ?>' /><div></div></div>
    <br/>
    <?

}



function adminhmenu($rod) //6
{
    global $put,$m;

    @$pref=func_get_arg (1);
    @$ur=func_get_arg (2);
    if ( !$ur)
    {
        find_rod($m['ident']);
        //v($m['ident']);
        if ($put) $put=array_reverse ($put);
        //$pref=qvar('ident',$rod,'d1');

        $out='<ul id="nav">'.adminhmenu($rod,$pref,1).'</ul>'."\r\n";

        $namereg[0]='Обычный';$namereg[5]='Продвинутый';$namereg[9]='Профессиональный';


        $rep['[reg]']=$namereg[reg()];
        $rep['[hrpref]']=hrpref;

        return strtr($out,$rep);
    }
    else
    {


        $prebase=my_mysql_query( 'SELECT * FROM '.pref_db.'content WHERE rod=\''.$rod.'\'  AND `d100`<='.reg().'   AND ok<>\'\'  ORDER BY `num` ASC ' ) ;

        $mm=mysqli_fetch_array($prebase);

        if ($mm)
        {


            while ($mm)// затем начинаем перебирать каждый пункт и делать выборку по каждому (рекурсивно)
            {
                setsub($mm);
                if (is_ok_opt($mm['d99'])) {
                    if ($mm['ident']==$m['ident'])
                    {
                        $xx='class="submen"';

                    }
                    else
                    {
                        $xx='';
                    }

                    if ($mm['nolink'])
                    {
                        $href='';

                    }
                    else
                    {
                        if ($mm['ident']=='index')
                        {
                            $href='href="'.hrpref.lngsuf.'"';
                        }
                        else
                        {
                            $href='href="'.hrpref.trim($mm['ident']).hrsuf.lngsuf.'"';
                        }
                    }

                    if ($mm['d1']<>'')
                    {
                        if (substr($mm['d1'],0,7)=='http://')
                        {
                            $href='href="'.trim($mm['d1']).'"';
                        }
                        else
                        {
                            $href='href="'.trim($mm['d1']).hrsuf.lngsuf.'"';
                        }

                    }

                    if ( ($m['ident']==$mm['ident']) & ($ur<2) )
                    {
                        $class='act '.$pref.$ur;
                    }
                    else
                    {$class=$pref.$ur; }




                    if ($ur==1)
                    {
                        $class_L='top';
                        $class_A='top_link';
                        $subs='sub';

                    }
                    else
                    {
                        $class_L=$class_A=$subs='';
                        $iconfile=jscripts.'/tpladmin/'.$mm['ident'].'.png' ;
                        if ( file_exists($iconfile) )
                        {
                            $icon='<img src="'.$iconfile.'" />';
                        }
                        else
                        {
                            $icon='<span>&nbsp;</span>';
                        }

                    }


                    if ( $mm['ident'] =='pereklyuchit-testovyiy-rezhim' OR qvar('rod',$mm['ident'],'ok')  )
                    {
                        //MenuBarSubmenuVisible

                        $sub= "<ul class='$subs'>\r\n";
                        if ($mm['ident'] =='pereklyuchit-testovyiy-rezhim') {
                            $sub.=block('admul', test_cores_list() ,$pref) ;
                        } else {
                            $sub.=block('admul', adminhmenu ($mm["ident"],$pref,$ur+1) );
                        }
                        $sub.= '</ul>'."\r\n";
                        $subli='fl';
                        $class_A=$class_A.' fla';
                    }
                    else
                    {
                        $sub='';
                        $subli='';
                    }

                    $href= $mm['sub']['onclick'] ? " onclick=\"{$mm['sub']['onclick']}\" " : $href ;
                    $target= $mm['sub']['is_target_blank'] ? ' target="_blank" ' : '' ;
                    $out.= "<li class='$class_L $subli {$mm['ident']}'>
	<a class='{$class_A}' $target $href  >{$icon}{$mm['name']}</a>{$sub}</li>\r\n"	;


                }

                $mm=mysqli_fetch_array($prebase);

            }

            return $out;
        }
    }

}



function findnotrod()
{
    $prebase=my_mysql_query( 'SELECT * FROM '.pref_db.'content ') ;
    while($mm=mysqli_fetch_array($prebase) )
    {
        if (!qvar('ident',$mm['rod'],'id') and $mm['rod'] )
        {
            $out[]=$mm['ident'];
            echo $mm['ident'].my_mysql_query( 'UPDATE `'.pref_db."content` SET `rod`='norod' WHERE `id`=".xs($mm['id']) ) .'<br/>' ;
        }
    }
    return $out;
}



function menu_edit()

{

    $mod[$GLOBALS['mode']]='cur';

    $lng[$GLOBALS['lng']]='lng';

    $lim='lim='.$GLOBALS['lim'].'&';

    //

    ?><div id ='outmenu'><table class="menu_edit" width="100%" border="0" cellspacing="0" cellpadding="3">

        <tr>

            <td align="center" class="<?=$mod[1]; ?>"><div><a onClick="javascript:chpage(<?=$GLOBALS['m']['id']; ?>,1)" >Свойства</a></div></td>



            <td align="center" class="<?=$mod[2]; ?>">
                <div id="editdiv">

                    <? if(o('multi_lang')) { ?>

                        <a onclick="javascript:chpage(<?=$GLOBALS['m']['id']; ?>,2)"><span class="context4"><img src="/<?=jscripts; ?>/tpladmin/spacer.gif"></span> Редактор
                            <img class="lng" src="/<?=jscripts; ?>/tpladmin/flag_ru.png" width="25" height="15" border="1" align="absmiddle"></a>&nbsp;
                        <a href="javascript:chpage(<?=$GLOBALS['m']['id']; ?>,2,'uk')">
                            <img class="" src="/<?=jscripts; ?>/tpladmin/flag_uk.png" width="25" height="17" border="0" align="absmiddle"></a>&nbsp;
                        <a href="javascript:chpage(<?=$GLOBALS['m']['id']; ?>,2,'en')">
                            <img class="" src="/<?=jscripts; ?>/tpladmin/flag_en.png" width="25" height="17" border="0" align="absmiddle"></a>

                    <? } else { ?>

                        <a  onClick="javascript:chpage(<?=$GLOBALS['m']['id']; ?>,2)"  ><span class="context4"><img src='/<?=jscripts; ?>/tpladmin/spacer.gif'></span> Редактор
                        </a>&nbsp;

                    <? } ?>

                </div>

            </td>


            <td align="center" class="<?=$mod[0]; ?>"><div><a onClick="javascript:chpage(<?=$GLOBALS['m']['id']; ?>,0)">Настройка</a></div></td>
            <?

            if( rg(5) )
            {
                ?>
                <td align="center" ><div><a target="_blank" href="/myadmin.php?id=<?=$GLOBALS['m']['id']; ?>&mode=multi">Таблица</a></div></td>
            <? } ?>
            <td align="center"><div><a href="/myadmin.php?prev=<?=$GLOBALS['m']['id']; ?>" target="_blank">Просмотр страницы</a>

                </div>

            </td>

        </tr>

    </table></div>

    <?



}





function edit_album() //2
{
    global $m, $submit_img, $submit_del;
    ?>
    <SCRIPT LANGUAGE="JavaScript">

        function edit_name(id)
        {
            myWin= open("edit_name.php?id="+id, "window_edit_name",  "width=500,height=100,status=no,toolbar=no,menubar=no");
        }
        //  End -->
    </script>

    <?
//v($GLOBALS['submit_img']);
    if (1)
    {
        if (isset($GLOBALS['submit_img']) )
        {
            if ($GLOBALS['url'])
            {
                $fe=file_get_contents($GLOBALS['url']);
                file_put_contents(img.'/cat/big/tmp.'.$fe,	file_get_contents($GLOBALS['url']) );
                $_FILES['fileimg']['tmp_name']=img.'/cat/big/tmp.'.$fe;
                $urlmini=array_reverse( ( explode('/',$GLOBALS['url']) ) );
                $_FILES['fileimg']['name']=$urlmini[0];
                unset($GLOBALS['url']);
            }

            if ($_GET['name'])
            {
                $targetDir = 'cont/img/cat';
                $_GET['name']=charset_x_win ( $_GET['name']);
                $filePath = $targetDir.'/'.$_GET['name'];
                file_put_contents($filePath, file_get_contents('php://input'));
                $submit1=1;
                $_FILES['fileimg']['name']=$_GET['name'];
                $_FILES['fileimg']['tmp_name']=$filePath;
                ob_clean();
            }



            if($GLOBALS['uv']=='false') $GLOBALS['uv']=false;
            if($GLOBALS['cat']=='false') $GLOBALS['cat']=false;

            if ($GLOBALS['url'])
            {
                $fe=file_get_contents($GLOBALS['url']);
                file_put_contents(img.'/cat/big/tmp.'.$fe,	file_get_contents($GLOBALS['url']) );
                $_FILES['image']['tmp_name']=img.'/cat/big/tmp.'.$fe;
                $urlmini=array_reverse( ( explode('/',$GLOBALS['url']) ) );
                $_FILES['image']['name']=$urlmini[0];
                unset($GLOBALS['url']);
            }


            if ( $_FILES['fileimg']['name'])
            {
                ($_FILES['fileimg']['name']);
                my_mysql_query("INSERT INTO `".pref_db."img` (`album`,`file`)	VALUES (".xs($m['ident']).",".xs($m['ident'].$_FILES['fileimg']['name']).") ;	");

                imresize( array(
                    'in_file_image'=>$_FILES['fileimg']['tmp_name'],
                    'out_file_image'=> album.'/'.$m['ident'].translit($_FILES['fileimg']['name']),
                    'limw'=>o('fotoalbum_limw_mini'),
                    'limh'=> o('fotoalbum_limh_mini'),
                    '_mode'=>($GLOBALS['obrez'] ?  'crop' : '' )
                ) );

                imresize($_FILES['fileimg']['tmp_name'], album.'/big/'.$m['ident'].translit($_FILES['fileimg']['name']), o('fotoalbum_limw_big'), o('fotoalbum_limh_big') );

                echo '<div></div>';
                echo '<img src="'.album.'/'.$m['ident'].translit($_FILES['fileimg']['name']).rand().'" >';
                echo '<div></div>';

            }

            if($_GET['name']) { die(); }

        }



        if ($GLOBALS['multisubmit']) return;

        //
        ?>
        <?=menu_edit(); ?>
        <form action="" method="post" enctype="multipart/form-data" name="form"  >
            <input type="hidden" name="ajax" value="" />

            <br /><br />
            Загрузить фото:
            <label>
                <input type="file" name="fileimg" id="fileField" /></label><br /><br />

            <label>URL фото: <input style='width:50%' type="input" name="url" /></label><br /><br />




            <input  type="hidden" name="id" value="<?=$m['id'] ?>" />
            <input  type="hidden" name="view" value="pages" />
            <input  type="hidden" name="edit" value="<?=$m['num'] ?>" />
            <input  type="hidden" name="mode" value="4" />
            <input  type="hidden" name="album" value="<?=$m['ident'] ?>" />

            <input type="submit" name="submit_img" id="button" value="Поехали!" />





        </form>

        <!-- <BASE href="<?=basehref; ?>">
   <iframe width="100%" height="300px" src="/jscripts/lib/mupload/upload.php?album=<?=$m['ident'] ?>" style="border:none">-->


        <!-- <BASE href="<?=basehref; ?>/mupload/">
<link rel="stylesheet" href="jquery.jcuploadUI.css" />
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript" src="jquery.jcupload.js"></script>
                <script type="text/javascript" src="jquery.jcuploadUI.config.js"></script>
                <script type="text/javascript" src="jquery.jcuploadUI.js"></script>

 <script type="text/javascript">



 var jcu;
                        $(document).ready(function() {
                                var conf= {url: "upload.php",
                                        callback: {
                                                init: function() {
                                                        /*
                                                        TO DO:
                                                        insert JS code to execute on jcUpload init
                                                        */
                                                },
                                                queue_upload_end: function() {
                                                        /*
                                                        TO DO:
                                                        insert JS code to execute when file queue uploaded
                                                        */
														alert('e');
                                                }
                                        }
                                };

                                jcu= $.jcuploadUI(conf);
                                jcu.append_to("#jcupload_content");
                        });

                </script>
                <div id="jcupload_content"></div //-->
        <!-- jcUpload dialog output -->


        <?

    }




    if ($submit_del)
    {

        foreach ($GLOBALS['del_img'] as $delx)
        {

            unlink(album.'/'.qvar('img.id',$delx,'file') );
            unlink(album.'/big/'.qvar('img.id',$delx,'file') );

            my_mysql_query("DELETE FROM `".pref_db."img` WHERE `id` = '".$delx."' ; " );

        }



    }

    $prebase_im=my_mysql_query( 'SELECT * FROM '.pref_db.'img WHERE album="'.$m['ident'].'"  ' ) ;
    //var_dump ($m[ident],$prebase_im);
    $mim=mysqli_fetch_array($prebase_im);

    ?>   <form  method="post" action="">
    <table width="700" align="center">
        <?
        while ($mim)
        {
            ?>
            <tr>
                <td width="25"><input type="checkbox" name="del_img[]"  value="<?=$mim['id'] ?>" /></td>
                <td width="85" align="center"><label></label>
                    <img src="<?=album.'/'.$mim['file'] ?>" /> </td><td width="574" align="center" valign="middle">

                    <label>

                        <?=$mim['name'] ?><br />
                        <!--  <a  href="javascript:edit_name(<?=$mim['id'] ?>)" >Редактировать</a>-->          </label>

                </td>
            </tr>
            <?
            $mim=mysqli_fetch_array($prebase_im);
        }
        ?>
        <tr>
            <td colspan="2" align="center"><label>
                    <input type="submit" name="submit_del" id="button2" value="Удалить отмеченные" />
                </label></td>
            <td align="center"><input  type="hidden" name="id2" value="<?=$m['id'] ?>" />
                <input  type="hidden" name="view2" value="pages" />
                <input  type="hidden" name="edit2" value="<?=$m['num'] ?>" />
                <input  type="hidden" name="mode2" value="<?=$mode ?>" />
                <input  type="hidden" name="album2" value="<?=$m['ident'] ?>" /></td>
    </table>
    <br />
    <label>


    </label>
</form>
    <?



}

function edit__audio()
{
    global $submit1, $m, $elm1, $mode, $name, $ident, $id;
    $rootform='audio-1';

    $vars= select_fields($rootform); set_spis($vars);


    if (@$submit1)
    {
        $ident=newident($id,$ident);

        if ( $_FILES['file']['name'])
        {
            if(move_uploaded_file($_FILES['file']['tmp_name'],'cont/files/'.$id.'.flv') ) echo 'ok!';
        }


        update($vars);
    }
    else
    {
        select($vars) ;
    }
    if ($GLOBALS['multisubmit']) return; menu_edit();;

    ?>
    <form class='ajaxform' method="post" action="myadmin.php" enctype="multipart/form-data">
        <input type="hidden" name="ajax" value="1" />
        <?=hidden(); ?>
        <h3> Аудио:	</h3>
        <?=form($rootform); ?><br />
        <br />
        <a href="/cont/files/" target="_blank">Склад файлов</a><br />
        <br />
        <input type="submit" name="submit1" value="Обновить" id="submit_tn" />
    </form>

    <? //<h3>Просмотр:</h3>  =form($GLOBALS['ident']); ?>
    <?
}


function  edit_but_3() //2
{
    global $m, $elm1, $mode, $name, $ident,$id;

    $pk=array_keys($_POST);
    foreach ($pk as $k) { global $$k;  }

    $vars= select_fields('r3_set');

    if (isset($submit1))
    {
        $ident=newident($id,$ident);

        if ( $_FILES['hm2_img']['tmp_name'] )
        {
            imresize($_FILES['hm2_img']['tmp_name'], img.'/tpl/'.$ident.'.png' , 1500, 4000,'', '', FALSE );
        }

        $size_im = getimagesize(img.'/tpl/'.$ident.'.png');
        $w=$size_im[0];
        $h=$size_im[1];
        if ( !($h1_img2) )
        {
            $h1_img2=ceil($h/2);
        }

        if ( !($hm2_hp) )
        {
            $hm2_hp=ceil( ($h-$fontsize)*8/(2*13) );
        }

        @$img=  img.'/tpl/'.$ident.'.png' ;


        crim(0,0, $w,$h1_img2, $ident.'-1', $img);
        crim(0,$h1_img2, $w, 1, $ident.'-2', $img);
        crim(0,$h1_img2+1, $w ,$h-$h1_img2-1,$ident.'-3', $img);

        update($vars);
        truecss(1);
    }
    else
    {
        //select($vars) ;
    }
    select($vars) ;
    if ($GLOBALS['multisubmit']) return; menu_edit();;
    ?>
    <h2>Кнопка</h2>
    <form class='ajaxform' method="post" action="myadmin.php" enctype="multipart/form-data">
        <input type="hidden" name="ajax" value="1" />

        <?=hidden(); ?>
        <?=form('r3_set'); ?>
        <input type="submit" name="submit1" value="Сохранить!!" id="submit_tn" />
    </form>
    <img src="<?=img.'/tpl/'.$ident.'.png?'.rand(); ?>" />
    <?
}
function edit_baner() //3
{
    global $submit1, $m, $elm1, $mode, $name, $ident, $id;
    $rootform='baner-1';

    $vars= select_fields($rootform); set_spis($vars); set_spis($vars);
    if (isset($submit1))
    {
        // newident
        $newident=newident($id,$ident);
        if ( $newident<>$m['ident'] )
        {

            @rename(content.'/'.$m['ident'].'.htm',content.'/'.$newident.'.htm');
        }
        $ident=$newident;
        //


        update($vars);
    }
    else
    {
        select($vars) ;
    }
    if ($GLOBALS['multisubmit']) return; menu_edit();;

    ?>
    <form class='ajaxform' method="post" action="myadmin.php">
        <input type="hidden" name="ajax" value="1" /> <?=hidden(); ?>
        <h3> Банер:	</h3>
        <?=form($rootform); ?>
        <input type="submit" name="submit1" value="Сохранить!" id="submit_tn" />
    </form>
    <h3>Просмотр:</h3>
    <? //=form($GLOBALS['ident']); ?>
    <?
}
function edit_block() //2
{
    global $m, $elm1, $mode, $name, $ident,$id;

    $pk=array_keys($_POST);
    foreach ($pk as $k) { global $$k;  }

    $vars= select_fields('block-1');





    if (isset($submit1))
    {
        $ident=newident($id,$ident);


        // img
        if ( $_FILES['hm2_img']['tmp_name'] )
        {
            imresize($_FILES['hm2_img']['tmp_name'], img.'/tpl/'.$ident.'2.png' , 1500, 4000,'', '', FALSE);
        }
        $size_im = getimagesize(img.'/tpl/'.$ident.'2.png');
        $w=$size_im[0];
        $h=$size_im[1];
        if ( !($h1_img2) )
        {
            $h1_img2=ceil($h/2);
        }

        if ( !($hm2_hp) )
        {
            //$hm2_hp=ceil( ($h-$fontsize)*8/(2*13) );
        }

        @$img=  img.'/tpl/'.$ident.'2.png' ;

        crim(0,0, $w,$h1_img2, $ident.'-2-1', $img,0, $GLOBALS['size_offset']);
        crim(0,$h1_img2, $w, 1, $ident.'-2-2', $img,0, $GLOBALS['size_offset']);
        crim(0,$h1_img2+1, $w ,$h-$h1_img2-1,$ident.'-2-3', $img, 0, $GLOBALS['size_offset']);

        update($vars);
        truecss(1);
    }
    else
    {
        //select($vars) ;
    }
    select($vars) ;
    if ($GLOBALS['multisubmit']) return; menu_edit();;
    ?>
    <form class='ajaxform' method="post" action="myadmin.php" enctype="multipart/form-data">
        <input type="hidden" name="ajax" value="1" />

        <?=hidden(); ?>
        <?=form('block-1'); ?>
        <input type="submit" name="submit1" value="Сохранить!" id="submit_tn" />

    </form>
    <?
    $size_im = getimagesize(img.'/tpl/'.$ident.'2.png');
    $w=$size_im[0];
    $h=$size_im[1];
    ?>
    Высота - <?=$h; ?><br>
    Ширина - <?=$w; ?><br>
    <img src="<?=img.'/tpl/'.$ident.'2.png?'.rand(); ?>" />

    <?

}

function edit_bmenu() //2
{
    global $m, $elm1, $mode, $name, $ident,$id;

    $pk=array_keys($_POST);
    foreach ($pk as $k) { global $$k;  }

    $vars=  select_fields('bmenu-set');

    $vars = str_replace('d4~h1','d5~h1',$vars);



    if (isset($submit1))
    {
        $ident=newident($id,$ident);

        update($vars);
    }
    else
    {
        //select($vars) ;
    }
    select($vars) ;
    if ($GLOBALS['multisubmit']) return; menu_edit();;

    ?>
    <h1> Блок </h1>
    <form class='ajaxform' method="post" action="myadmin.php" enctype="multipart/form-data">
        <input type="hidden" name="ajax" value="1" />

        <?=hidden(); ?>
        <?=form('bmenu-set'); ?>
        <input type="submit" name="submit1" value="Сохранить!" id="submit_tn" />

    </form>

    <?=block($GLOBALS['m']['d1'],'<br/><br/>'); ?>



    <?

}

function edit_bnews() //2
{

    global $submit1, $m, $elm1, $mode, $name, $ident,$id,$adnews;
    $form='bnews-set';
    $vars= select_fields($form);

    if (@$adnews)
    {
        $qmin=10; // ishem minimalniy svobodniy id
        while ( mysqli_fetch_array( my_mysql_query ( "select `id` FROM `".pref_db."content` where `id`='$qmin' ") ) )
        {
            $qmin++;
        }
        // ishem tekushuyu yacheyku


        //v($m);

        // otodvigaem yacheyki vniz
        //my_mysql_query("UPDATE `".pref_db."content` SET `num` = `num`+1 WHERE `num`>'".$m['num']."' " );

        //$m['ident']=date("Y.m.d");

        $qu="INSERT  `".pref_db."content` set
		`num`= '0',
		`ident`= ".xs( newident($qmin,date("Y.m.d") )  ).",
		`id`=$qmin,
		`rod`= '".$m['ident']."',
		`type`='_news',
		`d1`='".date("Y.m.d")."',
		`bbcode`='on'
			";
        //v($qu);
        my_mysql_query(($qu));


        $id=$qmin;
        $m=qqu('`id`='.$id);

        header("Location: ".basehref."/myadmin.php?id=".$id."&mode=1");
        exit;

        //id=312&mode=1
    }

    if (isset($submit1))
    {

        $ident=newident($id,$ident);

        update($vars);

    }
    else
    {

    }
    select($vars) ;
    if ($GLOBALS['multisubmit']) return; menu_edit();;
    //v(intval($ident));
    ?>


    <form class='ajaxform' method="post" action="myadmin.php" enctype="multipart/form-data">
        <input type="hidden" name="ajax" value="1" />

        <?=hidden(); ?><br />


        <?=form($form); ?>
        <br />
        <input class="admbut" type="submit" name="submit1" value="Сохранить!" id="submit_tn" />
        <br /><br />
        <br/></form>
    <form  method="post" action="myadmin.php" enctype="multipart/form-data"><?=hidden(); ?>
        <input class="admbut" type="submit" name="adnews" value="Добавить новую новость" id="submit_tn" />
    </form>

    <?

}

function invert_ch(&$val)
{
    if ($val)
    {
        $val='';
    }
    else
    {
        $val='on';
    }
}

function set_spis($vars)
{
    if ( defined( 'set_spis') )
    {
        $GLOBALS['spisfld']= super_explode_n('~',str_replace('=','~',$vars)) ;
    }
    //v(trimarr( $GLOBALS['spisfld']));
}

function edit_checkbox() //2
{
    global $submit1, $m, $elm1, $mode, $name, $ident,$id;

    $vars='				
		ident~ident
		d98~d98
		name~name
		d1~variable
		ok~ok
		';

    if ( rg(5) )
    {
        $vars.='
			d2~base
			d3~ww
			d4~w1
			d5~need
			d6~h
			d7~invers
			d9~nosep
			d12~price	
			d14~Obed374		
			sub~addclass		
			';
    }


    set_spis($vars);


    if (isset($submit1))
    {
        $ident=newident($id,$ident);

        if ( !$GLOBALS['variable'])
        {

            $GLOBALS['variable']=translitvar( $GLOBALS['name']  ).$GLOBALS['id'];
        }







        update($vars);
    }
    else
    {
        select($vars) ;
    }
    if ($GLOBALS['multisubmit']) return; menu_edit();;
    //v(intval($ident));
    ?>
    <form class='ajaxform' method="post" action="myadmin.php">
        <input type="hidden" name="ajax" value="1" /> <?=hidden(); ?>


        <h3> Чекбокс</h3>
        <?=i('t','ident','style="width:200px"','Идентификатор')  ?>
        <?=i('t','name','style="width:200px"','Описание')  ?>
        <?=i("t",'d98','','Зависимость от переключателей'); ?>
        <?=i("ch",'ok','','Включить'); ?>
        <? if (rg(5) ) { ?>

            <?=i("t", 'variable', 'style="width:200px"', 'Имя переменной'); //i_t( $variable, $name,  $width_all, $width_name )  ?>
            <?=i('t','base','style="width:200px"','Поле в базе')  ?>
            <?=i("ch",'invers','','Инверсия значения по уполчанию');  ?>
            <?=i("ch",'nosep','','Отключение сепаратора'); ?>
            <?=i("ch",'Obed374','','Объединить со следующей формой в одну строчку'); ?>
            <?=i("t",'ww','','Общая ширина'); ?>
            <?=i("t",'w1','','Ширина описания'); ?>
            <?=i("t",'price','','Цена опции'); ?>
            <?=i("t",'addclass','','Дополнительный класс'); ?>

        <? } ?>

        <input type="submit" name="submit1" value="Сохранить!" id="submit_tn" />

    </form>

    <h3>Просмотр:</h3>
    <?=form($ident) ?>





    <?
}


function edit_color() //2
{
    global $submit1, $m, $elm1, $mode, $name, $ident,$id;
    $vars= select_fields('color_set');

    if (isset($submit1))
    {
        $ident=newident($id,$ident);

        if ( $_FILES['autoimg']['tmp_name'] )
        {
            imresize($_FILES['autoimg']['tmp_name'], img.'/autoimg/'.$m['ident'].'.jpg' , 150, 400 );
        }

        update($vars);

    }
    else
    {
        //select($vars) ;
    }
    select($vars) ;
    if ($GLOBALS['multisubmit']) return; menu_edit();;
    ?>
    <form class='ajaxform' method="post" action="myadmin.php" enctype="multipart/form-data">
        <input type="hidden" name="ajax" value="1" />

        <?=hidden(); ?>
        <?=form('color_set'); ?>
        <input type="submit" name="submit1" value="Сохранить!" id="submit_tn" />
    </form>

    <?

}


function edit_universal($type)
{

    global $submit1, $m, $elm1, $mode, $name, $ident,$id;


    $form= $m['d80'] ? $m['d80'] : $type.'-1';
    $mm_form=qvar('ident', $form);

    $GLOBALS['mtype']=(qqu('`type`=\'contenttype\' and `d1`='.xs($type)));

    $form_name=$m['d80'] ?     $mm_form['name'].':' : $GLOBALS['mtype']['name'].':' ;




    $vars= select_fields($form);



    if ($GLOBALS['mtype']['d6'] )
    {
        $vars.='
			ident~ident			
			';
    }

    set_spis($vars);

    //v($GLOBALS['mtype'])	;

    $hand='hand_'.$type; // имя хандлера

    if (isset($submit1))
    {

        if ($GLOBALS['mtype']['d6'])
        {
            $ident=$GLOBALS[$GLOBALS['mtype']['d6']];
        }

        $newident=newident($id,$ident);

        if ( $newident<>$m['ident'] )
        {
            renamer($m['ident'],$newident );
        }

        $ident=$newident;

        $form=formsec($type.'-1');
        if ($form['ifiles'])
        {
            foreach ( $form['ifiles'] as $k=>$v )
            {
                if ($_POST['url-'.$k])
                {
                    $fe=curl_file_get_contents($GLOBALS['url-'.$k],'auto', FALSE);
                    file_put_contents(img.'/cat/big/tmp',	$fe );
                    $_FILES[$k]['tmp_name']=img.'/cat/big/tmp';
                    unset($GLOBALS['url-'.$k]);
                }

                if ( $_FILES[$k]['tmp_name']  )
                {
                    if ($v['d9'])
                    {
                        $identname=$m[$v['d9']];
                    }
                    else
                    {
                        $identname=$ident;
                    }
                    if( $v['d3']=='cat')
                    {
                        imresize($_FILES[$k]['tmp_name'], img.'/cat/min/'.url2mnemo($v['d10'].$identname).'.jpg' , 60, 60 );
                        imresize($_FILES[$k]['tmp_name'], img.'/cat/mid/'.url2mnemo($v['d10'].$identname).'.jpg' , o('tov_img_w'), o('tov_img_h') );
                        imresize($_FILES[$k]['tmp_name'], img.'/cat/big/'.url2mnemo($v['d10'].$identname).'.jpg' , 1000, 700 );
                    }
                    elseif($v['d3']=='tpl')
                    {
                        if ($v['d8']) { $suf='_'.$v['d8']; } else { $suf=''; }
                        move_uploaded_file($_FILES[$k]['tmp_name'], img.'/tpl/'.$v['d10'].$identname.$suf.'.'.$v['d5'])  ;
                    }
                    elseif ($v['d3']=='tpladmin')
                    {
                        move_uploaded_file($_FILES[$k]['tmp_name'], jscripts.'/tpladmin/'.$v['d10'].$identname.$suf.'.'.$v['d5']) ;
                    }
                    $GLOBALS['response']['updateimg'][$k]=1;

                }
            }
        }



        // handler
        if (function_exists($hand) )
        {
            $response=$hand();
        }

        update($vars);

        if ($GLOBALS['mtype']['d7']) truecss(1);

    }
    else
    {
        if (function_exists($hand) )
        {
            $response=$hand('view');
        }
    }
    select($vars) ;

    if ($_POST['ajax']==1)
    {

    }
    else
    {
        if ($GLOBALS['multisubmit']) return; menu_edit();;

        ?>

        <h2><?=$form_name; ?></h2>
        <form class='ajaxform' method="post" action="myadmin.php" enctype="multipart/form-data"   >
            <input type="hidden" name="ajax" value="1" />

            <?=hidden(); ?><br />

            <?=form($form); ?>
            <br />
            <input class="admbut" type="submit" name="submit1" value="Сохранить!" id="submit_tn" /></form>
        <br /><br /><div id='response'><div></div><?=$response; ?> <div></div></div>
        <br/>
        <?
    }


}



function hand_file($action)
{

}


function edit_chopt() //2
{
    global $submit1, $m, $elm1, $mode, $name, $ident, $id;
    $rootform='chopt-1';

    $vars= select_fields($rootform); set_spis($vars); set_spis($vars);
    if (isset($submit1))
    {
        $ident=newident($id,$ident);

        if (
            ( rg(9) and $GLOBALS['d7'])
            or
            ( !rg(9) and qvar('ident',$ident,'d7') )
        )
        {
            invert_ch($GLOBALS['d1']);
        }

        update($vars);
    }
    else
    {
        select($vars) ;
    }

    if (
        ( rg(9) and $GLOBALS['d7'])
        or
        ( !rg(9) and qvar('ident',$ident,'d7') )
    )
    {
        invert_ch($GLOBALS['d1']);
    }

    if ($GLOBALS['multisubmit']) return; menu_edit();;

    ?>
    <form class='ajaxform' method="post" action="myadmin.php">
        <input type="hidden" name="ajax" value="1" /> <?=hidden(); ?>
        <h3>Опция - галочка</h3>
        <?=form($rootform); ?>
        <input type="submit" name="submit1" value="Сохранить!" id="submit_tn" />
    </form>

    <br />
    <br />
    <?=fgc($ident); ?>

    <?
}

function edit_vermenu() //2
{
    global $submit1, $m, $elm1, $mode, $name, $ident,$id;

    $vars='				
		ident~ident
		name~name
		d1~pref
		'; set_spis($vars);


    if (isset($submit1))
    {
        $ident=newident($id,$ident);

        update($vars);
    }
    else
    {
        select($vars) ;
    }
    if ($GLOBALS['multisubmit']) return; menu_edit();;
    //v(intval($ident));
    ?>
    <form class='ajaxform' method="post" action="myadmin.php">
        <input type="hidden" name="ajax" value="1" /> <?=hidden(); ?>




        <?=i("t",'name','style="width:500px"','Имя');  ?>
        <?=i('t','ident','style="width:500px"','Идентификатор')  ?>
        <?=i('t','pref','style="width:500px"','Стиль меню')  ?>


        <input type="submit" name="submit1" value="Сохранить!" id="submit_tn" />
        <br/>
    </form>
    <?
}
function edit_hormenu() //2
{
    global $elm1, $mode, $name, $ident,$id;

    $pk=array_keys($_POST);
    foreach ($pk as $k) { global $$k;  }

    $vars= select_fields('hormenu-set');

    if (isset($GLOBALS['submit1']))
    {
        $ident=newident($id,$ident);

        if ( $_FILES['hm1_img']['tmp_name'] )
        {
            imresize($_FILES['hm1_img']['tmp_name'], img.'/tpl/'.$ident.'1.png' , 1500, 4000, 98, FALSE );
        }



        // hm1
        $size_im = getimagesize(img.'/tpl/'.$ident.'1.png');
        $w=$size_im[0];
        $h=$size_im[1]/3;
        if ( !($w1) )
        {
            $w1=ceil($w/2);
        }
        if ( !($hpad) )
        {
            $hpad=ceil( ($h/3-$fontsize)*8/(2*13) );
        }

        @$img=  img.'/tpl/'.$ident.'1.png' ;

        if ($type_nar1=='fix')
        {
            $w2=$w-2;
            $w1=1;
        }
        else
        {
            $w2=1;
        }
        if ($ident=='smset')
        {
            $h=$h*3;
            $w=$w/2;
            crim(0,0, $w,$h, $ident.'-1-1', $img);
            crim($w,0, $w,$h, $ident.'-1-2', $img);
        }
        else
        {
            crim(0,0, $w1,$h, $ident.'-1-1', $img);
            crim($w1,0, $w2 ,$h, $ident.'-1-2', $img);
            crim($w1+$w2,0, $w-$w1-$w2,$h,$ident.'-1-3', $img);

            crim(0,$h, $w1,$h, $ident.'-1-4', $img);
            crim($w1,$h, $w2,$h, $ident.'-1-5', $img);
            crim($w1+$w2,$h, $w-$w1-$w2,$h,$ident.'-1-6', $img);


            crim(0,$h*2, $w1,$h, $ident.'-1-7', $img);
            crim($w1,$h*2, $w2,$h, $ident.'-1-8', $img);
            crim($w1+$w2,$h*2, $w-$w1-$w2,$h,$ident.'-1-9', $img);
        }
        //separator
        if ( $_FILES['imgsep']['tmp_name'] )
        {
            imresize($_FILES['imgsep']['tmp_name'], img.'/tpl/'.$ident.'-sep.png' , 1500, 4000 );
            //v(1);
        }

        // hm2
        if ( $_FILES['hm2_img']['tmp_name'] )
        {
            imresize($_FILES['hm2_img']['tmp_name'], img.'/tpl/'.$ident.'2.png' , 1500, 4000 );
        }
        $size_im = getimagesize(img.'/tpl/'.$ident.'2.png');
        $w=$size_im[0];
        $h=$size_im[1];
        if ( !($h1_img2) )
        {
            $h1_img2=ceil($h/2);
        }

        if ( !($hm2_hp) )
        {
            $hm2_hp=ceil( ($h-$fontsize)*8/(2*13) );
        }

        @$img=  img.'/tpl/'.$ident.'2.png' ;

        if ($ident=='smset')
        {
            $h=$h;
            $w=$w/2;
            crim(0,0, $w,$h, $ident.'-2-1', $img);
            crim($w,0, $w,$h, $ident.'-2-2', $img);
        }
        else
        {
            crim(0,0, $w,$h1_img2, $ident.'-2-1', $img);
            crim(0,$h1_img2, $w, 1, $ident.'-2-2', $img);
            crim(0,$h1_img2+1, $w ,$h-$h1_img2-1,$ident.'-2-3', $img);
        }

        update($vars);
        truecss(1);
    }
    else
    {
        select($vars) ;
    }
    select($vars) ;
    if ($GLOBALS['multisubmit']) return; menu_edit();;
    ?>
    <form class='ajaxform' method="post" action="myadmin.php" enctype="multipart/form-data">
        <input type="hidden" name="ajax" value="1" />

        <?=hidden(); ?>
        <?=form('hormenu-set'); ?>
        <input type="submit" name="submit1" value="Сохранить!" id="submit_tn" />
    </form>
    <br /> <img src="<?=img.'/tpl/'.$ident.'1.png?'.rand(); ?>" /><br />
    <img  src="<?=img.'/tpl/'.$ident.'2.png?'.rand(); ?>" />

    <?

}

function edit_mpunkt()

{

    global $submit1, $m, $elm1, $mode, $name, $ident,$id;

    $ident=newident($GLOBALS['d1'],$id);

    $vars='			
		ident~ident
		name~name
		d1~variable		
		d98~jsdep
		';

    $rootform='mpunkt-1';

    if ( rg(5) ) $vars= select_fields($rootform);
    set_spis($vars);

    if (isset($submit1))

    {

        if (  ! rg(5) )
        {
            if ( $m['d1'] )
            {
                $GLOBALS['variable']=$m['d1'];
            }
            else
            {
                $GLOBALS['variable']=translitvar( $GLOBALS['name'] ).$GLOBALS['id'];
            }

        }


        $ident=newident($id,$ident);
        //v($saveplace);
        update($vars);

    }

    else

    {

        select($vars) ;

    }

    if ($GLOBALS['multisubmit']) return; menu_edit();;

    //v(intval($ident));

    ?>

    <form class='ajaxform' method="post" action="myadmin.php">
        <input type="hidden" name="ajax" value="1" /> <?=hidden(); ?>

        <?

        ?>
        <br>
        <h2>Пункт списка меню</h2>

        <?=form($rootform); ?><br />

        <input type="submit" name="submit1" value="Сохранить!" id="submit_tn" />

        <br/>

    </form>

    <?

}



function edit_tn() //2
{
    global $submit_tn, $m, $elm1, $mode;

    /*$prebase=my_mysql_query( 'SELECT * FROM '.pref_db.'content WHERE `id`='.xs($_REQUEST['id']).'  ORDER BY `num` ASC ' ) ;
	$m=mysqli_fetch_array($prebase);	*/

    $vars='				
		content~elm1
		'; set_spis($vars);

    if ($GLOBALS['lng']) $lng='_'.$GLOBALS['lng'];


    $folder= ( $m['d16'] AND $m['type']=='shab' ) ? $m['d16'] : cont.'/htm';

    $saveurl=strtr($m['ident'],$GLOBALS['repurl']);
    //v($saveurl);

    if ( strpos(  ' '.$saveurl , 'jscripts+sl+' ) or strpos(  ' '.$saveurl , 'cont+sl+' ) ) {
        $filename=( urlFromMnemo( $saveurl ) );

    } else if ( strpos(  ' '.$saveurl , 'files+sl+' ) ) {
        $filename=(cont.'/files/'. str_replace( 'files+sl+','' , $saveurl ));
    } else {
        $filename=$folder.'/'.($saveurl).$lng.".htm";
    }

    vl($filename);

    if (@$submit_tn)
    {
        //v($elm1);
        $elm1=str_replace(basehref,'', magic($elm1));

        if ( ($m['d15']=='disc') | ($m['type']!=='shab') )
        {






            file_put_contents($filename, ($elm1 ));
            if ( file_get_contents($filename)!==$elm1 )
            { $GLOBALS['error_save']='Save error!'; }

            $em=explode('<!-- pagebreak -->',$elm1);
            if ($em[1])
            {
                file_put_contents(cont.'/htm/prev_'.$m['ident'].$lng.".htm",$em[0]);
            }
        }
        else
        {
            //v($GLOBALS['elm1']);
            update($vars);
        }





    }
    else
    {

    }
    if ($GLOBALS['multisubmit']) return;

    select($vars) ;



    if ($GLOBALS['multisubmit']) return; menu_edit();;
    if ( $m['bbcode']!=='')
    {
        if (o('tinymce339') )
        {
            ?>
            <script type="text/javascript" src="/jscripts/tiny_mce/jquery.tinymce.js"></script>
            <script type="text/javascript">

                $(document).ready(function()
                {
                    edit_tn('#elm1');
                });

            </script>
            <?
        }
        else
        {

            ?>
            <!-- TinyMCE -->


            <!-- /TinyMCE -->
            <?
        }
    }
    ?>
    <input type="submit" name="submit_tn" value="Сохранить!" id="submit_tn" onclick="tn_save_cont()" style="margin: 3px;"  /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span id="error_save" class="red" ></span>

    <form method="post" action="" class="ajaxform">

        <?=hidden(); ?>

        <?
        if ( !$_REQUEST['ajax'] AND  $m['bbcode'] ) {
            ?><script type="text/javascript">

                edit_tn_old();
                truesize();

            </script><?
        }
        ?>

        <input type="hidden" name="ajax" value="1">
        <input type="hidden" name="bbcode" id="bbcode" value="<?= $m['bbcode']; ?>">
        <input type="hidden" name="submit_tn" value="1">
        <input type="hidden" name="lng" value="<?=$GLOBALS['lng'] ?>">



        <textarea name="elm1" rows="30" style="width:100%;height:80%;"  class="elm1"  ><?
            if ( ($m['d15']=='disc') | ($m['type']!=='shab') )
            {

                $textarea =   file_get_contents(  $filename   );

                /*if ( strpos(  ' '.($m['ident']) , 'files+sl+' ) ) {

				$filename=cont.'/files/'. str_replace( 'files+sl+', '' , $m['ident'] );
				$textarea =   file_get_contents(  $filename   );

			} else {
				$textarea =   file_get_contents( vl($folder.'/'.$m['ident'].$lng.'.htm'));
			}*/

            } else {
                $textarea =  $elm1;
            }

            echo htmlspecialchars( $textarea );

            ?></textarea>
        <div>
            <!-- Some integration calls -->
            <a href="javascript:;" onMouseDown="tinyMCE.activeEditor.show();">[Визуально]</a>
            <a href="javascript:;" onMouseDown="tinyMCE.activeEditor.hide();">[HTML]</a></div>


    </form><?



}



function edit_page() //2
{


    global $submit1, $m, $elm1, $mode, $name, $ident,$id;



    $vars='
		title~title
		desc~description
		keyw~keywords	
		bok~bok	
		ident~ident
		name~name
		ok~ok
		bbcode~bbcode	
		saveplace~saveplace
		nolink~nolink
		d1~link
		d2~okalb
		d6~albumtype
		d3~autoname
		d5~okautolen
		prio~prioritet
		';

    $vars= select_fields('page_set'); set_spis($vars);



    if ($GLOBALS['delautoimg'])
    {
        unlink(img.'/autoimg/'.$GLOBALS['delautoimg'].'.jpg');
    }

    if (isset($submit1))
    {

        if (!$GLOBALS['d5']) $GLOBALS['d5']=rand();

        // newident
        $newident=newident($id,$ident);

        if ( $newident<>$m['ident'] )
        {
            renamer($m['ident'],$newident );
        }
        $ident=$newident;



        if ( $_FILES['autoimg']['tmp_name'] )
        {
            imresize($_FILES['autoimg']['tmp_name'], img.'/autoimg/'.$m['ident'].'.jpg' , 150, 400 );
        }

        if ( $_FILES['imgind']['tmp_name'] )
        {
            //move_uploaded_file($_FILES['imgind']['tmp_name'],img.'/cat/tmp.png');
            $size_im = getimagesize($_FILES['imgind']['tmp_name']);
            $w=$size_im[0];
            $h=$size_im[1];
            //v($w);
            //v($h);
            crim(0,0, $w,$h, $ident.'-1', $_FILES['imgind']['tmp_name'],$GLOBALS['qjpeg']);

        }




        update($vars);

    }
    else
    {
        //select($vars) ;
    }
    select($vars) ;
    if ($GLOBALS['multisubmit']) return; menu_edit();;
    //v(intval($ident));
    ?>


    <form class='ajaxform' method="post" action="myadmin.php" enctype="multipart/form-data">
        <input type="hidden" name="ajax" value="1" />

        <?=hidden(); ?><br />

        <input class="admbut" type="submit" name="submit1" value="Сохранить!" id="submit_tn" />
        <?=form('page_set'); ?>



        <!--<a href="?edit=<?=$GLOBALS['edit']; ?>&mode=1&delautoimg=<?=$ident; ?>" >Удалить картинку</a><br />-->


        <? //i_select('Место хранения','saveplace','saveplace','name','art',''); ?>



        <br />
        <input class="admbut" type="submit" name="submit1" value="Сохранить!" id="submit_tn" />
        <br/>

        <!--<div id="autoh"></div>-->


    </form>
    <?

    if ($GLOBALS['okautolen'] )
    {
        //v($m);
        echo "<div class='autom'><div class='autom-1'></div><div class='autom-2'><a href='{$m['ident']}'>".imgex(img.'/autoimg/'.$m['ident'].'.jpg')."{$m['d3']}</a></div><div class='autom-3'></div></div>";
    }

}

function edit_img() //2
{
    global $image, $submit1;

    $vars='				
		d1~limh
		d2~limw
		d3~limhb
		d4~limwb
		d5~uv
		'; set_spis($vars);
    //ob_clean();

    if ($_GET['name'])
    {
        $targetDir = 'cont/img/cat';
        $_GET['name']=charset_x_win ( $_GET['name']);
        $filePath = $targetDir.'/'.$_GET['name'];
        file_put_contents($filePath, file_get_contents('php://input'));
        $submit1=1;
        $_FILES['image']['name']=$_GET['name'];
        $_FILES['image']['tmp_name']=$filePath;
        ob_clean();
        //v(1);
    }


    if (isset($submit1) or $_REQUEST['fileupload'])
    {
        if($GLOBALS['uv']=='false') $GLOBALS['uv']=false;
        if($GLOBALS['cat']=='false') $GLOBALS['cat']=false;

        if ($GLOBALS['url'])
        {
            (file_put_contents('cont/img/cat/big/tmp' , (( curl_file_get_contents($GLOBALS['url']) ) )  ));
            $_FILES['image']['tmp_name']='cont/img/cat/big/tmp';
            $urlmini=array_reverse( ( explode('/',$GLOBALS['url']) ) );
            $_FILES['image']['name']=$urlmini[0];
            unset($GLOBALS['url']);

        }

        if ( $_FILES['image']['name'])
        {



            $_FILES['image']['name']=( translit(($_FILES['image']['name'])));
            $fend=fileending($_FILES['image']['name']);

            if ($fend=='gif' )
            {
                move_uploaded_file($_FILES['image']['tmp_name'],img.'/'.$_FILES['image']['name']);
                echo '<div></div>';
                echo '<img src="'.img.'/'.$_FILES['image']['name'].'?'.rand().'" >';
                echo '<div></div>';
            }

            if(  $fend=='ico' | $fend=='swf')
            {
                move_uploaded_file($_FILES['image']['tmp_name'],img.'/'.$_FILES['image']['name']);
                echo '<div></div>';
                echo '<a href="'.cont.'/img/'.$_FILES['image']['name'].'?'.rand().'" >'.$_FILES['image']['name'].'</a>';
                echo '<div></div>';
            }

            elseif ($fend=='png' | $fend=='jpg')
            {
                if ($GLOBALS['folder']=='cat')
                {
                    $filename= filename($_FILES['image']['name']);
                    creat_cat_img( $_FILES['image']['tmp_name'], $filename );

                    //imresize($_FILES['image']['tmp_name'], img.'/cat/'.$, 1000,700,90);
                    echo '<div></div>';
                    echo '<img src="'.img.'/cat/min/'.$filename.'.jpg?'.rand().'" >';
                    echo '<div></div>';
                }
                else
                {
                    $folder= $_REQUEST['folder']=='tpl' ? 'cont/img/tpl' : 'cont/img';
                    if ( !$_REQUEST['limw'] and !$_REQUEST['limh'] )
                    {
                        if ( $_FILES['image']['tmp_name'] == 'cont/img/cat/big/tmp' ) {
                            rename ($_FILES['image']['tmp_name'],$folder.'/'.$_FILES['image']['name']);
                        } else {
                            move_uploaded_file($_FILES['image']['tmp_name'],$folder.'/'.$_FILES['image']['name']);
                        }
                    }
                    else
                    {
                        fn('imresize', $_FILES['image']['tmp_name'], $folder.'/'.$_FILES['image']['name'], $_REQUEST['limw'], $_REQUEST['limh'],90,FALSE,($_REQUEST['notrounder'] ? false : o('system-rounder') ) );
                    }

                    echo '<div></div>';
                    //echo '<div>'.img.'/'.$_FILES['image']['name'].'</div>';
                    echo '<div class="img_up">';
                    echo '<img src="'.img.'/'.$_FILES['image']['name'].'?'.rand().'" >';
                    echo '<br/><input type="text" value="/'.img.'/'.$_FILES['image']['name'].'"  onclick="$(this).select();" />';

                    echo '</div><div></div>';

                    if ( $GLOBALS['uv'] and $_REQUEST['folder']!=='tpl' )
                    {

                        fn( 'imresize', ($_FILES['image']['tmp_name']), (img.'/big/'.$_FILES['image']['name']), $_REQUEST['limwb'], $_REQUEST['limhb'],80);
                    }
                    if ( $_REQUEST['folder']=='tpl' ) truecss(1);
                }

            }
            else
            {

                if (move_uploaded_file($_FILES['image']['tmp_name'],cont.'/files/'.$_FILES['image']['name']) )
                {
                    echo '<div></div>';
                    echo '<div  class="img_up" ><input type="text" value="/'.cont.'/files/'.$_FILES['image']['name'].'" />';
                    echo '<br/><a href="'.cont.'/files/'.$_FILES['image']['name'].'?'.rand().'" >'.$_FILES['image']['name'].'</a>';
                    echo '</div><div></div>';
                }
            }

            echo "ok";

        }

        if($_GET['name']) { die(); }

        update($vars);
    }
    else
    {
        select($vars) ;
    }




    ?>


    <a class="pointer" onclick="chpage(0,3)" >Просмотр картинок</a>&nbsp;&nbsp;&nbsp;&nbsp; <a  class="pointer" onclick="chpage(0,5)" >Просмотр файлов</a> <br /><br />
    <form enctype="multipart/form-data" method="post" action="myadmin.php">
        <?=hidden(); ?>

        <h3>Загрузка картинки</h3>

        <?=i("t",'limh','style="width:50px"','Подогнать по высоте');  ?>
        <?=i("t",'limw','style="width:50px"','Подогнать по ширине');  ?>


        <?=i("ch",'uv','','Увеличенный вариант');  ?>
        <?=i("t",'limhb','style="width:50px"','Подогнать по высоте');  ?>
        <?=i("t",'limwb','style="width:50px"','Подогнать по ширине');  ?>
        <?=i("ch",'notrounder','','Отключить временно закруглятор');  ?>
        <?=form('img-1');  ?>
        <br>

        Файл картинки: <input type="file" name="image" id="fileField" multiple><br /><br />


        <label>URL картинки: <input style='width:50%' type="input" name="url" /></label><br /><br />
        <input type="submit" name="submit1" id="button" value="Загрузить!">
        </label>


    </form>

    <!-- The global progress bar -->
    <div id="progress" class="progress progress-success progress-striped">
        <div class="bar"></div>
    </div>
    <!-- The container for the uploaded files -->
    <div id="files"></div>
    <p align="center"><br />
    </p>

    <?

}

function fileuploader()
{
    v( $_POST );
}

function viev_img() //2
{

    if ( isset ($_REQUEST['delimg']) )
    {
        //var_dump($name);

        foreach(array_keys($_REQUEST['delimg']) as $namedel)
        {
            unlink( img."/".$namedel );
        }


    }

    unset($fileindir);
    if ($handle = opendir(img) )
    {
        //echo "<p>Файлы:<br/>";
        while (false !== ($fileindir[] = readdir($handle)))

        {

        }

        closedir($handle);
        //echo sizeof($fileindir);
    }
    sort($fileindir);
    //print_r($file);
    ?>
    <form  name="form2" method="post" action=""><?=hidden(); ?>
        <input type="submit" sub="button2" id="button2" value="Удалить отмеченные" /></<br/><br/><br/>
    <?
    for ($x=sizeof($fileindir);$x>=0;$x=$x-1 )
    {
        //v(fileending($fileindir[$x]));
        if ( (fileending($fileindir[$x]) =='jpg') | (fileending($fileindir[$x])=='png') | (fileending($fileindir[$x])=='gif') )

        {
            $imsize=getimagesize(img.'/'.$fileindir[$x]);
            if ($imsize[0]>200) $imwidth=' width="200" ';
            ?>
            <div id="imf">
					<span class="st7">&nbsp;
                        <?=$fileindir[$x] ?>
                    </span>    <span class="st7"><span class="style13">
                    <input name="delimg[<?=$fileindir[$x] ?>]" type="checkbox" />
                    </span></span>

                <input type="hidden" name="name[]" value="<?=$fileindir[$x] ?>" />
                <br />
                <img <?=$imwidth; ?> src="<?=img."/".$fileindir[$x]; ?>"    /><br />
            </div>
            <?
        }
    }
    ?>
    </form>
    <?


}



function viev_files() //2
{

    if ( isset ($_REQUEST['delimg']) )
    {
        //var_dump($name);

        foreach(array_keys($_REQUEST['delimg']) as $namedel)
        {
            unlink( cont."/files/".$namedel );
        }


    }

    unset($fileindir);
    if ($handle = opendir(cont.'/files') )
    {
        echo "<p>Файлы:<br/>";
        while (false !== ($fileindir[] = readdir($handle)))

        {

        }
        //v($fileindir);
        closedir($handle);
        //echo sizeof($fileindir);
    }
    sort($fileindir);
    //print_r($file);
    ?>
    <form  name="form2" method="post" action=""><?=hidden(); ?>
        <input type="submit" sub="button2" id="button2" value="Удалить отмеченные" /></<br/><br/><br/>
    <table align="center"><tr><td>
                <?
                foreach ( $fileindir as $f )
                {
                    //v(fileending($fileindir[$x]));
                    if ( is_file(cont."/files/".$f) )
                    {
                        ?>
                        <div >
                            <input name="delimg[<?=$f ?>]" type="checkbox" />
                            <input type="hidden" name="name[]" value="<?=$f ?>" />
                            <a <?=$imwidth; ?> href="<?=cont."/files/".$f; ?>"  target="_blank"  ><?=$f; ?></a> <br />
                        </div>
                        <?
                    }
                }
                ?>
            </td></tr></table></form>
    <?


}




function edit__news() //2
{


    global $submit1, $m, $elm1, $mode, $name, $ident,$id;
    $form='news-set';
    $vars= select_fields($form);

    if (isset($submit1))
    {


        $newident=newident($id, $ident );

        renamer( $m['ident'], $newident);

        $ident=$newident;

        if ($GLOBALS['url'])
        {
            $fe=file_get_contents($GLOBALS['url']);
            file_put_contents(img.'/cat/big/tmp',	file_get_contents($GLOBALS['url']) );
            $_FILES['img']['tmp_name']=img.'/cat/big/tmp';
            unset($GLOBALS['url']);
        }

        if ( $_FILES['img']['tmp_name'] )
        {
            if ($_POST['dopimg'] )
            {
                $num=0;
                while (1)
                {
                    $file=url2mnemo($ident).'_'.num2str($num,2);
                    $num++;
                    if ( !file_exists( img.'/cat/big/'.$file.'.jpg' ) ) break;
                }

                creat_cat_img( $_FILES['img']['tmp_name'], $file );
            }
            else
            {
                creat_cat_img($_FILES['img']['tmp_name'],url2mnemo($ident));
            }
            unset($_FILES['img']['tmp_name']);

        }
        else
        {

        }


        if ( !$GLOBALS['multisubmit'] or ( $GLOBALS['multisubmit'] AND !$_COOKIE['hidecoll']['url'] ) )
        {
            creat_cat_midimg($ident);
        }


        update($vars);


    }
    else
    {

    }
    select($vars) ;
    if ($GLOBALS['multisubmit']) return; menu_edit();;
    //v(intval($ident));
    ?>


    <form class='ajaxform' method="post" action="myadmin.php" enctype="multipart/form-data">
        <input type="hidden" name="ajax" value="1" />
        <?=hidden(); ?><br />



        <?=form(($form)); ?>
        <a href="javascript:addate()">Вставить текущую дату</a></<br /><br />
    <input class="admbut" type="submit" name="submit1" value="Сохранить!" id="submit_tn" /></form>
    <br /><br />
    <br/>

    <div id='response'><div></div><img src='/<?=img.'/cat/mid/'.url2mnemo($ident).'.jpg?'.rand(); ?>' />
        <div></div></div>

    <?

}


function prepareForDOM($html, $encoding) {
    $html = iconv($encoding, 'UTF-8//TRANSLIT', $html);
    $html = preg_replace('/<(script|style|noscript)\b[^>]*>.*?<\/\1\b[^>]*>/is', '', $html);
    $tidy = new tidy;
    $config = array(
        'drop-font-tags' => true,
        'drop-proprietary-attributes' => true,
        'hide-comments' => true,
        'indent' => true,
        'logical-emphasis' => true,
        'numeric-entities' => true,
        'output-xhtml' => true,
        'wrap' => 0
    );
    $tidy->parseString($html, $config, 'utf8');
    $tidy->cleanRepair();
    $html = $tidy->value;
    $html = preg_replace('#<meta[^>]+>#isu', '', $html);
    $html = preg_replace('#<head\b[^>]*>#isu', "<head>\r\n<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />", $html);
    return $html;
};



function edit_file_old() //2
{
    global $submit1, $m, $elm1, $mode, $name, $ident,$id;

    $vars='				
		ident~ident
		name~name
		d1~variable		
		d3~ww
		'; set_spis($vars);


    if (isset($submit1))
    {
        $ident=newident($id,$ident);




        update($vars);
    }
    else
    {
        select($vars) ;
    }
    if ($GLOBALS['multisubmit']) return; menu_edit();;
    //v(intval($ident));
    ?>
    <form class='ajaxform' method="post" action="myadmin.php">
        <input type="hidden" name="ajax" value="1" /> <?=hidden(); ?>


        <h3> Форма выбора файла:	</h3>
        <?=i('t','name','style="width:200px"','Описание')  ?>
        <?=i("t", 'variable', 'style="width:200px"', 'Имя переменной html'); //i_t( $variable, $name,  $width_all, $width_name )  ?>
        <?=i('t','ww','style="width:200px"','Ширина (px или %) ')  ?>



        <input type="submit" name="submit1" value="Сохранить!" id="submit_tn" />

    </form>
    <h3>Просмотр:</h3><br />

    <?=form($ident); ?>





    <?
}
function  edit_fiximg() //2
{
    global $m, $elm1, $mode, $name, $ident,$id;

    $pk=array_keys($_POST);
    foreach ($pk as $k) { global $$k;  }
    $vars= select_fields('fiximg-1');

    if (isset($submit1))
    {
        $ident=newident($id,$ident);
        if ( $_FILES['img']['tmp_name'] )
        {
            imresize($_FILES['img']['tmp_name'], img.'/tpl/'.$ident.'.png' , 1500, 4000 ,98,FALSE  );
        }

        update($vars);
        truecss(1);
    }
    else
    {
        //select($vars) ;
    }
    select($vars) ;
    if ($GLOBALS['multisubmit']) return; menu_edit();;
    ?>
    <h2>Картинка</h2>
    <form class='ajaxform' method="post" action="myadmin.php" enctype="multipart/form-data">
        <input type="hidden" name="ajax" value="1" />

        <?=hidden(); ?>
        <?=form('fiximg-1'); ?>
        <input type="submit" name="submit1" value="Сохранить!" id="submit_tn" />
    </form>
    <img src="<?=img.'/tpl/'.$ident.'.png?'.rand(); ?>" />
    <?
}




function edit_font() //2
{
    global $submit1, $m, $elm1, $mode, $name, $ident,$id;
    $form='font-set';
    $vars= select_fields($form);
    if (isset($submit1))
    {
        $ident=newident($id,$ident);

        if ( $_FILES['img']['tmp_name'] )
        {
            imresize($_FILES['img']['tmp_name'], img.'/cat/mid/'.$ident.'.jpg' , 250, 250 );
        }

        update($vars);
        truecss(1);
        //die();
    }
    else
    {

    }
    select($vars) ;
    if ($GLOBALS['multisubmit']) return; menu_edit();;
    //v(intval($ident));
    ?>


    <form class='ajaxform' method="post" action="myadmin.php" enctype="multipart/form-data">
        <input type="hidden" name="ajax" value="1" />
        <?=hidden(); ?><br />

        <input class="admbut" type="submit" name="submit1" value="Сохранить!" id="submit_tn" />
        <?=form($form); ?>
        <br />
        <input class="admbut" type="submit" name="submit1" value="Сохранить!" id="submit_tn" /></form>
    <br /><br />
    <br/>



    <?

    $font=array(

        'arial'=>'Arial, Helvetica, sans-serif',

        'times'=>'Times New Roman, Times, serif',

        'courier'=>'Courier New, Courier, monospace',

        'georgia'=>'Georgia, Times New Roman, Times, serif',

        'geneva'=>'Geneva, Arial, Helvetica, sans-serif',

        'verdana'=>'Verdana, Arial, Helvetica, sans-serif');

    $pr=my_mysql_query( 'SELECT * FROM '.pref_db."content WHERE `id`=".$id ) ;

    while ($mm=mysqli_fetch_assoc($pr) )

    {
        $class=substr($mm['art'].'pr',1);
        ?>
        <style>
            .predfont {

            <? if ($mm['d18']) { ?> font-size: <?=$mm['d18']; ?>px; <? } ?>

                <? if ($mm['d9']) { ?>font-family: <?=$font[$mm['d9']]; ?>; <? } ?>

                <? if ($mm['d13']) { ?>color: #<?=$mm['d13']; ?>; <? } ?>

                <? if ($mm['d10']) { ?>font-weight: <?=$mm['d10']; ?>; <? } ?>

                <? if ($mm['d20']) { ?>padding: 0 <?=$mm['d20']; ?>px 0 <?=$mm['19']; ?>px;  <? } ?>

                <? if ($mm['d4']) { ?>text-align:<?=$mm['d4']; ?>; <? } ?>

                <? if ($mm['d23']) { ?>font-style:<?=$mm['d23']; ?>; <? } ?>
                width:200px;

            } </style>
        <div class="predfont" >Просмотр </div>


        <?
    }

}



function edit_formlink () //2
{
    global $submit1, $m, $elm1, $mode, $name, $ident,$id;

    $vars='				
		ident~ident
		name~name
		d1~pref
		ok~ok
		'; set_spis($vars);


    if (isset($submit1))
    {
        $ident=newident($id,$ident);

        update($vars);
    }
    else
    {
        select($vars) ;
    }
    if ($GLOBALS['multisubmit']) return; menu_edit();;
    //v(intval($ident));
    ?>
    <form class='ajaxform' method="post" action="myadmin.php">
        <input type="hidden" name="ajax" value="1" /> <?=hidden(); ?>


        <h3>Cсылка на другую ячейку</h3>

        <?=i("t",'name','style="width:500px"','Имя');  ?>
        <?=i('t','ident','style="width:500px"','Идентификатор')  ?>
        <?=i('t','pref','style="width:500px"','Идентификатор ссылки')  ?>
        <?=i("ch",'ok','','Опубликовать');  ?>


        <input type="submit" name="submit1" value="Сохранить!" id="submit_tn" />

    </form>
    <h1> Просмотр </h1>
    <?=form($ident); ?>



    <?
}

function translitvar($varname)
{
    $rep=array('.'=>'','-'=>'','('=>'',')'=>'');
    $out=strtr( translit( substr($varname,0,8) ),$rep );

    return var_normal_prenum($out) ;
}

function var_normal_prenum ($varname)
{
    $out=$varname;
    while (  !(strpos(  ' /.-"<>@	0123456789',  substr(  $out,0,1 )  )===FALSE)  and $x<300 )
    {
        $out=substr( $out,1,200 );
        $x++;

    }
    if (!$out) $out='_var';
    return $out;
}

function edit_ftext() //2
{
    global $submit1, $m, $elm1, $mode, $name, $ident,$id;

    $vars='				
		ident~ident
		name~name
		d1~variable		
		d5~need		
		d6~h
		d98~jsdep
		d12~filter
		';

    if ( rg(5) ) {
        $vars.='						
		d3~ww
		d4~w1				
		d7~tright		
		d8~pass
		d9~namealign		
		ok~ok
		d13~Vnutr316
		d14~Obed374
		sub~addclass
		sub~addclass_input
		sub~comment
		sub~form_tpl
		sub~is_form_tpl
		
		';
    }
    if ( rg(9) )
    {
        $vars.='						
		d2~base	
		d10~funhundler
		';
    }

    set_spis($vars);


    if (isset($submit1))
    {
        //$GLOBALS['name']=iconv('utf-8','windows-1251', $GLOBALS['name'] );
        $ident=$_REQUEST['ident'] ? newident($id,  $_REQUEST['ident'] )  :  newident($id,'tform'.$id);

        if ( !$GLOBALS['variable'])
        {

            $GLOBALS['variable']=translitvar( $GLOBALS['name']  ).$GLOBALS['id'];
        }



        if ( ($GLOBALS['ww']=='') & ($GLOBALS['w1']=='') )
        {
            $GLOBALS['ww']='100%';
            $GLOBALS['w1']='200px';
        }


        update($vars);
    }
    else
    {
        select($vars) ;
    }
    if ($GLOBALS['multisubmit']) return; menu_edit();;
    //v(intval($ident));
    ?>
    <form class='ajaxform' method="post" action="myadmin.php">
        <input type="hidden" name="ajax" value="1" />
        <?=hidden(); ?>


        <h3> Текстовое поле:	</h3>
        <?=i('t','ident','style="width:200px"','Идентификатор')  ?>
        <?=i('t','name','style="width:200px"','Описание')  ?>
        <?=i('t','h','style="width:200px"','Количество строк')  ?>
        <?=i("t", 'jsdep', 'style="width:200px"', 'Зависимость от переменной галочки'); ?>
        <?=i("ch",'need','','Обязательно для заполнения');  ?>
        <?=i("ch",'pass','','Ввод пароля');  ?>
        <?=form('ftext-1'); ?>

        <? if ( rg(5) ) { ?>
            <?=i('t','ww','style="width:200px"','Общая ширина (px или %) ')  ?>
            <?=i('t','w1','style="width:200px"','Ширина Описания (px или %) ')  ?>
            <?=i("t", 'variable', 'style="width:200px"', 'Имя переменной html'); //i_t( $variable, $name,  $width_all, $width_name )  ?>



        <? }   if (rg(9)) { ?>
            <?=form('Pole_v_baze'); ?>

        <? } ?>
        <input type="submit" name="submit1" value="Сохранить!" id="submit_tn" />

    </form>
    <h3>Просмотр:</h3>
    <?=i_t($GLOBALS['variable'],$GLOBALS['name'],$GLOBALS['ww'],$GLOBALS['w1'],$GLOBALS['h'],'',$m); ?>





    <?
}

function edit_spis() //2
{
    global $submit1, $m, $elm1, $mode, $name, $ident, $id;

    $vars='				
		ident~ident
		name~name
		d1~variable
		d98~jsdep
		';
    if ( rg(5) ) $vars= select_fields('svoystva_spis');
    set_spis($vars);


    if (isset($submit1))
    {


        if (  rg(5) )
        {
            if (!$GLOBALS['variable'])
            {
                $GLOBALS['variable']=translitvar( $GLOBALS['name'] ).$GLOBALS['id'];
            }
            else
            {
                $GLOBALS['variable']=var_normal_prenum($GLOBALS['variable']);

            }

        }
        else
        {
            if ($m['d1'])
            {
                $GLOBALS['variable']=translitvar( $m['d1'] );
            }
            else
            {
                $GLOBALS['variable']=translitvar( $GLOBALS['name'] ).$GLOBALS['id'];
            }
        }

        if($GLOBALS['variable']=='_var') $GLOBALS['variable'].=$GLOBALS['id'];

        $ident=newident($id, $ident ? $ident : $GLOBALS['variable']  );

        update($vars);
        set_subpunkt($ident,$GLOBALS['subpunkt']);
    }
    else
    {
        select($vars) ;
    }
    if ($GLOBALS['multisubmit']) return; menu_edit();;
    //v(intval($ident));
    ?>
    <form class='ajaxform' method="post" action="myadmin.php">
        <input type="hidden" name="ajax" value="1" /> <?=hidden(); ?>

        <h3> Список выбора:	</h3>
        <?=form('svoystva_spis'); ?>

        <input type="submit" name="submit1" value="Сохранить!" id="submit_tn" />

    </form>


    <h3>Просмотр:</h3>
    <?=form($GLOBALS['ident']); ?>






    <?
}



function set_subpunkt($ident,$sub)
{
    $qu='SELECT max(num) maxn FROM `'.pref_db.'content` WHERE `rod`='.xs($ident);
    $pr=my_mysql_query($qu);
    $dm=mysqli_fetch_assoc($pr);

    foreach ( explode(';',$sub) as $tovse )
    {
        $tovs=trimarr(explode(':',$tovse));
        if ( $tovs[0] or isset( $tovs[1] ) )
        {

            $data=array(
                'ident'=>rand(),
                'rod'=>$ident,
                'num'=>($dm['maxn']++),
                'type'=>'mpunkt',
                'name'=>$tovs[0],
                'd1'=>( isset($tovs[1]) ? $tovs[1] : $tovs[0]  )

            );

            mydbAddLine('content',$data);
        }

    }



}

function edit_rootform() //3
{
    global $submit1, $m, $elm1, $mode, $name, $ident,$id;

    $rootform='rootform-1';

    $vars= select_fields($rootform); set_spis($vars);


    if (isset($submit1))
    {
        $ident=newident($id,$ident);

        update($vars);
    }
    else
    {
        select($vars) ;
    }
    if ($GLOBALS['multisubmit']) return; menu_edit();;
    //v(intval($ident));
    ?>
    <form class='ajaxform' method="post" action="myadmin.php">
        <input type="hidden" name="ajax" value="1" /> <?=hidden(); ?>


        <h3>Корень формы</h3>

        <?=form($rootform); ?><br />

        <input type="submit" name="submit1" value="Сохранить!" id="submit_tn" />

    </form>
    <?  if (rg(9)) { ?>


    <h1> Доступные поля:</h1>
    <?
    $allfield=select_fields($ident,'d2');
    $exallfield=array_flip( explode('	',$allfield) );
    $mkeys=array_keys($m);
    foreach ($mkeys as $km)
    {
        if ( !$exallfield[$km] )
        {
            if (substr($km,1,1)>=1 )
            {
                $diff2[]=$km;
            }
            else
            {
                $diff[]=$km;
            }



        }
    }
    //v($exallfield);
    //sort($diff );
    //sort($diff2 );

    echo ( implode(' ',$diff).'<br/>'.implode(' ',$diff2) );
    ?>

    <h1>Поля для Excel:</h1>
    <textarea style="width:100%;" wrap="off"  rows="3">type	rod	<?=$allfield."\r\nТип	Родитель	".select_fields($ident,'name'); ?></textarea>

<? } ?>

    <br><br>
    <h2> Просмотр: </h2>
    <?//form($ident); ?>





    <?
}


function edit_punkt() //2
{
    global $submit1, $m, $elm1, $mode, $name, $ident, $id;
    $rootform='punkt-1';

    $vars= select_fields($rootform); set_spis($vars);
    if (isset($submit1))
    {
        $ident=newident($id,$ident);

        update($vars);
    }
    else
    {
        select($vars) ;
    }
    if ($GLOBALS['multisubmit']) return; menu_edit();;

    ?>
    <form class='ajaxform' method="post" action="myadmin.php">
        <input type="hidden" name="ajax" value="1" />
        <?=hidden(); ?>
        <h3> </h3>
        <?=form($rootform); ?><br>
        <input type="submit" name="submit1" value="Сохранить!" id="submit_tn" />
    </form>
    <h3> </h3>
    <? //=form($GLOBALS['ident']); ?>
    <?
}

function edit_rootopros() //2
{
    global $submit1, $m, $elm1, $mode, $name, $ident, $id;
    $rootform='rootopros_set';

    $vars= select_fields($rootform); set_spis($vars);
    $vars.= select_fields('page_set_automenu');
    if (isset($submit1))
    {
        $ident=newident($id,$ident);

        update($vars);
    }
    else
    {
        select($vars) ;
    }
    if ($GLOBALS['multisubmit']) return; menu_edit();;

    ?>
    <form class='ajaxform' method="post" action="myadmin.php">
        <input type="hidden" name="ajax" value="1" /> <?=hidden(); ?>
        <h3> Опрос:	</h3>
        <?=form($rootform); ?><br />
        <br />

        <input type="submit" name="submit1" value="Сохранить!" id="submit_tn" />
    </form>

    <!-- <h3> Показ в банерной ленте </h3>
    <?=form('page_set_automenu'); ?>

    <div id="autoh"></div>
    <h3>Просмотр:</h3>
	<? if ($GLOBALS['okautolen'] )
    {
        //v($m);
        $opros=opros($ident);

        echo "<div class='autom'><div class='autom-1'></div><div class='autom-2'>".imgex(img.'/autoimg/'.$m['ident'].'.jpg')."{$opros}</div><div class='autom-3'></div></div>";
    }
    ?>    -->
    <?
}

function edit_rotator() //2
{


    global $submit1, $m, $elm1, $mode, $name, $ident,$id;
    $form='rotator-set';
    $vars= select_fields($form);

    if (isset($submit1))
    {
        $newident=newident($id, $ident );

        if ( $newident<>$m['ident'] )
        {

            @rename(content.'/'.$m['ident'].'.htm',content.'/'.$newident.'.htm');
        }

        $ident=$newident;

        update($vars);


    }
    else
    {

    }
    select($vars) ;
    if ($GLOBALS['multisubmit']) return; menu_edit();;
    //v(intval($ident));
    ?>


    <form class='ajaxform' method="post" action="myadmin.php" enctype="multipart/form-data" id="adminform">
        <input type="hidden" name="ajax" value="1" />
        <?=hidden(); ?><br />



        <?=form($form); ?>
        <br />
        <input class="admbut" type="submit" name="submit1" value="Сохранить!" id="submit_tn" /></form>
    <br /><br />
    <br/>
    <?

}

function edit_ropt() //2
{
    global $submit1, $m, $elm1, $mode, $name, $ident, $id;
    $rootform='ropt-1';
    $radioform='ropt-3';

    $vars=select_fields($rootform).select_fields($radioform);	set_spis($vars);

    if (isset($submit1))
    {
        $ident=newident($id,$ident);

        update($vars);
    }
    else
    {
        select($vars) ;
    }

    if ($GLOBALS['multisubmit']) return; menu_edit();;
    if ( !$GLOBALS['d2'] )
    {$ident_spis=$ident ; }
    else
    {$ident_spis=$GLOBALS['d2'] ;}
    //sv($GLOBALS['m'] );
    ?>
    <form class='ajaxform' method="post" action="myadmin.php">
        <input type="hidden" name="ajax" value="1" /> <?=hidden(); ?>
        <h3>Выборная опция</h3>
        <?=form($rootform); ?>
        <?="<div title='$alt'>".i_r('d1', 'Опции:',  '', '' , $ident_spis ,$mm['d9'],$mm['d5'] ).'</div>'; ?>
        <?=( rg(9) ?  "<div title='$alt'>".i_r('d6', 'Опции для репозитория:',  '', '' , $ident_spis ,$mm['d9'],$mm['d5'] ).'</div>' : '' ); ?>
        <input type="submit" name="submit1" value="Сохранить!" id="submit_tn" />
    </form>
    <br />
    <br />

    <?=fgc($ident); ?>

    <?

}


function edit_topt() //2
{
    global $submit1, $m, $elm1, $mode, $name, $ident, $id;
    $rootform='topt-1';

    $vars= select_fields($rootform); set_spis($vars);
    if (isset($submit1))
    {
        $ident=newident($id,$ident);

        update($vars);
    }
    else
    {
        select($vars) ;
    }
    if ($GLOBALS['multisubmit']) return; menu_edit();;

    ?>
    <form class='ajaxform' method="post" action="myadmin.php">
        <input type="hidden" name="ajax" value="1" /> <?=hidden(); ?>
        <h3> Текстовая опция</h3>
        <?=form($rootform); ?>
        <input type="submit" name="submit1" value="Сохранить!" id="submit_tn" />
    </form>
    <br />
    <br />

    <?=fgc($ident); ?>

    <?
}

function subcat_add($attr,$ident,$subcat_array = false,  $array_as_text = false, $_debug = false )
{

    //v($attr.' - '.$ident.' - '.$subcat_array )	;

    if ($array_as_text)
    {
        $rep=array( basehref.'/'=>' ');
        $subrep=strtr($subcat_array,$rep);
        $subcat_array=explode( ' ',$subrep);
    }

    //v($subcat_array);

    foreach ($subcat_array as $k => $v )
    {
        //v($k);
        //v($v);

        $v=(mb_strtolower( translit( trim($v)  ) ) );

        $exist=subcat_get($attr,$ident,$v);
        //v($exist);

        if (!$exist AND $v AND $ident AND $attr )
        {
            $qu='INSERT  `'.pref_db.'subcat` set			
					`attr`= '.xs($attr).',
					`identsub`='.xs($ident).',
					`subcat`= '.xs($v).'					
						';
            if( $_debug ) v($qu);

            my_mysql_query($qu);
        }



    }


}

function subcat_del($attr,$ident,$subcat)
{
    if ($ident)	$ident=' AND `identsub`='.xs($ident);
    if ($subcat) $subcat=' AND `subcat`='.xs($subcat);

    $qu='DELETE FROM `'.pref_db.'subcat` WHERE `attr`='.xs($attr).' '.$ident.$subcat;
    //v($qu);
    $pr=my_mysql_query($qu);

}

function subcat_get ($attr,$ident,$subcat)
{
    if ($ident)	$ident=' AND `identsub`='.xs($ident);
    if ($subcat) $subcat=' AND `subcat`='.xs($subcat);
    $qu='SELECT * FROM `'.pref_db.'subcat` WHERE `attr`='.xs($attr).' '.$ident.$subcat;
    //v($qu);
    $pr=my_mysql_query($qu);
    while (@$mm=mysqli_fetch_assoc($pr) )
    {
        if ($mm) $out[]=$mm;
    }

    return $out;
}

function edit_fopt() //2
{
    global $submit1, $m, $elm1, $mode, $name, $ident, $id;
    $rootform='fopt-1';

    $vars= select_fields($rootform); set_spis($vars);
    if (isset($submit1))
    {
        $ident=newident($id,$ident);


        if ( $_FILES['file']['name'])
        {
            $_FILES['file']['name']=translit($_FILES['file']['name']);
            if ($GLOBALS['rename'])
            {
                $filename=$ident.'.'.fileending($_FILES['file']['name']);
            }
            else
            {
                $filename=$_FILES['file']['name'];
            }
            move_uploaded_file($_FILES['file']['tmp_name'],cont.'/files/'.$filename);
        }

        update($vars);
    }
    else
    {
        select($vars) ;
    }
    if ($GLOBALS['multisubmit']) return; menu_edit();;

    ?>
    <form class='ajaxform' method="post" action="myadmin.php" enctype="multipart/form-data">
        <input type="hidden" name="ajax" value="1" />
        <?=hidden(); ?>
        <h3>Выбор файла для загрузки:</h3>
        <?=form($rootform); ?>
        <input type="submit" name="submit1" value="Сохранить!" id="submit_tn" />
    </form>
    <br />
    <br />

    <?=fgc($ident); ?>

    <?
}

function export3($ident) //3
{
    //@$add2=func_get_arg(1);
    //v($_dbpref);

    $prebase=my_mysql_query( 'SELECT * FROM `'.pref_db.'content` WHERE ident='.xs($ident).' limit 1') ;
    $mm=mysqli_fetch_assoc($prebase);


    $out.="@@##$$".implode("@#$", $mm).'@#$'.fgc($ident);
    echo $out;

    $prebase=my_mysql_query( 'SELECT * FROM `'.pref_db.'content` WHERE rod='.xs($ident).' ORDER BY `num` ASC ' ) ;

    while ($mm=mysqli_fetch_assoc($prebase) )
    {
        //v(__LINE__);
        $out.=export3($mm['ident']);
    }
    //return $out;

}

function renamer( $oldident, $newident, $_istov=false )
{
    $oldident=url2mnemo($oldident);
    $newident=url2mnemo($newident);
    @rename(cont.'/htm/'.$oldident.'.htm',cont.'/htm/'.$newident.'.htm');
    if (!$_istov)
    {
        @rename(img.'/cat/min/'.$oldident.'.jpg', img.'/cat/min/'.$newident.'.jpg' );
        @rename(img.'/cat/mid/'.$oldident.'.jpg', img.'/cat/mid/'.$newident.'.jpg' );
        @rename(img.'/cat/big/'.$oldident.'.jpg', img.'/cat/big/'.$newident.'.jpg' );
        @rename(img.'/cat/cmid/'.$oldident.'.jpg', img.'/cat/cmid/'.$newident.'.jpg' );

        $images=glob( img.'/cat/mid/'.url2mnemo($oldident).'_??.jpg'  );

        foreach ( $images as $f )
        {
            $newname=str_replace($oldident,$newident, basename($f));
            @rename(img.'/cat/min/'.basename($f), img.'/cat/min/'.$newname );
            @rename(img.'/cat/mid/'.basename($f), img.'/cat/mid/'.$newname );
            @rename(img.'/cat/cmid/'.basename($f), img.'/cat/cmid/'.$newname );
            @rename(img.'/cat/big/'.basename($f), img.'/cat/big/'.$newname );
        }
    }


}





function tovimg()
{
    //array($GLOBALS['d25']);

    $d26=(unserialize_must( $GLOBALS['d26'] ));
    $imgs=$d26 ? (array_merge( (array(($GLOBALS['d25']))), $d26 ) ) : array(($GLOBALS['d25'])) ;
    $imgs[]='';
    $x=0;


    foreach ( ($imgs) as $img )
    {

        $file = filename ( $img ).'.'.( fileending($img) ?: 'jpg') ;


        if ( filename($file)   )
        {
            $hiden='';

            $imurl= 'src="'.img.'/cat/mid/'.$file.'?'.filemtime( img.'/cat/min/'.$file ).'"';
        }
        else
        {
            $imurl='';
            $hiden='hided';
        }
        $rnd=rand();
        $out.= '<li class="tovimg '.$hiden.'" rel="'.$rnd.'" ><a class="delimg" onclick="delimg(this)" ></a>  url: <input type="text" name="dd['.$rnd.']" value="'.( filename( $file) ?  $file : '' ).'" />&nbsp;&nbsp;&nbsp;<input type="file" name="imgfile'.$rnd.'" /><img height="'.( o('adm_tovimg_h') ? : 32 ).'"  '.$imurl.' /></li>' ;

        //
        $x++;
    }
    return $out;
}


function set_tov_set( $mm, $tovset ,$_single )
{
    // need: mm[rod], mm[id],

    if( $_single )  mydbDelSub( 'subcontent', array('tid'=>$mm['id'])  );

    if ($tovset)
    {
        $tovgroup= tovgroupsets_get( 'rod', $mm['rod'] );
        $tovfields=tovsetsfields($tovgroup) ;


        //if ($mm['id']=='4660' ) ($mm);

        foreach ( explode(';', ($tovset)) as $tovse )
        {
            // нужно отловить num


            $tovs=trimarr( explode(':',$tovse)  ); // cвойство: список значений

            if( $tovs[0] AND $tovs[1] )
            {
                if( $tovfields[($tovs[0])]['d5']=='num' )
                {
                    // нужно выявить границы

                    $ranges= (explode( ' ', $tovfields[($tovs[0])]['d7'] ));

                    // нужно выявить к какой именно границе принадлежит


                    if ( $tovs[1] < $ranges[0]   )
                    {
                        $numgrp=1;
                        $pref ='[1]';
                        $val = 'до '.$ranges[0].' '.$tovfields[($tovs[0])]['d6'];
                    }
                    elseif ( $tovs[1] >=  $ranges[( sizeof($ranges)-1 )]   )
                    {
                        $numgrp= sizeof($ranges)+1;
                        $pref ='[3]';
                        $val = 'от '.$ranges[$numgrp-2].' '.$tovfields[($tovs[0])]['d6'];
                    }
                    else
                    {
                        foreach ($ranges as $k=>$v)
                        {
                            if ( $tovs[1] >= $ranges[$k] AND $tovs[1] < $ranges[$k+1]   )
                            {
                                $numgrp=$k+2;
                            }
                        }
                        $pref ='[2]';
                        $val = $ranges[$numgrp-2].' - '.$ranges[$numgrp-1].' '.$tovfields[($tovs[0])]['d6'];
                    }



                    // сгенерировать вставку в базу val2id



                    $tovs[1] = val2id(  $pref.$val ,
                        $tovgroup['d1'],
                        $tovfields[($tovs[0])]['d1'] ,
                        ( $tovfields[($tovs[0])]['d13'] ? $tovfields[($tovs[0])]['d13'] : $tovfields[($tovs[0])]['d1'] ).'-'.intval($ranges[$numgrp-2]).'-'.intval($ranges[$numgrp-1])
                    );
                }
                else
                {
                    //$tovs[1]=$tovfields[($tovs[0])]['d13'] ?
                }



                $to=trimarr( explode(',',$tovs[1]) );

                if(  $tovfields[($tovs[0])]['d11'] )   // $tovfields[$k]['d11']
                {
                    // INSERT INTO tbl_name (a,b,c) VALUES(1,2,3),(4,5,6),(7,8,9);
                    if ($_single)
                    {
                        mydbUpdateSub('subcontent',array('tid'=>$mm['id'],'field'=>$tovs[0], 'folder'=>$tovgroup['d1'] ), 'value',  $to );
                    }
                    else
                    {
                        foreach ($to as $t)
                        {
                            // tid field folder value
                            $out[]= implode( "','" , array($mm['id'],$tovs[0],$tovgroup['d1'], $t ) ) ;
                        }
                    }



                    //
                }
            }


        }
        return implode( "'),('", $out );
    }
}




function creat_cat_img($in_file,$ident, $_filetype='jpg')
{
    //v($in_file);
    //v($ident);
    $mid_w=o('img-minicart-crop') ? 150 : o('tov_img_w') ;
    $mid_h=o('img-minicart-crop') ? 150 : o('tov_img_h') ;

    //$crop=o('img-minicart-crop')?'crop':'' ;
    if ( o('img-mark') )
    {
        imresize($in_file, img.'/cat/bigorig/'.$ident.'.'.$_filetype , o('tov_big_img_w'), o('tov_big_img_h') , FALSE,  $crop, FALSE, o('img-mark') );
        watermark_file( $ident.'.'.$_filetype);
    }
    else
    {
        imresize($in_file, img.'/cat/big/'.$ident.'.'.$_filetype , o('tov_big_img_w'), o('tov_big_img_h') , FALSE,  $crop, FALSE, o('img-mark') );
    }



    fn('imresize', $in_file, img.'/cat/mid/'.$ident.'.'.$_filetype ,$mid_w, $mid_h , FALSE,  $crop );

    if (o('img-minicart-crop'))
    {
        resizer_smart($in_file, img.'/cat/min/'.$ident.'.'.$_filetype , o('microtovimg_w') , o('microtovimg_h') , $imgcoord, FALSE  );
    }
    else
    {
        imresize($in_file, img.'/cat/min/'.$ident.'.'.$_filetype , o('microtovimg_w'), o('microtovimg_h') , FALSE,  'crop', FALSE );
    }

    resizer_smart($in_file, img.'/cat/cmid/'.$ident.'.'.$_filetype , $mid_w , $mid_h   );

}
function creat_cat_admidimg($d26)
{
    foreach(unserialize_must($d26) as $f )
    {
        if($f) creat_cat_midimg( filename( $f) );
    }

}

function creat_cat_midimg($ident,$_filetype='.jpg' )	{

    if ( (file_exists(img.'/cat/big/'.$ident.'.'.$_filetype )))
    {

        if (o('img-minicart-crop'))
        {
            resizer_smart(img.'/cat/big/'.$ident.'.'.$_filetype, img.'/cat/min/'.$ident.'.'.$_filetype , o('microtovimg_w') , o('microtovimg_h') , $imgcoord , FALSE );
        }
        else
        {
            imresize(img.'/cat/big/'.$ident.'.'.$_filetype, img.'/cat/min/'.$ident.'.'.$_filetype , o('microtovimg_w'), o('microtovimg_h'),FALSE,'crop' ,FALSE);
        }



        resizer_smart(img.'/cat/big/'.$ident.'.'.$_filetype , img.'/cat/cmid/'.$ident.'.'.$_filetype , o('tov_img_w'), o('tov_img_h'), $imgcoord );

        $mid_w=o('img-minicart-crop') ? 150 : o('tov_img_w') ;
        $mid_h=o('img-minicart-crop') ? 150 : o('tov_img_h') ;

        imresize(img.'/cat/big/'.$ident.'.'.$_filetype, img.'/cat/mid/'.$ident.'.'.$_filetype , $mid_w , $mid_h );
    }

}

function edit_mod()
{
    global $submit1, $m, $elm1, $mode, $name, $ident,$id;
    edit_tov();
}

function edit_update()
{
    edit_update::x();
}

function imagehbs($ident, $h, $b=100, $s=100,$bp=0,$g=1,$wp=1 )
{
    //if ($_REQUEST['gamma'])  $img->levelImage  ( $_REQUEST['blackPoint'], $_REQUEST['gamma'], $_REQUEST['whitePoint'] );
    if(  file_get_contents( ('http://inst.giperplan.ru/img2.php?brightness='.$b.'&saturation='.$s.'&hue='.$h.'&blackPoint='.$bp.'&gamma='.$g.'&whitePoint='.$wp.'&url='.basehref.'/cont/img/tpl/'.$ident.'.png' )) )
    {
        copy(  'http://inst.giperplan.ru/cont/img/out.png',  'cont/img/tpl/'.$ident.'-hbs.png'  );
    }
}

function mb_trim( $string )
{
    $string = preg_replace( "/(^\s+)|(\s+$)/us", "", $string );

    return $string;
}

function reindextov($id=false)
{
    $where_id=$id ? ' AND id='.xs($id) : '' ;
    $qu='SELECT * FROM `'.pref_db.'content` WHERE  `type`=\'tov\' AND `d89`=\'rei\'  AND ok<>\'\' '.$where_id;
    $pr=my_mysql_query($qu);
    $dm=mysqli_fetch_assoc($pr);

    if (  !$dm  )
    {
        echo 'Начало новой индексации';
        my_mysql_query( 'UPDATE '.pref_db.'content SET `d89`=\'rei\' WHERE `type`=\'tov\'  AND ok<>\'\'  '.$where_id );
        if ($id) {
            mydbDelSub('subcontent',array('tid'=>$id) ) ;
        } else {
            my_mysql_query( ' TRUNCATE TABLE  `'.pref_db.'subcontent` ' );
        }
    }
    else
    {
        echo 'Продолжение прерваной индексации...';
    }



    $pr=my_mysql_query( ( 'SELECT d9, id, rod, d81, d15 FROM '.pref_db.'content  WHERE `type`=\'tov\' AND `d89`=\'rei\'  AND ok<>\'\'  '     )) ;

    while ( $mm=mysqli_fetch_assoc($pr) )
    {
        $val=set_tov_set( $mm, $mm['d9'] );

        if ( $val ) $values[] =$val;
        echo  $mm['id'].' ';

        $updated[]=$mm['id'];

        // вычисление по формуле если есть

        //if ( !$tovgroup[$mm['rod'] ] ) $tovgroup[$mm['rod'] ]= tovgroupsets_get( 'rod', $mm['rod'] );


        subcat_del('subcat',$mm['id'],'');

        if (  $mm['d15']  ) {
            subcat_add('subcat', $mm['id'] , $mm['d15']  , TRUE );
        }


        if( sizeof( $values) > 500 )
        {
            set_tov_flush($updated, $values  );
            unset(  $values , $updated  );
        }

        //mydbUpdate('content',array('d89'=>''),'id', $mm['id'] );
    }

    set_tov_flush($updated, $values );

    echo 'Переиндексация завершена! ';
}

function set_tov_flush($updated, $values  )
{
    //v($values);
    //INSERT INTO tbl_name (a,b,c) VALUES(1,2,3),(4,5,6),(7,8,9);
    my_mysql_query( ( 'INSERT INTO  '.pref_db.'subcontent (tid,field,folder,value) VALUES  '."('".implode( "'),('", $values)."')"  ) );

    //v($updated);
    //mydbUpdate('content',array('d89'=>''),'id', $mm['id'] );
    my_mysql_query(  ( 'UPDATE  '.pref_db.'content set d89=\'\' WHERE id in  '."('".implode( "','", $updated)."')"  ) );

}

function edit_shab()
{
    global $submit1, $m, $elm1, $mode, $name, $ident, $id;
    $rootform='shab-1';

    $vars= select_fields($rootform); set_spis($vars);
    if (@$submit1)
    {
        $ident=newident($id,$ident);



        update($vars);
    }
    else
    {
        select($vars) ;
    }
    if ($GLOBALS['multisubmit']) return; menu_edit();;

    ?>
    <form class='ajaxform' method="post" action="myadmin.php" enctype="multipart/form-data">
        <input type="hidden" name="ajax" value="1" />
        <?=hidden(); ?>
        <h3> Шаблон:	</h3>
        <?=form($rootform); ?><br />
        <br />
        <input type="submit" name="submit1" value="Обновить" id="submit_tn" />
    </form>

    <? //<h3>Просмотр:</h3>  =form($GLOBALS['ident']); ?>
    <?
}

function edit_fotoalbum()
{
    global $m,$ident,$id,$name,$submit1;
    $rootform='fotoalbum-2';
    $vars= select_fields($rootform); set_spis($vars);

    if ($GLOBALS['delfoto'])
    {
        pdel($id,1);
    }



    if ($_GET['name'])
    {
        $targetDir = 'cont/img/cat';
        $_GET['name']=charset_x_win ( $_GET['name']);
        $filePath = $targetDir.'/'.$_GET['name'];
        file_put_contents($filePath, file_get_contents('php://input'));
        $submit1=1;
        $_FILES['image']['name']=$_GET['name'];
        $_FILES['image']['tmp_name']=$filePath;
        ob_clean();
    }



    if ( isset($submit1) or ( $GLOBALS['ajax'] AND $_FILES )   ) // | ($GLOBALS['ajax'])
    {
        if($GLOBALS['uv']=='false') $GLOBALS['uv']=false;
        if($GLOBALS['cat']=='false') $GLOBALS['cat']=false;

        $ident=newident($id,$ident);

        if ($GLOBALS['url'])
        {
            $fe=file_get_contents($GLOBALS['url']);
            file_put_contents(img.'/cat/big/tmp.'.$fe,	file_get_contents($GLOBALS['url']) );
            $_FILES['image']['tmp_name']=img.'/cat/big/tmp.'.$fe;
            $urlmini=array_reverse( ( explode('/',$GLOBALS['url']) ) );
            $_FILES['image']['name']=$urlmini[0];
            unset($GLOBALS['url']);
        }

        if ( $_FILES['image']['name'])
        {
            //v($_FILES['image']['name']);
            $_FILES['image']['name']=newident($id,$_FILES['image']['name']);
            $fend=fileending($_FILES['image']['name']);
            $fname=filename($_FILES['image']['name']);
            //v($fname);

            if ( strpos(' jpg png gif jpeg', $fend))
            {
                if ( $fend=='png' )
                {
                    move_uploaded_file($_FILES['image']['tmp_name'],  img.'/cat/big/'.$m['ident'].'-'.$fname.'.png' );
                }
                else
                {
                    if ( ($GLOBALS['obrez']) ) $obrez='crop';
                    imresize($_FILES['image']['tmp_name'], img.'/cat/mid/'.$m['ident'].'-'.$fname.'.jpg',  o('fotoalbum_limw_mini'), o('fotoalbum_limh_mini') , 90, $obrez);

                    imresize($_FILES['image']['tmp_name'], img.'/cat/big/'.$m['ident'].'-'.$fname.'.jpg', o('fotoalbum_limw_big'), o('fotoalbum_limh_big'), ( $GLOBALS['qjpeg'] ? $GLOBALS['qjpeg'] : 80 ) );
                    //v(filename($_FILES['image']['name']));
                }




                if (!qvar('ident',$m['ident'].'-'.$fname.( $fend=='png' ? '.png' : '' ) ) )
                {
                    //v($m['ident']);

                    $qu="SELECT MAX(`num`) FROM `".pref_db."content` WHERE `rod`=".xs($m['ident'] ) ;
                    $pr=my_mysql_query($qu);
                    $mmax=mysqli_fetch_array($pr);


                    $qu="INSERT  `".pref_db."content` set
						`num`= '".($mmax[0]+1)."',
						`ident`= ".xs( $m['ident'].'-'.$fname.( $fend=='png' ? '.png' : '' ) ).",
						`rod`= '".$m['ident']."',
						`type`='imgalbum'					
							";
                    my_mysql_query($qu);

                }

                echo '<div></div>';
                echo '<img src="'.img.'/cat/mid/'.$m['ident'].'-'.$fname.'.jpg'.'?'.rand().'" >';
                echo '<div></div>';


            }

        }
        if ( !$GLOBALS['type'] ) update($vars);
        //v(1);
        if($_GET['name']) { die(); }


    }
    else
    {
        select($vars) ;
    }

    if ($GLOBALS['multisubmit']) return; menu_edit();;

    ?>
    <form class='ajaxform' method="post" action="myadmin.php" enctype="multipart/form-data" id="edit_fotoalbum">
        <input type="hidden" name="ajax" value="1" />
        <?=hidden(); ?>
        <h3> Фотоальбом:	</h3>
        <?=form($rootform); ?><br />
        <br />
        <input type="submit" name="submit1" value="Обновить" id="submit_tn" />
    </form>
    <!-- The global progress bar -->
    <div id="progress" class="progress progress-success progress-striped">
        <div class="bar"></div>
    </div>
    <!-- The container for the uploaded files -->
    <div id="files"></div>

    <? //<h3>Просмотр:</h3>  =form($GLOBALS['ident']); ?>
    <?
}


function edit_imgalbum()
{
    global $submit1, $m, $elm1, $mode, $name, $ident, $id;
    $rootform='imgalbum-1';

    $vars= select_fields($rootform); set_spis($vars);
    if (@$submit1)
    {
        $ident=newident($id,$ident);

        if ( $_FILES['image']['name'])
        {

            //v($_FILES['image']['name']);
            $_FILES['image']['name']=newident($id,$_FILES['image']['name']);

            $fend=fileending($_FILES['image']['name']);
            $fname=filename($_FILES['image']['name']);
            //v($fname);
            $ident=$m['rod'].'-'.$fname;

            if ( strpos(' jpg png gif', $fend))
            {
                imresize($_FILES['image']['tmp_name'], img.'/cat/mid/'.$ident.'.jpg',  o('fotoalbum_limw_mini'), o('fotoalbum_limh_mini') , 90);

                imresize($_FILES['image']['tmp_name'], img.'/cat/big/'.$ident.'.jpg', $_REQUEST['limwb'], $_REQUEST['limhb'],80);
                //v(filename($_FILES['image']['name']));
                $m['ident']=$ident;

                echo '<div></div>';
                echo '<img src="'.img.'/cat/mid/'.$ident.'.jpg'.'?'.rand().'" >';
                echo '<div></div>';


            }
        }

        update($vars);
    }
    else
    {
        select($vars) ;
    }
    if ($GLOBALS['multisubmit']) return; menu_edit();;

    ?>
    <form class='ajaxform' method="post" action="myadmin.php" enctype="multipart/form-data">
        <input type="hidden" name="ajax" value="1" />
        <?=hidden(); ?>
        <h3> Фотоальбом:	</h3>
        <?=form($rootform); ?><br />
        <br />
        <input type="submit" name="submit1" value="Обновить" id="submit_tn" />
    </form>

    <?
    if ( fileending($m['ident'])  == 'png' )
    {
        $img=    '/cont/img/cat/big/'.$m['ident'];
    }
    else
    {
        $img=    '/cont/img/cat/mid/'.$m['ident'].'.jpg'  ;
    }


    echo '<img src="'.$img.'?'.rand().'" >';
    //<h3>Просмотр:</h3>  =form($GLOBALS['ident']); ?>
    <?
}


function edit_contentt_ype()

{
    global $submit1, $m, $elm1, $mode, $name, $ident,$id;
    $ident=newident($id,$GLOBALS['d1']);
    $vars='		
		ident~ident
		name~name
		d1~d1
		d2~field2
		d3~types
		d4~doing
		d5~field3
		d6~Ident629
		';
    $vars=select_fields('contenttype-1');		 set_spis($vars);

    if (isset($submit1))
    {

        $ident=newident($id,$ident);

        move_uploaded_file($_FILES['img']['tmp_name'], jscripts.'/tpladmin/t-'.$ident.'.png');

        //v($saveplace);
        update($vars);
    }
    else
    {
        select($vars) ;
    }
    if ($GLOBALS['multisubmit']) return; menu_edit();;
    //v(intval($ident));
    ?>
    <form class='ajaxform' method="post" action="myadmin.php">
        <input type="hidden" name="ajax" value="1" /> <?=hidden(); ?>
        <?
        ?>
        <a href="?edit=<?=$m['num'] ?>&mode=2" >Визуальный редактор</a>


        <?//=i("t",'name','style="width:100px"','Имя');  ?>
        <?//=i("t",'d1','style="width:100px"','Значение');  ?>
        <?//=i_select('Отображаемое второе поле','field2','viewfild','name','d1',''); ?>
        <?=form('contenttype-1'); ?>
        <br/><br/>


        <input type="submit" name="submit1" value="Сохранить!" id="submit_tn" />
        <br/>
    </form>
    <?

}

function edit__video()
{
    global $submit1, $m, $elm1, $mode, $name, $ident, $id;
    $rootform='video-1';

    $vars= select_fields($rootform); set_spis($vars);
    if (@$submit1)
    {
        $ident=newident($id,$ident);

        if ( $_FILES['file']['name'])
        {
            if(move_uploaded_file($_FILES['file']['tmp_name'],'cont/files/'.$id.'.flv') ) echo 'ok!';
        }


        update($vars);
    }
    else
    {
        select($vars) ;
    }
    if ($GLOBALS['multisubmit']) return; menu_edit();;

    ?>
    <form class='ajaxform' method="post" action="myadmin.php" enctype="multipart/form-data">
        <input type="hidden" name="ajax" value="1" />
        <?=hidden(); ?>
        <h3> Видео:	</h3>
        <?=form($rootform); ?><br />
        <br />
        <a href="/cont/files/" target="_blank">Склад файлов</a><br />
        <br />
        <input type="submit" name="submit1" value="Обновить" id="submit_tn" />
    </form>

    <? //<h3>Просмотр:</h3>  =form($GLOBALS['ident']); ?>
    <?
}

function edit_superbaner()
{
    global $submit1, $m, $elm1, $mode, $name, $ident, $id;
    $rootform='superbaner-1';

    $vars= select_fields($rootform); set_spis($vars);
    if (@$submit1)
    {

        $newident=newident($id,$ident);
        //v($newident);
        if ( $newident<>$m['ident'] )
        {

            @rename(content.'/'.$m['ident'].'.htm',content.'/'.$newident.'.htm');

        }
        $ident=$newident;
        //


        update($vars);
    }
    else
    {
        select($vars) ;
    }
    if ($GLOBALS['multisubmit']) return; menu_edit();;

    ?>
    <form class='ajaxform' method="post" action="myadmin.php" enctype="multipart/form-data">
        <input type="hidden" name="ajax" value="1" />
        <?=hidden(); ?>
        <h3> Блок плавающий:	</h3>
        <?=form($rootform); ?><br />

        <br />
        <input type="submit" name="submit1" value="Обновить" id="submit_tn" />
    </form>

    <? //<h3>Просмотр:</h3>  =form($GLOBALS['ident']); ?>
    <?
}
function unseter(&$mm)
{
    unset($mm['priceall'], $mm['id'],$mm['num'],$mm['prio'],$mm['nolink'],$mm['nadcat'],$mm['saveplace'],$mm['bok'],$mm['d22'], $mm['sub'] );
    if ($mm['type']=='tov' AND o('fullsearch_ver')) unset( $mm['d9'] );
}


function export($ident, $without_cur_ident, $only_cur_ident, $_colls=false) //3
{

    if ($_colls=='used') return export_used_main($ident, $without_cur_ident, $only_cur_ident);

    $mm=qvar('ident',$ident);

    unseter($mm);
    $fgc=fgc($ident);
    //vta($fgc);
    if  (!$without_cur_ident) $out="@@##$$".implode("@#$", $mm).'@#$'.fgc($ident);

    $rep=array("\r\n"	=>	'[br]', "\n"	=>	'[br]', '	' => '[tab]'	);
    $out= strtr($out,$rep) ;
    $rep=array('@@##$$'		=>	"\r\n", '@#$' => '	'	);
    $out= strtr($out,$rep) ;

    if(!$only_cur_ident)
    {
        $prebase=my_mysql_query( 'SELECT * FROM '.pref_db.'content WHERE rod="'.$ident.'" AND  d83<\'1\' ORDER BY `num` ASC ' ) ;
        while ($mm=mysqli_fetch_assoc($prebase) )
        {
            $out.=export($mm['ident'],0,0);
        }
    }

    return $out;
}

function export_used($mm, $without_cur_ident, $only_cur_ident) //3
{

    unseter( $mm);

    $mm['cont']=fgc($mm['ident']);

    if  (!$without_cur_ident)
    {
        $diff=( array_diff( $mm,array('') ));
        $GLOBALS['cache']['usedkeys'] = ($GLOBALS['cache']['usedkeys']) ?  array_merge ( $GLOBALS['cache']['usedkeys'], $diff  ) : $diff ;
    }

    $GLOBALS['cache']['exportout'][]=$mm;

    if(!$only_cur_ident)
    {
        $prebase=my_mysql_query( 'SELECT * FROM '.pref_db.'content WHERE rod="'.$mm['ident'].'" AND  d83<\'1\' ORDER BY `num` ASC ' ) ;
        while ($mmnew=mysqli_fetch_assoc($prebase) )
        {
            export_used($mmnew,0,0);
        }
    }

}

function export_used_main($ident, $without_cur_ident, $only_cur_ident)
{

    $mm=(qvar('ident',$ident));
    export_used(($mm), $without_cur_ident, $only_cur_ident);
    //$mm['cont']=1;

    foreach ( ($GLOBALS['cache']['exportout']) as $mmn )
    {
        if( !$outfull ) $outfull= implode("	", array_keys( array_intersect_key ( ($mmn), ($GLOBALS['cache']['usedkeys']) ) ) ) ;

        $mmn=( array_intersect_key ( $mmn, $GLOBALS['cache']['usedkeys'])  );

        $out="@@##$$".implode("@#$", $mmn);
        $rep=array("\r\n"	=>	'[br]', "\n"	=>	'[br]', '	' => '[tab]'	);
        $out= strtr($out,$rep) ;
        $rep=array('@@##$$'		=>	"\r\n", '@#$' => '	'	);
        $outfull.= strtr($out,$rep) ;

    }
    return $outfull;
}

function clone_elements($id , $_new_rod=FALSE ) {
    $oldmm=qvar('id',$id);
    $newid = padd( $id , 'clone' );
    if ( $_new_rod ) {
        mydbUpdate('content',array( 'rod'=>$_new_rod ), 'id', $newid );
    }
    $newmm = qvar('id', $newid );

    $pr=my_mysql_query(v( 'SELECT `id` FROM '.pref_db.'content WHERE rod='.xs($oldmm['ident']).'  ORDER BY `num` ASC ' ) ) ;

    while ( $mm=mysqli_fetch_array($pr) ) {
        ($mm['id']);
        clone_elements( $mm['id'], $newmm['ident'] );
    }
    return $newid;
}

function padd($id, $_act=false) {

    $qmin=10; // ishem minimalniy svobodniy id
    while ( mysqli_fetch_array( my_mysql_query ( "select `id` FROM `".pref_db."content` where `id`='$qmin' ") ) )
    {
        $qmin++;
    }
    // ishem tekushuyu yacheyku
    //$qu="SELECT * FROM `".pref_db."content` WHERE `id`='".intval($id)."' ";
    //v($qu);
    //$m=mysqli_fetch_array(my_mysql_query($qu));

    $m=qvar('id',$id);


    my_mysql_query("UPDATE `".pref_db."content` SET `num` = `num`+1 WHERE `num`>'".$m['num']."' " );
    if($m['type']=='_news')
    {
        $num=0;
    }
    else
    {
        $num=intval($m['num'])+1;
    }

    $newident= newident($qmin,prefup($m['ident']) );

    if (oror($_act,'','add'))
    {
        $qu="INSERT  `".pref_db."content` set
				`num`= $num,
				`ident`= ".xs( $newident ).",
				`id`=$qmin,
				`rod`= '".$m['rod']."',
				`type`='".$m['type']."',
				`bbcode`='on'
					";
        //v($qu);
        my_mysql_query($qu);
    }
    elseif ( $_act=='clone' )
    {
        $m['id']=$qmin;

        $m['num']=$m['num']+1;
        $idnext=qvar('num',$m['num']);
        if ($idnext) pdown($idnext);
        file_put_contents('cont/htm/'.$newident.'.htm', fgc($m['ident']) );
        $m['ident']=$newident;
        mydbAddLine('content',$m);
    }


    $edit=$add+1;
    return $qmin;

}

function pup($up)
{
    $cm=qqu("`id`=".intval($up));

    //$dm=qqu("`num`<".$cm['num']." and `rod`='{$cm['rod']}' ORDER BY `num` DESC ");
    $qu='SELECT * FROM `'.pref_db.'content` WHERE `num`<'.$cm['num'].' and `rod`='.xs($cm['rod']).'  ORDER BY `num` DESC LIMIT 1 ';
    $pr=my_mysql_query($qu);
    $dm=mysqli_fetch_assoc($pr);
    if ($dm)
    {
        $qu="UPDATE `".pref_db."content` SET `num` = ".intval($dm['num'])." WHERE `num`=".$cm['num']." limit 1 " ;
        //v($qu);
        my_mysql_query($qu);

        $qu="UPDATE `".pref_db."content` SET `num` = ".$cm['num']." WHERE `id`=".intval($dm['id'])." limit 1 " ;
        //v($qu);
        my_mysql_query($qu);

        //$edit=$dm['num'];
    }
}

function pdown($down)
{

    $cm=qqu("`id`=".intval($down));

    //$dm=qqu("`num`>".$cm['num']." and `rod`='{$cm['rod']}'");

    $qu='SELECT * FROM `'.pref_db.'content` WHERE `num`>'.$cm['num'].' and `rod`='.xs($cm['rod']).'  ORDER BY `num` ASC LIMIT 1 ';
    $pr=my_mysql_query($qu);
    $dm=mysqli_fetch_assoc($pr);

    //$dm=qqu("`num`>".$cm['num']." and `rod`='{$cm['rod']}'");
    if($dm)
    {
        $qu="UPDATE `".pref_db."content` SET `num` = ".intval($dm['num'])." WHERE `num`=".$cm['num']." limit 1 " ;
        //v($qu);
        my_mysql_query($qu);
        $qu="UPDATE `".pref_db."content` SET `num` = ".$cm['num']." WHERE `id`=".intval($dm['id'])." limit 1 " ;
        //v($qu);
        my_mysql_query($qu);

        $edit=$dm['num'];
    }

}

function paddd($id) //2
{
    $qmin=10;
    while ( mysqli_fetch_array( my_mysql_query ( "select `id` FROM `".pref_db."content` where `id`='$qmin' ") ) )
    {
        $qmin++;
    }
    $m=qvar('id',$id);

    my_mysql_query("UPDATE `".pref_db."content` SET `num` = `num`+1 WHERE `num`>".intval($m['num'])." ; " );

    if($m['type']=='bnews')
    {
        $num=0;
    }
    else
    {
        $num=intval($m['num'])+1;
    }

    $mtyp=qvar('rod',$m['ident']);
    $newtype=$mtyp['type'];

    if ($m['type']=='rootopros') $newtype='punkt';
    if ($m['type']=='ropt' | $m['type']=='spis' ) $newtype='mpunkt';
    if ($m['type']=='rotator' ) $newtype='baner';
    if ($m['type']=='fotoalbum' ) $newtype='imgalbum';
    if ($m['type']=='tov' ) $newtype='mod';
    if ($m['type']=='contenttype' ) $newtype='rootform';

    if (!$newtype )
    {
        $newtype=$m['type'];
    }

    $qu="INSERT  `".pref_db."content` set
	`num`= '".$num."',
	`ident`= ".xs( newident($qmin,prefup($m['ident']) )  ).",
	`id`=$qmin,
	`rod`= '".$m['ident']."',
	`type`='".$newtype."',
	`bbcode`='on'
		";
    //v($qu);
    my_mysql_query($qu);
    $GLOBALS['id']=$qmin;
}

function pdel($id) //2
{
    @$onlysub=func_get_arg(1);
    $prem=qvar('id',$id);

    if (!(bool)$onlysub) // ???
    {
        my_mysql_query("DELETE FROM `".pref_db."content` WHERE `id` = '".intval($id)."'  " );
        //v(2);
    }

    //unset( $edit );
    //$fordel=

    if ($prem) $fordel=findtree($prem['ident']);

    if($fordel)
    {
        foreach ($fordel as $fd)
        {
            my_mysql_query("DELETE FROM `".pref_db."content` WHERE `ident` = '".$fd."'  " );
        }
    }

    $qu="`num`<'".$prem['num']."' and `rod`='".$prem['rod']."' ORDER BY `num` DESC  ";
    $m=qqu($qu );
    if (!$m)
    {
        $m=qvar('ident',$prem['rod'] );
        //v($m);
    }

    if (!$onlysub) $GLOBALS['id']=$m['id'];
}



function remeditimg ($m)
{
    global $elm1, $submit;
    if (@$submit)
    {
        if ($GLOBALS['url'])
        {
            $file=file_get_contents($GLOBALS['url']);

            file_put_contents(img.'/cat/big/img.tmp', $file );

            $_FILES['image']['tmp_name']=img.'/cat/big/img.tmp';

            $urlmini=array_reverse( ( explode('/',$GLOBALS['url']) ) );
            $_FILES['image']['name']=$urlmini[0];
            unset($GLOBALS['url']);
        }
        //v($_FILES['image']['name']);
        $fend=fileending($_FILES['image']['name']);
        if ( @strpos(' jpg png gif jpeg', $fend) )
        {

            imresize($_FILES['image']['tmp_name'], img.'/'.$m['ident'].'-r-'.$_FILES['image']['name'], $_POST['resize'], $_POST['resize'],90);
            if ($_POST['bigresize']!=='non')
            {
                imresize($_FILES['image']['tmp_name'], img.'/big/'.$m['ident'].'-r-'.$_FILES['image']['name'], 950, 800,90);
            }
            else
            {
                unlink(img.'/big/'.$m['ident'].'-r-'.$_FILES['image']['name']);
            }



        }

    }
    ?>
    <html>
    <head>
        <script src="/jscripts/jquery.fancybox/jquery-1.3.2.min.js"></script>
        <script src="/jscripts/plug.js"></script>
        <script language="javascript">
            var ie6=false;var adminpanel=false;
        </script>
        <script src="/cont/js/my.js"></script>
        <link rel="stylesheet" type="text/css" href="/jscripts/jquery.fancybox/jquery.fancybox.css"/>
        <style>
            img {padding:5px;margin:10px; border:solid 1px; float:left;}
            p:before { content:"p:"; }


        </style></head>
    <body>
    <table width="500" align="center"><tr><td align="center"><form method="post" action="" enctype="multipart/form-data">
                    <?=form('imremeditform'); ?>
                    <input type="submit" name="submit" value="Загрузить!"/>
                </form></td></tr></table>
    <div id="center">
        <?
        chdir(img);
        @$files= glob("{$m['ident']}-r-*.???");
        //v($files);
        foreach (@$files as $filename)
        {

            if ( file_exists('big/'.$filename))
            {
                echo "<img class='imgbox' src='/cont/img/{$filename}' >";
            }
            else
            {
                echo "<img src='/cont/img/{$filename}' >";
            }


        }
        ?>
    </div></body></html>
    <?

}



function remedit ($m)
{
global $elm1, $submit_tn;
if ($GLOBALS['lng']) $lng='_'.$GLOBALS['lng'];

if (@$submit_tn)
{
    $eml=magic($elm1);
    file_put_contents(content.'/'.$m['ident'].$lng.".htm",$eml);

    if ($em[1])
    {
        file_put_contents(content.'/prev_'.$m['ident'].$lng.".htm",$em[0]);
    }

    //update(qu_tpl_page());

}
else
{
    //select(qu_tpl_page()) ;
}
?>
<html><head>
    <script src="/jscripts/jquery.fancybox/jquery-1.3.2.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function()
        {
            $('#fullview').click( function ()
            {
                $('#tinymce').toggleClass('showhelp');
            });
        });
    </script>
    <?

    if ( $m['bbcode']!=='')
    {

        ?>
        <!-- TinyMCE -->

        <script type="text/javascript" src="<?=tnroot; ?>/tiny_mce.js"></script>
        <script type="text/javascript">
            tinyMCE.init({
                // General options
                mode : "textareas",
                theme : "advanced",
                plugins : "safari,pagebreak,style,table,advhr,advimage,advlink,inlinepopups,insertdatetime,preview,media,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
                //iespell,searchreplace,print,
                // Theme options
                theme_advanced_buttons1 : "bold,italic,underline,strikethrough,sub,sup,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect,bullist,numlist, outdent,indent,charmap,image",
                //save,newdocument,|,,fontselect,fontsizeselect
                theme_advanced_buttons2 : "fullscreen,|,pasteword,undo,redo,|,link,unlink,|,tablecontrols",
                //|,search,replace,|,help,code,preview,|,forecolor,backcolor
                theme_advanced_buttons3 : "",
                //|,print,|,
                theme_advanced_buttons4 : "",
                //insertlayer,moveforward,movebackward,absolute,|,attribs,
                theme_advanced_toolbar_location : "top",
                theme_advanced_toolbar_align : "left",
                theme_advanced_statusbar_location : "bottom",
                theme_advanced_resizing : true,

                // Example content CSS (should be your site CSS)
                content_css : "/cont/css/remedit.css",

                // Drop lists for link/image/media/template dialogs
                template_external_list_url : "",
                external_link_list_url : "",
                external_image_list_url : "",
                media_external_list_url : "",

                // Replace values for the template plugin
                template_replace_values : {
                    username : "Some User",
                    staffid : "991234"
                }
            });
        </script>
        <!-- /TinyMCE -->
        <?
    }
    ?>
</head>
<body id='fullview'>
<form method="post" action="">



    <input type="submit" name="submit_tn" value="Сохранить!"  id="submit_tn" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;


    <a href="?<?=$_SERVER['QUERY_STRING']; ?>">Перезагрузить</a>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="javascript:;" onMouseDown="tinyMCE.get('elm1').show();">[Визуально]</a>
    <a href="javascript:;" onMouseDown="tinyMCE.get('elm1').hide();">[HTML]</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="?<?=$_SERVER['QUERY_STRING']; ?>&getimg=1" target="_blank">Картинки</a>

    </span><br/>

    <textarea name="elm1" rows="30" style="width:100%;" id="elm1"  ><?=fgc($m['ident'].$lng);?></textarea>




</form>
<table align="center" width="800" cellpadding="10" border="1"><tr><td><?=fgc($m['ident'].$lng);?></td></tr></table>
</body>
<?

}






// определение charset:

function _charset_count_bad($s)
{ //count "bad" symbols in russian, in windows-1251
    $r=0;
    for($i=0;$i<strlen($s);$i++)
    {
        switch($s[$i])
        {
            case 'ё':
            case 'Ё':
            case '«':
            case '»':
                break;
            default:
                $c=ord($s[$i]);
                if($c>=0x80&&$c<0xc0||$c<32)
                    $r++;
        }
    }
    return $r;
}

function _charset_count_chars($s)
{ //count "good" symbols in russian, in windows-1251
    $r=0;
    for($i=0;$i<strlen($s);$i++)
    {
        $c=ord($s[$i]);
        if($c>=0xc0)
            $r++;
    }
    return $r;
}

function _charset_count_pairs($s)
{ //count "bad" pairs of chars for a string in russian, in windows-1251
    $a=array(
        0 => 'ъыь',
        1 => 'йпфэ',
        2 => 'йфэ',
        3 => 'жйпфхцщъыьэю',
        4 => 'йфщ',
        5 => 'ъыь',
        6 => 'зйтфхшщъыэя',
        7 => 'йпфхщ',
        8 => 'ъыь',
        9 => 'абжийущъыьэюя',
        10 => 'бгйпфхщъыьэюя',
        11 => 'йрцъэ',
        12 => 'джзйъ',
        13 => 'ймпъ',
        14 => 'ъыь',
        15 => 'бвгджзйхщъэю',
        16 => 'йъэ',
        17 => 'й',
        18 => 'жй',
        19 => 'ъыь',
        20 => 'бвгджзйкпхцшщъьэюя',
        21 => 'бжзйфхцчщъыьюя',
        22 => 'бгджзйлнпрстфхцчшщъьэюя',
        23 => 'бгджзйпсфхцчщъыэюя',
        24 => 'бгджзйфхшщъыэя',
        25 => 'бвгджзйклмпстфхцчшщъыэюя',
        26 => 'абвгджзийклмнопрстуфхцчшщъыьэ',
        27 => 'аофъыьэю',
        28 => 'айлрухъыьэ',
        29 => 'абежиоуцчшщъыьэю',
        30 => 'иоуфъыьэя',
        31 => 'аоуфъыьэ'
    );
    $b=array(
        0  => 'ааабавагадаеажазаиайакаланаоасатауафахацачашащаэаюаябгбмбтбхбцбчбшбщбъбьбюбявбвжвхвъвюгзгкгтгчгядддхдэеаебегееежеиеоепесеуефецещеэеюеяжбжвжлжпжржцжчжюзззсзтзшзэзюиаиеижииийиоипиуифицишищиэиюияйпйркзкмкчкшлблвлзлнлшлщмвмгмхмчмэмюнбнвнэоаовогоеожозоиойоколомооопоуофохоцошощоэоюояпмпцрзсгсдсжсзсъсэтбтгтдтзтптштщтътэуаубувужуиуйуоуууфухуцущуюуяфлфмхгхдхкхпхсхшхэцвцмцуцычвчмчрчшшршсшчщнщрщьэвэгэдэзэйэкэмэнэпэтэфэхэяюаюбювюгюдюеюжюзюйюлюмюнюпюрюхюцюшююябягядяеяжязяияйяпяряшящяюяя',
        1  => 'ааажаоапафащаэбабббвбгбдбжбзбкблбмбнбсбтбубхбцбчбшбщбъбыбьбюбявбвввгвдвжвзвквлвмвнвпврвсвтвувхвцвчвшвщвъвьвювягагбгвгггдгегзгигкглгмгнгргсгтгугчгшгядбдвдгдддждздкдлдмдндодпдрдсдтдхдцдчдшдъдыдьдэдюеаебепеуефеэеяжбжвжгжджжжкжлжмжнжожпжржсжцжчжьжюзбзвзгздзезжзззизкзлзмзнзрзсзтзузцзчзшзъзызьзэзюзяиаиэквкдкжкзккклкмкнкскткцкчкшлблвлглдлжлзлклллмлнлплслтлфлхлчлшлщмбмвмгмкмлмммнмпмрмсмтмумфмхмцмчмшмщмымьмэмюнбнвнгнднжнзнкнлнннрнснтнфнхнцнчншнщньнэоаооофохрбрвргрдржрзркрлрмрнрпрррсртрфрхрцрчршрщсбсвсгсдсжсзсмснспсрсссфсхсчсшсщсъсысьсэсютатбтвтгтдтзтитктлтмтнтптртстттутфтхтцтчтштщтътытьтэтюуоуууцущуэхгхдхехихкхлхмхнхпхрхсхтхухшхэцвцицкцмцучвчечкчлчмчнчочрчтчучшчьшвшкшлшмшншошпшршсштшушцшчшьшющощрщьъюыбыгыжыиыпырыуыцышыяьбьвьгьдьжьзькьмьньоьпьсьтьфьцьчьшьщюаюбювюгюеюжюзюйюкюмюнюпюхюцючюшющююябявягядяеяжяияйякянярясяхяцячяшяюяя',
        2  => 'аааоауафащаэбабббвбгбдбжбзбкбмбнбсбтбхбцбчбшбщбъбыбьбюбявбвввгвдвжвзвквлвмвнвпвтвувхвцвчвшвщвъвьвюгагбгвгггдгегзгигкгмгнгсгтгчгшгядбдгдддждздкдлдмдндпдсдтдхдцдчдшдъдьдэдюдяеаеиеуефеэжажбжвжгжджежжжкжлжмжнжожпжржсжужцжчжьжюзезжзззкзсзтзузцзчзшзьзэзюиуифиэквкдкжкзкккмкскткцкчкшлблвлглдлжлзлклллмлнлплслтлулфлхлчлшлщлымбмвмгмкмлмммнмпмрмсмтмумфмхмцмчмшмщмьмэмюнбнвнгнднжнзнкнлнннрнснтнфнхнцнчншнщньнэоаофоэпкпмпнпппсптпфпцпчпшпыпьпярбрвргрдржрзркрлрмрнрпрррсртрфрхрцрчршрщрьрюсбсвсгсдсжсзсиснсрсссфсцсчсшсщсъсьсэтбтвтгтдтзтктлтмтнтптстттутфтхтцтчтштщтътьтэтюубувужуиуоупуууфуцуэхахвхгхдхехихкхлхмхнхпхрхсхтхухшхэцвцицкцмчвчкчлчмчнчрчтчшчьшвшкшлшмшншпшршсштшцшчшьшющащещнщощрщущьъюъяыщьбьвьгьдьжьзькьмьньоьпьфьцьчьшьщюаюбювюгюдюеюжюзюйюкюлюмюнюпюрюсютюхюцючюшющююябявягядяеяияйяпяряцячяшяюяя',
        3  => 'ааакаоафашаэбббвбгбдбжбзбибкблбмбнбобрбсбтбубхбцбчбшбщбъбыбьбюбявбвввгвдвжвзвквлвмвнвпврвсвтвувхвцвчвшвщвъвывьвювягагбгвгггдгзгкглгмгнгогргсгтгугчгшгядбдвдгдддждздидкдлдмдндпдрдсдтдудхдцдчдшдъдыдьдэдюдяеаевегежезепеуехецечешещеэзбзвзгздзезжзззизкзлзмзнзозрзсзтзузцзчзшзъзызьзэзюзяижиуифицищиэквкдкекжкзккклкмкнкркскткцкчкшлблвлглдлжлзлклллмлнлплслтлфлхлчлшлщльмбмвмгмимкмлмммнмпмрмсмтмфмхмцмчмшмщмьмэмюмянбнвнгнднжнзнкнлнннрнснтнфнхнцнчншнщныньнэоюрбрвргрдржрзркрлрмрпрррсртрфрхрцрчршрщрьсасбсвсгсдсесжсзсислсмснспсрсссусфсхсцсчсшсщсъсысьсэсютатбтвтгтдтзтктлтмтнтптртстттфтхтцтчтштщтътытьтэувугузуиуйукуоупуууфуцуэуячвчкчлчмчнчочрчтчшчьшашвшкшлшмшншошпшршсшушцшчшьшюябявягяеяжязяияйякяляняпярясятяхяцячяшящяюяя',
        4  => 'ааазауащаэбббвбгбдбжбзбкблбмбнбсбтбубхбцбчбшбщбъбыбьбюбявбвввгвдвжвмвнвпврвсвтвхвцвчвшвщвъвьвювягбгвгггдгегзгкгмгнгсгтгугчгшгядбдгдддждздкдлдмдндпдрдсдтдхдцдчдшдъдыдьдэдюдяеуехещеэжбжвжгжджжжкжлжмжнжпжржсжцжчжьжюзбзвзгздзжзззкзлзмзрзсзтзцзчзшзъзызьзюзяигихиэквкдкжкзкккмкнкскткцкчкшлблвлглдлжлзлклллмлнлплслтлфлхлчлшлщльлюмбмвмгмкмлмммпмрмсмтмфмхмцмчмшмщмьмэмюмянбнвнгнднжнзнлнннрнснфнхнцнчншнщньнэоаофоэоюояпепкпмпнпппсптпфпцпчпшпыпьпярбрвргрдржрзркрлрмрнрпррртрфрхрцрчршрщрьсбсгсдсжсзснсрсссфсхсцсшсщсъсысьсэсюсятбтгтдтзтктлтмтнтптстттутфтхтцтчтштщтътытьтэтюудузуфхгхдхихкхлхмхнхпхрхсхтхухшхэцвцицкцмчвчкчлчмчнчочрчтчшчьшвшкшлшмшншошпшршсшцшчшьшюызыиыуыцыяьвьгьдьжьзьньоьпьсьфьцьчьшьщэгэдэзэйэлэмэпэсэтэфэхэяюаюбювюгюдюеюлюмюсютюхюцючющююябявяеяжязякяляпяряцяшяюяя',
        5  => 'аааеажазаиайаоапарасауафахацачашащаэаюаябббвбгбдбжбзбхбцбчбшбщбъвбвввгвжвпвхвщвъвюгбгвгггзгтгшдгдхдцдюеаебегедееежеиеленеоеуефещеэеюеяжбжвжжжмжчжюзсзцзшзъзэиаибидиеижииийиоириуифицичишищиэиюияйвйойхкжкзкккмкчлдлжлзлплфлхлшлщмвмрмфмхмшмэмюнжнлнфнэоаоеоиойоуочошоюояпмпппфрщсгсжсзсщсъсэсютгтдтзтптфтцтщтътэтюуауиуйуоуууфуцушущуэфмфнфсфчфыхгхкхрхтхшцвцмцучвчмчрчшшвшмшпшршцшчшющнщоэвэгэдэзэйэлэмэнэпэрэсэхэяюаюбювюгюдюеюжюзюйюкюлюмюнюпюрюхюцючюшююябягяеяжязяияйякяпяцячяшящяюяя',
        6  => 'ааабазаиаоапауафацашаэбббвбгбдбжбзбкблбмбнбрбсбтбхбцбчбшбщбъбьбюбявбвввгвдвевжвзвивквлвмвнвовпврвсвтвувхвцвчвшвщвъвывьвювягбгвгггдгегзгкгмгнгогргсгтгчгшгядбдвдгдддждздкдмдндпдрдсдтдхдцдчдшдъдэеаежеиеоеуехеэеюеяжбжвжгжджжжкжлжмжнжожпжржсжужцжчжьжюиаибиуифиэквкдкжкзккклкмкнкркскткцкчкшлблвлглдлелжлзлклллмлнлолплслтлулфлхлчлшлщлыльлюлямамбмвмгмкмлмммнмпмрмсмтмфмхмцмчмшмщмымьмэмюмянбнвнгнднжнзнкнлнннрнснтнфнхнцнчншнщньнэоаобоводожоиолооопотоуофохоцощоэоюояпапепипкпмпнпопппрпсптпупфпцпчпшпыпьпярбрвргрдржрзркрлрмрнрпрррсртрфрхрцрчршрщрырьрюрясасбсвсгсдсесжсзсислсмснсоспсрссстсусфсхсцсчсшсщсъсысьсэсюсяубувугудузумуоупуууфуцушуэцвцецицкцмцочачвчечкчлчмчнчочрчтчучшчььбьвьгьдьжьзькьмьньоьпьфьцьчьшьщюаюбювюгюдюеюжюзюйюкюмюнюпюсютюхюцючюшющюю',
        7  => 'аэбббвбгбдбжбзбкбмбнбсбтбхбцбчбшбщбъбьбюбявбвввгвдвжвзвмвпвсвтвхвцвчвшвщвъвьвюгбгвгггдгзгкгмгсгтгчгшгядбдгдддждздлдмдпдсдтдхдцдчдшдъдэдюеаегедежезеоепесеуехечещеэжбжвжгжджжжкжлжмжнжожпжржсжцжчжьжюзбзгздзезжзззизкзлзмзозсзтзузцзчзшзъзызьзэзюзяибизипифихищиэквкдкжкзккклкмкнкркскткцкчкшлблвлглдлжлзлклллмлнлплслтлфлхлчлшлщльмбмвмгмкмлмммрмсмтмфмхмцмчмшмщмьмэмюнбнвнгнднжнзнкнлнннрнснтнфнхнцнчншнщнэоаожоиоуофоцоэоюоярбрвргрдржрзркрлрмрнрпрррсртрфрхрцрчршрщрьрюсасбсвсгсдсесжсзсислсмснсоспсрссстсусфсхсцсчсшсщсъсысьсэсюсятатбтвтгтдтетзтктлтмтнтптртстттутфтхтцтчтштщтътытэтютяувугужуоуууфуцушуэцвцецицкцмчачвчкчлчмчнчочрчтчшчьшашвшешкшлшмшншошпшршсштшушцшчшьшюыдыжыиылыпытыуышыяьвьгьдьжьзьиькьньоьпьсьфьцьчьшьщэвэгэдэзэйэкэлэмэпэрэсэтэфэхэяюаюбювюгюдюеюжюзюйюкюлюнюпюрюсютюхюцючюшющююягядяжязякяпярясяхяцяшяюяя',
        8  => 'аааеажаиайаоауахачащаэаюбвбгбжбзбмбтбхбцбчбщбъбявбвввгвдвжвзвмвпвтвщвъвюгбгвгггкгсгчгядбдгдддлдпдхдчдшдъдьдэеаебееежеиекеоепеуефечешещеэеюеяжбжвжгжжжлжмжпжржцжчжьжюзззтзцзьзэзюиаибивигидиеижизииийикилиниоипитиуифихицичишищиэиюияйвйгйдйейзйкйлймйойрйфйхйчйшкдкжкзлблглжлзлмлнлплтлфлхлчлшлщмрмтмхмшмщмьмэмюнлнрншнщнэоеожоиойоооуофоцочошощоюояпмпфпцпчпьргрзрфрхрцрщрьсбсгсжсзсрсфсщсъсэтбтгтдтзтптфтхтштщтътюуаубувуеужузуиуйуоуруууфухуцушущуэуюуяфлфнфчхгхдхкхмхтхшхэцвцмчвчлчмчрчшшвшпшршсштшцшчшющощьэвэгэдэзэйэкэлэмэнэпэрэсэфэхэяюаюбювюгюдюеюжюзюйюкюмюрюсюхюцючюшююябягядяияйякярясяцячяшящ',
        9  => 'вбвввгвдвевжвзвивквлвмвнвпврвсвтвувхвцвчвшвщвъвывьвювягбгвгггдгегзгигкглгмгнгогргсгтгугчгшгядбдвдгдддздкдлдмдндпдрдсдхдцдчдшдъдыдьдэдюеаебевегедееежезеиейекемеоепесетеуефехецечешещеэеюеязбзвзгздзжзззизкзлзмзнзозрзсзтзузцзчзшзъзызьзэзюзяквкдкжкзкккмкнкркткцкчкшлблвлглдлжлклллмлнлплслтлфлхлчлшлщльлюлямбмвмгмкмммнмпмрмтмфмхмцмчмшмщмьмэмюмянбнвнгнднжнзнкнлнннрнснтнфнхнцнчнщньнэоаобоеожоиойоломооопосоуофохоцочошощоэоюояпапипкплпмпнпопппрпсптпупцпчпшпыпьпярарбрвргрдрержрзриркрлрмрнрпрррсртрурфрхрцрчршрщрырьрюрясвсгсдсесжсзслснспсрсссфсхсцсчсшсщсъсьсэсютбтвтгтдтзтктлтмтнтптттфтхтцтчтштщтътытьтэтютяфлфмфнфрфсфтфффчфыхвхгхдхехихкхлхмхнхохпхрхсхтхухшхэцвцицкцмчвчечкчлчмчнчочрчтчучшчьшвшкшлшмшншошпшршсштшцшчшьшю',
        10 => 'ааащаэвбвввгвдвжвзвквлвмвнвпврвсвтвхвцвчвшвщвъвьвювядадбдвдгдддедждздидкдлдмдндпдрдсдтдудхдцдчдшдъдыдьдэдюдяебегежеиеоеуефехецечешещеэеюеяжажбжвжгжджжжижкжлжмжнжожпжржсжужцжчжьжюзбзвзгздзжзззкзлзмзнзрзсзтзузцзчзшзъзызьзэзюзяигижицищиэиюквкдкжкзккклкмкнкркскткцкчкшлблвлглдлжлзлклллмлнлплслтлфлхлчлшлщмбмвмгмимкмлмммнмомпмрмсмтмумфмхмцмчмшмщмымьмэмюмянбнвнгнднжнзнкнлнннрнснтнфнхнцнчншнщньнэнюоаоцрбрвргрдржрзркрлрмрнрпрррсртрфрхрцрчрщрьсбсдсжсзслсмсрсссхсчсщсъсьсэсютбтвтгтдтзтктлтмтптстттфтхтцтчтштщтътьтэтюуаугужуйуоуфуцуэцацвцкцмцоцуцычачвчичкчлчмчнчочрчтчучшчьшвшкшлшмшншошпшршсштшцшчшьшю',
        11 => 'аааоафаэбббвбгбдбжбзбкблбмбсбтбхбчбшбщбъбьбюбявбвввгвдвжвзвмвпврвтвувхвцвчвшвщвъвывьвювягбгвгггдгзгкглгмгнгсгтгчгшгядбдвдгдддждздкдлдмдпдрдтдхдцдчдшдъдьдэдяеажбжвжгжджжжкжлжмжожпжржсжужцжчжьжюзбзвзгздзжзззизмзозрзсзцзчзшзъзызьзэзюзяиуиэкдкжкзкккмкскткцкчкшлблвлглдлжлзлклллмлнлплтлфлхлчлшлщмбмвмгмкмлмммнмпмрмтмумфмхмцмчмшмщмьмэмюмянбнвнгнднжнзнкнлнннрнснтнфнхнчншнщньнэнюоэоюпкплпмпнпппрпсптпфпцпчпшпьсбсвсгсдсжсзслсмснспсрсссфсхсцсчсшсщсъсысьсэсютбтвтгтдтзтитктлтмтнтптртстттфтхтцтчтштщтътьтэтютяущфифлфмфнфофрфсфтфуфффчфыхахгхдхехкхлхмхнхпхрхсхтхухшхэчвчлчмчрчтчшшашвшишкшлшмшншошпшршсштшушцшчшьшющнщощрщущьыгыдызыиырыуыцыяюаюгюеюйябяи',
        12 => 'ааабаоапауащбббвбгбдбжбзбкбмбнбсбтбхбцбчбшбщбъбьбюбявбвввгвевжвзвивквлвмвнвпврвсвтвувхвцвчвшвщвъвывьвювягагбгвгггдгегзгигкгмгогргсгтгугчгшгяеаепеуеэиаибижищиэквкдкжкзккклкмкркскткцкчкшлблвлглдлжлзлклллмлолплслтлулфлхлчлшлщлыльмбмвмгмкмлмпмрмсмтмфмхмцмчмшмщмьмэмюмянбнвнгнднжнзнкнлнннрнснтнфнхнцнчншнщньнэоапкпмпнпппспфпцпчпярбрвргрдржрзркрлрмрнрорпрррсртрфрхрцрчршрщрырьрюрясбсвсгсдсжсзслсмснспсрсссфсхсцсчсшсщсъсысэсютатбтвтгтдтзтктлтмтнтотптртстттутфтхтцтчтштщтътытьтэтютяубувуеуиуйуоуууцуэуяфлфмфнфрфсфтфффчфыхахвхгхдхехихкхлхмхнхпхрхсхтхухшхэцвцицкцмцочвчкчлчмчнчочрчтчшчьшашвшкшлшмшншошпшршсштшушцшчшьшющащнщощрщущьыбыдыжызыиыпырыуыцыщыяьбьвьгьжьзькьмьньоьпьфьцьчьшьщэвэгэдэзэйэкэлэмэпэсэтэфэхэяюаюбювюгюдюеюжюзюйюкюмюпюрюсютюхюцючюшющююябядяеяжязяйяпяряхяцяюяя',
        13 => 'ааафаэбббвбгбдбжбзбибкблбмбнбобрбсбтбхбцбчбшбщбъбыбьбюбявбвввгвдвжвзвквлвмвнвпврвсвтвхвцвчвшвщвъвывьвювягбгггзгкгмгнгчгшгядбдвдгдддздкдпдчдъдьдэдяещжбжвжгжджжжкжлжмжнжожпжржсжцжчжьжюзбзвзгздзжзззкзлзмзнзрзсзтзцзчзшзъзьзэзюзяиуиэкдкжкзкккмкскчкшлалблвлглдлелжлзлклллмлнлолплслтлулфлхлчлшлщлыльлюлянбнвнгнднжнзнкнлнннрнснфнхнцнчншнщньнэощрбрвргрдржрзркрлрмрнрпрррсртрурфрхрцрчршрщрырьрюрясгсдсжсзсрсхсчсшсщсъсьсэсюсятбтвтдтзтмтптттфтхтцтчтштщтътьтэубугузуиуоупуууфухуцуэфмфнфсфтфффчфыхахвхгхдхихкхлхмхнхпхсхтхухшхэцвцкцмчвчкчлчмчнчрчтчшчьшвшешкшлшмшншошпшршсшушцшчшьшющащещнщощрщущьыбыгыдыжызыиыкысыуыцыщыяьбьвьдьзьмьньпьфьцьчьщэвэгэдэзэйэкэлэмэнэрэсэтэфэхэяюбювюгюеюжюзюйюкюлюмюнюрюцябядяпяряц',
        14 => 'ааабавагадаеажаиайаламаоауафахацачашащаэаюаябцбювдвжвхвъвюгвгггзгкгсгчгшгядэдюеаебеееиереуефещеэеяжбжвжлжпжржсжчжюзсзтзцзчзшзъзэзюиаибиеижииийиоипиуифиэиюияйвйгйейзйпйрйфйхйшкдкжкмкцкчлфмвмгмхмшмщмэмюнхншнэоаовогоеожозоиойоломоуофошоэоюояпмпфпшсгсзсщсэтэтюуауеужуиуйуоуруууфуцушущуюуяфмфнфтфчхгхдхкхпхсхэцуцычмчрчшшршсшцшчщоэдэйэпэсэхэяюаюбювюгюжюйюкюлюмюнюпюрюхюцючюшягяеяияпяцячяшяюяя',
        15 => 'аааоаэеаебеиемеуефеэеяибиуифиэквкдкжкзккклкмкнкркскткцкчкшлблвлглдлжлзлклллмлнлплслтлфлхлчлшлщмамбмвмгмемкмлмммнмомпмрмсмтмумфмхмцмчмшмщмымьмэмюмянбнвнгнднжнзнкнлнннрнснтнфнхнцнчншнщньнэнюоапмпппрпсптпфпцпчпшпьпярбрвргрдржрзркрлрмрнрпрррсртрфрхрцрчршрщрьрюсбсвсгсдсжсзслсмснспсрсссфсхсцсчсшсщсъсьсэсюсятбтвтгтдтзтктлтмтнтптртстттфтхтцтчтштщтътьтэтютяувуеужуиуйумуоуууфуцуэуяфефлфмфнфофсфтфуфффчфыцацвцецкцмцочвчкчлчмчнчочрчтчучшчьшашвшкшлшмшншошпшршсштшцшчшьшюыбыдызыиыкыныпысыуыцычыяьбьвьгьдьжьзьиькьмьньоьпьсьтьфьцьчьшьщябявягядяеяжязяияйякямяняпяряцячяшяюяя',
        16 => 'аэбббвбгбдбжбзбкбмбтбхбцбчбшбщбъбыбьбявбвввгвдвжвзвквлвмвпврвсвтвхвцвчвшвщвъгбгвгггдгзгкгмгтгчгядбдвдгдддздкдлдпдрдтдхдчдшдъдьдэеэжбжвжгжжжлжмжожпжржсжчжьжюзбзвзгздзжзззмзрзсзтзузцзчзшзъзызьзэзюзяиэкдкжкзкккмкркцкчкшлблвлглдлжлклллмлнлплслтлфлхлчлшлщлюмбмвмгмкмммнмпмрмсмтмфмхмцмшмщмьмэмюнвнгнднжнзнкнлнннрнтнфнхнцнчншнщньнэпкпмпппсптпфпцпчпшпьрбрвргрдржрзркрлрмрнрпрррсртрфрхрцрчршрщрырьрюрясбсвсгсдсжсзслсмснсрсссфсхсцсчсшсщсъсьсэсютбтгтдтптттхтцтчтштщтътэтюувуоуууцуэфлфмфрфсфтфуфффчфыхвхгхлхрхтхшцвцкцмчвчкчмчнчочрчтчшчьшвшлшмшпшсштшцшчшьшющащнщощрщущьыиырыуыяьвьдьжьзьньоьпьфьцьчьшьщюаюбювюдюеюжюйюпюрюцююяияряц',
        17 => 'ааазаиаоащбббвбгбдбжбзбкбмбнбсбтбубхбцбчбшбщбъбьбявбвввгвдвжвзвквлвмвнвпвсвтвхвцвчвшвщвъвьвюгагбгвгггдгзгкгмгсгтгчгшгядбдгдддждздкдлдмдндпдсдтдхдцдчдшдъдыдьдэдюдяежефеэжбжвжгжджжжкжлжмжнжожпжржсжужцжчжьжюзбзвзгздзезжзззизкзлзмзнзозрзсзтзузцзчзшзъзызьзэзюзяиуищиэкдкжкзкккмкткцкчкшлблвлглдлжлзлклллмлнлплслтлфлхлчлшлщмбмвмгмкмлмммнмпмтмфмхмцмчмшмщмьмэмюнбнвнднжнзнкнлнннрнснтнфнхнцнчншнщньнэоаоэояпкпмпнпппсптпфпцпчпшпьрбрвргрдржрзркрлрмрнрпрррсртрфрхрцрчршрщрьрюрясбсгсдсжсзсссфсхсцсшсщсъсьсэсютдтзтптттфтхтштщтъуууцфлфмфнфрфсфтфуфффчфыхгхдхкхмхнхпхсхтхшхэцкцмцуцычвчкчмчнчочрчтчшчьшвшкшлшмшншпшршсшцшчшьшющащищнщощрщущьъюыбыдыжызыиыкыуыцычышыяьвьгьдьжьзьиьньоьпьсьфьцьчьшьщьяэвэгэдэзэйэлэпэсэтэфэхэяюаюбювюгюеюзюйюмюнюпютюхюцючющююябявяияйяляпяряшяюяя',
        18 => 'аааоаэбббвбгбдбебжбзбкбмбнбсбтбхбцбчбшбщбъбьбюбявбвввгвдвжвзвквмвнвпвсвтвхвцвчвшвщвъвьвюгбгвгггдгзгигкглгмгнгсгтгугчгшгядбдвдгдддждздкдлдмдндпдрдсдтдудхдцдчдшдъдьдэдюдяеуеэзбзгздзезжзззизкзлзмзнзрзсзтзузцзчзшзъзьзэзюзяквкдкжкзкккмкткцкчкшлблвлглдлжлклллмлнлплслтлфлхлчлшлщмбмвмгмкмлмммнмпмрмсмтмфмхмцмчмшмьмэмюмянбнвнгнднжнзнкнлнннрнснтнфнхнцнчншнщньнэпкпмпнпппсптпфпцпчпшпыпьпярбрвргрдржрзркрлрмрнрпрсртрфрхрцрчршрщсбсгсдсжсзспсссфсхсцсшсщсъсьсэтбтвтгтдтзтктлтмтптстттфтхтцтчтштщтътытьтэтюувууфафлфмфнфрфсфтфффчфыхгхдхехкхмхнхпхрхсхтхшхэцвцицкцмчвчкчлчмчнчочрчтчшчьшкшмшншошпшршсшцшчшьшющнщощрщущьъюъяыбыжызыиытыуыцыяьвьгьжьзьньоьпьцьчьшьщэвэгэдэзэйэлэмэпэрэсэтэфэхэяюбювюгюдюеюжюзюйюнюпюсютюхюцючющююядяеяиялярячяю',
        19 => 'ааабавагаеажаиакамаоапауафахачашащаэаюаябвбгбдбзбмбтбхбшбщбювбвввгвдвжвзвмвпврвтвувхвцвчвщвъвюгбгвгдгзгкгмгсгтгчгшгядбдгдпдхдцдчдъдэеаебегееежеиекеленеоепеуефецечещеэеюеяжвжгжмжпжржцжюзбзгзжзззтзцзшзъзэзюзяиаибивигиеижииийимиоипиуифихичишищиэиюияйгйейзйлйойпйрйфйхйцйшкдкжкзкчкшлблглжлзлмлнлплтлхлчлшлщмвмгмпмтмхмшмщмэмюнбнвнжнзнлнрнфнхнчнщнэнюняоаовогоеожоиойоломонооопоуофохоцочошощоэоюояпмпсптпфпшрзрфрхрщсбсгсдсжсзсфсхсцсчсшсщсъсэсютгтдтзтмтптфтхтцтштщтътэуаубувугудуеужузуиуйукунуоупурусутуууфухуцучушущуэуюуяфафмфнфофрфсфтфчфыхдхрхэцацвцмцоцуцычвшвшмшпшршсштшцшющрщьэвэгэдэзэйэкэмэпэфэхэяюаюбювюгюдюеюжюзюйюкюлюмюнюпюрюхюцючюшююябягядяеяжяияйякяняпярятяхяцячяшящяюяя',
        20 => 'ааадаеажаиаоапафацачащаэаюаяеаебегежезепеуефецечещеэеюивижиуифихищиэлблвлглдлжлзлклллмлнлслтлфлхлчлшлщлыльмбмвмгмимкмлмммнмпмрмсмтмфмхмцмчмшмщмьмэмюмянанбнвнгндненжнзнинкнлнннонрнснтнунфнхнцнчншнщньнэнюняоаоеожозоиооопоуофохоцочошощоэоюоярбрврдржрзркрлрмрнрпрррсртрфрхрцрчршрщрьрюрясасбсвсгсдсжсзсислсмснспсрсссусфсхсцсчсшсщсъсысьсэсюсятбтвтгтдтзтктлтмтнтптртстттфтхтцтчтштщтътэтюуаубувудуеужуиуйукулумуоупусуууфухучушущуэуяфлфмфнфрфсфтфффчфычачвчечкчлчмчнчочрчтчучшчьыбыгыдыеыжызыиыйыкылымыныпысытыуыхыцычышыщыя',
        21 => 'ааагазафацачашащвбвввгвдвевжвзвивквлвмвнвпврвсвтвувхвцвчвшвщвъвывьвювягбгвгггдгегзгигкглгмгнгргсгтгугчгшгядадбдвдгдддедждздидкдлдмдпдрдсдтдудхдцдчдшдъдыдьдэдюдяеаебегедееежезеиекелепесетеуефехецечещеэеюеяипиуифицишиэкаквкдкекжкзкиккклкмкнкскткукцкчкшлблвлглдлжлзлклллмлнлплслтлфлхлчлшлщльмбмвмгмкмлмнмпмрмсмтмфмхмцмчмшмщмьмэмюмянбнвнгнднжнзнкнлнннрнснтнфнхнцнчншнщныньнэоиоооцошоэоюояпепипкплпмпнпппсптпупфпцпчпшпыпьпярбрвргрдржрзркрлрмрнрпрррсртрфрхрцрчршрщрырясасбсвсгсдсжсзслсмснспсрсссусфсхсцсчсшсщсъсысьсэсютбтвтгтдтзтктлтмтнтптртстттфтхтцтчтштщтътьтэтюубувугузуиуйумунуоусуууфухуцучущуэуяшашвшкшлшмшншпшршсштшушцшчшьшюэвэгэдэзэйэнэрэсэфэхэя',
        22 => 'ааабавагадажазаиайакаоасауафацачашащаэвавбвввгвдвжвзвквлвмвнвовпврвсвтвувхвцвчвшвщвъвывьвювяеаебежеиефецечешещеэеяибигижиуихицичишищиэквкдкжкзккклкмкнкркскткцкчкшмбмвмгмемимкмлмммнмомпмрмсмтмумфмхмцмчмшмщмымьмэмюмяоаободоеожозоиолонооопосотоуофохоцочошощоэоюояуаубувугудуеуиукулумуоупурусутуууфухуцучушущуэуяыбывыдыжызыиыкылынырысытыуыхыцычышыщыя',
        23 => 'ааабазаоапауафацачаэвбвввгвдвжвзвивквлвмвнвпврвсвтвхвцвчвшвщвъвьвювяеаежеоеуефецещеюибидижизиоипиуифиэквкдкжкзккклкмкнкркскткцкчкшлблвлглдлжлзлклллмлнлплслтлулфлхлчлшлщлыльлюлямбмвмгмемимкмлмммнмпмрмсмтмумфмхмцмчмшмщмьмэмюмянбнвнгнднжнзнкнлнннрнснтнфнхнцнчншнщньнэнюоаобовогодоеожозоиолоооросотоуофохоцошощоэоюоярарбрвргрдржрзриркрлрмрнрорпрррсртрурфрхрцрчршрщрырьрюрятбтвтгтдтзтктлтмтнтптртстттфтхтцтчтштщтътьтэтюуауеузуиуйукуоуууфуцучуэшвшкшлшмшншошпшршсштшцшчшьшюьбьвьгьдьжьзькьмьньоьпьфьцьчьшьщ',
        24 => 'ааазаиаоауацаэвбвввгвдвжвзвквлвмвнвовпврвсвтвувхвцвчвшвщвъвьвювяеаежеоецещеэиаигидижизиииоиуицичиэкдкжкзккклкмкнкркскткцкчкшлблвлглдлжлзлклллмлнлплслтлулфлхлчлшлщльмбмвмгмемкмлмммнмомпмрмсмтмумфмхмцмчмшмщмьмэмюнбнвнгнднжнзнкнлнннрнснфнхнцнчншнщньнэоаодожозоиооохоцочошощоэоюояпепкплпмпнпппсптпупфпцпчпшпыпьпярарбрвргрдрержрзркрлрмрнрорпрррсртрфрхрцрчршрщрырьрюрясасбсвсгсдсесжсзсислсмснсоспсрссстсусфсхсцсчсшсщсъсысьсэсюсятбтвтгтдтзтктлтмтнтптстттфтхтцтчтштщтътьтэтютяувугудужузуиукунуоупуууфухуцуэуяцацвцицкцмцоцучвчечичкчлчмчнчочрчтчучшчььбьвьгьдьеьжьзькьмьньоьпьфьцьчьшьщюаюбювюгюдюеюжюзюйюкюлюмюнюпюсюхюцючюшющюю',
        25 => 'ааабажазаиаоапарауафацачашащаэеаезеуехецещеэеюеяиаибигидижизиииоирисиуифиэиюиянбнвнгнднжнзнкнлнннрнснтнфнхнцнчншнщньнэнюняоаоводоеожозоиойоколонооопоросотоуофохоцочошощоэоюоярарбрвргрдржрзркрлрмрнрорпрррсртрурфрхрцрчршрщрырьрюуаубувугудуеужузуиуйукулумуоуууфухуцучушуэуяьбьвьгьдьеьжьзьиькьмьньоьпьсьтьфьцьчьшьщья',
        26 => 'еаебевегеееиейенеоепетеуефецечещеэеюеяюаюбювюгюдюеюжюзюйюкюлюмюпюрюсюхюцючюшющююябягядяеяжяияйякямяпяхяцячяшящяюяя',
        27 => 'бббвбгбдбжбзбмбсбтбхбцбчбшбщбъбюбявбвввгвдвжвзвлвмвпврвсвтвхвцвщвъвьвювягбгвгггдгегзгкгмгсгтгугчгшдбдгдддждздкдмдпдсдтдхдцдчдшдъдыдьдэдюдяеаебевегееежеиейекеленеоепересетеуефецечешещеэеюеяжбжвжлжмжпжржсжужцжчжьжюзбздзжзззизкзмзрзсзцзчзшзъзьзэзюзяиаибивидиеижизииийикилиминиоипиритиуифихицичишищиэиюияйвйгйейзйкйлймйнйойрйсйфйхйцйчйшкдкжкзкккмкскткцкчлблвлглжлзлллмлнлплтлфлхлчлшлщлюмвмгмлмммнмрмтмфмхмцмшмщмьмэмюнбнвнгнднжнзнлнрнтнфнхнцншнщнэпмпппсптпфпцпшпырбргрдржрзрлрмрпрррсртрфрхрцрчрщсбсгсдсжсзснсрсссфсцсщсъсэсютбтгтдтзтмтптттфтхтцтчтштщтътэтюуаубувугуеузуиуйукулумунуоупурусутуууфухуцушущуэуюуяхгхдхкхмхпхрхсхухшхэцицкцмцоцуцычвчмчрчтчшчьшмшпшршсштшчшющащнщощрщьябягядяеяжязяияйякялямяняпярятяхяцячяшящяюяя',
        28 => 'бббвбгбдбжбзбкблбмбнбрбсбтбхбцбчбшбщбъбьбюбявбвввгвдвжвзвквлвмвнвпврвсвтвувхвцвчвшвщвъвьвювягбгвгггдгзгкглгмгнгргсгтгчгшгядбдвдгдддждздидкдлдмдндпдрдтдудхдцдчдъдыдьдэдюдяеаебееежеиеленеоеуехецечещеэеюеяжбжвжгжджжжижкжлжмжнжожпжржсжужцжчжьжюзбзгздзжзззлзмзрзсзтзцзчзшзъзэзюиаибивигидиеижизииийикилиниоиписитиуифичищиэиюияквкдкжкзкккмкркткцкчкшмбмвмгмкмлмммнмпмрмсмтмфмхмцмчмшмщмьмэмюнбнвнгнднжнзнкнлнннрнснтнфнхнцнчншнщньнэоаобовогодоеожозоиойоколомооопосоуофохоцочошощоэоюояпкплпмпнпопппрпспупфпцпчпшпыпьпясвсгсдсесжсзслсмснспсрсссфсхсцсчсшсщсъсьсэсютбтвтгтдтзтктлтмтптстттфтхтцтчтштщтътьтэтютяфефлфмфнфсфтфффчцвцкцмчвчкчлчмчнчрчтчшчьшвшкшлшмшншпшршсштшцшчшьшющнщощрщьюаюбювюгюдюеюжюйюлюмюнюпюрюхюцючюшююябядяеяжязяияйяляпясятяцячяшящяя',
        29 => 'вбвввгвдвжвзвквлвнвпвсвтвувхвцвчвшвщвъвывьвювягагбгвгггдгзгигкглгмгнгргсгтгугчгшгядбдждздкдлдндодпдрдсдтдудхдцдчдшдъдыдьдэдюдязазбзвзгздзжзззкзлзмзнзрзсзтзузцзчзшзъзызьзэзюзяйвйгйдйейзйкймйойпйрйтйхйцйчйшкдкекжккклкмкнкткцкчкшлблвлглдлжлзлклнлолплслтлфлхлчлшлщлылюлямвмгмкмлмнмрмсмтмфмхмцмчмшмщмымьмэмюмянанбнжнзнкнлнрнунфнхнчншнщныньнэнюняпапепкплпмпнпрпсптпупфпцпчпшпыпьпярбргрдржркрмрпрррфрхрцрчршрщрьрюрясасбсвсгсдсесжсзсислсмснсоспсрсссусфсцсчсшсщсъсысьсэсюсятбтвтгтдтзтлтмтптстттфтхтцтчтштщтътьтэтютяфафлфмфнфофрфсфтфуфчфыхвхгхдхехихкхлхмхпхрхсхтхухшхэябявягядяеяжязяияйялямяняпярясятяхяцячяшящяюяя',
        30 => 'ааабавагадаеажазаиайакаламаоапасатауафахацачашащаэаюаябббгбдбжбзбмбрбсбтбхбцбчбшбщбъбьбювавбвввгвдвжвзвквлвмвнвпврвсвтвувхвцвчвшвщвъвьвювягбгвгггдгзгигкглгмгнгргсгтгугчгшгядбдвдгдддздкдлдмдпдрдтдхдчдшдъдыдэдюеаебевегедееежезеиейекеленеоепересеуефехецечешещеэеюеяжбжвжгжджжжкжлжмжожпжржсжужцжчжьжюзбзвздзззкзлзмзрзсзтзцзчзшзъзьзэзюзяйвйгйдйейзйкйлйнйойпйрйсйтйфйхйцйчйшкдкжкзккклкмкркткцкчкшлалблвлглдлжлзлклмлнлолплслтлулфлхлчлшлщлымбмвмгмлмммнмпмрмсмтмумфмхмцмчмшмщмьмэмюмянбнвнднжнзнлнннрнснтнфнчншнщнэнюпаплпмпнпопппрпсптпупфпцпчпшпыпьпярбрвргрдржрзрлрмрррсртрфрхрцрчршрщрюсбсвсгсдсжсзслсмснспсрсссусфсхсцсчсшсщсъсэтбтвтгтдтзтлтмтптртттфтхтцтчтштщтътьтэтютяхвхгхдхкхлхмхпхрхсхтхшхэцацвцецкцмцоцуцычвчлчмчрчтчшшвшешишлшмшошпшршсштшцшчшьшющнщощрщьюаюбювюгюдюеюжюзюйюкюлюмюнюпюрюсюхюцючюшющюю',
        31 => 'бббвбгбдбебжбзбкбмбнбсбтбубхбцбчбшбщбъбыбюбявбвввгвдвжвзвмвпврвтвхвцвчвщвъвюгбгвгггдгзгмгргтгшгядбдвдгдддждздлдмдпдсдтдхдцдшдъдэеаебегееежезеиейеленеоепересеуефехецечещеэеюеяжвжгжджжжлжмжожпжржсжцжчжьжюзбздзжзззмзрзсзтзцзшзъзэзюиаибивигидиеижииийикилимиоипириситиуифихишищиэиюияйвйгйдйейзйлймйнйойпйрйфйхйчйшквкдкжкзкккмкркткцкчлблвлглдлелжлзлллмлнлплфлхлчлшлщлямвмгмммнмрмтмфмхмчмшмэмюмянбнжнзнлнрнтнфнхншнэплпмпппрпсптпфпцпшпьпярбрвргржрзрпррртрфрхрцршрщрьрюрясбсвсгсдсжсзсмспсрсссфсхсцсчсшсщсъсэсютбтгтдтзтлтмтптртттфтхтцтчтштщтътэтютяхвхгхдхехкхмхохрхсхухшхэцвцмцоцучвчлчмчрчтчшшашвшмшошпшршсштшушцшчшьшющощрщьюаюбювюгюдюеюжюзюйюкюлюмюнюпюрюхюцючюшююябявягядяеяжязяияйякяляняпярятяхяцячяшящяюяя'
    );
    $res=0;
    for($i=0;$i<strlen($s)-1;$i++)
    {
        $c1=$s[$i];
        if($c1<'а'||$c1>'я') continue;
        $c2=$s[$i+1];
        if($c2<'а'||$c2>'я') continue;
        $i1=ord($c1)-ord('а');
        if(strpos($a[$i1],$c2)!==false)
        {
            $res++;
            continue;
        }
        if($i>=strlen($s)-2) continue;
        $c3=$s[$i+2];
        if($c3<'а'||$c3>'я') continue;
        $i2=ord($c2)-ord('а');
        if(strpos($a[$i2],$c3)!==false)
        {
            $res++;
            $i++;
            continue;
        }
        $l=0;
        $r=strlen($b[$i1])/2-1;
        while($l<=$r)
        {
            $c=$l+(($r-$l)>>1);
            $ca=$b[$i1][$c*2];
            $cb=$b[$i1][$c*2+1];
            if($ca==$c2&&$cb==$c3)
            {
                $res++;
                break;
            }
            if($ca<$c2||$ca==$c2&&$cb<$c3)
                $l=$c+1;
            else
                $r=$c-1;
        }
    }
    return $res;
}

function _charset_alt_win($s)
{
    for($i=0;$i<strlen($s);$i++)
    {
        $c=ord($s[$i]);
        if($c>=0x80&&$c<=0x9f)
            $s[$i]=chr($c-0x80+0xc0);
        else if($c>=0xa0&&$c<=0xaf)
            $s[$i]=chr($c-0xa0+0xe0);
        else if($c>=0xc0&&$c<=0xdf)
            $s[$i]=chr($c-0xc0+0x80);
        else if($c>=0xf0&&$c<=0xff)
            $s[$i]=chr($c-0xf0+0xa0);
    }
    return $s;
}

function _charset_koi_win($s)
{
    $kw = array(
        //00   01   02   03   04   05   06   07   08   09   0a    0b   0c    0d   0e   0f
        0x80, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138,  139, 140,  141, 142, 143, //0x80 - 0x8f
        144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 0xbb, 156, 0xab, 158, 159, //0x90 - 0x9f
        160, 161, 162, 184, 164, 165, 166, 167, 168, 169, 170,  171, 172,  173, 174, 175, //0xa0 - 0xaf
        176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186,  187, 188,  189, 190, 191, //0xb0 - 0xbf
        254, 224, 225, 246, 228, 229, 244, 227, 245, 232, 233,  234, 235,  236, 237, 238, //0xc0 - 0xcf
        239, 255, 240, 241, 242, 243, 230, 226, 252, 251, 231,  248, 253,  249, 247, 250, //0xd0 - 0xdf
        222, 192, 193, 214, 196, 197, 212, 195, 213, 200, 201,  202, 203,  204, 205, 206, //0xe0 - 0xef
        207, 223, 208, 209, 210, 211, 198, 194, 220, 219, 199,  216, 221,  217, 215, 218  //0xf0 - 0xff
    );
    for($i=0;$i<strlen($s);$i++)
    {
        $c=ord($s[$i]);
        if($c>=128)
            $s[$i]=chr($kw[$c-128]);
    }
    return $s;
}

function _charset_utf8_win($s)
{
    $r='';
    $state=1;
    for ($i=0;$i<strlen($s);$i++)
    {
        $c=ord($s[$i]);
        switch($state)
        {
            case 1: //not a special symbol
                if($c<=127)
                {
                    $r.=$s[$i];
                }
                else
                {
                    if(($c>>5)==6)
                    {
                        $c1=$c;
                        $state=2;
                    }
                    else
                        $r.=chr(128);
                }
                break;
            case 2: //an utf-8 encoded symbol has been meet
                $new_c2=($c1&3)*64+($c&63);
                $new_c1=($c1>>2)&5;
                $new_i=$new_c1*256+$new_c2;
                switch($new_i)
                {
                    case   1025: $out_c='Ё'; break;
                    case   1105: $out_c='ё'; break;
                    case 0x00ab: $out_c='«'; break;
                    case 0x00bb: $out_c='»'; break;
                    default: $out_c=chr($new_i-848);
                }
                $r.=$out_c;
                $state=1;
                break;
        }
    }
    return $r;
}

function _charset_prepare($s)
{
    $r=0;
    $k=0;
    for($i=0;$i<strlen($s)&&$r<255;$i++)
    {
        $c=ord($s[$i]);
        if($c>=0x80)
        {
            $r++;
            $k=$i;
        }
    }
    return substr($s,0,$k+1);
}

function charset_win_lowercase($s)
{
    for($i=0;$i<strlen($s);$i++)
    {
        $c=ord($s[$i]);
        if($c>=0xc0&&$c<=0xdf)
            $s[$i]=chr($c+32);
        else if($s[$i]>='A'&&$s[$i]<='Z')
            $s[$i]=chr($c+32);
    }
    return $s;
}

function charset_x_win($s)
{
// returns a string converted from a best encoding (windows-1251 or koi-8r) to windows-1251
    $sa=_charset_prepare($s);
    $s1=charset_win_lowercase($sa);
    $r1='windows-1251';

    $c1=_charset_count_chars($s1);
    $b1=_charset_count_bad($s1);
    $p1=_charset_count_pairs($s1);
    $w1=$p1*32+$b1*64-$c1;

    $s2=charset_win_lowercase(_charset_koi_win($sa));
    $w2=-$c1; //Особенность кодировки koi-8r: тот же диапазон символов, что и для windows-1251
    if($w2<$w1)
    {
        $b2=_charset_count_bad($s2);
        $w2+=64*$b2;
        if($w2<$w1)
        {
            $p2=_charset_count_pairs($s2);
            $w2+=32*$p2;
            if($w2<$w1)
            {
                $r1='koi-8r';
                $w1=$w2;
            }
        }
    }

    $s2=charset_win_lowercase(_charset_utf8_win($sa));

    $c2=_charset_count_chars($s2);
    $w2=-$c2;
    if($w2<$w1)
    {
        $b2=_charset_count_bad($s2);
        $w2+=64*$b2;
        if($w2<$w1)
        {
            $p2=_charset_count_pairs($s2);
            $w2+=32*$p2;
            if($w2<$w1)
            {
                $r1='utf';
                $w1=$w2;
            }
        }
    }

    switch($r1)
    {
        case 'alt':
            return _charset_alt_win($s);
        case 'koi-8r':
            return _charset_koi_win($s);
        case 'utf':
            return _charset_utf8_win($s);
        default:
            return $s;
    }

    return $s;
}

function create_menu2 ($rod,$ur,$offset)

{

    global $put,$m;

    $prebase=my_mysql_query( 'SELECT * FROM '.pref_db.'content WHERE rod="'.$rod.'"  AND ok="on"  ORDER BY `num` ASC ' ) ;

    $out="";

    $mm=mysqli_fetch_array($prebase);

    if ($mm)

    {

        $out.= "[m{$ur}{$ur}]";

        while ($mm)// затем начинаем перебирать каждый пункт и делать выборку по каждому (рекурсивно)

        {

            //вывод строки   str_repeat("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;",$ur).

            $out.= "[m{$ur}]<a class=\"m{$ur}\" href=\"".hrpref.trim($mm['ident']).hrsuf.lngsuf.'">'.$mm['name']."</a>[/m{$ur}]";

            //echo $out;

            if (isset($put[$ur]) )

            {

                if ($put[$ur+$offset]==$mm['ident'])

                {

                    //$out.= "[m{$ur}{$ur}]";

                    $out.=create_menu2 ($mm["ident"],$ur+1,$offset);

                    //$out.= "[/m{$ur}{$ur}]";

                }

            }

            $mm=mysqli_fetch_array($prebase);

        }

        $out.= "[/m{$ur}{$ur}]";

        return $out;

    }

}



function create_main_menu( $rod )

{

    global $m,$put;



    find_rod($m['ident']);

    //v($m['ident']);

    if ($put) $put=array_reverse ($put);

    $out=create_menu2($rod,0,1);

    //print_r ($put);

    return $out;

}





function create_cat ($rod,$ur,$offset)

{

    // 			  500   1

    global $put,$m;

    static $m_id;

    $prebase=my_mysql_query( 'SELECT * FROM '.pref_db.'content WHERE rod="'.$rod.'"  AND ok="1"  ORDER BY `num` ASC ' ) ;

    $out="";

    $mm=mysqli_fetch_array($prebase);

    if ($mm)

    {



        if ( ($ur==0) | ( $put[$ur]==$rod))

        {

            $disp=" ".$put[$ur]."=={$rod} ";

        }



        else

        {

            $disp=' style="display:none" '.$put[$ur]."=={$rod} ";

        }



        $out.= "<div $disp id=\"".($m_id+1)."\">[m{$ur}{$ur}]";

        while ($mm)// затем начинаем перебирать каждый пункт и делать выборку по каждому (рекурсивно)

        {

            $m_id++;

            //вывод строки   str_repeat("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;",$ur).







            if ($mm['nadcat']==1)

            {

                $href="javascript:chn(".($m_id+1).")";

            }

            else

            {

                $href="category--".trim($mm['ident']);

            }





            $out.= "[m{$ur}]<a  href=\"{$href}\" >".$mm['name']."</a>[/m{$ur}]";

            //echo $out;

            if (isset($put[$ur]) )

            {

                if ($put[$ur+$offset]==$mm['ident'])

                {

                    //$out.= "[m{$ur}{$ur}]";

                    //$out.=create_cat ($mm["ident"],$ur+1,$offset);

                    //$out.= "[/m{$ur}{$ur}]";

                }

            }

            $out.=create_cat ($mm["ident"],$ur+1,$offset);



            $mm=mysqli_fetch_array($prebase);

        }

        $out.= "[/m{$ur}{$ur}]</div>";

        return $out;

    }

}



function create_catalog( $rod )

{

    global $m,$put;





    find_rod($GLOBALS['longpage'][1]);







    if ($put) $put=array_reverse ($put);

    //v($put);

    $out=create_cat($rod,0,1);

    //print_r ($put);

    return $out;

}

function create_news($rod)

{
    $prebase=my_mysql_query( 'SELECT * FROM '.pref_db.'content WHERE rod="'.$rod.'"  AND ok="1"  ORDER BY `num` ASC ' ) ;
    ;
    $out="";
    while ($m=mysqli_fetch_array($prebase))
    {
        $out.="<p>".fgc($m['ident'])."</p>";
    }
    return $out;
}


function crim( $x, $y, $w, $h, $pref, $img, $qjpeg, $size_offset )

{


    $im = @imagecreatefrompng($img);
    @$thumb = imagecreatetruecolor($w, $h);// создаём пустую картинку
    imagesavealpha($thumb,true);
    imagealphablending($thumb, false);
    $transparent = imagecolorallocatealpha($thumb, 255, 255, 255, 127);
    imagefilledrectangle($thumb, 0, 0,  $w, $h, $transparent);


    //		$dst_x , $dst_y , 		$src_x ,  $src_y , 		 $dst_w , $dst_h , 		 $src_w ,  $src_h
    imagesavealpha($thumb,true);
    //@imagecopyresampled($thumb, $im , 0, 0, $x, $y, 	$w, $h,		$w, $h);



    @imagecopyresized($thumb, $im , 0, 0, $x, $y, 	$w, $h,		$w, $h);

    imgcor::gen($thumb, $size_offset );

    imageinterlace($thumb, true);

    @imagepng ( $thumb, img.'/tpl/'.$pref.'.png'); //записываем В файл
    imageinterlace($thumb, true);
    imagejpeg($thumb, img.'/tpl/'.$pref.'.jpg',$qjpeg);

}

function crim_virtual( $x, $y, $w, $h, $im)
{
    @$thumb = imagecreatetruecolor($w, $h);// создаём пустую картинку
    imagesavealpha($thumb,true);
    imagealphablending($thumb, false);
    $transparent = imagecolorallocatealpha($thumb, 255, 255, 255, 127);
    imagefilledrectangle($thumb, 0, 0,  $w, $h, $transparent);

    //		$dst_x , $dst_y , 		$src_x ,  $src_y , 		 $dst_w , $dst_h , 		 $src_w ,  $src_h
    imagesavealpha($thumb,true);
    //@imagecopyresampled($thumb, $im , 0, 0, $x, $y, 	$w, $h,		$w, $h);
    @imagecopyresized($thumb, $im , 0, 0, $x, $y, 	$w, $h,	$w, $h);
    imageinterlace($thumb, true);
    //@imagepng ( $thumb, img.'/tpl/'.$pref.'.png'); //записываем В файл
    //imageinterlace($thumb, true);
    //imagejpeg($thumb, img.'/tpl/'.$pref.'.jpg',$qjpeg);
    return $thumb;

}


function cat($ident)

{

    if ($GLOBALS['longpage'][1] =="") $GLOBALS['longpage'][1]='cat';



    if (intval($_POST['num_tov'])>=1) // vstavlaem tovar v korzinu

    {



        if ( !isset($_REQUEST[session_name()])) session_start();





        // viz_sid 	art 	price 	num 	datetime

        $quc='

						INSERT `'.pref_db.'cart` SET 

						`viz_sid` 	='.xs(session_id()).',

						`art` 	='.xs($_POST['art']).',

						`price` ='.xs($_POST['price']).',

						`num` ='.xs($_POST['num_tov']).',

						`datetime`=NOW()

						';





        $pr=my_mysql_query($quc);



    }

    //v($_POST);

    if ($_POST['del_x'])  //udaliaem iz korzini

    {

        $quc='DELETE FROM `'.pref_db.'cart` WHERE `art`='.xs($_POST['art']).'	AND `viz_sid`='.xs(session_id()).'		';

        //v($quc);

        $pr=my_mysql_query($quc);

    }



    $out='<div align="center" class="navigat" >'.create_navigation ($GLOBALS['longpage'][1]).'</div>';



    if ($GLOBALS['longpage'][2] ) // polniy pokaz tovara



    {



        $qu="SELECT * FROM `".pref_db."content` WHERE `ident`='".xss($GLOBALS['longpage'][2])."'  ";

        $pr1=my_mysql_query($qu);

        $mm1=mysqli_fetch_array($pr1);



        $out.= head_smartpl ('prod',

            '$art' ,	$mm1['art'] );



        $qu="SELECT * FROM `".pref_db."content` WHERE `rod`='".xss($GLOBALS['longpage'][2])."' ORDER BY `num` ASC";



        //v($qu);

        $pr2=my_mysql_query($qu);

        //$mm2=mysqli_fetch_array($pr2);



        //v($mm2);



        if  ( !($mm2=mysqli_fetch_array($pr2)) ) $mm2=$mm1;

        //v($mm2['art']);

        while ($mm2)

        {

            $im=trim($mm2['art']);



            //v($mm2['art']);



            if ( 1 )

            {



                $qu='SELECT `art` FROM `'.pref_db.'cart` WHERE `viz_sid`='.xs(session_id()).' AND `art`='.xs($mm2['art']).' ';



                //v($qu);

                if ( mysqli_fetch_array(my_mysql_query($qu) ) )

                {$cart=0;} else {$cart=1;}



                $out.=smartpl('prod',



                    '$art' 		,	$mm2['art'] ,

                    '$hr'		,	$_SERVER['REQUEST_URI'],

                    '$name'		,	$mm2['name'],

                    '$price'	,	$mm2['price'],

                    '$cart'		,	$cart,

                    '$mod'		, 	$mm2['desc'] );



            }

            $mm2=mysqli_fetch_array($pr2);



        }



        $out.= foot_smartpl ('prod');



    }



    elseif ( qqu("`rod`='".xss($GLOBALS['longpage'][1])."' AND `type`='category'",'') )

    {



        $tp="tov";



        $out.= head_smartpl ('catcat');



        $qu="SELECT * FROM `".pref_db."content` WHERE `rod`='".xss($ident)."' ORDER BY `num` ASC";

        $pr=my_mysql_query($qu);





        while ($mm=mysqli_fetch_array($pr))

        {

            $im=trim($mm['d2']);



            if ($mm['type']=='category' )

            {



                $out.=smartpl('catcat',

                    '$hr'		, 	"category--".trim($mm['ident']),

                    '$art' 		,	$mm['art'] ,

                    '$name'		,	$mm['name'],

                    '$title'	, 	$mm['d3'] );



            }

        }



        $out.= foot_smartpl ('catcat');



    }



    else



    {





        //v($ident);





        $qu="SELECT * FROM `".pref_db."content` WHERE `rod`='".xss($ident)."' ORDER BY `num` ASC";

        $pr=my_mysql_query($qu);



        $out.= head_smartpl ('prodprod');



        while ($mm=mysqli_fetch_array($pr))

        {



            $hr="category--".$GLOBALS['longpage'][1]."--".trim($mm['ident']);

            $n=$mm['name'];

            $im=trim($mm['art']);



            if ($mm['type']=='prod')

            {



                $out.=smartpl('prodprod',

                    '$hr'		, 	$hr	,

                    '$art' 		,	$mm['art'] ,

                    '$name'		,	$n 	);



            }





        }



        $out.= foot_smartpl ('prodprod');



    }







    return $out;







}



function breakLongWords($str, $maxLength){

    $maxLength--;

    $count = 0;

    $newStr = "";

    $openTag = false;

    $openJava= false;

    $openCSS = false;

    $openCom = false;

    $openAnd = false;

    $openKav1 = false;

    $openKav2 = false;

    $j=0;

    $count=mb_strlen($str);

    for($i=0; $i<$count; $i++){

        $newStr .= $str{$i};

        if($str{$i}=="<"){

            $openTag = true;

            if(preg_match('/sc/i',$str{$i+1}.$str{$i+2})){

                $openJava = true;

            }

            if(preg_match('/\/sc/i',$str{$i+1}.$str{$i+2}.$str{$i+3})){

                $openJava = false;

            }

            if(preg_match('/sty/i',$str{$i+1}.$str{$i+2}.$str{$i+3})){

                $openCSS = true;

            }

            if(preg_match('/\/sty/i',$str{$i+1}.$str{$i+2}.$str{$i+3}.$str{$i+4})){

                $openCSS = false;

            }

            if($str{$i+1}.$str{$i+2}=='!-'){

                $openCom = true;

            }

            continue;

        }

        if($str{$i}=='&'){

            $openAnd = true;

            $maxLength--;

            continue;

        }

        if(($openTag) && !$openKav1 && ($str{$i} == '"')){

            $openKav1 = true;

            continue;

        }

        if(($openTag) && !$openKav2 && ($str{$i} == "'")){

            $openKav2 = true;

            continue;

        }

        if(($openTag) && $openKav1 && ($str{$i} == '"')){

            $openKav1 = false;

            continue;

        }

        if(($openTag) && $openKav2 && ($str{$i} == "'")){

            $openKav2 = false;

            continue;

        }

        if(($openTag) && !$openJava && !$openKav2 && !$openKav1 && ($str{$i} == ">")){

            $openTag = false;

            if($str{$i-1}=='-'){

                $openCom = false;

            }

            continue;

        }

        if(($openAnd) && ($str{$i} == ";")){

            $openAnd = false;

            continue;

        }

        if(!$openTag && !$openCom && !$openJava && !$openCSS && !$openAnd && $str{$i}!=" " && $str{$i}!="\n" && $str{$i}!="\t"){

            $j++;

        }

        if($j>$maxLength){

            return $newStr;

        }

    }

    return $newStr;

}




function create_hor_menu ($ident)

{







    $out.= head_smartpl ('hormenu');



    $qu="SELECT * FROM `".pref_db."content` WHERE `rod`='".xss($ident)."' and `ok`<>'' ORDER BY `num` ASC";

    $pr=my_mysql_query($qu);



    //v(pref_db.hrpref);

    while ( $mm=mysqli_fetch_array($pr)	 )

    {

        if ( $mm['ident']=='index' ) $mm['ident']='';

        $out.=smartpl('hormenu',

            '$hr'		, 	hrpref.$mm['ident'].hrsuf.lngsuf ,

            '$name'		,	lang($mm['name'] ));

    }



    $out.= foot_smartpl ('hormenu');



    return $out;



}







function newident($id,$ident,$_notsearch=false)
{
    if  ( substr($ident,0,7)=='http://' )
    {
        $arr=explode('/',$ident);  // http://www.alros.ru/dl_price.phtml
        unset($arr[0],$arr[1],$arr[2]);
        //v($arr);
        $ident=implode('/',$arr);
    }

    $ident=strtr(($ident),$GLOBALS['repurl']);
    $ident=translit(($ident));
    if ($ident=="")
    {
        $ident=translit($GLOBALS['name']);
    }


    $ident=substr( ( o('no_ident_strtolower') ? $ident :  mb_strtolower( $ident ) ) ,0,200) ;

    if ($ident=="")
    {
        $ident=qvar('id',$id,'rod');
    }

    $mad=mysqli_fetch_array(my_mysql_query('SELECT `id` FROM '.pref_db.'content 
										WHERE ident=\''.xss($ident).'\' AND id<>\''.xss($id).'\' ') );
    while($mad and !$_notsearch)
    {
        $ident=prefup($ident);
        $mad=mysqli_fetch_array(my_mysql_query('SELECT `id` FROM '.pref_db.'content WHERE ident=\''.xss($ident).'\' AND id<>\''.xss($id).'\' ') ) ;
    }
    //echo ($ident)
    //exit;
    return $ident;
}


function select($qu)
{
    $id=$GLOBALS['m']['id'];
    $query="SELECT * FROM `".pref_db."content` WHERE `id`= '$id'";
    $mm=mysqli_fetch_array(my_mysql_query($query));
    $adds=explode("\n",$qu);

    foreach ($adds as $add)
    {
        if ( strpos($add,'='))
        {
            $sep='=';
        }
        elseif (strpos($add,'~'))
        {

            $sep='~';

        }

        elseif (strpos($add,'%'))

        {

            $sep='%';

        }

        else

        {

            $sep='===';

        }





        if ( trim($add)!=='' )

        {

            $ad=explode($sep,$add);

            if( !$sub AND trim($ad[0])=='d81' OR  trim($ad[0])=='sub' )
            {
                reglobalArray( (unserialize_must( $mm['d81'] ) ) );
                //v($GLOBALS['is_lp']);
                $sub=1;
                //exit();
            }
            else
            {
                $GLOBALS[trim($ad[1])]=$mm[trim($ad[0])];
            }



            //echo $GLOBALS[trim($ad[1])]." ".$mm[trim($ad[0])]."<br/>";

        }





    }





}



function smartpl ($template,$part)

{

    if ( !isset($GLOBALS['t'.$template]) )	$GLOBALS['t'.$template]=explode('-t-',fgc($template));



    $x=2;



    while ( @$add=func_get_arg ($x)  )

    {

        @$rep[$add]=func_get_arg ($x+1);



        $x=$x+2;

    }

    //v(strtr($tpl[1],$rep));

    if ($rep)

    {

        $out=strtr($GLOBALS['t'.$template][$part],$rep);

    }

    else

    {

        $out=$GLOBALS['t'.$template][$part];

    }



    return 	$out;



}



function findtree($rod)
{
    $qu="SELECT `ident`,`rod` FROM `".pref_db."content` WHERE `rod`=".xs($rod);
    $pr=my_mysql_query($qu);

    while ($mm=mysqli_fetch_assoc($pr) )

    {
        $out[]=$mm['ident'];
        $ou=findtree($mm['ident']);

        if ($ou)
        {
            $out=array_merge($out,$ou);
        }
    }
    return $out;
}



function update($qu)

{
    global $m;
    $qu=( $GLOBALS['newvars'] ? $GLOBALS['newvars'] : $qu) ;
    //$GLOBALS['v']['nv']='49849';
    $adds=explode("\n",$qu);
    foreach ($adds as $add)
    {
        if ( strpos($add,'='))
        {
            $sep='=';
            $ad=explode($sep,$add);
            $ad[0]=trim($ad[0]);$ad[1]=trim($ad[1]);
            $insert=intval($GLOBALS[trim($ad[1])]);
        }
        elseif (strpos($add,'~'))
        {
            $sep='~';
            $ad=explode($sep,$add);
            $ad[0]=trim($ad[0]);$ad[1]=trim($ad[1]);
            $ins=($GLOBALS[trim($ad[1] )]) ;	// имя переменой
            if ( is_array( $ins ) )
            {
                $insert=xss(implode(' ',$GLOBALS[trim($ad[1])]) );
            }
            else
            {
                $insert=xss($GLOBALS[trim($ad[1])]);
            }
        }
        else //(strpos($add,'%'))
        {
            $sep='%';
            //$ad=explode($sep,$add);
            //$insert=fulxss($GLOBALS[trim($ad[1])]);
        }

        if ( trim($add)!=='' )
        {
            $ad=explode($sep,$add);
            $ad[0]=trim($ad[0]);$ad[1]=trim($ad[1]);
            //v($ad);
            $qu='SELECT * FROM `'.pref_db.'content` limit 0,1';
            //v($qu);
            $pr=my_mysql_query($qu);
            $mm=mysqli_fetch_assoc($pr);

            if ( $ad[0] and ( isset($mm[trim($ad[0])]) or  ($ad[0])=='sub')  )
            {
                if ( ($ad[0])=='d81' or ($ad[0])=='sub' )
                {
                    $sub[$ad[1]]=($GLOBALS[($ad[1])]);
                }
                else
                {
                    $set[]= "`".trim($ad[0])."`='".$insert."'";
                    $userfld[]=$ad[0];
                }

            }

        }
        if ( ( trim($ad[0])=='ident' ) & ( !(bool)$insert) ) $fatal=1;

        if ( (trim($ad[1])=='ident') & ($GLOBALS['ident']!==$GLOBALS['m']['ident'])   )
        {
            $qu="UPDATE `".pref_db."content` SET `rod`=".xs($GLOBALS[trim($ad[1])])." WHERE `rod` = ".xs($GLOBALS['m']['ident']);
            //v($qu);
            my_mysql_query($qu);
        }
    }
    $id=$GLOBALS['m']['id'];

    if (  vl($sub) ) {

        $set[]= "`d81`=".xs( serialize( (array_merge_null( ($m['sub']) , $sub ))) );

    }

    //$userfld[]=$ad[0];
    // v($set[1]);
    //v(strlen($set[1]));
    $set=implode(",",$set);
    //v($set);
    //$set=iconv('utf-8','windows-1251', $set );
    //v($set);

    if ( !($GLOBALS['del']) & ( !(bool)$fatal )  )
    {
        $query="UPDATE `".pref_db."content` SET $set WHERE `id` = '".intval($id)."' LIMIT 1 ";
        $GLOBALS['vv']['query']=$query;
        //v($query);exit();
        my_mysql_query(vl($query,'update query'));
    }
    //v($GLOBALS['m[id]']);
    $adds=explode("\n",$qu);
    update_rod_info();

}


function toUpper($content) {

    $content = strtr($content, "абвгдеёжзийклмнорпстуфхцчшщъьыэюя",

        "АБВГДЕЁЖЗИЙКЛМHОРПСТУФХЦЧШЩЪЬЫЭЮЯ");

    return strtoupper($content);

}



function toLower($content) {

    $content = strtr($content, 'АБВГДЕЁЖЗИЙКЛМHОРПСТУФХЦЧШЩЪЬЫЭЮЯ',

        'абвгдеёжзийклмнорпстуфхцчшщъьыэюя');

    return strtolower($content);

}





?>
