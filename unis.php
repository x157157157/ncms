<?php 

  /**
   * Клас по работе с юнисендером
   */
class unis {


	/**  
	 * получает все контакты из кеша
	 */
	public static function  contacts_get_saved (  ) {
	 	 return unserialize( gzdecode( file_get_contents(   'cont/uni_all.tmp' ) ) );
	 }

	/**
	 * получает все контакты
	 * @param (array) поля для получения
	 */
	 public static function  contacts_get_all ( $fields=false,  $page_max=20 ) {
		$out=[];
		if ( !$fields  ) $fields=['email','enter_key'];
				
		for($x=0; $x<=$page_max; $x++) {
			$data=unis::contacts_get_of( $fields , 1000, $x )->result->data;
			if ($data) {
				
				$out =  array_merge( $out, $data  )  ;
				
			} else {
				
				break;
				
			}
		}
		file_put_contents(   'cont/uni_all.tmp', gzencode( serialize( $out ) , 3)  );
	 	return $out;
	 }

	/**
	 * получает все контакты из кеша
	 */
	 public static function  contacts_get_of ( $fields , $limit, $page, $list_id ) {
	 	return json_decode(unis::ob()->exportContacts ( [ 
			'field_names'=>$fields,
			'limit'=>$limit,
			'offset'=>$limit * $page,
			'list_id'=>$list_id,
		]));
	 }

	/**
	 * это какойто тест
	 */
	 public static function contacts_set_enter_key(  ) {
	 
	 	$data=[
	 				['rev-ln@yandex.ru', enter_key( 'rev-ln@yandex.ru' ) ],
	 				['kisa_8585@mail.ru', enter_key( 'kisa_8585@mail.ru' ) ],
	 	
	 	];
		return unis::contacts_set_out(['email','enter_key'], $data );
	 }
	
	/**
	 * назначает всем сушествуюущим пользователям enter_key для автоавторизации на сайте из email
	 */
	 public static function  contacts_set_enter_key_all (  ) {
	 	
	 	set_time_limit(300);
	 	
	 	foreach ( self::contacts_get_saved( ) as $mm ){
	 		
	 		if ( !$mm[1] ) {
				$data[]=[$mm[0] , enter_key( $mm[0] )]   ;
				
				$k++;
			} 
			
			if ( $k>400 ) {
				v(  vl ( unis::contacts_set_out(['email','enter_key'], vl($data) ) ) ) ;
				unset ( $data, $k );
				 	
				ob_flush();
			}
			
		}
		
	 	v(  vl ( unis::contacts_set_out(['email','enter_key'], $data ) ) ) ;
	 	
	 	return $rep;
	 	
	 }

	/**
	 * Отправляет (импортирует) новых пользователей (или если они уже существуют, то обновляются данные) В ЮНИСЕНДЕР
	 * @fields Поля
	 * @data Массив данных подписчиков, каждый элемент которого — массив полей в том порядке, в котором следуют field_names
	 */
	 public static function  contacts_set_out ( $fields, $data ) {
	 	
	 	return json_decode(unis::ob()->importContacts ( [  
	 		'field_names'	=> $fields, $data	 ,
	 		'data'		=> $data
	 	] ) );
		
		
		
	 }



	 /**
	  * расширенное добавление пользователя
	  * $opts_arr:
	  *
	  * 'data' => [ 'email'=>$email , //поля
	  * 			'Name'=>$name ,  //поля
	  * 			'enter_key'] ,   //поля
	  *'list_ids'=>list_ids=123456,345678,2344423 // списки куда добавляется человек (через запятую)
	  *
	  */
	 public static function  ad_user2 ( $opts_arr ) {

	 	extract($opts_arr);
	 	
	 	$p=[
			'list_ids'=>$list_ids,
			'fields'=>$data, // 
			'double_optin'=>3 
			
		];
		
		return json_decode( unis::ob()->subscribe( $p  ) );
	 	
	 }


	/**
	 * добавляет нового пользщователя (старая функция)
	 */
	 public static function  ad_user ( $email, $name, $list_ids ) {
		
		$p=[
			'list_ids'=>$list_ids,
			'fields'=>[ 
				'email'=>$email ,
				'Name'=>$name
			],
			'double_optin'=>3
			
		];
		
		return json_decode( unis::ob()->subscribe( $p  ) );
		
		
		
	}

	/**
	 * получает все списки (папки) пользователей, которые есть в юнисендере
	 */
 	public static function  getLists() {
 		
 	 	return json_decode(unis::ob()->getLists());
 	 	
 	}

	/**
	 * получает все контакты из кеша
	 */
 	public static function  ob() {
		require_once(jscripts."/lib/unisender_api.php"); //подключаем файл класса
		$apikey=o('unisender_api_key'); //API-ключ к вашему кабинету
		return new UniSenderApi($apikey); //создаем экземляр класса, с которым потом будем работать
	}
	
}
?>