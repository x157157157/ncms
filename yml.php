<?php class yml {
	public static function feed() {

		header("Content-Type: application/xml; charset=utf-8");

		echo '<?xml version="1.0" encoding="UTF-8"?>' ;

		$p['not_inadm_comment']=1;

		$cats=fn('yml::categories_get_arr', explode(' ',  o('yml-shop-root') ) );

		$p1['categories']=self::categories( $cats );

		echo  smartyfull( smarty_tpl( 'yml-feed-tpl-start' , array_merge( $p, $p1 )  ) , 2 );

		$x=0;

		foreach ( self::tovs_get_all_arr($cats)   as $mm  ) {
		    $x++;
			setsub($mm);
			//v($mm);
			//
			//v( $mm['ident'] ) ;

			if( $mm['price'] AND ( !$_GET['limit'] OR $_GET['limit'] >= $x )  ) {
				echo  fn('yml::offer', $mm);
			}
		}

		echo smartyfull( smarty_tpl( 'yml-feed-tpl-end' , $p ), 2 );

		exit();

	}

	/**
	 * получает массив всех КАТЕГОРИЙ товаров
	 * @param string $rod
	 * @return array
	 */
	public static function tovs_cat_arr( $rod ) {

		return vl( mydb_query_all( vl( '
		
				SELECT * 
				FROM '.pref_db.'content 
				WHERE
				 
				type=\'tov\'
					AND
				d32=\'\'
					AND
				ok<>\'\' 
					AND 
				`rod` =  '.xs( $rod ). ' 
				ORDER BY num ASC
		' ) ) );

	}

	/**
	 * получает товары со всех корневых категорий
	 * @param $cats array массив идентификаторов категорий
	 * @return array
	 */
	public static function tovs_get_all_arr( $cats  ) {

		$out=[];

		foreach ( $cats as $cat) {

			$tovs_new=( self::tovs_cat_arr( vl($cat['ident']) ));

			foreach ( $tovs_new as $tov ) {
				$out[]=$tov;
			}

		}

		//var_dump( $out );
		//exit();

		return ($out);
	}


	public static function categories_get_arr( $rod  ) {

		if ( is_array( $rod ) ) {
			$cat=[];
			foreach ( $rod as $r ) {
				$new_cats=fn('yml::categories_get_arr', $r );
				$cat=array_merge_null( $cat ,  $new_cats) ;
			}
			return $cat;
		}

		$qu= 'SELECT * FROM '.pref_db.'content WHERE 
			type=\'category\' 
				AND
			d32=\'\'
				AND
			ok<>\'\' 
				AND 
			`rod` =  '.xs($rod). ' 
			ORDER BY num ASC
			
			' ;

		foreach ( mydb_query_all ( $qu ) as $mm ) {
			if ( !$GLOBALS['cats'][$mm['ident']] ) {
                $out[]=vl($mm);
                $sub= self::categories_get_arr( $mm['ident']) ;

                if ( $sub ) {
                    $out=array_merge($out, $sub);
                }
                $GLOBALS['cats'][$mm['ident']]=true;
            }

		}

		return $out ;

	}




	public static function categories( $cats  ) {

		$out='';
		$ident_old=[];
		foreach (  $cats  as $mm  ) {
            if ( !$ident_old[$mm['ident']] ) {
                $out.='			<category id="'.$mm['id'].'" '.( $mm['rod_id'] ? 'parentId="'.$mm['rod_id'].'" ' : '' ).'>'.self::prepare_value($mm['name']).'</category>'.PHP_EOL;
                $ident_old[$mm['ident']]=1;
            }
		}

		return $out;

	}

	/**
	 * Тестировщик
	 *
	 * price_actual( qvar(4440) );
	 * yml::offer_test_1()
	 */

	public static function offer_test_1 ($id=false) {
		return yml::offer( qvar($id ? : 4440) );
	}

	public static function prepare_value( $value ) {
	    return htmlspecialchars($value, ENT_QUOTES | ENT_XML1, 'UTF-8');
	}

	public static function offer($mm) {

		$p['not_inadm_comment']=1;


		$mm = self::prepare_values($mm);


		// price
		$p['price']=   price_actual($mm)  ;
		if ( is_sale($mm) ) {
			$p['oldprice']='<oldprice>'.price_prepare( $mm['price'] ).'</oldprice>';
		}

		// categoryId
		$p['category_id']=(qvar(  $mm['rod'] )['id']);

		//picture
		$p['picture']= bh().'/'.img.'/cat/big/'.$mm['d25'];

		// store
		if ( o('yml-offer-store') ) {
			$p['store']='<store>'.o('yml-offer-store').'</store>';
		}

		// pickup
		if ( o('yml-offer-pickup') ) {
			$p['pickup']='<pickup>'.o('yml-offer-pickup').'</pickup>';
		}

		// description
		$p['description'] = '<description>'. $mm['desc'].'</description>';

		// model
		$p['model'] = '<model>'. $mm['name'] .'</model>';

		// model
		$p['vendor'] = '<vendor>'.( self::prepare_value( par(  $mm, 'd29' )) ?: 'Unknown' ).'</vendor>';

		// typePrefix
		if ( $typePrefix=par( $mm, 'd31') ) {
			$p['typePrefix'] = '<typePrefix>'.$typePrefix.'</typePrefix>';
		}

		// pickup
		if ( $mm['d23'] ) {
			$p['vendorCode']='<vendorCode>'.$mm['d23'].'</vendorCode>';
		}

		// options tovmc
		$p['tpl']= 'yml-feed-tpl-offer';
		$p['mc_border_shab']= 'none' ;

		return smartyfull(   fn('tovmc::x' ,$mm,  $p ), 2 );
	}

    public static function name_prepare($val)
    {
        return self::prepare_value( mb_substr( lang( $val ), 0, 175 , 'UTF-8'  ) );
	}

	public static function desc_prepare($val)
    {
        return self::prepare_value( mb_substr( lang( $val ), 0, 175 , 'UTF-8'  ) );
	}

    public static function prepare_values($mm)
    {
        $fields = [
            'name'=>176,
            'desc'=>175,
        ];

        foreach ($mm as $k=>$v) {
            $mm[$k]=  self::prepare_value( mb_substr( lang( $mm[$k] ), 0,  ( $fields[$k] ? : 10000 ) , 'UTF-8'  ) );
        }
        return $mm;
	}
}

?>