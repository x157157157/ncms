<? class ems {
	public static function pricedost($p) {
		extract($p);
		
		//$weight_all
		
		
		$country=$_COOKIE['cart_country'];
		$spdost=$_COOKIE['cart_spdost'];

		if ($GLOBALS['cache']['ems_pricedost'][$country.'-'.$spdost] ) { 
			return $GLOBALS['cache']['ems_pricedost'][$country.'-'.$spdost];
		}
		
		if ($country AND $spdost=='international_ems') {
			$url= vl('http://emspost.ru/api/rest?method=ems.calculate&to='.$country.'&weight='.$weight_all.'&type=att', 'url') ;
			$res= json_decode(file_get_contents($url) ) ;
			vl($res  ,'response' );
			if ( $res->rsp->stat == 'ok' ) {
				
				$GLOBALS['cache']['ems_pricedost'][$country.'-'.$spdost]=$res->rsp->price;
				return $res->rsp->price+100;
			}	
		} else {
			$p['country']=$country;
			$p['spdost']=$spdost;
			
			return $GLOBALS['cache']['ems_pricedost'][$country.'-'.$spdost]=fn('ems::local_dost',$p);
		}
		
				
	}
	public static function local_dost($p) {
		extract($p);
		/* стоимость доставки Курьер по Москве и Курьер по Санкт-Петербургу, исходя из этих цифр:
До суммы заказа 6499 руб. стоимость доставки =250 руб.
При сумме заказа от 6500 до 9499 руб. доставка 150 руб.
При сумме заказа от 9500 руб. доставка Бесплатно (и это надо обозначить, чтобы клиент видел) */
		if ( $spdost=='self' ) {
			return 0;
		} elseif ( $spdost=='zamkad' ) {
			return 450;
		} elseif ( !$spdost or $spdost=='kur_spb' ) {
			$cur_summ=fn('cart::cur_summ');
			if( $cur_summ <= 6499  ) {
				return 250;				
			} elseif ( $cur_summ <= 9499 ) {
				return 150;								
			} elseif ( $cur_summ > 9499 ) {
				return 0;
			}
			
		}
		
	}
}

?>