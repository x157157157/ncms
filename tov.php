<?php

/**
 * Class tov функции связанные с товаром. 
 * todo Сюда надо перенести большую часть действий из функции tov() 
 */
class tov {

    public static function main($ident) {

    global $m;
    $ajaxout=[];

    $mm=$m;
    //$tovgroup=qqu(' rod=\'tov_subtype\' and d1='.xs( par( qvar('ident', $m['rod'] ) ,'d6' )  ) );

    $tovgrsets=( tovgroupsets_get( 'ident', ($ident? $ident : $m['ident'] ) ) );
    $tovgroup=$tovgrsets['d1'];
    $tovfields=(tovsetsfields($tovgrsets));
    //exit();

    if ( vl($_REQUEST['art'] or $_REQUEST['adcart'] ) )
    {
        if (!$_REQUEST['art'] AND $_REQUEST['tovid'])
        {
            $_REQUEST['art']= qvar( 'ident' , $_REQUEST['art'], 'ident' );
        }

        $_REQUEST['num_tov'] = $_REQUEST['num_tov'] ? $_REQUEST['num_tov'] : $_REQUEST['tabnum'][$_REQUEST['num_tablemod']] ;

        if ( !($_REQUEST['num_tov']) or $_REQUEST['num_tov']=='undefined' ) $_REQUEST['num_tov']=1; //v(__LINE__);
    }
    $mptov=fn('minimodparser', $mm['d16'], $mm );

    // массив добавляемого товара в корзину
    $mmtov=qvar( 'ident' , $_REQUEST['art'] );

    //Определяем добавлять ли товар или нет
    if ( vl( intval( ($_REQUEST['num_tov']))>=1 ) AND $mmtov['ok'] AND !$mmtov['sub']['nonal'] ) // обработчик вставки в корзину
    {

        if ( !isset($_REQUEST[session_name()]) ) session_start();

        // массив товара

        $tovgrsetstov= fn( 'tovgroupsets_get' , 'ident', $mmtov['ident'] )  ;
        $tovgrouptov=$tovgrsetstov['d1'];
        $tovfieldstov=fn( 'tovsetsfields', $tovgrsetstov );

        $_REQUEST['num_tov']=( ( $mmtov['d19']>0 and o('tov_num_min') and $mmtov['d19']>$_REQUEST['num_tov']   )    ? ($mmtov['d19']) :  $_REQUEST['num_tov']  );

        $mp=fn( 'minimodparser', $mmtov['d16'], $mmtov );
        if ( !$mmtov['price'] and !$_REQUEST['opts']  AND $mp )  $_REQUEST['opts'][1]=1 ;
        //v($zakaz);
        //v($_REQUEST['opts']);
        foreach( ($_REQUEST['opts'])  as $k=>$v)
        {
            //v($v);
            if( $v )
            {
                //Название-мод (цена руб)
                if($mp[$k][1][$v][1]>0)
                {
                    $price= $mp[$k][1][$v][1];
                }
                elseif($mp[$k][2]>0)
                {
                    $price=  $mp[$k][2]  ;
                }
                else
                {
                    $price=   FALSE ;
                }

                $price= str_replace(',','.', trim($price) ) ;

                $dopprice=$dopprice+$price;
                //v($price);

                $zakaz[] = $mp[$k][0].(  $mp[$k][1][$v][0] ? ': '. $mp[$k][1][$v][0] :'' ). ( ($price AND !o('tov_noprice') ) ? ('( '.$price.' [val] )') : '' );
            }
        }
        //

        vl($zakaz);

        if( vl($tovgrsetstov['sub']['tabmodfields']) )
        {
            foreach ( (super_explode_n('::', $tovgrsetstov['sub']['tabmodfields']) ) as $k=>$v  )
            {
                $tabfld[$v[0]]=$k;
            }

        }

        $complect=$tovgrsets['sub']['complect_name'] ? "\r\n".$tovgrsets['sub']['complect_name'] : '' ;

        $tabmods= (super_explode_n('++', str_replace('	','++', $mmtov['sub']['tabmod'].$complect ) )  );

        if ( vl($mmtov['sub']['tabmod']) )
        {
            $tabmod= ( $tabmods[($_REQUEST['num_tablemod'])-1] );
            $dopprice= price_auto_cop (  price_prepare( $tabmod[$tabfld['price']] ) ) ;

            unset($tabmod[$tabfld['price']]);
            $zakaz[]= '('.implode('\; ', $tabmod).')';

        }
        $k_complect=  ( $tovgrsets['sub']['complect_name'] AND 0 ) ? 2 : 1 ;
        //v($dopprice);
        $quc='
		SELECT * FROM `'.pref_db.'cart` WHERE  
		`viz_sid` 	='.xs(session_id()).'
		AND
		`art` 	='.xs($_REQUEST['art']).'
		AND 
		`zakaz`='.xs(implode('<br/> ', ($zakaz)) ).'
		AND 
		is_order<>1
		';
        $pr=my_mysql_query( ($quc) );
        $mmold=mysqli_fetch_assoc($pr);

        if (vl($mmold)) // if product alredy added to cart
        {
            $ajaxout['status']= 'added';
            $ajaxout['message']= smarty_tpl('cart_gotocart/addcart_added_head');
        }
        else
        {

            $main_price= !$tovgrsetstov['sub']['minimod_not_summ_price']  ?  (strtr( trim(  price_actual( $mmtov ) ), array(','=>'.', ' '=>'' ) ) ) : 0 ;


            $add_in_cart_arr= array(
                'viz_sid' 	=>session_id() ,
                'art' 	=> $_REQUEST['art'] ,
                'price' =>   ( $main_price  +$dopprice) *$k_complect    ,
                'num' =>  $_REQUEST['num_tov'] ,
                'num_opt' =>  ($tovgrsetstov['sub']['num_opt'] ? sizeof( $zakaz ) : false) ,
                'zakaz' => implode('<br/> ', $zakaz)  ,
                'datetime'=> date('Y-m-d H:i:s'),
                'uid'=> uid()
            );
            vl();
            fn('mydbAddLine', 'cart', $add_in_cart_arr );

            $ajaxout['status']= 'ok';
            $ajaxout['message']= smarty_tpl('cart_gotocart/addcart_success');


        }
        unset($GLOBALS['cahe']['get_cur_cart_arr']);
        // если мультидобавление
		if ( $_REQUEST['tovars']) {
        	echo '[fc]ok[fc]';

		} else {

        	header("Content-Type: text/html; charset=utf-8");

        	$ajaxout['minicart'] = fn( o::create_cart_fn , 'auto');

        	$ajaxout['error']= ob_get_clean();
        	echo json_encode_pretty($ajaxout);
		}

        return;
    }
    if ($_REQUEST['del'])  //udaliaem iz korzini
    {
        $quc='DELETE FROM `'.pref_db.'cart` WHERE `art`='.xs($_REQUEST['art']).'	AND `viz_sid`='.xs(session_id()).'		';
        //v($quc);
        $pr=my_mysql_query($quc);
    }
    if  ($_REQUEST['by'])
    {
        header('Location: '.basehref.'/cart' );
        die();
    }

    $form='<form onkeypress="if(event.keyCode == 13) return false;" onsubmit="return adcart(this,0,'.$mm['id'].')" '." action='' method='post' name='form1' id='tov{$mm['id']}'>".'<input type="hidden" value="'.$mm['ident'].'" name="art" /><input type="hidden" value="'.$mm['id'].'" name="tovid" />';

    $maintpl=shab(($tovgrsets['d5'])); // большая карточка
    $mtpl = $maintpl ? $maintpl : 'tov_tpl_big';
    if (!(bool)$maintpl)
    {
        $maintpl=shab(  ($mtpl)  );
    }
    if ($mm['d7'])
    {
        $optshab=shab('tov_tpl_big_opt'.$mm['d6']);
    }
    $alb=view_album();
    $tovalbum=fn( 'slider::x', $mm['ident'],TRUE,1);

    //$imurl=url2mnemo($mm2['ident']);

    $imurl=   $m['d25'].( !(oror( ( fileending( $m['d25'] )), 'jpg', 'png' )) ? '.jpg' : '' )  ;

    $s=(getimagesize('cont/img/cat/big/'.$imurl));
    $k=$s[0]/$s[1]; // w / h
    $k1=o('tovimg_w')/$s[0]; // во сколько раз реальная ширина меньше ограничителя
    $k2=o('tovimg_h')/$s[1];
    if($k1>1 and $k2>1 )
    {
        $w=$s[0];
        $h=$s[1];
    }
    elseif ($k1<$k2)
    {
        $w=o('tovimg_w');
        $h=o('tovimg_w')/$k;
    }
    else
    {
        $h=o('tovimg_h');
        $w=o('tovimg_h')*$k;
    }

    if ( (noimage("/cont/img/cat/big/{$imurl}",1)) )
    {

        $midimg="<a class='group bc_midimg_a' rel='group' href='/".filemtime_file("cont/img/cat/big/{$imurl}")."'><img style='width:{$w}px;height:{$h}px;' alt='".lang(($mm['d20']))."' title='".lang($mm['d21'])."'  class='tovimg' src='/".filemtime_file("cont/img/cat/big/{$imurl}")."'/></a>";
    }
    else
    {
        $midimg="<img  src='/".noimage("cont/img/cat/big/{$imurl}")."'/>";
    }

    $minimg="<img src='/cont/img/cat/min/{$imurl}'/>";

    watermark_file(($imurl));

    $bigimg="<img src='/".filemtime_file( noimage(("cont/img/cat/big/{$imurl}")))."' alt='".lang(($mm['d20']))."' title='".lang($mm['d21'])."' >";
    $content=fgc($m['ident'].$GLOBALS['lng']);

    if (o('tov_adcart_txt') )
    {
        $adcart=$by='<input type="hidden" value="'.$mm['ident'].'" name="art" /><div  class="adcart" > <a href="javascript:adcartbut('.($mm['id']).')">Купить</a></div>'  ;

    }
    else
    {
        $adcart="<input type='hidden' value='".$mm['ident']."' name='art'/>
			
			".ibut('adcart');
        $by="<input type='hidden' value='{$mm['ident']}' name='art'><input type='submit' value=' ' class='bytov' name='by'/>";
    }

    if ($mm['sub']['nonal'])
    {
        $adcart='<span class="nonal">Товара нет в наличии</span>';
        $num='';
    }
    else
    {
        //$num='<input type="text" name="num_tov" class="num_tov" value="1"/> ';
        $num='<input type="text" id="num_tov" name="num_tov" class="num_tov" value="'.( ( $mm['d19']>0 and o('tov_num_min'))  ? ($mm['d19']) : '1' ).'"/> ';
    }

    $src=img.'/cat/mid/'.$imurl;

    $other = othertov($mm['d8'],$mm['ident'],'tov_tpl_big_other', 'blocktov' );


    //v($mptov);
    $mm['price']= ( !$mm['price'] AND $mptov[1][1][1][1]  ) ? '  ' : $mm['price'];
    list($tabmod) = tabmod($mm, $tovgrsets);

    unset($labels);

    foreach( $mm['sub']['labels'] as $k=>$v )
    {
        $labels[$v]=1;
    }
    if ( $mm['sub']['nonal'] or $mm['sub']['tov_cur']==='0' or $mm['sub']['tov_cur']===0  ) {
        $nonal = $labels['nonal']=  1 ;
        $mm['sub']['labels'][]='nonal';
    }

    if ( o('fullsearch_ver')==1 )
    {
        list( $tovset, $ts_name, $ts_val ) = view_tov_set2($mm['d9'], $tovfields) ;
    }
    else
    {
        $tovset = view_tov_set($mm['d9']) ;
    }

    $imgs2=  unserialize ( $mm['d26'] ) ;
    $imgs=  $imgs2 ?  array_merge ( array( $mm['d25'] ) , $imgs2 ) : array( $mm['d25'])  ;
    $nonal = ( $mm['sub']['nonal'] or $mm['sub']['tov_cur']==='0' or $mm['sub']['tov_cur']===0 ) ? true : FALSE ;
    $vendor =par_tovcat($mm, 'd29');


    $rep=array(
        '[name]'	=>	lang($mm['name']),
        '[art]'	=>	$mm['art'],
        '[href]'	=>	hrpref.trim($mm['ident']).hrsuf.lngsuf,
        '[minidesc]'	=> lang(str_replace( "\n",'<br/>',  $mm['sub']['big_minidesc'] ) ) ,
        '[midimg]'	=>	$midimg ,
        '[bigimg]'	=>	$bigimg,
        '[minimg]'	=>	$minimg,
        '[content]'	=>	$content,
        '[price]'	=>	pricer($mm['price'],TRUE),
        '[adcart]'	=>	is_price ? $adcart : '' ,
        '[mod]'		=>	$modific ,
        '[other]'		=>  $other,
        '[minimod]'	=>	is_price ? fn('minimod', $mm['d16'], $mm) : '' ,
        '[tabmod]'	=>	is_price ? $tabmod : '' ,
        '[tovset]'	=>	$tovset ,
        '[curcat]'	=>	catcurclassic($mm['ident'],o('tov-bigcart-microtov')),
        '<form>'	=>	$form,
        '[num]'		=>	$num,
        '[album]'	=>	$alb,
        '[tovalbum]'	=>	$tovalbum,
        '[collect]'=> collection($tovgrsets,$mm),
        '[labels]'=>labels($mm),
        '[by]'		=> 	is_price ? $by : '' ,
        '[d1]'		=>	$mm['d1'],
        '[d2]'		=>	$mm['d2'],
        '[d3]'		=>	$mm['d3'],
        '[h1]'		=>	$mm['d4'],
        '[d5]'		=>	$mm['d5'],
        '[d6]'		=>	$mm['d6'],
        '[d7]'		=>	$mm['d7'],
        '[d8]'		=>	$mm['d8'],
        '[d9]'		=>	$mm['d9'],
        '[d10]'		=>	$mm['d10'],
        '[d11]'		=>	$mm['d11'],
        '[d12]'		=>	$mm['d12'],
        '[d13]'		=>	$mm['d13'],
        '[d14]'		=>	$mm['d14'],
        '[d15]'		=>	$mm['d15'],
        '[d16]'		=>	$mm['d16'],
        '[d17]'		=>	$mm['d17'],
        '[d18]'		=>	$mm['d18'],
        '[newprice]'		=>	$mm['d27'],
        '[%sale]'=>   round (100*($mm['price'] - $mm['d27'])/$mm['price']),
        '[d30]'		=>	$mm['d30'],
        '[d31]'		=>	$mm['d31'],
        '[d32]'		=>	$mm['d32'],
        '[d33]'		=>	$mm['d33'],
        '[d34]'		=>	$mm['d34'],
        '[d35]'		=>	$mm['d35'],
        '[d36]'		=>	$mm['d36'],
        '[d37]'		=>	$mm['d37'],
        '[d38]'		=>	$mm['d38']
    );

    $smarty= (smarty_gen ( strtr($maintpl, $rep) ) );
    eval ( $smarty );
    $out.= $sm_str;

    return $out;
}
	public static function redirect_offed( $mm ) {
	

		
		if ( $mm['type']=='tov' // если объект это товар
				AND
			!$mm['ok'] ) { // если НЕ включен

			$url = $mm['rod'].o('fullsearch_url_suf'); // адрес родительской категории
			if ( is_testing() ) {
				return $url;
			} else {

				loc301($url);
			}

		}

	}

}



?>