﻿(function($) {
	$.fn = $.fn || {};

	$.jcuploadUI_config= {
	    flash_file: "flash.swf",
		flash_width: 100,
        flash_height: 22,
        flash_background: "button.png",
		flash_background_offset_x: 0,
		flash_background_offset_y: 0,

        box_height: 200,

        file_icon_ready: 'file_ready.gif',
        file_icon_uploading: 'file_uploading.gif',
        file_icon_finished: 'file_finished.gif',

        hide_file_after_finish: false,
        hide_file_after_finish_timeout: 100,

        error_timeout: 3000,

        max_file_size: 8 * 1024 *1024,
        max_queue_count: 100,
        max_queue_size: 0,

        extensions: ["Допустимые типы файлов (*.jpg,*.jpeg, *.gif, *.png)| *.jpeg; *.jpg;*.gif;*.png"],

        multi_file: 1,

        callback: {
     		init: true,
     		pre_dialog: true,
     		file_added: true,
     	    upload_start: true,
     	    upload_progress: true,
     	    upload_end: true,
     	    queue_upload_end: true,

     		//errors
     		error_file_size: true,
     		error_queue_count: true,
     		error_queue_size: true
        }
	}
})($);