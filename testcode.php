<?php class testcode
{
	public static function main()	{
		$tests = [
			'testcode::mob_url_test',
			'testcode::loc301_test',
			'testcode::isMobile_test',
			'tov_test::redirect_offed_test',
			'_1c_test::data_gen_test',

		];
		test::run($tests);
	}
	public static function mob_url_test_1() {
		global_set ('basehref', 'http://mtddd.coo' );
		$REQUEST_URI = '';
		$result = mob::mob_url($basehref, $REQUEST_URI);
// если http без www - правильно
		if ($result === 'http://m.mtddd.coo') {
			return true;
		}
	}
	public static function mob_url_test_2()	{
		global_set ('basehref', 'http://www.mtddd.coo' );
		$REQUEST_URI = '';
		$result = mob::mob_url($basehref, $REQUEST_URI);
// если http www - правильно
		if ($result === 'http://m.mtddd.coo') {
			return true;
		}
	}
	public static function mob_url_test_3()	{
		global_set ('basehref', 'http://www.mtddd.coo' );
		$REQUEST_URI = '';
		$result = mob::mob_url($basehref, $REQUEST_URI);
// если http www - правильно
		if ($result === 'http://m.mtddd.coo') {
			return true;
		}
	}
	public static function mob_url_test_4()	{
		global_set ('protocol', 'https' );
		global_set ('basehref', 'http://www.mtddd.coo' );
		$REQUEST_URI = '/uuu';
		$result = mob::mob_url($basehref, $REQUEST_URI);
// если http www - правильно
		$result_true= 'https://m.mtddd.coo/uuu';
		if ($result === $result_true) {
			return true;
		} else {
			return $result.' ожидаемый результат '.$result_true;
		}
	}
	public static function loc301_test_1()	{
		global_set ('is_testing', true);
		global_set ('protocol', 'https' );
		global_set ('basehref', 'http://www.mtddd.coo' );
		$loc= 'http://www.mtddd.coo/ttth';
		$result = fn('loc301',$loc);
		global_unset ();
// если http www - правилhttps://www.mtddd.coo/ttth
		$result_true= "Location: https://www.mtddd.coo/ttth";
		global_unset ();
		if ($result === $result_true) {
			return true;
		} else {
			return $result.' ожидаемый результат '.$result_true;
		}
	}
	public static function loc301_test_2()	{
		global_set ('is_testing', true);
		global_set ('protocol', 'http' );
		global_set ('basehref', 'http://www.mtddd.coo' );
		$loc= 'http://www.mtddd.coo/ttth';
		$result = loc301($loc);
		global_unset ();
// если http www - правилhttps://www.mtddd.coo/ttth
		$result_true= "Location: http://www.mtddd.coo/ttth";
		if ($result === $result_true) {
			return true;
		} else {
			return $result.' ожидаемый результат '.$result_true;

		}
	}
	public static function loc301_test_3()	{
		global_set('is_testing', true);
		global_set('protocol', 'https');
		global_set('basehref', 'www.mtddd.coo');
		$loc = 'http://www.mtddd.coo/ttth';
		$result = loc301($loc);
		global_unset();
// если http www - правилhttps://www.mtddd.coo/ttth
		$result_true = "Location: https://www.mtddd.coo/ttth";
		if ($result === $result_true) {
			return true;
		} else {
			return $result . ' ожидаемый результат ' . $result_true;

		}
	}
		public static function loc301_test_4()	{
			global_set ('is_testing', true);
			global_set ('protocol', 'https' );
			global_set ('basehref', 'mtddd.coo' );
		$loc= 'http://mtddd.coo/ttth';
		$result = loc301($loc);
			global_unset ();
// если http www - правилhttps://www.mtddd.coo/ttth
		$result_true= "Location: https://mtddd.coo/ttth";
		if ($result === $result_true) {
			return true;
		} else {
			return $result.' ожидаемый результат '.$result_true;

		}
	}
	public static function loc301_test_5()	{
		global_set ('is_testing', true);
		global_set ('protocol', 'https' );
		global_set ('basehref', 'http://mtddd.coo' );
		$loc= '/ttth';
		$result = loc301($loc);
		global_unset ();
// если http www - правилhttps://www.mtddd.coo/ttth
		$result_true= "Location: https://mtddd.coo/ttth";
		if ($result === $result_true) {
			return true;
		} else {
			return $result.' ожидаемый результат '.$result_true;

		}
	}
	public static function loc301_test_6()	{
		global_set ('is_testing', true);
		global_set ('protocol', 'https' );
		global_set ('basehref', 'http://www.mtddd.coo' );
		$loc= '/ttth';
		$result = loc301($loc);
		global_unset ();
// если http www - правилhttps://www.mtddd.coo/ttth
		$result_true= "Location: https://www.mtddd.coo/ttth";
		if ($result === $result_true) {
			return true;
		} else {
			return $result.' ожидаемый результат '.$result_true;

		}
	}
	public static function isMobile_test_1()	{
		$useragents= [
			'avan',
			'mmp',
			'Apple',
			'Mozilla/5.0 (iPhone; U; CPU iPhone OS 5_1_1 like Mac OS X; en) AppleWebKit/534.46.0 (KHTML, like Gecko) CriOS/19.0.1084.60 Mobile/9B206 Safari/7534.48.3'
		];
		$errors='';
		foreach ($useragents as $useragent ) {
			if (mob::isMobile($useragent) !== true) {
				$errors.= 'not mob: '.$useragent.'<br>';
			}
		}
		return $errors ? : true;
	}

}
