<?php
/**
	Library CSSE - online CSS WYSIWYG Editor
	
	@file
		class CSSE_Request

	@version
		current version 0.9
		development started at 2009.11.20

	@license
		MIT License
		Copyright (c) 2009 Pavel Gudanets

		Permission is hereby granted, free of charge, to any person obtaining a copy
		of this software and associated documentation files (the "Software"), to deal
		in the Software without restriction, including without limitation the rights
		to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
		copies of the Software, and to permit persons to whom the Software is
		furnished to do so, subject to the following conditions:

		The above copyright notice and this permission notice shall be included in
		all copies or substantial portions of the Software.

		THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
		IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
		FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
		AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
		LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
		OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
		THE SOFTWARE.

	@status
		testing
	
*/



require(jscripts.'/csse/csse_tools.php');


class CSSE_Request
{

	static public function edit($css_filename)
	{
		$css_array = CSSE_Parser::get($css_filename);
		if ($css_array === false)
		{
			?><?=CSSE_Tools::text('Cannot_Find_File')?> <?=$css_filename?><?php
			return;
		}

		/**
			unique prefix for this form
		*/
		//static $postfix = 0;
		//$postfix += 1;

		{
			//CSSE_Tools::js('jquery/jquery.min');
			//CSSE_Tools::js('jquery/jquery.form');
			//CSSE_Tools::js('jquery/jquery.ui.min');	// accordion, draggable
			//CSSE_Tools::js('jquery/jquery.maskedinput');

			//CSSE_Tools::js('csse');
			//CSSE_Tools::js('csse_fields');

			CSSE_Tools::css('csse');
		}

		?><div class="csse">
		<a class="csse_close" onclick="$('#se_window').toggleClass('se_show')"><img src="/<?=jscripts;?>/tpladmin/close.png" /></a>
		<form action="/myadmin.php?mode=csse_update" method="POST" name="csse_form" id="csse_form" onsubmit="return false">

		<div class="header">
			<?=CSSE_Tools::text('CSS_Editor')?>
		</div>

		<div class="msg" id="csse_msg"></div>
		<table ><tr>
		<td class="tab_list"><?php

		$tab_id = 0;
		foreach ($css_array as $selector => $rule)
		{
			$tab_id += 1;
			?><a class="tab_button" id="csse_tab_button<?=$tab_id?>" href="#" onClick="javascript:CSSE_Tools.show_tab(<?=$tab_id?>); return false"><?=CSSE_Tools::text( $rule[0] )?></a><?php
		}
		
		?></td>
		<td class="property_list"><?php

		$tab_id = 0;
		foreach ($css_array as $selector => $rule)
		{
			$tab_id += 1;
			
			//
			$propertys=explode(' ', 'background-color color width height padding padding-left padding-right padding-top padding-bottom margin margin-top margin-bottom margin-left margin-right border-width border-style border-color' );
			foreach ( $propertys as $pr )
				{
					if ( !isset( $rule[1][$pr] ) ) $rule[1][$pr]='';
				}
			
			
			?><div class="tab_content" id="csse_tab<?=$tab_id?>" style="display:none"><?php
			
			foreach ($rule[1] as $property => $value)
			{
				CSSE_Tools::get_type_object($property)->show($selector, $property, $value);
			}
			
			?></div><?php
		}

		?>
		</td></tr></table>
		<div class="clr"></div>
</form>
		<div class="submit">
			<input type="submit" onclick="CSSE_save()" name="csse_go" value="<?=CSSE_Tools::text('Save')?>">
		</div>

		
		</div>
		</script><?php
	}

}


?>