<?php
/**
	Library CSSE - online CSS WYSIWYG Editor
	
	@file
		class CSSE_Response

	@version
		current version 0.9
		development started at 2009.11.20

	@license
		MIT License
		Copyright (c) 2009 Pavel Gudanets

		Permission is hereby granted, free of charge, to any person obtaining a copy
		of this software and associated documentation files (the "Software"), to deal
		in the Software without restriction, including without limitation the rights
		to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
		copies of the Software, and to permit persons to whom the Software is
		furnished to do so, subject to the following conditions:

		The above copyright notice and this permission notice shall be included in
		all copies or substantial portions of the Software.

		THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
		IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
		FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
		AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
		LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
		OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
		THE SOFTWARE.

	@status
		testing
	
*/



require(jscripts.'/csse/csse_tools.php');


class CSSE_Response
{
	/**
		process received form
		@param $css_filename - corresponding CSS file name to process
		@return $response - json encoded response
			response format:
			array(
				'msg' - message to show (error)
				//'error' - if any error
			)
	*/
	static public function process($css_filename)
	{
		$response = array();

		$css_array = CSSE_Parser::get($css_filename);
		if ($css_array === false)
		{
			$response['msg'] = CSSE_Tools::text('Cannot_Find_File');
			return json_encode($response);
		}
		//var_dump($css_array );
		
		
		/*["body"]=>
		  array(2) {
		    [0]=>
		    string(4) "body"
		    [1]=>
		    array(2) {
		      ["color"]=>
		      string(7) "#ab2bab"
		      ["margin"]=>
		      string(3) "1px"
		    }
		  }*/
		foreach ($css_array as $selector => $rule)
		{
			if (!isset($rule[1]['background-color'] ) ) $rule[1]['background-color']=''; 
			foreach ($rule[1] as $property => $value)
			{
				
				$result = CSSE_Tools::get_type_object($property)->process($selector, $property);
				
				//unset($result2);
				//$result2[$property]=$value;
				
				//if ($result === null)
				//	continue;
				//if ($result === false)
				//	continue;
				//var_dump($result);var_dump($result2);
				
				if (is_array($result))
				{
					foreach ($result as $key => $value)
						$css_array[$selector][1][$key] = $value;
				}
			}
		}
		//return json_encode($css_array);

		$res = CSSE_Parser::file_set($css_filename, $css_array);
		if ($res === false)
		{
			$response['msg'] = CSSE_Tools::text('File_Write_Error');
			return json_encode($response);
		}

		$response['msg'] = CSSE_Tools::text('Form_Processed_Ok');
		return json_encode($response);
	}
}


?>