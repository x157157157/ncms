<?php
/**
	Library CSSE - online CSS WYSIWYG Editor
	
	@file
		class CSSE_Parser

	@version
		current version 0.9
		development started at 2009.11.20

	@license
		MIT License
		Copyright (c) 2009 Pavel Gudanets

		Permission is hereby granted, free of charge, to any person obtaining a copy
		of this software and associated documentation files (the "Software"), to deal
		in the Software without restriction, including without limitation the rights
		to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
		copies of the Software, and to permit persons to whom the Software is
		furnished to do so, subject to the following conditions:

		The above copyright notice and this permission notice shall be included in
		all copies or substantial portions of the Software.

		THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
		IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
		FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
		AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
		LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
		OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
		THE SOFTWARE.

	@status
		testing
	
*/



define('CSSE_CACHE_PATH', 'cont/cache/');


class CSSE_Parser
{
	/**
		get CSS - parsed from file or cached
		@param $css_filename - css filename
	*/
	static public function get($css_filename)
	{
		if (CSSE_CACHE_ON)
		{
			$css = self::cache_get($css_filename);
			if ($css === false)
			{
				$css = self::file_get($css_filename);
				if ($css)
					self::cache_set($css_filename, $css);
			}
			return $css;
		}
		else
		{
			return self::file_get($css_filename);
		}
	}

	/**
		create parsed CSS array from CSS file
		can use it if you do not want use caching
		@param $css_filename - css filename
		@return $css_array - parsed css array
	*/
	static public function file_get($css_filename)
	{
		if (!is_file($css_filename))
			return false;

		$css_str = file_get_contents($css_filename);
		$css_array = array();

	//	preg_match_all('/(?ims)(\s*\/\*(.*)\*\/)?\s*([a-z0-9\s\.\:#_\-@]+)\{\s*([^\}]*)\}/', $css_str, $rules, PREG_SET_ORDER);
	//	preg_match_all('/(\s*\/\*(.*)\*\/)?\s*([a-z0-9\s\.\:#_\-@]+)\{\s*([^\}]*)\}/i', $css_str, $rules, PREG_SET_ORDER);
		preg_match_all('/(?:\s*\/\*((?:.|\n|\r)*?)\*\/)?\s*([a-z0-9\s\.\:#_\-@]+)\{\s*([^\}]*)\}/i', $css_str, $rules, PREG_SET_ORDER);

		foreach ($rules as $rule)
		{
			// selector
			$selector = trim( $rule[2] );

			// title (comment /* title */ before rule OR selector is used)
			$title = trim( $rule[1] );
			if (!$title)
				$title = $selector;

			// values
			$values1 = explode(';', $rule[3]);
			$values = array();
			foreach ($values1 as $value)
			{
				$value = explode(':', $value);
				if (!isset($value[1]))
					continue;
				$value[0] = strtolower( trim($value[0]) );
				$value[1] = trim($value[1]);
				if ($value[0] && $value[1])
					$values[ $value[0] ] = $value[1];
			}

			// generate
			$css_array[ $selector ] = array(
				0 => $title
				,1 => $values
			);
		}

		//d($css_str);d($rules);d($css_array);die;
		
		return $css_array;
	}

	/**
		create CSS file from parsed CSS array
		@param $css_filename - css filename
		@param $css_array - parsed css array
	*/
	static public function file_set($css_filename, &$css_array)
	{
		$delim = "
";

		$css_str = '';
		foreach ($css_array as $selector => $rule)
		{
			if ($rule[0] != $selector)
				$css_str .= "/* {$rule[0]} */ ".$delim;

			$css_str .= "{$selector} {".$delim;

			foreach ($rule[1] as $property => $value)
			{
				$css_str .= "	{$property}: {$value};".$delim;
			}

			$css_str .= "}".$delim.$delim;
		}

		$res = file_put_contents($css_filename, $css_str);

		if (CSSE_CACHE_ON)
			self::cache_set($css_filename, $css_array);

		return $res;
	}

	/**
		get parsed CSS from cache
	*/
	static private function cache_get($css_filename)
	{
		$css_filename = str_replace(array('/','\\','.'), '__', $css_filename);
		if (file_exists(CSSE_CACHE_PATH.$css_filename.'.tmp'))
		{
			require(CSSE_CACHE_PATH.$css_filename.'.tmp');
			return $css_array;
		}
		return false;
	}

	/**
		set parsed CSS to cache
	*/
	static private function cache_set($css_filename, &$css_array)
	{
		$css_filename = str_replace(array('/','\\','.'), '__', $css_filename);
		return file_put_contents(CSSE_CACHE_PATH.$css_filename.'.tmp', '<?php $css_array = '.self::php_value_get($css_array).'; ?>' );
	}

	/**
		create PHP variable string dump - for caching
		@param $data - value
		@return $str - valid PHP code, declaring this value
	*/
	static private function php_value_get($data, $offset='')
	{
		if (is_string($data))
			return "'" . str_replace("'", "\'", $data) . "'";

		if (is_numeric($data))
			return $data;

		if (is_object($data))
			$data = get_object_vars($data);	// object -> to array

		if (is_array($data))
		{
			$delim = "
";
			$str = '';
			foreach ($data as $k => $v)
			{
				if ($str)
					$str .= ',';
				$str .= $delim.$offset . self::php_value_get($k) . ' => ' . self::php_value_get($v, $offset."   ");
			}
			return $delim.$offset . 'array(' . $str . $delim.$offset . ')' . $delim;
		}

		if (is_bool($data))
		{
			return $data ? 'true' : 'false';
		}

		if ($data === null)
			return 'null';

		//class JcacheError {}
		return 'null'; // 'new JCacheError()';
	}

}

/*
$css_array = CSSE_Parser::get('css/edit.css');
d($css_array, 'css/edit.css');

CSSE_Parser::file_set('css/edit.css', $css_array);
*/

?>