<?php
if (defined('CSSE_PATH')) return;
/**
	Library CSSE - online CSS WYSIWYG Editor
	
	@file
		configuration
		class CSSE_Tools

	@version
		current version 0.9
		development started at 2009.11.20

	@license
		MIT License
		Copyright (c) 2009 Pavel Gudanets

		Permission is hereby granted, free of charge, to any person obtaining a copy
		of this software and associated documentation files (the "Software"), to deal
		in the Software without restriction, including without limitation the rights
		to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
		copies of the Software, and to permit persons to whom the Software is
		furnished to do so, subject to the following conditions:

		The above copyright notice and this permission notice shall be included in
		all copies or substantial portions of the Software.

		THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
		IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
		FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
		AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
		LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
		OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
		THE SOFTWARE.

	@usage

		require('csse/csse_request.php');
		CSSE_Request::edit('css/edit.css'); 

	@status
		testing
	
*/


/**
	EDIT
	Base URL
	mandatory: trailing /
	example: '/', '/subdir/'
*/
define('CSSE_BASE_URL', '/');


/**
	EDIT
	Path to JavaScript, CSS, standard image files and user uploaded image files
	URL to resources will be CSSE_BASE_URL.CSSE_RES_PATH
	mandatory: trailing /
	example: 'csse/res/'
*/
define('CSSE_RES_PATH', jscripts.'/csse/res/');


/**
	root directory for all CSSE scripts
*/
define('CSSE_PATH', dirname(__FILE__).'/');


/**
	CSS caching is on/off
*/
define('CSSE_CACHE_ON', false);



require(jscripts.'/csse/lang/en.php');		// constants
require(jscripts.'/csse/csse_parser.php');	// parser


/**
	Debug
*/
function d($value)
{
	echo '<pre>';
	print_r($value);
	echo '</pre>';
}


/**
	CSSE_Tools
*/
class CSSE_Tools
{

	/**
		map CSS property name to it's type

		@param $property - CSS property name
		@return $type - type of the CSS property
	*/
	static public function get_type_name($property)
	{
		static $properties = array(

			// Color

			'color' => 1
			,'background-color' => 1
			,'border-color' => 1

			,'border-left-color' => 1
			,'border-right-color' => 1
			,'border-top-color' => 1
			,'border-bottom-color' => 1

			// Num

			,'width' => 2
			,'height' => 2

			,'left' => 2
			,'right' => 2
			,'top' => 2
			,'bottom' => 2

			,'font-size' => 2

			,'margin' => 2
			,'margin-left' => 2
			,'margin-right' => 2
			,'margin-top' => 2
			,'margin-bottom' => 2

			,'padding' => 2
			,'padding-left' => 2
			,'padding-right' => 2
			,'padding-top' => 2
			,'padding-bottom' => 2

			,'border-width' => 2
			,'border-left-width' => 2
			,'border-right-width' => 2
			,'border-top-width' => 2
			,'border-bottom-width' => 2

			// Position

			,'position' => 3

			// BorderStyle

			,'border-style' => 4
			,'border-left-style' => 4
			,'border-right-style' => 4
			,'border-top-style' => 4
			,'border-bottom-style' => 4

			,'overflow' => 5

			,'background-image' => 6
		);

		static $types = array(
			 1 => 'Color'
			,2 => 'Num'
			,3 => 'Position'
			,4 => 'BorderStyle'
			,5 => 'Overflow'
			,6 => 'BackgroundImage'
		);
		
		if (isset($properties[$property]) && isset($types[$properties[$property]]))
			return $types[$properties[$property]];
		else
			return 'Unknown';
	}

	/**
		@param $property - CSS property name
		@return $object - instance of the CSS property controller
	*/
	static public function get_type_object($property)
	{
		$type = self::get_type_name($property);
		
		$class_name = 'CSSE_Field_'.$type;

		if (!class_exists($class_name))
		{
			$file_name = CSSE_PATH.'fields/'.$type.'.php';

			if (!file_exists($file_name))
			{
				return false;
			}

			require($file_name);

			if (!class_exists($class_name))
			{
				return false;
			}
		}

		return new $class_name();
	}

	/**
		get text
		the simplies implementation - return the same text as received
		if you need multilanguage, you can change this method and use $text_id as constant name or array key or something else

		@param $text_id - text id
		@return $text - text string
	*/
	static public function text($text_id)
	{
		if (defined($text_id))
			return constant($text_id);
		return $text_id;
	}

	/**
		get property title
		@param $property - CSS property name
		@return $title - title of the CSS property
	*/
	static public function get_property_title($property)
	{
		return $property;
	}

	/**
		get (and process, if needed) POSTed variable name
	*/
	static public function post_name($selector, $property)
	{
		return 'csse'.md5($selector.'_'.$property);
	}

	/**
		get (and process, if needed) POSTed variable value
	*/
	static public function post_value($selector, $property, $default_value=null)
	{
		$name = 'csse'.md5($selector.'_'.$property);
		if (isset($_POST[$name]))
			return $_POST[$name];
		return $default_value;
	}

	/**
		include JS file
		prevents multiple includings of one file
	*/
	static public function js($filename)
	{
		static $included = array();
		if (isset($included[$filename]))
			return;
		$included[$filename] = true;

		?><script type="text/javascript" src="<?=CSSE_BASE_URL.CSSE_RES_PATH.$filename?>.js"></script><?php
	}

	/**
		include CSS file
		prevents multiple includings of one file
	*/
	static public function css($filename)
	{
		static $included = array();
		if (isset($included[$filename]))
			return;
		$included[$filename] = true;
		
		?><link rel="stylesheet" href="<?=filemtime_file(substr( CSSE_BASE_URL.CSSE_RES_PATH,1,1000 ).$filename.'.css');?>" type="text/css"><?php
	}

}




?>