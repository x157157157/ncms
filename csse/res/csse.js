/**
	Library CSSE - online CSS WYSIWYG Editor
	
	@file
		CSSE_Editor
		CSSE_Tools

	@version
		current version 0.9
		development started at 2009.11.20

	@license
		MIT License
		Copyright (c) 2009 Pavel Gudanets

		Permission is hereby granted, free of charge, to any person obtaining a copy
		of this software and associated documentation files (the "Software"), to deal
		in the Software without restriction, including without limitation the rights
		to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
		copies of the Software, and to permit persons to whom the Software is
		furnished to do so, subject to the following conditions:

		The above copyright notice and this permission notice shall be included in
		all copies or substantial portions of the Software.

		THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
		IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
		FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
		AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
		LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
		OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
		THE SOFTWARE.

	@status
		testing
	
*/

function CSSE_save()
	{
		CSSE_Tools.submit_form( $('#csse_form') );
		return false;
	}

function CSSE_Editor()
{
	this.css = null;
	this.cssRules = null;
	this.cssFilename = null;
	this.cssNode = null;

	this.select_style_sheet = function(cssFilename)
	{
		this.cssFilename = cssFilename;
		for (var i = 0; i < document.styleSheets.length; i++)
		{
			if (document.styleSheets.item)
				var styleSheet = document.styleSheets.item(i);
			else if (typeof document.styleSheets[i] == 'object')
				var styleSheet = document.styleSheets[i];
			else
				return false;

			if (!styleSheet.href && cssFilename)
				continue;

			if ((!styleSheet.href && !cssFilename) ||
				styleSheet.href.indexOf(cssFilename) != -1)
			{
				this.css = styleSheet;

				if (this.css.cssRules) // firefox, opera
					this.cssRules = this.css.cssRules;
				else if (this.css.rules) // ie6, ie7
					this.cssRules = this.css.rules;
				else
					return false;
				break;
			}
		}

		if (this.css == null)
			return false;
		this.cssNode = this.css.ownerNode || this.css.owningElement;
		return true;
	}

	/**
		find specific rule by its selector ('body', '.class_name', '#id_name' etc)
	*/
	this.get_rule = function(selector)
	{
		if (!this.cssRules)
			return false;

		for (var i = 0; i < this.cssRules.length; i++)
		{
			if (this.cssRules.item)
				var rule = this.cssRules.item(i);
			else if (typeof this.cssRules[i] == 'object')
				var rule = this.cssRules[i];

			if (typeof rule.selectorText == 'undefined')
				continue;
			if (rule.selectorText.toLowerCase() == selector.toLowerCase())
				return rule;
		}
		return null;
	}

	/**
		append new rule or find existed and return it
	*/
	this.init_rule = function(selector, declaration)
	{
		var rule = this.get_rule(selector);
		if (!rule)
		{
			this.append_rule(selector, declaration);
			rule = this.get_rule(selector);
		}

		return rule;
	}

	this.get_css_text = function()
	{
		if (!this.css)
			return false;

		// ie
		if (this.css.cssText)
			return this.css.cssText;

		// firefox
		var text = '';
		for (var i in this.cssRules)
		{
			if (typeof this.cssRules[ i ] != 'object')
				continue;
			text += this.cssRules[ i ].cssText + ' ';
		}
		return text;
	}

	this.init_param = function(obj, param)
	{
		if (obj.style[param])
			return obj.style[param];

		if (obj.className)
		{
			var rule = this.get_rule( '.' + obj.className );
			if (rule)
			{
				var value = rule.style[param];
				if (value)
					return value;
			}
		}

		var obj_id = obj.getAttribute('id');
		var rule = this.get_rule( '#' + obj_id );
		if (rule)
		{
			var value = rule.style[param];
			if (value)
				return value;
		}

		return '';
	}

	/**
		rewrite style parameter for the rule
	*/
	this.set_param = function(selector, css_property, value)
	{
		var rule = this.get_rule(selector);
		css_property = this.get_js_property(css_property);
		if (rule && css_property)
			rule.style[css_property] = value;
	}

	/**
		map CSS property name to JavaScript name
		@param css_property - CSS property name (font-weight)
		@param value - CSS property value (usedin some cases like text-decoration)
		@return js_property - JavaScript property name (fontWeight)
	*/
	this.get_js_property = function(css_property, value)
	{
		switch (css_property)
		{
			case 'float':
				return 'styleFloat';
			/*
			case 'text-decoration':
			break;
			*/
			default:
				var js_property = '';
				css_property = css_property.split('-');
				for (var i in css_property)
				{
					if (i == 0)
						js_property += css_property[i];
					else
						js_property += css_property[i].substr(0,1).toUpperCase() + css_property[i].substr(1);
				}
				//alert(js_property);
				return js_property;
		}
	}

}


var CSSE_Tools = {

	editor: null

	,init: function(css_filename)
	{
		this.editor = new CSSE_Editor();
		this.editor.select_style_sheet(css_filename);

		//$('#csse_form').submit(function(){ return CSSE_Tools.submit_form(this); });
		CSSE_Tools.show_tab(1);
		/*
		$('.csse .field').each(function() {
			$(this).focus(function(){
				var Interval = setInterval(function(){ });
			});
		});
		*/
	}

	,submit_form: function(form)
	{
		form.ajaxSubmit({
			url: form.attr('action'),
			success: function(json)
			{
				//alert(json);
				try { eval('var data = ' + json + ';'); } catch(e) { alert(json); }
				$('#csse_msg').html(data.msg);
			}
		});

		return false;
	}
	
	,current_tab_id: 0
	,show_tab: function(tab_id)
	{
		$('#csse_tab'+CSSE_Tools.current_tab_id).css('display', 'none');
		$('#csse_tab_button'+CSSE_Tools.current_tab_id).removeClass('active');
		$('#csse_tab'+tab_id).css('display', 'block');
		$('#csse_tab_button'+tab_id).addClass('active');
		CSSE_Tools.current_tab_id = tab_id;
	}
	
	/**
		open window
	*/
	,open_window: function(url, win_id, width, height)
	{
		window.open(url, win_id, 'width='+width+',height='+height );
	}

}