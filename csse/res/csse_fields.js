/**
	validate field and update CSS
*/
var CSSE_Field = {
	update: function(selector, property, value_name)
	{
		CSSE_Tools.editor.set_param(selector, property, document.csse_form[value_name].value);
	}
}


var CSSE_Field_Num = {
	update: function(selector, property, value_name, countin_name)
	{
		var value = parseFloat(document.csse_form[value_name].value);
		var countin = document.csse_form[countin_name].value;
		CSSE_Tools.editor.set_param(selector, property, value+countin);
	}
}

var CSSE_Field_Color = {
	update: function(selector, property, value_name)
	{
		CSSE_Tools.editor.set_param(selector, property, '#'+document.csse_form[value_name].value);
	}
}

