<?php

class CSSE_Field_Unknown
{
	/**
		show html to edit value
		@param $postfix - unique id of the form and fields in it
		@param $property - CSS property name
		@param $value - CSS property value from CSS
	*/
	function show($selector, $property, $value)
	{
		?><div class="title">
			<?=$property?>
		</div>
		<div class="value">
			<?=$value?>
		</div>
		<div class="clr"></div><?php
	}

	/**
		process POSTed value
		@param $postfix - unique id of the form and fields in it
		@param $property - CSS property name
		@return $values - array of valid CSS values or NULL (if nothing to add)
	*/
	function process($selector, $property)
	{
		return null;
	}
}

?>