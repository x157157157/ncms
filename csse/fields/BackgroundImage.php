<?php

class CSSE_Field_BackgroundImage
{
	function show($selector, $property, $value)
	{
		$value_name = CSSE_Tools::post_name($selector, $property);

		$js = "javascript:CSSE_Field_Background.update('$selector', '$property', '$value_name')";
		
		static $images_win_id = 0;
		$images_win_id += 1;

		?><div class="title">
			<?=CSSE_Tools::get_property_title( $property )?>
		</div>
		<div class="value">
			<a href="#" onClick="javascript:CSSE_Tools.open_window('<?=CSSE_BASE_URL?>csse_images.php', 'CSSE_images_win'+<?=$images_win_id?>, 300, 300)"><?=CSSE_Tools::text('Image_Gallery_Show')?></a>
		</div>
		<div class="clr"></div><?php
	}

	function process($selector, $property)
	{
		$value = CSSE_Tools::post_value($selector, $property);

		return array($property => $value);
	}

}

?>