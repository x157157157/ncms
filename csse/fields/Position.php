<?php

class CSSE_Field_Position
{
	var $options = array(
		'static' => 'default'
		,'relative' => 'relative'
		,'absolute' => 'absolute'
		,'fixed' => 'fixed'
	);

	function show($selector, $property, $value)
	{
		$value_name = CSSE_Tools::post_name($selector, $property);

		$js = "javascript:CSSE_Field.update('$selector', '$property', '$value_name')";

		?><div class="title">
			<?=CSSE_Tools::get_property_title( $property )?>
		</div>
		<div class="value">
			<select name="<?=$value_name?>" onChange="<?=$js?>"><?php
			foreach ($this->options as $option_value => $option_title)
			{
				?><option value="<?=$option_value?>"<?=$option_value==$value?' selected':''?>><?=CSSE_Tools::text($option_title)?></option><?php
			}
			?></select>
		</div>
		<div class="clr"></div><?php
	}

	function process($selector, $property)
	{
		$value = CSSE_Tools::post_value($selector, $property);

		if(!isset($this->options[$value]))
			$value = '';

		return array($property => $value);
	}
	
	function preprocess($value)
	{
		if (strpos($value, '#') === 0)
			return substr($value, 1);
		return $value;
	}
}

?>