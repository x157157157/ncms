<?php

class CSSE_Field_Num
{
	function show($selector, $property, $value)
	{
		$value_name = CSSE_Tools::post_name($selector, $property);
		$countin_name = CSSE_Tools::post_name($selector, $property.'countin');

		$js = "javascript:CSSE_Field_Num.update('$selector', '$property', '$value_name', '$countin_name')";

		?><div class="title">
			<?=CSSE_Tools::get_property_title( $property )?>
		</div>
		<div class="value">
			<input class="num" type="text" name="<?=$value_name?>" value="<?=floatval($value)?>" maxlength="10" onBlur="<?=$js?>" onKeyUp="<?=$js?>">
			
			<select name="<?=$countin_name?>" onChange="<?=$js?>">
				<option value="px"<?=(strpos($value, 'px')?' selected':'')?>>px</option>
				<option value="%"<?=(strpos($value, '%')?' selected':'')?>>%</option>
				<option value="pt"<?=(strpos($value, 'pt')?' selected':'')?>>pt</option>
				<option value="em"<?=(strpos($value, 'em')?' selected':'')?>>em</option>
			</select>
		</div>
		<div class="clr"></div><?php
	}

	function process($selector, $property)
	{
		$value = floatval( CSSE_Tools::post_value($selector, $property) );
		$countin = CSSE_Tools::post_value($selector, $property.'countin');

		if ($countin!='px' && $countin!='%' && $countin!='pt' && $countin!='em')
			$countin = 'px';

		return array($property => $value.$countin);
	}
}

?>