<?php

class CSSE_Field_Color
{
	function show($selector, $property, $value)
	{
		$value_name = CSSE_Tools::post_name($selector, $property);

		$js = "javascript:CSSE_Field_Color.update('$selector', '$property', '$value_name')";

		?><div class="title">
			<?=CSSE_Tools::get_property_title( $property )?>
		</div>
		<div class="value">
			<input type="text" id="<?=$value_name?>" name="<?=$value_name?>" value="<?=$this->preprocess($value)?>" maxlength="10" onKeyDown="<?=$js?>" onBlur="<?=$js?>">
<?php
	CSSE_Tools::js('colorpicker/colorpicker');
	CSSE_Tools::css('colorpicker/colorpicker');
?>
<script type="text/javascript"><!--
$(document).ready(function() {
$('#<?=$value_name?>').ColorPicker({
	onSubmit: function(hsb, hex, rgb, el) {
		$(el).val(hex).ColorPickerHide();
		CSSE_Field_Color.update('<?=$selector?>', '<?=$property?>', '<?=$value_name?>');
	}
	,onBeforeShow: function () {
		$(this).ColorPickerSetColor(this.value);
	}
	,onChange: function(hsb, hex)
	{
		$('#<?=$value_name?>').val(hex);
		CSSE_Field_Color.update('<?=$selector?>', '<?=$property?>', '<?=$value_name?>');
	}
})
.bind('keyup', function(){
	$(this).ColorPickerSetColor(this.value);
});
});
// -->
</script>
		</div>
		<div class="clr"></div><?php
	}

	function process($selector, $property)
	{
		$value = CSSE_Tools::post_value($selector, $property);

		if(!preg_match('/^[a-f0-9]{6}$/i', $value))
			$value = 'ffffff';

		return array($property => '#'.$value);
	}
	
	function preprocess($value)
	{
		if (strpos($value, '#') === 0)
			return substr($value, 1);
		return $value;
	}
}

?>