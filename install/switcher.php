<?php

$is_test = $_GET['is_test'] ? : $_COOKIE['is_test'];

if ( $is_test AND file_exists('core_'.$is_test) ) {

    define('jscripts','core_'.$is_test.'/vendor/giperplan/ncms');
    define('core',jscripts);
	define('local_core','core_'.$is_test);

} else {

    //define('jscripts','jscripts');
	//define('local_core','local_core');

    define('jscripts','core/vendor/giperplan/ncms');
    define('core',jscripts);
	define('local_core','core');

}