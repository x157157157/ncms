<?php

// check mysets.php
include 'funs.php';
include '../../../autoload.php';


 define( 'ROOT',  $_SERVER['DOCUMENT_ROOT'] ) ;

if ( file_exists(ROOT.'/mysets.php') ) { // stop install

    include ROOT.'/mysets.php';

    echo '<a href="'.$basehref.'/myadmin.php?login='.$adminlogin.'&pass='.$adminpass.'">NEXT...</a><br>';

} else {

    if ( $_POST['admin_login'] ) { // save adm install
        $result= save_mysets($_POST);
        if ( $result === true ) {
            echo 'mysets_saved!';
        } else {
            $error = $result;
            include ('install.form.php'); // show form
        };

    } else {
        include ('install.form.php'); // show form
    }
}


copy_safe('cont', ROOT.'/cont' );
copy_safe('index.php', ROOT.'/index.php' );
copy_safe('myadmin.php', ROOT.'/myadmin.php' );
copy_safe('switcher.php', ROOT.'/switcher.php' );
copy_safe('.htaccess', ROOT.'/.htaccess' );



