<?php
function text ( $field, $name, $value=false ) {
    $form=compact('field', 'name', 'value' );
    include 'install.field.php';
}

function save_mysets ($post) {

    $data= file_get_contents('mysets.php');
    foreach ($post as $k=>$v) {
        $data=str_replace('{'.$k.'}', $v, $data );
    }
    
    if ( !check_mysql($post) ) {
        return ' mysql error ';
    }
    
    if (  !file_put_contents( ROOT.'/mysets.php', $data ) ) {
            return 'mysets.php not saved!';
        };
    
    return true;
}

function check_mysql ($post) {
    
    $GLOBALS['db_link'] = mysqli_connect($post['mysql_host'] , $post['mysql_user'], $post['mysql_pass'], $post['mysql_base'], $post['mysql_port'] );
    
    return mysqli_stat( $GLOBALS['db_link'] ) ;
    
}


function rand_string( $length ) {

$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
return substr(str_shuffle($chars),0,$length);

}


function copy_dir($src, $dst) {  
  
    // open the source directory 
    $dir = opendir($src);  
  
    // Make the destination directory if not exist 
    @mkdir($dst);  
  
    // Loop through the files in source directory 
    while( $file = readdir($dir) ) {  
  
        if (( $file != '.' ) && ( $file != '..' )) {  
            if ( is_dir($src . '/' . $file) )  
            {  
  
                // Recursively calling custom copy function 
                // for sub directory  
                copy_dir($src . '/' . $file, $dst . '/' . $file);
  
            }  
            else {  
                copy($src . '/' . $file, $dst . '/' . $file);  
            }  
        }  
    }  
  
    closedir($dir); 
}  


function copy_safe ( $sourse, $destination  ) {
    if ( file_exists($destination) ) {
        echo $destination.' alraedy exist!<br>';
    } else {
        if (is_file($sourse)) {
            copy ( $sourse, $destination );
        } else {
            copy_dir ( $sourse, $destination );
        }
        echo $sourse.' copied!<br>';
    }  
} 
 
  
