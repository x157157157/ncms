 <table class='order_info table'>
							<tr>
								<th colspan='2' >Данные о заказе № {{$mm_htm['id']}} :</th>
								
							</tr>
							<tr>
								<td class='col1'>Сумма:</td>
								<td  class='col2'>{{price2html($mm_htm['cur_summ'])}} [val]</td>
							</tr>
							<tr>
								<td class='col1'>Дата заказа:</td>
								<td class='col2'>{{ date( 'd.m.Y' , $mm_htm['ordertime']   ) }}</td>
							</tr>
							
							<tr>
								<td class='col1'>Адрес доставки:</td>
								<td class='col2'>{{implode_notnul(', ', array( $mm_htm['country'], $mm_htm['postindex'],  $mm_htm['region'], $mm_htm['city'], $mm_htm['adress']) ) }}</td>
							</tr>
							<tr>
								<td class='col1'>Получатель:</td>
								<td class='col2'>{{ implode_notnul(', ', array( $mm_htm['orgname'] , $mm_htm['family'].' '.$mm_htm['name'].' '.$mm_htm['patronymic'] )) }}</td>
							</tr>
							<tr>
								<td class='col1'>Телефон:</td>
								<td class='col2'>{$mm_htm['phone']}</td>
							</tr>
						</table>