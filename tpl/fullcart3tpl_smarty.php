<div id="outfullcart">
	<table class="ramka fullcart" width="100%" >


		  <tr>
				<th class="name" >Название</th>
				<th width="60" align="center"><?=( o('cart-artname') ? o('cart-artname') : 'Артикул'  );  ?></th>
				<? if ( o('codtov_on') ) { ?><th width="70" align="center" valign="middle">Код товара</th><? } ?>
				<? if ( o('weight_on') ) { ?><th width="60" align="center" valign="middle">Вес</th><? } ?>
				<? if ( !o('cart-priceoff') ) { ?><th width="60" align="center" valign="middle">Цена</th><? } ?>
				
				<th width="76" align="center">Кол-во</th>		
				<? if ( !o('cart-priceoff') ) { ?><th width="70" align="center" valign="middle">Сумма</th><? } ?>
				<th width="30" align="center"> </th>		
				
		   </tr>
		   <? foreach ( $tovs as $mm ) { ?>
			  <tr id="tov{{ $mm['id'] }}">
					<td  align="left" valign="middle" class="foncart">
						<table width="100%">	
							<tr>
								<td class="fullcart_img" ><img src="{{ $mm['img'] }}"  style="margin:5px 15px 5px 5px; width: 64px;"  /></td>
								<td>
									<a href="{{ $mm['href'] }}">{{ $mm['name'] }}</a>
									<div>{{ $mm['desc'] }}</div>
									<div class="cart_desc">{{ $mm['cart_desc'] }}</div>
								</td>
							</tr>
						</table>
					</td>
					<td align="center">{{ $mm['idart'] }}</td>
					<? if ( o('codtov_on') ) { ?><td align="center">{{ $mm['codtov'] }}</td><? } ?>
					<? if ( o('weight_on') ) { ?><td align="center">{{ $mm['weight'] }} кг</td><? } ?>
					<? if ( !o('cart-priceoff') ) { ?> 
						<td  align="center" valign="middle" class="foncart">{{ $mm['price'] }}&nbsp;[val]</td> 
					<? } ?>
					<td align="center" valign="middle" class="foncart">
						<div class="num_tov_out" >
							<input  name="maxvalue[{{ $mm['id'] }}]" class="maxvalue" type="hidden" value="{{ $mm['maxvalue'] }}" />
							<input  name="num_tov[{{ $mm['id'] }}]" type="text" class="num_tov"  min="{{ $mm['min_val'] }}" value="{{ $mm['num'] }}" />
							<? if( o('tov_num_up_down') )  { ?>
								<span class="chnum chnum_up" onclick="tov_num_change(this,1)" >+</span>
								<span class="chnum chnum_down" onclick="tov_num_change(this,-1)" >-</span>&nbsp;&nbsp;
							<? } ?>&nbsp;шт.  
						</div>
					</td>
					<? if ( !o('cart-priceoff') ) { ?> 
						<td  align="center" valign="middle" class="foncart">{{ $mm['price_all_tov'] }} [val]</td> 
					<? } ?>
					<td align="center">
						<a href="javascript:deltovcart('{{ $mm['id'] }}')" class="deltovbut" title="Удалить товар из корзины"></a>
					</td>
			  </tr>
		  <? } ?>
		 
	</table>

	<div class="cartit {{ ( $is_pricedost ? 'pricedost_on' : '' )  }}" >
		<div class="cartit_summ">
			<? if ( !o('cart-priceoff') ) { ?>
				
				<? if(!$is_pricedost ) { ?>
					<b>Итого: {{ $fprice }}&nbsp;[val] </b> 
				<? } else { ?>
					Стоимость товара: {{ $fprice }}&nbsp;[val]
				<? } ?>
					
				<br/>
				<? if(!$is_pricedost) { ?>
					<small>(без учёта доставки)</small>
				<? } ?>
				
				<? if($is_pricedost) { ?>
					<div class="pricedost">
						<?  if ( $spdost=='self' ) { ?>
							Самовывоз: БЕСПЛАТНО							
						<? } else if ($pricedost) { ?>
							Стоимость доставки: {{ $pricedost }}&nbsp;[val]
						<? } else { ?>
							Стоимость доставки: БЕСПЛАТНО							
						<? } ?>
					</div>
					<div class="price_itogo">
						<b>Итого к оплате: {{ $price_all }}&nbsp;[val]</b>
					</div>
				<? } ?>
				<div class="cartit_weight">
					<? if(o('weight_on') ) { ?>
						Вес: {{ $weight_all }} кг
					<? } ?>
				</div>
			<? } ?>
		</div>
		
			
	</div>
</div>

  