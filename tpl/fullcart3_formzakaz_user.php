<div class="fullcart3_formzakaz_user">
		<? if (o('is_cart_country')) { ?>
			 
			<h2 class="cart_country_h2">Выберите страну:</h2>
			
			{{form('cart_country')}}
		
			
		<? } ?>
		
		
		{{form('spdost_sel')}}

	
		 <? if (
			 	o('is_adresses_last') // если включено отображение ранее введённых адресов
					AND
				$p1['adresses'] = fn('cabinet::adresses_arr') ) // если текущего пользователя есть раннее введённые адреса
		 { ?>
		 	<div class="obform  form_hidden" group="vl('spdost')!=='self';dp='spdost;'" rel="rad">
				<h2>Выберите адрес доставки:</h2>
			
				{{smarty_tpl('cart_adresses', $p1)}}
			</div>
		<? } else { ?>
			
			<div class="obform  form_hidden" group="vl('spdost')!=='self';dp='spdost;'" rel="rad">
				<h2 class="cart_dost_data_h2">Укажите данные для доставки:</h2>
			</div>
			
			<div class="obform  form_hidden" group="vl('spdost')=='self';dp='spdost;'" rel="rad">
				<h2 class="cart_user_data_h2">Укажите данные о получателе:</h2>
			</div>
			
			{{form('adress-form')}}
			
			<? // тут мы будем выводить ВСЕ введённые ранее адреса
			 ?>
			 <script id="adresses_json">

				 adresses = {{ json_encode (cabinet::adresses_arr()) }};

			 </script>


			 <?
			 	//todo надо учесть вариант, когда способ оплаты или доставки поменяли, и нужно делать подстановку нового значения, чуть позже
			 	// todo придётся заглядывать в последний заказ чела, чтобы понять способ доставки, оплаты, и страну, т.к эти поля небыли сохранены
			 ?>
			
		
		<? } ?>
		
		<div class="obform  hidden" group="vl('spdost')!=='self';dp='spdost;'" rel="rad">
				
		{{form('adv-form-dop')}}
		
		</div>
	
	
	<? if ( !uid() ) { ?>
		{{form('email_tel')}}
	<? } ?>	
	
	<? if ( qvar('ident', 'sposob-oplatyi', 'ok') ) { ?>	
		<h2 class="cart_sposopl_h2">Выберите способ оплаты:</h2>
			{{form('sposob-oplatyi')}}
	<? } ?>
		
	<h2 class="cart_comment_h2">Комментарий к заказу:</h2>
		
		{{form('form_comment')}}
		
	<center>
		{{ibut('gozakaz')}} 
		<? if (inadm() AND rg(9) ) {?> 
			<label>
				<input type="checkbox" name="debug" <?=$_POST['debug']? ' checked="checked" ' : ''  ?> /> debug
			</label>	
		<? } ?>
	</center>
	
</div>