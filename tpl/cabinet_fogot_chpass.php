<form action="/fogotpass" method="POST">
	<input type="hidden" name="id" value="{{ $u['id'] }}">
	<input type="hidden" name="crc" value="{{ $crc }}">
	<table class="fogotpass-newpass">
		<tr>
			<td>
				{{ form('fogotpass-newpass') }}		
			</td>
		</tr>
		<tr>
			<td align="center" class="save_but_out">  {{ ibut('save_but' ) }} </td>
		</tr>
	</table>
</form>