<? if ($role=='out_1' ) { ?>

    <input type="{{$t}}" name="{{$name}}" id="{{$name}}" value="{{htmlspecialchars($GLOBALS[$name])}}" style='width:300px' />

<? } else if ($role=='out_2') { ?>

    <table align="center">
        <tr>
            <td align="right" width="50%">
                {{$add3}}:
            </td>
            <td align="left" valign="midle" width="50%">
                {{$out}}
            </td>
        </tr>
    </table>

<? } else if ($role=='out_3') { ?>

    <table  width="{{$width_all}}">
        <tr>
            <td align="right" width="{{$width_name}}" class="namecheck">
                <label for="{{$name}}">
                    {{ $add3.helpop($mm).formprice($mm['d12']).$sep}}
                </label>
            </td>
            <td  align="left" class="inputcheck">
                {{($out)}}
            </td>
        </tr>
    </table>

<? } else if ($role=='out_4') { ?>

    <table>
        <tr>
            <td align="left" colspan="2">
                {{$out}}
                <label id="{{$add}}">
                    - {{$add3}}
                </label>
            </td>
        </tr>
    </table>

<? } else if ($role=='out_5') { ?>

    <table>
        <tr>
            <td align="center" colspan="2" >
                <b>
                    {{$name}}
                </b>
            </td>
        </tr>
    </table>

<? } else if ($role=='out_6') { ?>

    <table>
        <tr>
            <td align="right">
                {{$out}}
            </td>
            <td align="left">
                - {{$add3}}
            </td>
        </tr>
    </table>

<? } else if ($role=='out_7') { ?>

    <table>
        <tr>
            <td align="right">
                {{$out}}
            </td>
            <td align="left">
                - {{$add3}}
            </td>
        </tr>
    </table>

<? } else if ($role=='out_8') { ?>

    <input type="file" <?=($mm['d7'] ? 'multiple class="multiple"' : '') ?> style='width:100%;' name='{{$name}}' {{$ch}} {{$add}} />

<? } else if ($role=='out_9') { ?>

    <table width="100%">
        <tr>
            <td align="center" class="input_file_name">
                {{$add3}}: 
            </td>
        </tr>
        <tr>
            <td align="left"   class="input_file">
                {{$out}}
            </td>
        </tr>
    </table>

<? } else if ($role=='out_10') { ?>

    <table>
        <tr>
            <td align="right" width="50%">
                {{$add3}}: 
            </td>
            <td align="left" width="50%">
                {{$out}}
            </td>
        </tr>
    </table>

<? } ?>