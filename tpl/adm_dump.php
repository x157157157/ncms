<script>
function dump_restore(dump) {
	
	confirm('Заменить текущую базу данным дампом?');
	$('.status').addClass('disabled');
	
	$.post('/myadmin.php?&act=dump::restore', { dump: dump }, function (data) {
		
		$('.status').removeClass('disabled');
		
		res=data.split('[data]');
		
		if ( res[1]=='ok' ) {
			//dump_updatelist();
			$('.status').append('<div class="red">Дамп восстановлен!</div>');
			$('.status .red').fadeOut(5000);
		} else {
			alert(res[1]);
		}
	});
}
function dump_del(dump) {
	confirm('Удалить данный дамп?');
	$('.status').animate({opacity:0.3}, 400 );
	
	$.post('/myadmin.php?&act=dump::delete', { dump: dump }, function (data) {
		
		$('.status').animate({opacity:1}, 400 );
		
		res=data.split('[data]');
		
		if ( res[1]=='ok' ) {
			//dump_updatelist();
			$('.status').html(res[2]);
		} else {
			alert(res[1]);
		}
	});
}
function dump_create() {
	
	$('.dump_create').addClass('disabled');
	
	$.post('/myadmin.php?&act=dump::create', { }, function (data) {
		
		$('.dump_create').removeClass('disabled');
		
		res=data.split('[data]');
		
		if ( res[1]=='ok' ) {
			//dump_updatelist();
			$('.status').html(res[2]);
		} else {
			alert(res[1]);
		}
	});
}
</script>
<a class="dump_create pointer" onclick="dump_create()" >Создать дамп</a>
<div class="status" >
	{{ dump::files() }}
</div>
