{{ o('doctype') ? :  '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">'  }}
<html xmlns="http://www.w3.org/1999/xhtml" id="<?=$GLOBALS['mbod']['ident']; ?>" {{ o('html_tag_property')  }} >
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<title>[title]</title>
		<!--<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />-->
		<? if (! o('keywords_off') ) { ?><meta name="keywords" content="[keyw]" /><? } ?>
		<meta name="description" content="[desc]" />
		

		<? if(  o('redirect_to_base_href')  AND $GLOBALS['site']['d6'] == 'y' ){
			?><meta name='robots' content='noindex,nofollow' />  <?
		} ?>

		<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
		
		<script src="<?=o('jquery_url')? o('jquery_url') : '/'.jscripts.'/lib/jquery-1.5.min.js'; ?>"  charset="utf-8" type="text/javascript"></script>
		
		<? if ( o('jquery_noconflict') )  { ?> 
			<script src="<?=o('jquery_noconflict'); ?>"  charset="utf-8" type="text/javascript"></script>
			<script type="text/javascript">jq2 = jQuery.noConflict(true); </script>
		<? } ?>
		<script src="/<?=jscripts; ?>/lib/jquery-ui.js"  charset="utf-8" type="text/javascript"></script>
		<script src="/<?=jscripts; ?>/plug.js?<?=filemtime(jscripts.'/plug.js'); ?>"  charset="utf-8" type="text/javascript"></script>
		<? if  (inadm()) { ?><script src="/<?=jscripts; ?>/css_edit.js?<?=filemtime(jscripts.'/css_edit.js'); ?>"  charset="utf-8" type="text/javascript"></script><? } ?>
		
		<base href="<?=basehref; ?>/"/>
		
		
		<? if ( inadm() ) { ?>
			<link href="/<?=filemtime_file('cont/css/'.local_core.'_all_admin.css'); ?>" rel="stylesheet" type="text/css"/>
		<? } else { ?>
			<link href="/<?=filemtime_file('cont/css/'.local_core.'_all.css'); ?>" rel="stylesheet" type="text/css"/>
		<? } ?>

		<?=file_theme_add('csse.css'); ?>
		
		<?=moduls_get('head'); ?>
		<?=fgc('yandex-verification'); ?>
		<? if(file_exists('cont/files/favicon.png') ){
			?> <link rel="shortcut icon" type="image/png" href="<?=basehref; ?>/cont/files/favicon.png?<?=filemtime('cont/files/favicon.png'); ?>" /><? 
		} ?>
		<?=rel_canonical(); ?>
	</head>
	<body id="id<?=$m['id']; ?>" class="<?=$m['sub']['class_body']; ?>">

		<?=$GLOBALS['mbod']['sub']['_body'] ?  smarty( shab( $GLOBALS['mbod']['sub']['_body'] ),1 ) : smarty(theme(jscripts.'/tpl/_body'.$GLOBALS['mbod']['d51'].'.php')); ?>

		<script type="text/javascript">
		// <!--
			var ie6=ie7=ie8=false;var adminpanel=false;
		// -->
		</script>
		<!--[if IE 6]><script language="javascript">var ie6=true;</script><![endif]-->
		<!--[if IE 7]><script language="javascript">var ie7=true;</script><![endif]-->
		<!--[if IE 8]><script language="javascript">var ie8=true;</script><![endif]-->


		<script type="text/javascript">
		// <!--
			if ( ie6 ) {
				$('body').attr('id','ie6');
			}
			if ( ie7 ) {
				$('body').attr('id','ie7');
			}
			if ( ie8 ) {
				$('body').attr('id','ie8');
			}

			var basehref="<?=basehref; ?>";
			var hrpref="<?=hrpref; ?>";
			var mainval="<?=(o('mainval')?o('mainval'):'руб.'); ?>";
			var capcha_auto="<?=o('capcha_auto'); ?>";
			var cart_nopopup="<?=o('cart_nopopup'); ?>";
			var inadmin=<?=inadm ? 'true' : 'false' ; ?>;


		// -->
		</script>


		<script type="text/javascript" src="/<?=jscripts; ?>/my.js?<?=filemtime(jscripts.'/my.js'); ?>" charset="utf-8">
		</script>
		<script type="text/javascript" src="/<?=local_core; ?>/js/local.js" charset="utf-8">
		</script>

		<script type="text/javascript">
			// <!--
			var MenuBar1 = new Spry.Widget.MenuBar("MenuBar1", {});
			var MenuBar1 = new Spry.Widget.MenuBar("MenuBarVer", {});

			
			var is_myauth=<?=uid() ? "true" : 'false' ;  ?>;
			// -->
		</script>
		<?=moduls_get('body'); ?>
		<?=fgc('body-verification'); ?>
	</body>
</html>