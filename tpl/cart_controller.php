<? 

if ( $_REQUEST['adcart'] ) { fn('cart::adcart'); } // если добавление в корзину
if ( $_REQUEST['renum'] ) { fn('cart::renum'); } // если пересчёт корзины
if ( $_REQUEST['cartclear'] ) { fn('cart::cartclear'); } // если очистка корзины
if ( $_REQUEST['deltovcart'] ) { fn('cart::deltovcart'); } // если удаление конкретного товара из корзины

// обработка возврата от платёжных систем
if ($GLOBALS['longpage'][1]=='result')  { $out.= fn('cart::result'); } // дёргается шлюзом при совершении удачной оплаты 
if ($GLOBALS['longpage'][1]=='success')  { $out.= fn('cart::success');  } // вывод страницы благодарности за покупку, куда перебрасывает после совершения платежа в шлюзе
if ($GLOBALS['longpage'][1]=='fail') {  $out.=fn('cart::fail'); } // вывод сообзщение о неудачи плаетжа

// обработка оформления заказа (после нажатия на кнопку "Оформить заказ"
if ( $GLOBALS['longpage'][1]=='merchant' )  {   $out.= fn('cart::order_process');  } 


if ( $fullcart3table= fn( 'fullcart3table' )  ) { // проверяем есть ли в корзине товары, генерируя таблицу товаров

	// TODO слить в один шаблон и разбить на роли
	$out.= smarty_tpl( 'fullcart3_head' ); // TODO 1

	// встаавляем вычесленню ранее таблицу товаров
	$out.= ($fullcart3table);


	$out.= smarty_tpl( 'fullcart3_beforetable' ) ; //TODO 2



	if (  (
			curver('cart_oformit') == '30' // TODO убрать архаизмы - первые попытки сделать версионность в движке
				AND o('kabinet') // если у нас установлен и активирован ли модуль kabinet (кабинет)
	)
	)   {

		// генерируем форму заказа в которой можно указывать страны и в которой активирован модуль просчёта стоимости доставки по стране и EMS
		$out.=smarty_tpl('fullcart3_formzakaz_user_country');
		
	} else if (  (
			o('cart_oformit')=='adv' //если включена продвинутая корзина
				AND
			o('kabinet') // если включен модуль кабинета

	)
	)   {
		// генерируется форма заказа для авторизированных пользователей
		$out.=smarty_tpl('fullcart3_formzakaz_user');
	
	} else { 
		// для не авторизированных, либо если кабиньетная форма отключена вообще, либо не установлена
		$out.=smarty_tpl('fullcart3_formzakaz'); 
	
	} 
	
	$out.= smarty_tpl( 'fullcart3_footer' ); // TODO 3
}

else if ( 
		!order('email_sended') // если email не отправлялся 
			AND 
		$GLOBALS['longpage'][1]!=='success' // если текущая подстарница не равна success 
) {
	
	// выводим сообщение о пустой странице
	$out.='<h2 style="text-align:center;">Ваша корзина пуста...</h2>';
	
}
	

?><?=$out; ?>