<? if ($orders ) { ?>
<table class='table orders'>
	<tr>
		<th class='check'><input type='checkbox' onchange='orders_all(this)' /></th>
		<th class='art'>Артикул</th>
		<th class='name'>Наименование</th>
		<th class='h_price'>Цена</th>
		<th class='tov_num_h'>Кол-во</th>	
		<th class='summ'>Cумма</th>
	</tr>
	<?
	foreach (  $orders as $ord ) {

		$mmtov=qvar($ord['tid']);

		?>
		<tr>
			<td class='check'>
				<? if ( $ord['ident'] AND $mmtov['ok'] AND !$mmtov['sub']['nonal'] ) { ?>
					<input type='checkbox' name="tov[{{$ord['id']}}]" ident="{{$ord['ident']}}" /> 
				<? } else { ?>
					Нет в наличии
				<? } ?>
			</td>
			<td class='art'>{{( $ord['art'] ? $ord['art'] : $ord['id'] )}}</td>
			<td class='name'>
				<? if ($ord['ident'] ) { ?>
					<a href="{{urlFromMnemo($ord['ident'])}}" >{{lang($ord['tovname'])}}</a>
				<? } else { ?>
					{{lang( $ord['tovname'])}}	
				<? } ?>
				<div>{{ ( oror(  $ord['opt'], ';' , '' )  ? '' : $ord['opt']  ) }}</div>
								</td>
			<td class='c_price'>{{ ($ord['price'] ? price2html( $ord['price']*1 ) : '' ) }} [val]</td>
			<td class='num'>{{$ord['num']}}</td>
			<td class='c_price'>{{$ord['price']*$ord['num']}} [val]</td>
		</tr>
		
	<? } ?>
	<tr class='table_bottom' >
		<td class='' colspan="6">
			<input type='checkbox' onchange='orders_all(this)' id='bottom_orders_all' />
			<label for='bottom_orders_all'>Выбрать всё</label> &nbsp;&nbsp;&nbsp;&nbsp; 
			<a onclick='adcart_multi(this)' class='pointer' >Добавить в корзину</a>
		</td>
	</tr>
</table>
<? } ?>
