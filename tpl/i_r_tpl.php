<? if ($role=='out_3' ) { ?>

    <div  {{$dep[0]}} {{$dep[1]}} rel='radio_label' >{{$label}}</div>


<? } else if ($role=='out_1') { ?>

    <fieldset class='i_r' style='width:{{  $width_all  }}' >
        <legend align='{{  ( ($mm['d10']) ? : 'right' )  }}'>
        <b> {{$name . helpop($mm)  }}: </b>
        </legend>
        <table  align='center' style='width:100%'  id={{$variable}} rel='rad'>
            <tr>
                <td  align='left' valign='middle' class='i_r_labels'>{{$out}}</td>
            </tr>
        </table>
    </fieldset>


<? } else if ($role=='out_2') { ?>

    <table class='i_r' align='center'  style='width:{{$width_all}}' id={{$variable}} rel='rad'>
        <tr>
            <td style='width:{{ $width_name }}' align='{{ (($mm['d10']) ? : 'right' ) }}'>
            <b> {{$name . helpop($mm) }}: </b>
            </td>
            <td  align='left' valign='middle' class='i_r_labels'>{{$out}}</td>
        </tr>
    </table>

<? } ?>
