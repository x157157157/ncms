<? class partner {
	public static function x() {
		
		echo smarty_tpl( 'adm_partner', $p );
	}
	 
	public static function main() {
		// вывод pid
		
		if( !uid() ){ return o('page_noauth'); }
		
		$p['pid_main']= partner::pid_main( ) ;
		$p['stat_clicks']= partner::stat_clicks( ) ;
		
		$p['stat_regs']= partner::stat_regs( ) ;
		$p['stat_regs2']= partner::stat_regs2( ) ;
		
		$p['stat_orders']=partner::stat_orders();
		$p['stat_orders2']=partner::stat_orders2();
		
		$p['stat_orders_end']=partner::stat_orders_end();
		$p['stat_orders_end2']=partner::stat_orders_end2();
		
		$p['stat_orders_summ']=partner::stat_orders_summ();
		$p['stat_orders_summ2']=partner::stat_orders_summ2();
	
		$p['stat_paid']=partner::stat_paid();
		
		$p['stat_balance']=$p['stat_orders_summ'] + $p['stat_orders_summ2'] - $p['stat_paid'];
		
		
		return smarty_tpl('partner_main',$p);
		
	}
	
	public static function paid() {
		$data=array(
			'event'=>'paid',
			'summ'=>intval( $_POST['cost'] ),
			'p_uid'=>uid()
		);
		mydbAddLine('partner_events',$data);
		echo '[data]ok[data]<script> page_reload(); </script>';
	}
	
	public static function stat_paid( $uid=false ) {
		
		 $mm= mydb_query('SELECT sum(summ) stat 
		 					FROM `'.pref_db.'partner_events` 
		 					WHERE 
		 						p_uid='.xs( $uid ?: uid() ).' 
		 							AND 
		 						event=\'paid\'
		 						' 
		 );
		 
		 return intval($mm['stat']);
	}
	
	public static function stat_orders_summ( $uid=false ) {
		
		 $mm= mydb_query('SELECT sum(summ) stat 
		 					FROM `'.pref_db.'partner_events` 
		 					WHERE 
		 						p_uid='.xs( $uid ?: uid() ).' 
		 							AND 
		 						event=\'ord\'
		 							AND
		 						is_end=1
		 						'  
		 );
		 
		 return intval($mm['stat']);
	}
	
	public static function stat_orders_summ2( $uid=false ) {
		
		 $mm= mydb_query('SELECT sum(summ) stat 
		 					FROM `'.pref_db.'partner_events` 
		 					WHERE 
		 						p_uid='.xs( $uid ?: uid() ).' 
		 							AND 
		 						event=\'subord\'
		 							AND
		 						is_end=1
		 						'  
		 );
		 
		 return intval($mm['stat']);
	}
	
	
	
	public static function stat_orders_end( $uid=false ) {
		
		 $mm= mydb_query('SELECT count(*) stat 
		 					FROM `'.pref_db.'partner_events` 
		 					WHERE 
		 						p_uid='.xs( $uid ?: uid() ).' 
		 							AND 
		 						event=\'ord\'
		 							AND
		 						is_end=1
		 						'  
		 );
		 
		 return intval($mm['stat']);
	}
	public static function stat_orders_end2( $uid=false ) {
		
		 $mm= mydb_query('SELECT count(*) stat 
		 					FROM `'.pref_db.'partner_events` 
		 					WHERE 
		 						p_uid='.xs( $uid ?: uid() ).' 
		 							AND 
		 						event=\'subord\'
		 							AND
		 						is_end=1
		 						'  
		 );
		 
		 return intval($mm['stat']);
	}
	
	public static function stat_orders2( $uid=false ) {
		
		 $mm= mydb_query('SELECT count(*) stat 
		 					FROM `'.pref_db.'partner_events` 
		 					WHERE 
		 						p_uid='.xs( $uid ?: uid() ).' 
		 							AND 
		 						event=\'subord\'
		 						'  
		 );
		 
		 return intval($mm['stat']);
	}
	
	public static function stat_orders( $uid=false ) {
		
		 $mm= mydb_query('SELECT count(*) stat 
		 					FROM `'.pref_db.'partner_events` 
		 					WHERE 
		 						p_uid='.xs( $uid ?: uid() ).' 
		 							AND 
		 						event=\'ord\'
		 						'  
		 );
		 
		 return intval($mm['stat']);
	}
	
	public static function stat_regs2( $uid=false ) {
		 $mm= mydb_query('SELECT count(*) stat 
		 					FROM `'.pref_db.'partner_events` 
		 					WHERE 
		 						p_uid='.xs( $uid ?: uid() ).' 
		 							AND 
		 						event=\'subreg\'
		 						'  
		 );
		 return intval($mm['stat']);
	}
	
	public static function stat_regs( $uid=false ) {
		 $mm= mydb_query('SELECT count(*) stat 
		 					FROM `'.pref_db.'partner_events` 
		 					WHERE 
		 						p_uid='.xs( $uid ?: uid() ).' 
		 							AND 
		 						event=\'reg\'
		 						'  
		 );
		 return $mm['stat'];
	}
		
	
	
	public static function stat_clicks( $uid=false ) {
		 $mm= mydb_query('SELECT count(*) clicks FROM `'.pref_db.'partner_clicks` WHERE p_uid='.xs( $uid ?: uid() ) );
		 return $mm['clicks'];
	}
	
	public static function pid_main( $uid=false ){
		if ( $pid = mydbGetSub('pids', array( 'uid'=>oro ( $uid, uid() ), 'is_main'=>1 ) ) ) {
			return $pid[0]['pid'];
		} else {
			$newpid=partner::pid_gen();
			mydbAddLine('pids', array('pid'=>$newpid,'is_main'=>1, 'uid'=>oro ( $uid, uid() ) ) );
			return $newpid;
		}
	}
	
	public static function event_new_order( &$insert_arr ) {
		// 
		if ( $p_uid=$GLOBALS['userdata']['p_uid'] ?: intval( $_COOKIE['p_uid'] )  ) {
			
			$insert_arr['is_pay_comission']='needpay';
			$insert_arr['p_uid']=$p_uid;
			
			$p_summ=$insert_arr['p_tov_summ'] ?: $insert_arr['cur_summ'] * o('p_percent')/100 ; 
			
			$data=array(
				'p_uid'=> $p_uid ,
				'event'=>'ord',
				'summ'=>$p_summ,
				'order_id'=>$insert_arr['id']
			);
			mydbAddLine('partner_events', $data);
			
			if ( $p_uid_2L=partner::p_uid_2L( $p_uid ) ) {
				$data=array(
					'p_uid'=> $p_uid_2L ,
					'event'=>'subord',
					'summ'=>( $p_summ * o('p_percent2') / 100 ),
					'order_id'=>$insert_arr['id']
				);
				
				mydbAddLine('partner_events', $data);
			}
			
			
		}
		
		
	}
	
	public static function event_order_end() {
		$order_id=$_POST['order_id'];
		
		$mm_ord=mydbget('orders', 'id', $order_id );
		
		if ($mm_ord['p_summ_correct']) {
			$mm_event=  fn('mydbGetSub', 'partner_events', [ 'order_id'=> $order_id, 'event'=>'ord' ] )[0]; 
			fn('mydbUpdate','partner_events',['summ'=> ($mm_event['summ'] + $mm_ord['p_summ_correct'] )  ], 'id', $mm_event['id'] ); 
		}
		
		mydbUpdate('partner_events', ['is_end'=>1], 'order_id', $order_id );
		
		mydbUpdate('orders', ['is_pay_comission'=>'payed'], 'id', $order_id );
		
		return 'ok[data]<script> $("#obta'.intval($order_id).' .is_pay_comission a").addClass(\'disabled\') </script>';	
	}
	
	public static function event_new_reg($uid_new) {
		// ищем uid  
		
		if ( $p_uid=$_COOKIE['p_uid'] ) {
			$data = array(
				'uid'=> $uid_new,
				'p_uid'=>$p_uid,
				'event'=>'reg'
				
			);
			mydbAddLine('partner_events', $data );
			
			setcookie("p_uid", "", time()-3600);
			
			mydbUpdate('users', array('p_uid'=>$p_uid), 'id', $uid_new );
			
			//обрабатываем 2 уровень
			
			
			
			if ( $p_uid_2L=partner::p_uid_2L($p_uid) ) {
				$data = array(
					'uid'=> $p_uid,
					'p_uid'=>$p_uid_2L,
					'event'=>'subreg'
					
				);
				
				mydbAddLine('partner_events', $data );
			}
			
			
			// cooc (p_uid)
			// need= uid event (reg) summ	
		}
		
		
	}
	
	public static function p_uid_2L ( $p_uid ) {
		return mydbget( 'users', 'id', $p_uid, 'p_uid' );
	}
	public static function new_click($p_uid, $pid) {
		
		$data=array (
			'pid'=> $pid,
			'p_uid'=> $p_uid,
			'referer'=>$_SERVER['HTTP_REFERER']
		);
		
		if ( !uid() ) {
			setcookie("p_uid", $p_uid , time() + 3600*24*30+3, '/' );
		}
		
		mydbAddLine('partner_clicks', $data);
		
	}
	public static function pid_gen(){
		
		
		srand(  );
		
		$out = rand(10000000, 99999999).'';
		
		for ( $x=0 ; $x<=6 ; $x++ ) {
			if ( $out{$x}==$out{$x+1} ) {
				$not_ok=1;
				//echo '$not_ok: '.$out.'<br/>';
				return partner::pid_gen();
			}
		}
		
		if ( mydbget('pids','pid', $out) ) {
			return partner::pid_gen();
		}
		
		return $out ;
	}
	
}

?>