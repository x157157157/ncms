<?php class ifun {
	
	public static function i( $t, $name, $add_opt=FALSE, $add3_opt=false,$add4_opt=false,$mm_opt=false) {

			$i_tpl= theme('i_tpl');
			@$add=func_get_arg (2);
			@$add3=func_get_arg (3);
			@$add4=func_get_arg (4);
			@$mm=func_get_arg(5);
			if ( $t=='t' )
			{
				$t='text';
				//$out= "<input type=\"$t\" name=\"$name\" id=\"$name\" value=\"".htmlspecialchars($GLOBALS[$name])."\" style='width:300px' />  ";
				$p=[
					'name'=>$name,
					'html_name'=>htmlspecialchars($GLOBALS[$name]),
				];

				$out=smarty_tpl($i_tpl."/out_1", $p);

				//$out='<table align="center"><tr><td align="right" width="50%">'.$add3.': </td><td align="left" valign="midle" width="50%">'.$out.'</td></tr></table>';

				$p=[
					'role'=>'out_2',
					'add3'=>$add3,
					'out'=>$out,
				];

				$out=smarty_tpl($i_tpl, $p);
			}
			else if ( $t=='ch')
			{
				$t='checkbox';
				if  ( $GLOBALS[$name] or (!$_POST and $mm['d7']) )
				{
					$ch='checked="checked"';
				}
				else
				{
					$ch='';
				}
				//V($GLOBALS['name']);
				if (!$mm['d9']) $sep=':';			
				$out= "<input rel='".$mm['d12']."' id='$name' type=\"$t\" name=\"$name\" $ch $add  />  ";
				
				$width_all=  $mm['d3'] ?  $mm['d3']  : '100%' ;
				$width_name=  ($mm['d4']) ? $mm['d4']  :  '50%'  ; 
			
				//$out='<table  width="'.$width_all.'"><tr><td align="right" width="'.$width_name.'" class="namecheck"><label for="'.$name.'">'.lang($add3).helpop($mm).formprice($mm['d12']).$sep.'</label></td><td  align="left" class="inputcheck">'.($out).'</td></tr></table>';

				$p=[
					'role'=>'out_3',
					'width_all'=>$width_all,
					'width_name'=>$width_name,
					'name'=>$name,
					'add3'=>lang($add3),
					'mm'=>($mm),
					'sep'=>$sep,
					'out'=>$out,
				];

				$out=smarty_tpl($i_tpl, $p);

			}
			else if ( $t=='r')
			{
				$t='radio';
				if  (@$GLOBALS[$name]==$add)
				{
					$ch='checked';
				}
				else
				{
					$ch='';
				}
				$out= "<input type=\"$t\" name=\"$name\" value=\"$add\" id=\"$add\" $ch $add3 />  ";
				$add3=$add4;
				//$out='<table><tr><td align="left" colspan="2">'.$out.'<label id="'.$add.'"> - '.$add3.'</label></td></tr></table>';

				$p=[
					'role'=>'out_4',
					'out'=>$out,
					'add'=>$add,
					'add3'=>$add3,
				];

				$out=smarty_tpl($i_tpl, $p);
			}
			elseif ($t=='s')
			{
				//$out='<table><tr><td align="center" colspan="2" ><b>'.$name.'</b></td></tr></table>';

				$p=[
					'role'=>'out_5',
					'name'=>$name,
				];

				$out=smarty_tpl($i_tpl, $p);
			}
			elseif ($t=='o')
			{
				$out="<select name=\"$name\" $add >";
				$qu= 'SELECT * FROM '.pref_db.'content WHERE rod="'.xss($add4).'"  AND ok="1"  ORDER BY `num` ASC ';
				$prebase=my_mysql_query($qu) ;
				//v($GLOBALS[m][type]);
				echo "<br/>";
				while ($m=mysqli_fetch_array($prebase) )
				{
					$out.='<option value="'.$m[ident].'" ';
					//v($GLOBALS[m][type]);
					//v($m['type']);
					if ( $GLOBALS[m][type]==$m['ident'] ) $out.="selected" ;
					$out.=" >{$m['name']}</option>	"	;
				}
				$out.='/<select>';
				//$out='<table><tr><td align="right">'.$out.'</td><td align="left"> - '.$add3.'</td></tr></table>';

				$p=[
					'role'=>'out_6',
					'out'=>$out,
					'add3'=>$add3,
				];

				$out=smarty_tpl($i_tpl, $p);
			}
			elseif ($t=='oo')
			{
				$out="<select name=\"$name\" $add >";
				$qu= 'SELECT * FROM '.pref_db.'content WHERE rod="'.xss($add4).'"   ORDER BY `num` ASC '; //AND ok="1" 
				$prebase=my_mysql_query($qu) ;
				//v($GLOBALS[m][type]);
				echo "<br/>";
				while ($m=mysqli_fetch_array($prebase) )
				{
					$out.='<option value="'.$m[d1].'" ';
					//v($GLOBALS[m][type]);
					//v($m['type']);
					if ( $GLOBALS[$name]==$m['d1'] ) $out.="selected" ;
					$out.=" >{$m['name']} ({$m['d1']})</option>	"	;
				}
				$out.='/<select>';
				//$out='<table><tr><td align="right">'.$out.'</td><td align="left"> - '.$add3.'</td></tr></table>';

				$p=[
					'role'=>'out_7',
					'out'=>$out,
					'add3'=>$add3,
				];

				$out=smarty_tpl($i_tpl, $p);
			}
			elseif ($t=='f')
			{
				//$out= "<input type=\"file\" ".($mm['d7'] ? 'multiple class="multiple"' : '' )." style='width:100%;' name=\"$name\" $ch $add />  ";
				$p=[
					'role'=>'out_8',
					'mm'=>$mm,
					'name'=>$name,
					'ch'=>$ch,
					'add'=>$add,
				];

				$out=smarty_tpl($i_tpl, $p);
				if ($mm['d11']=='top' )
					{
						//$out='<table width="100%"><tr><td align="center" class="input_file_name">'.$add3.': </td></tr><tr><td align="left"   class="input_file">'.$out.'</td></tr></table>';
						$p=[
							'role'=>'out_9',
							'add3'=>$add3,
							'out'=>$out,
						];

						$out=smarty_tpl($i_tpl, $p);
					}
					else
					{
						//$out='<table><tr><td align="right" width="50%">'.$add3.': </td><td align="left" width="50%">'.$out.'</td></tr></table>';
						$p=[
							'role'=>'out_10',
							'add3'=>$add3,
							'out'=>$out,
						];

						$out=smarty_tpl($i_tpl, $p);
					}
				
			}
			return $out;
		}
	
	public static function i_r($variable, $name, $width_all, $width_name, $ident, $valfield=FALSE, $arg6=FALSE,$arg7=FALSE,$arg8=FALSE) {

		$i_r_tpl= theme('i_r_tpl');
		$valfield = $valfield ? : 'd1';
		// func_get_arg
		$mm=$arg8;
		if (!$width_rro) $width_all='100%';
		if (!$width_name) $width_name='50%';
		
		if($mm['d2']='sub') 
			{
				$vars=array_flip(  $GLOBALS[$variable] );
			}
			else
			{
				if ( is_array( $GLOBALS[$variable] ) ) {
					
					$vars=array_flip( $GLOBALS[$variable] );
					
				} else {
					
					$vars=array_flip( explode(' ', $GLOBALS[$variable]) );
					
				}
			}
			
		if (  $ident{0}=='=' ) {
			
			eval ( '$labels_all'.$ident.';' ); 
			vl($labels_all);
			
		} else {
			
			$ident=explode( ' ', $ident ); 
			$rod = sizeof( $ident ) > 1 ? '( rod='.xs($ident[0]).' or rod='.xs( $ident[1] ).' ) ' : ' rod='.xs($ident[0]) ;
			
			if ($arg7) $ok=' AND `ok`<>\'\' ';
			$qu= 'SELECT `name`,`'.$valfield.'`,`d12`,`d99`, `d98`,`d97`,`d96`,`d95`,`d94` FROM '.pref_db.'content WHERE '.$rod.$ok.'  AND `d100`<='.reg().'    ORDER BY `num` ASC '; 
			$prebase=my_mysql_query( vl($qu)) ;
			while ( $m=mysqli_fetch_assoc($prebase) ) {
				$labels_all[]=$m;
			}
		}
		
		
		foreach ( vl($labels_all) as $m ) {
			
			$m[0]=$m['name'];
			$m[1]=$m[$valfield];
			vl($m);
			$dep=formDep2class($m['d98']);	
			if (  is_ok_opt($m['d99']) ) {
				if  ($name==='Опции для репозитория:') {
					$m[1]=$m[1];
				}
				$file=jscripts.'/tpladmin/t-'.$m[1].'.png';
				
				
				if (file_exists($file) and $m[1])
				{
					$img = "<img src='$file'/>";
				}
				else
				{
					$img='';
				}
				if ($mm['d8']=='multi')
				{
					//v($mm);
					//v($GLOBALS['m']);
					$label="<label><input  type='checkbox' name='{$variable}[]' value='{$m[1]}'  ";
					
					if ( isset($vars[$m[1]]) ) $label.='checked="checked"' ;
					
					if ( ($arg6) )
					{
						$val="({$m[1]})";
					}
					$label.=" />$img {$m[0]} $val </label>";
				}		
				else
				{
					$label="<label><input  type='radio' name='$variable' value='{$m[1]}' rel='".$m['d12']."' ";
					if ( $GLOBALS[$variable]==$m[1] ) $label.='checked="checked"' ;
					if ( ($arg6) )
					{
						$val="({$m[1]})";
					}
					$label.=' />'.$img.' '.mb_ucasefirst($m[0]).' '.($val).' '.( $mm['d16'] ? formprice(($m['d12'])) : '' ) .'</label>';
				}

				$p=[
					'role'=>'out_3',
					'dep'=>$dep,
					'label'=>$label,
				];

				$out.=smarty_tpl($i_r_tpl, $p);

				//$out.="<div  $dep[0] $dep[1] rel='radio_label' >$label</div> ";
				if ($m['d12'])
				{
					$out.='<input type="hidden" name="ival['.$variable.']['.$m[1].']" value="'.$m['d12'].'" />';
					$GLOBALS['ivals'][$variable][$m[1]]=$m['d12'];
				}
				$GLOBALS['ivalnames'][$variable][$m[1]]=$m['name'];
			}			
			
		}
		if($mm['d8']=='radiofieldset')
			{

				$p=[
					'role'=>'out_1',
					'width_all'=>$width_all,
					'mm'=>$mm,
					'name'=>$name,
					'variable'=>$variable,
					'out'=>$out,
				];

				$out=smarty_tpl($i_r_tpl, $p);
				
				/*$out="
				<fieldset class='i_r' style='width:".$width_all."' ><legend align='".(($mm['d10'])?$mm['d10']:'right')."'><b> $name".helpop($mm).": </b></legend>
				<table  align='center' style='width:100%'  id=\"$variable\" rel='rad'>
				<tr>			
				<td  align='left' valign='middle' class='i_r_labels'>$out</td>
				</tr>
				</table></fieldset>";*/
			}
			else
			{
				$p=[
					'role'=>'out_2',
					'width_all'=>$width_all,
					'width_name'=>$width_name,
					'mm'=>$mm,
					'name'=>$name,
					'variable'=>$variable,
					'out'=>$out,
				];

				$out=smarty_tpl($i_r_tpl, $p);

				/*$out="
				<table class='i_r' align='center'  style='width:".$width_all."' id=\"$variable\" rel='rad'>
				<tr>
				<td style='width:".$width_name."' align='".(($mm['d10'])?$mm['d10']:'right')."'><b> $name".helpop($mm).": </b></td>
				<td  align='left' valign='middle' class='i_r_labels'>$out</td>
				</tr>
				</table>";*/
			}
		
		return $out;
	}
	
	public static function i_select ($name, $var, $ident, $name_field, $value_field ,$attribute, $arg6=FALSE, $arg7=FALSE, $arg8=FALSE ) {

		$i_select_tpl= theme('i_select_tpl');
		if ($arg7) $ok=' AND `ok`<>\'\' ';
		$mm=$arg8;
		// func_get_arg()
		//$out="<select name=\"$var\" id=\"$var\" $attribute  rel='sel' style='width:100%;'>";
		$p=[
			'role'=>'out_1',
			'var'=>$var,
			'attribute'=>$attribute,
		];

		$out=smarty_tpl($i_select_tpl, $p);
		$subid=explode(':', $mm['d7']);
		if ($subid[0]=='autosearch')
			{
				$qu='SELECT value v,value vv FROM `'.pref_db.'subcontent` WHERE folder='.xs($GLOBALS['cache']['tovgroupp']).' and field='.xs(trim( $name )).' GROUP BY value ORDER BY VALUE ASC';
			}
		else if ($subid[0]=='subsearch')
			{
				$qu='SELECT value v,value vv FROM `'.pref_db.'subcontent` WHERE folder='.xs($subid[1]).' and field='.xs(trim( $subid[2]?$subid[2]:$name )).' GROUP BY value ORDER BY VALUE ASC';
			}
		else
			{
				$qu= 'SELECT `'.$name_field.'`,`'.$value_field.'`, `d98`, `d99`, `d12` FROM '.pref_db.'content WHERE rod='.xs($ident).$ok.'  AND `d100`<='.reg().' ORDER BY `num` ASC '; //AND ok="1" 
			}
		$prebase=my_mysql_query($qu) ;
		if( $mm['d15'] ){
			$out.= '<option value=""  rel="opt" >'.($subid[0]=='subsearch'?'Не важно':'').'</option>'	;
		}
		
		$folder=explode(':',$mm['d7']);
		
		if ( $folder[0]=='folder' ) 
			{	
				$file=folder2array_asort( ($folder[1] ));
				$file_key=(array_keys( $file ) );
			}
		$x=0;
							
		while ($m=mysqli_fetch_array($prebase) or ($file_key[$x]) ) {
			
				
			if ( is_ok_opt($m['d99'])  ) {
				
				$dep=formDep2class($m['d98'])  ;
			
				if(  $file_key[$x]) {
					$m[1]=$folder[1].'/'.$file_key[$x];
					$m[0]=$file_key[$x];
				}
				
				$out.='<option value="'.$m[1].'" '."  $dep[0] $dep[1] rel='opt' "; 
				//v($GLOBALS[m][type]);
				//v($m['type']); 
				if ( $GLOBALS[$var]==$m[1] ) $out.="selected" ;
				if ( ($arg6 ) )
				{
					$val="({$m[1]})";
				}
				if ( $mm['d16'] ){
						$price=' - '.$m['d12'].' [val]';
					}
					else {
						$price='';
					}
					
				$out.=' >'.mb_ucasefirst($m[0].' '.$val.$price).'</option>'	;  
				if ($m['d12'])
				{
					$postout.= '<input type="hidden" name="ival['.$var.']['.$m[1].']" value="'.$m['d12'].'" />'; 
					$GLOBALS['ivals'][$var][$m[1]]=$m['d12'];
				}
				$GLOBALS['ivalnames'][$var][$m[1]]=$m['name'];
				
				$x++;
				
			} 
			
		}
		
		$out.='</select>';
		$width_all=$mm['d3']?$mm['d3']: '100%';
		$width_name=$mm['d4']?$mm['d4']:'50%';
		
		if ($mm['d17'] and $mm['type']!=='rangeslider' )
			{
				$need=' class="need"';
				$red='<span class="red">*</span>';
			} 
		
		$sep= !$mm['sub']['not_sep'] ?  ':' : ''; 
		
		/*$out='
		<table class="i_select" width="'.$width_all.'" '.$need.'>
		<tr> 
		<td class="i_select_name" align="'.(($mm['d10'])?$mm['d10']:'right').'" width="'.$width_name.'" align="right">'.$name.$red.$sep.'</td>
		<td class="i_select_input" align="left"  align="left" style="position:relative;"> '.$out.$postout.formedit($mm['id']).'</td>
		</tr>
		</table>';*/

		$p=[
			'role'=>'out_2',
			'width_all'=>$width_all,
			'need'=>$need,
			'width_name'=>$width_name,
			'name'=>$name,
			'red'=>$red,
			'sep'=>$sep,
			'out'=>$out,
			'postout'=>$postout,
			'mm'=>$mm,
		];

		$out=smarty_tpl($i_select_tpl, $p);
		return $out;
	}

	public static function i_t($variable, $name, $width_all, $width_name,$rows,$x ,$mm) {

		$i_t_tpl= theme('i_t_tpl');
		global $nothiden;
		$exp=explode('[', $variable); 
		if ($mm['d13']) // внутренняя подсказка
		{
			$alt=' title="'.$mm['d13'].'" ';
			$hint=' hinted';
		}
		if ($exp[1]) // если перепенная - элемент массива
		{
			$exp[1]=substr($exp[1],0,strlen($exp[1])-1);
			$var=$GLOBALS[$exp[0]][$exp[1]] ;
		}
		else
		{
			$var=$GLOBALS[$variable];
		}
		
		if ( $mm['sub']['is_form_tpl'] ) {
			return smarty_tpl( array('tpl_content'=>$mm['sub']['is_form_tpl']), $mm );
		}
		
		if ($rows and $mm['type']!=='rangeslider') //если textarea
		{
			$rows_var=substr_count($var,"\n") +2 ;
			$rows=$rows > $rows_var ? $rows : $rows_var ;
			$out= "<textarea type='text' rows='$rows' name='$variable' style='width:100%' class='$hint ".$mm['sub']['addclass_input']."' $alt>".htmlspecialchars($var)."</textarea>
			".($nothiden?"<input type='hidden' name='ps[]' value='$variable'	/>":'' );

		}
		else
		{
			//v($var);
			if ($mm['type']=='rangeslider')
			{
				$type='hidden';
				if($GLOBALS[$mm['d1']])
				{
					$curval=explode('-',$GLOBALS[$mm['d1']]);
				}
				else
				{
					$curval=array( $mm['d5'], $mm['d6'] );
				}
				$curvalues=intval($curval[0]).','.intval($curval[1]);						
				$val= 'от '.intval($curval[0]).' до '.intval($curval[1]).' '.$mm['d8'] ;
				$valinp= intval($curval[0]).'-'.intval($curval[1]);
				$out.='<div class="rangediv" id="sp'.$mm['ident'].'" >'.$val.'</div>' ;
				$out.='<div id="rs'.$mm['ident'].'"></div>';			
				$out.='<script>
				$(function()
				{
					$( "#rs'.$mm['ident'].'" ).slider({
						range: true,
						min: '.($mm['d5']?$mm['d5']:0).',
						max: '.$mm['d6'].',
						step: '.($mm['d7']?$mm['d7']:1).',
						values: [ '.$curvalues.' ],
						slide: function( event, ui )
						{
							$( "#sp'.$mm['ident'].'" ).html(  "от " + ui.values[ 0 ] +  " до " + ui.values[ 1 ] + " '.$mm['d8'].'");
							$( "#'.$mm['ident'].'" ).val(ui.values[ 0 ] +"-"+ui.values[ 1 ] );
						}
						});
					$( "#sp'.$mm['ident'].'" ).html("'.$val.'");
					$( "#'.$mm['ident'].'" ).val( "'.$valinp.'" );
				}
				);
				</script>';
			}
			elseif ($mm['d8'])
			{
				$type='password';
			}
			elseif ($mm['d12']=='select_user')
			{
				$type='hidden';
				$varspan='<span>'.implode('</span> <span>', explode(' ',$var) ).'</span>';
				//$out.='<div class="inserter"><table width="100%"><tr><td width="16"><a class="selectajax" href="/ajax--?action='.$mm['d12'].($mm['d96']?'_'.$mm['d96']:'').'&ident='.$mm['ident'].'&var='.$mm['d1'].'" ><img src="/jscripts/tpladmin/add_group.png" /></a></td><td class="spanins">'.$varspan.'</td></tr></table></div>';
				$p=[
					'role'=>'out_1',
					'mm'=>$mm,
					'varspan'=>$varspan,
				];

				$out.=smarty_tpl($i_t_tpl, $p);
			}
			elseif( $mm['d10'] )
			{
				$type='hidden';
				if ( function_exists($mm['d10']) ) $out.=$mm['d10']($mm,$var);
			}
			else 
			{ 
				$type='text';
			}
			
			$out.= "<input type='$type'  id='$variable' name='$variable' {$alt} class='{$hint} ".$mm['sub']['addclass_input']."' value='".
			( $variable=='ident' ? urlFromMnemo( $var ) :  htmlspecialchars($var,ENT_QUOTES, 'cp1251')  ).
			"' style='width:100%' /> 
			".($nothiden?"<input type='hidden' name='ps[]' value='$variable'	/>":'' );
		}
		$edit= formedit( $mm['id']);	
		$tf=$GLOBALS['m']['type'];
		if (!$width_all) $width_all='100%';
		if (!$width_name) $width_name='200px';
		if ($mm['d5'] and $mm['type']!=='rangeslider' )
		{
			$need=' need';
			$red='<span class="red">*</span>';
		}
		$namealign=$mm['d9']?$mm['d9']:'left';
		if  ($mm['d7']=='r')
		{
			//$place1="<td align='left' width='$width_name' > - $name</td>";
			$p=[
				'role'=>'out_2',
				'width_name'=>$width_name,
				'name'=>$name,
			];

			$place1=smarty_tpl($i_t_tpl, $p);
		}
		elseif($mm['d7']=='t')
		{
			//$place2="<td   class='it_name' align='$namealign' style='padding-bottom:0;' valign='top'>".lang($name).$red.helpop($mm).":</td></tr><tr>";
			$p=[
				'role'=>'out_3',
				'namealign'=>$namealign,
				'red'=>$red,
				'mm'=>$mm,
				'name'=>$name,
			];

			$place2=smarty_tpl($i_t_tpl, $p);
		}
		elseif ($variable=='ident' and  strpos( ' page category tov _news' ,  $tf ) )
		{
			$name=basehref.'/'.hrpref;
			//$place2="<td align='left' width='1' style='border-right:0;'  class='it_name'>Адрес страницы: <b>$name</b></td>";
			$p=[
				'role'=>'out_4',
				'name'=>$name,
			];

			$place2=smarty_tpl($i_t_tpl, $p);
		}
		else
		{
			//$place2= "<td class='it_name' align='$namealign' valign='".( $mm['type']=='rangeslider' ? 'bottom':'top' )."' width='$width_name'>".lang($name).$red.helpop($mm).":</td>"  ;
			$p=[
				'role'=>'out_5',
				'namealign'=>$namealign,
				'mm'=>$mm,
				'width_name'=>$width_name,
				'name'=>($name),
				'red'=>$red,

			];

			$place2=smarty_tpl($i_t_tpl, $p);
		}
		if ($mm['d98']) $mm['d98'] = str_replace('=','--',$mm['d98']) .' hidden';
		if (adm) $alt=$mm['d2'].'~'.$mm['d1'];
		/* $out="<div  title='$alt' class='i_t{$need} {$mm['d12']}'><table align='center' width='$width_all' >
		<tr>
		".($name ? $place2 : '' )."
		<td align='left' valign='top' class='it_input'>$out".( $mm['sub']['comment']? '<div class="input_comment">'.$mm['sub']['comment'].'</div>' : '' ).( $mm['type']!=='rangeslider'? $edit:'')."</td>
		$place1
		
		</tr>
		</table></div>"; */

		$p=[
			'alt'=>$alt,
			'need'=>$need,
			'mm'=>$mm,
			'width_all'=>$width_all,
			'name'=>$name,
			'place2'=>$place2,
			'out'=>$out,
			'edit'=>$edit,
		];

		$out=smarty_tpl($i_t_tpl."/out_6", $p);
		return $out;
	}


	 }
	 
	
?>