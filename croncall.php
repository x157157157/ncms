<?

header("Cache-Control: no-store, no-cache, must-revalidate");  // HTTP/1.1
// на некоторых старющих сайтах, где mysets обновляется, используется данный файл для локальных скриптов
@include('cont/php/local.php');

// самый минимальный набор критических функций
// inadm, is_test, file_theme_name

include( defined ('minifun_path') ? constant('minifun_path') : jscripts.'/minifun.php' );

// главный конфиг
include('mysets.php');

// инициализация
include( defined ('init_path') ? constant( 'init_path')  :  jscripts.'/init.php');

// админ функции
include( file_theme_name( 'adminfun.php' ));

$time_limit=57; 	
set_time_limit( $time_limit+3 );

$start=time();

while( $start+$time_limit > time() )
	{
		unset($mm, $cron_error, $cron_error_mess );
		
		p('cron_cont', p('cron_cont')+1 );
		
		$pr=my_mysql_query( 'SELECT * 
							FROM `'.pref_db.'cron` 
							WHERE  
								cron_status=\'\'
									AND
								ts_start < CURRENT_TIMESTAMP()
							ORDER BY `prioritet` DESC, `id` ASC   
							LIMIT 1' ) ;			
		 
		if (  $mm=mysqli_fetch_assoc($pr) ) {
			
			mydbUpdate(	'cron', array ( 'cron_status'=> 'doing' ) , 'id' , $mm['id'] );
					
			fn( $mm['event'], unserialize($mm['params']) ) ; 
			
			$data= array (
				'cron_status'=>$cron_status ? $cron_status : 'ok' , 
				'cron_mess'=> $cron_mess 
			);
			
			mydbUpdate( 'cron', $data , 'id' , $mm['id'] );
			
			$doing++;	
			if ( $mm['msleep'] ) usleep($mm['msleep']*1000 );
			
		} else {
			$sleep++;
			sleep(3);
		}
		
		
	}

echo "sleep: $sleep <br/>doing: $doing";
?>
