<?php
/**
 * Created by PhpStorm.
 * User: Юрий
 * Date: 12.08.2016
 * Time: 16:33
 */
class tov_test {
	public static function redirect_offed_test_1() {
		
		$GLOBALS['is_testing']=1;
			
		$mm=[
				'ident'=>'tovar1',
				'rod'=>'cat',
				'type'=>'tov',
		]; 
			
		$result = tov::redirect_offed($mm);
		
		$result_true='cat';
		
		return $result===$result_true ? true : $result;

	}
	public static function redirect_offed_test_2() {

		$GLOBALS['is_testing']=1;

		$mm=[
				'ident'=>'tovar1',
				'rod'=>'cat',
				'type'=>'page',
		];

		$result = tov::redirect_offed($mm);

		$result_true='cat';

		return !$result ? true : $result;

	}
}