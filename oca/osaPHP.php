<?
/* ----------------------------------------------------- */
/* Автор: Евгений Дашкин (c)Deesoft 2008                 */
/* Функция расчета вопросов оксфордского теста           */
/* Вызов  OsaCalk(массив ответов)                        */
/* пол{1,2},возраст{1,2},ответы[200]{1-да,2-неопр,3-нет} */
/* Возвращаемое значение: строка для SMS сообщения       */
/* ----------------------------------------------------- */

function OcaCalk($otveti) {
//  $otveti            массив входящих ответов	
//  $PL;               ПОЛ 1-МУЖ. 2-ЖЕН.
//  $VZ;               ВОЗРAСТ 1-старше 18   2-до 18
//  $StrRezultCalk;    строка результатов расчетов теста
//  $mem_AP;           A РЕЗУЛЬТАТЫ ТЕСТА
//  $mem_BP;           B
//  $mem_CP;           C
//  $mem_DP;           D
//  $mem_EP;           E
//  $mem_FP;           F
//  $mem_GP;           G
//  $mem_HP;           H
//  $mem_IP;           I
//  $mem_JP;           J
//  $mem_P_T;          (1) M>50% ТЕСТ НЕ ПОКАЗАТЕЛЬНЫЙ

$TRAIT = array(
         array(2,4,6),
         array(6,5,3),
         array(6,4,3),
         array(3,4,6),
         array(4,4,5),
         array(3,3,6),
         array(3,4,5),
         array(2,4,5),
         array(3,4,4),
         array(5,4,4),
         array(3,4,5),
         array(2,4,6),
         array(5,4,3),
         array(2,4,5),
         array(3,3,6),
         array(3,4,4),
         array(2,4,5),
         array(2,6,6),
         array(5,3,3),
         array(5,5,3),
         array(2,3,6),
         array(1,5,6),
         array(5,4,3),
         array(3,5,6),
         array(3,1,5),
         array(2,3,6),
         array(5,4,4),
         array(6,2,2),
         array(6,3,3),
         array(3,4,6),
         array(5,4,4),
         array(2,4,6),
         array(6,5,3),
         array(6,3,3),
         array(5,5,2),
         array(3,5,6),
         array(3,4,6),
         array(4,4,5),
         array(3,4,5),
         array(2,3,5),
         array(6,4,4),
         array(5,3,3),
         array(4,4,5),
         array(2,3,6),
         array(3,4,5),
         array(3,3,5),
         array(2,5,6),
         array(3,4,5),
         array(6,4,4),
         array(4,4,3),
         array(5,4,2),
         array(3,4,4),
         array(3,4,6),
         array(6,3,3),
         array(2,5,6),
         array(5,3,3),
         array(5,4,4),
         array(6,3,3),
         array(1,3,5),
         array(2,5,6),
         array(2,4,6),
         array(5,4,4),
         array(5,4,4),
         array(2,2,6),
         array(4,4,5),
         array(4,4,3),
         array(2,4,5),
         array(6,4,3),
         array(6,3,3),
         array(4,4,5),
         array(3,5,6),
         array(3,4,4),
         array(2,4,5),
         array(2,4,6),
         array(5,3,3),
         array(1,3,6),
         array(6,2,2),
         array(2,5,6),
         array(5,4,3),
         array(6,3,3),
         array(6,3,3),
         array(3,4,6),
         array(3,3,6),
         array(6,3,2),
         array(6,3,3),
         array(2,4,6),
         array(3,3,5),
         array(5,4,3),
         array(2,5,6),
         array(3,3,5),
         array(3,4,6),
         array(3,4,4),
         array(5,3,3),
         array(5,4,4),
         array(3,4,5),
         array(6,4,2),
         array(5,5,3),
         array(4,4,5),
         array(2,4,5),
         array(4,3,1),
         array(5,4,4),
         array(2,3,5),
         array(4,4,6),
         array(2,3,6),
         array(6,4,3),
         array(2,3,7),
         array(2,3,6),
         array(3,4,5),
         array(3,4,5),
         array(5,4,4),
         array(2,4,5),
         array(6,2,2),
         array(6,4,3),
         array(6,4,3),
         array(3,4,6),
         array(6,4,3),
         array(3,4,5),
         array(7,2,2),
         array(5,4,3),
         array(5,4,3),
         array(4,4,5),
         array(3,4,6),
         array(5,4,3),
         array(2,3,6),
         array(1,3,4),
         array(4,4,6),
         array(5,4,4),
         array(3,4,6),
         array(2,5,6),
         array(3,5,6),
         array(5,3,2),
         array(2,4,6),
         array(6,4,3),
         array(4,4,3),
         array(2,4,5),
         array(3,5,5),
         array(3,3,6),
         array(5,3,3),
         array(5,4,4),
         array(2,5,6),
         array(2,5,6),
         array(1,3,5),
         array(2,4,6),
         array(5,4,3),
         array(5,5,3),
         array(1,3,5),
         array(6,4,4),
         array(3,4,6),
         array(3,5,5),
         array(2,5,6),
         array(2,5,6),
         array(6,5,3),
         array(2,4,6),
         array(3,3,5),
         array(7,1,1),
         array(6,4,2),
         array(7,5,2),
         array(5,5,3),
         array(5,4,2),
         array(2,2,6),
         array(2,5,6),
         array(6,4,3),
         array(5,4,4),
         array(5,5,2),
         array(6,3,3),
         array(3,3,6),
         array(6,4,2),
         array(6,4,3),
         array(2,3,6),
         array(3,4,5),
         array(3,3,6),
         array(3,4,6),
         array(6,3,2),
         array(5,4,3),
         array(0,1,5),
         array(2,2,6),
         array(6,4,4),
         array(5,3,3),
         array(0,1,5),
         array(3,5,6),
         array(3,3,6),
         array(3,4,6),
         array(2,4,6),
         array(3,4,6),
         array(6,6,3),
         array(1,3,5),
         array(3,3,5),
         array(2,2,6),
         array(3,5,5),
         array(6,2,2),
         array(1,4,5),
         array(2,4,6),
         array(5,4,3),
         array(4,5,6),
         array(6,4,2),
         array(2,5,6),
         array(2,4,5),
         array(2,5,6),
         array(0,3,5),
         array(5,3,3));

$WOMEN = array(
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-98,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-97,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-96,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-95,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-94,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-93,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-92,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-90,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-86,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-82,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-80,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-76,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-74,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-70,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-64,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-62,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-60,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-56,-98,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-52,-97,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-50,-96,-99,-99,-99,-99,-98),
  array(-99,-99,-99,-48,-94,-99,-99,-99,-99,-98),
  array(-99,-99,-99,-42,-92,-98,-99,-99,-99,-97),
  array(-99,-98,-99,-36,-90,-97,-99,-99,-99,-96),
  array(-99,-97,-98,-30,-86,-92,-99,-99,-99,-96),
  array(-98,-97,-98,-28,-82,-86,-99,-99,-99,-95),
  array(-97,-96,-97,-24,-78,-78,-99,-99,-99,-95),
  array(-96,-96,-97,-20,-74,-68,-99,-99,-99,-94),
  array(-95,-95,-96,-16,-66,-58,-99,-98,-99,-93),
  array(-94,-95,-96,-12,-58,-48,-99,-97,-99,-92),
  array(-92,-94,-95,-10,-48,-36,-99,-96,-98,-90),
  array(-90,-94,-94,-8,-38,-22,-98,-95,-98,-88),
  array(-88,-93,-93,-6,-26,-10,-98,-94,-98,-86),
  array(-84,-93,-92,-2,-12,3,-98,-93,-97,-84),
  array(-78,-92,-91,2,2,6,-97,-92,-97,-82),
  array(-74,-92,-90,6,4,9,-96,-91,-96,-78),
  array(-70,-91,-86,10,8,12,-96,-90,-96,-76),
  array(-64,-90,-82,14,12,15,-95,-89,-95,-74),
  array(-60,-88,-76,20,16,18,-94,-88,-92,-72),
  array(-54,-86,-70,24,20,21,-93,-87,-90,-70),
  array(-50,-82,-66,28,24,24,-92,-86,-86,-68),
  array(-44,-80,-60,32,28,27,-91,-85,-80,-66),
  array(-38,-76,-54,34,30,30,-90,-84,-70,-62),
  array(-28,-72,-48,36,34,33,-88,-83,-62,-60),
  array(-26,-68,-42,38,38,36,-87,-80,-50,-58),
  array(-20,-66,-36,40,42,39,-84,-76,-40,-56),
  array(-12,-62,-30,42,46,42,-80,-74,-28,-50),
  array(-4,-58,-26,44,50,45,-78,-70,-14,-46),
  array(4,-54,-20,46,54,48,-72,-66,-2,-44),
  array(12,-50,-14,48,58,51,-68,-62,10,-40),
  array(20,-46,-8,50,62,54,-64,-58,12,-36),
  array(28,-44,-2,52,66,57,-58,-56,22,-32),
  array(36,-40,6,54,70,61,-52,-52,34,-28),
  array(42,-38,8,58,72,64,-44,-46,46,-24),
  array(48,-32,18,62,76,67,-38,-42,56,-20),
  array(54,-28,24,66,80,70,-28,-36,64,-14),
  array(58,-22,30,68,84,72,-18,-30,72,-10),
  array(62,-16,38,70,88,75,-14,-22,80,-2),
  array(66,-10,44,76,90,78,-8,-12,86,2),
  array(70,-4,50,80,92,81,0,-10,92,8),
  array(72,2,58,85,94,84,10,-8,95,14),
  array(76,10,64,88,96,87,20,-6,98,22),
  array(78,20,70,90,97,90,30,-4,99,30),
  array(80,30,76,92,97,92,42,-2,99,38),
  array(84,38,80,95,98,94,54,0,99,44),
  array(86,46,84,96,99,96,66,6,99,52),
  array(88,54,88,97,99,97,78,16,99,58),
  array(90,62,92,97,99,98,80,24,99,64),
  array(92,68,94,98,99,99,82,34,99,68),
  array(94,74,96,99,99,99,86,42,99,74),
  array(96,80,98,99,99,99,88,50,99,80),
  array(98,86,99,99,99,99,90,58,99,84),
  array(99,92,99,99,99,99,98,64,99,88),
  array(99,98,99,99,99,99,99,70,99,90),
  array(99,99,99,99,99,99,99,76,99,94),
  array(99,99,99,99,99,99,99,80,99,98),
  array(99,99,99,99,99,99,99,86,99,99),
  array(99,99,99,99,99,99,99,90,99,99),
  array(99,99,99,99,99,99,99,99,99,99),
  array(99,99,99,99,99,99,99,99,99,99),
  array(99,99,99,99,99,99,99,99,99,99),
  array(99,99,99,99,99,99,99,99,99,99));

$MEN = array(
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-98,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-97,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-96,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-95,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-94,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-93,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-92,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-90,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-86,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-82,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-76,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-76,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-70,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-64,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-62,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-60,-98,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-56,-94,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-52,-92,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-50,-90,-99,-99,-99,-99,-98),
  array(-99,-99,-99,-48,-82,-99,-99,-99,-99,-97),
  array(-99,-99,-99,-42,-76,-98,-99,-99,-99,-96),
  array(-99,-99,-99,-36,-70,-97,-99,-99,-99,-95),
  array(-99,-99,-99,-30,-62,-96,-99,-99,-99,-94),
  array(-99,-99,-99,-28,-56,-95,-99,-99,-99,-92),
  array(-99,-99,-99,-24,-50,-94,-99,-98,-99,-90),
  array(-99,-99,-99,-20,-46,-90,-99,-98,-99,-86),
  array(-99,-99,-99,-16,-40,-85,-99,-97,-99,-84),
  array(-99,-99,-99,-12,-36,-80,-99,-96,-99,-82),
  array(-99,-99,-99,-10,-30,-72,-99,-96,-98,-80),
  array(-98,-99,-99,-8,-24,-64,-99,-96,-98,-78),
  array(-97,-99,-99,-6,-18,-58,-99,-95,-97,-77),
  array(-97,-99,-99,-2,-12,-50,-98,-94,-97,-76),
  array(-96,-99,-99,2,-6,-40,-98,-93,-96,-74),
  array(-94,-99,-99,8,-2,-30,-96,-92,-96,-72),
  array(-92,-99,-99,14,4,-20,-94,-91,-95,-70),
  array(-88,-99,-99,20,8,-12,-92,-90,-92,-68),
  array(-84,-99,-98,28,12,-2,-92,-90,-90,-64),
  array(-82,-98,-98,30,16,4,-90,-90,-86,-62),
  array(-80,-97,-98,32,20,8,-88,-89,-80,-60),
  array(-76,-96,-98,34,24,12,-88,-88,-76,-58),
  array(-74,-95,-97,36,28,16,-86,-87,-70,-56),
  array(-64,-95,-96,38,32,20,-84,-86,-64,-50),
  array(-58,-94,-94,40,36,24,-82,-84,-56,-46),
  array(-52,-93,-92,42,40,28,-80,-80,-46,-44),
  array(-46,-92,-90,44,44,32,-76,-78,-34,-40),
  array(-40,-90,-86,46,46,36,-72,-76,-22,-36),
  array(-34,-90,-82,48,48,40,-66,-74,-10,-32),
  array(-28,-86,-76,50,50,44,-62,-70,2,-28),
  array(-20,-82,-70,52,54,48,-56,-66,18,-24),
  array(-14,-80,-64,54,58,52,-52,-62,32,-20),
  array(-6,-78,-58,58,62,56,-46,-58,46,-14),
  array(0,-70,-52,62,66,60,-40,-54,60,-10),
  array(8,-66,-46,66,70,70,-32,-50,72,-4),
  array(14,-60,-40,68,72,74,-26,-46,80,2),
  array(22,-55,-34,70,76,78,-18,-42,82,8),
  array(28,-50,-28,76,80,82,-12,-36,86,16),
  array(36,-44,-22,80,84,86,-4,-32,90,22),
  array(42,-40,-16,85,88,88,4,-28,96,30),
  array(50,-34,-10,88,90,90,12,-24,97,38),
  array(56,-28,-4,90,93,91,16,-18,98,44),
  array(62,-24,2,92,94,92,20,-14,99,50),
  array(68,-18,8,95,97,93,28,-10,99,54),
  array(74,-12,14,96,98,94,36,-4,99,58),
  array(78,-4,20,98,99,95,46,2,99,60),
  array(82,2,28,98,99,96,56,16,99,62),
  array(90,8,34,98,99,97,66,28,99,66),
  array(92,16,40,98,99,98,74,36,99,70),
  array(94,24,48,98,99,99,82,40,99,76),
  array(95,32,54,98,99,99,88,48,99,80),
  array(96,40,60,99,99,99,92,56,99,90),
  array(98,48,68,99,99,99,94,64,99,96),
  array(99,56,74,99,99,99,96,72,99,97),
  array(99,64,82,99,99,99,99,78,99,98),
  array(99,72,88,99,99,99,99,84,99,99),
  array(99,78,94,99,99,99,99,90,99,99),
  array(99,84,99,99,99,99,99,99,99,99),
  array(99,88,99,99,99,99,99,99,99,99),
  array(99,92,99,99,99,99,99,99,99,99),
  array(99,99,99,99,99,99,99,99,99,99),
  array(99,99,99,99,99,99,99,99,99,99));

$GIRLS = array(
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-98,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-97,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-96,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-95,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-94,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-93,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-92,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-90,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-86,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-82,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-80,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-76,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-74,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-70,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-64,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-62,-99,-99,-99,-99,-99,-98),
  array(-98,-99,-99,-60,-99,-99,-99,-99,-99,-98),
  array(-98,-99,-99,-56,-99,-99,-99,-99,-99,-97),
  array(-97,-99,-99,-52,-98,-99,-99,-99,-99,-97),
  array(-97,-99,-99,-50,-98,-99,-99,-99,-99,-96),
  array(-96,-99,-99,-48,-97,-99,-99,-99,-99,-96),
  array(-96,-99,-99,-42,-96,-99,-99,-99,-99,-95),
  array(-95,-99,-99,-36,-94,-98,-99,-99,-99,-95),
  array(-94,-99,-99,-30,-92,-96,-99,-99,-99,-94),
  array(-93,-98,-98,-28,-90,-92,-98,-99,-99,-94),
  array(-92,-98,-98,-24,-88,-88,-98,-98,-99,-93),
  array(-91,-97,-97,-20,-86,-82,-97,-98,-98,-92),
  array(-90,-97,-97,-16,-80,-76,-97,-98,-98,-92),
  array(-86,-96,-96,-12,-74,-68,-96,-97,-97,-91),
  array(-82,-96,-95,-10,-70,-60,-95,-97,-97,-91),
  array(-78,-95,-95,-8,-64,-52,-94,-96,-96,-90),
  array(-74,-95,-94,-6,-58,-42,-93,-96,-96,-90),
  array(-72,-94,-94,-2,-50,-30,-92,-95,-95,-90),
  array(-68,-94,-93,2,-42,-20,-91,-95,-95,-90),
  array(-62,-93,-93,6,-34,-6,-90,-94,-94,-88),
  array(-56,-93,-92,10,-24,2,-88,-94,-93,-86),
  array(-50,-92,-92,14,-6,4,-86,-93,-92,-86),
  array(-42,-91,-91,20,-2,10,-84,-93,-91,-84),
  array(-34,-91,-91,24,2,16,-82,-92,-90,-82),
  array(-26,-90,-90,28,4,20,-78,-78,-88,-80),
  array(-16,-90,-90,32,6,24,-74,-76,-84,-78),
  array(-8,-88,-88,34,8,30,-68,-72,-78,-76),
  array(0,-86,-84,36,10,32,-62,-68,-72,-74),
  array(6,-84,-82,38,14,36,-54,-64,-68,-72),
  array(12,-82,-78,40,18,38,-46,-60,-56,-70),
  array(18,-80,-74,42,22,40,-38,-56,-46,-66),
  array(24,-76,-72,44,26,44,-28,-50,-34,-64),
  array(28,-72,-66,46,30,46,-20,-46,-20,-60),
  array(34,-68,-62,48,34,50,-12,-40,-8,-56),
  array(36,-64,-54,50,38,52,-2,-34,6,-52),
  array(42,-60,-48,52,42,54,6,-28,20,-48),
  array(46,-54,-44,54,46,56,14,-24,32,-44),
  array(52,-48,-38,58,50,58,22,-16,44,-38),
  array(56,-46,-30,62,54,60,32,-10,54,-36),
  array(60,-42,-24,66,58,64,40,-2,64,-32),
  array(64,-34,-18,68,62,66,48,6,74,-28),
  array(66,-30,-12,70,66,70,58,14,82,-18),
  array(70,-24,-6,76,70,72,66,22,88,-12),
  array(74,-16,2,80,74,76,72,32,90,-2),
  array(76,-10,10,85,78,80,78,36,94,6),
  array(78,-4,18,88,82,86,82,42,96,16),
  array(82,6,26,90,88,88,86,48,99,24),
  array(84,12,38,92,90,90,90,54,99,34),
  array(86,18,46,92,92,92,91,60,99,44),
  array(88,26,54,95,94,94,92,64,99,50),
  array(92,34,62,97,96,96,93,70,99,54),
  array(94,42,68,97,98,98,94,74,99,62),
  array(96,50,74,98,99,99,95,78,99,68),
  array(97,60,78,99,99,99,96,82,99,74),
  array(98,62,82,99,99,99,97,86,99,80),
  array(99,70,86,99,99,99,98,90,99,86),
  array(99,74,90,99,99,99,99,92,99,90),
  array(99,80,92,99,99,99,99,94,99,94),
  array(99,84,96,99,99,99,99,96,99,98),
  array(99,86,98,99,99,99,99,98,99,99),
  array(99,90,99,99,99,99,99,99,99,99),
  array(99,96,99,99,99,99,99,99,99,99),
  array(99,99,99,99,99,99,99,99,99,99),
  array(99,99,99,99,99,99,99,99,99,99),
  array(99,99,99,99,99,99,99,99,99,99),
  array(99,99,99,99,99,99,99,99,99,99));

$BOYS = array(
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-99,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-98,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-97,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-96,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-95,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-94,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-93,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-92,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-90,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-86,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-82,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-80,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-76,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-74,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-70,-99,-99,-99,-99,-99,-99),
  array(-99,-99,-99,-64,-99,-99,-99,-99,-99,-98),
  array(-99,-99,-99,-62,-99,-99,-99,-99,-99,-97),
  array(-99,-99,-99,-60,-99,-99,-99,-99,-99,-97),
  array(-99,-99,-99,-56,-98,-99,-99,-99,-99,-96),
  array(-98,-99,-99,-52,-97,-99,-99,-99,-99,-96),
  array(-98,-99,-99,-50,-95,-99,-99,-99,-99,-95),
  array(-97,-99,-99,-48,-94,-99,-99,-99,-99,-94),
  array(-97,-99,-99,-42,-93,-98,-99,-99,-99,-94),
  array(-96,-99,-99,-36,-92,-97,-99,-99,-99,-93),
  array(-96,-99,-99,-30,-91,-96,-99,-99,-99,-93),
  array(-95,-98,-99,-28,-90,-95,-99,-98,-99,-92),
  array(-94,-98,-99,-24,-88,-94,-99,-98,-99,-92),
  array(-93,-97,-99,-20,-86,-90,-98,-97,-98,-91),
  array(-92,-97,-98,-16,-82,-85,-98,-97,-98,-91),
  array(-91,-96,-98,-12,-78,-80,-97,-97,-97,-90),
  array(-90,-96,-98,-10,-72,-72,-97,-96,-96,-90),
  array(-88,-96,-97,-8,-66,-64,-96,-96,-95,-88),
  array(-84,-95,-97,-6,-58,-58,-96,-95,-94,-86),
  array(-82,-95,-96,-2,-48,-50,-95,-94,-93,-84),
  array(-80,-94,-96,2,-40,-40,-95,-94,-92,-82),
  array(-76,-94,-95,6,-26,-30,-94,-93,-91,-80),
  array(-72,-93,-95,10,-22,-20,-94,-92,-90,-76),
  array(-66,-93,-94,14,-14,-12,-93,-91,-88,-74),
  array(-60,-92,-94,20,-4,-2,-93,-90,-86,-72),
  array(-54,-92,-93,24,2,2,-92,-89,-84,-68),
  array(-46,-91,-93,28,4,4,-92,-88,-80,-66),
  array(-40,-91,-92,32,10,6,-91,-87,-78,-64),
  array(-32,-90,-92,34,16,10,-91,-86,-70,-60),
  array(-26,-90,-91,36,20,14,-90,-84,-62,-56),
  array(-18,-89,-91,38,24,18,-86,-82,-56,-52),
  array(-12,-89,-90,40,28,22,-80,-80,-40,-48),
  array(-6,-88,-88,42,32,24,-76,-78,-28,-44),
  array(0,-86,-86,44,36,26,-70,-76,-16,-40),
  array(6,-84,-84,46,40,30,-66,-74,-4,-36),
  array(12,-82,-80,48,44,36,-60,-72,6,-32),
  array(18,-78,-76,50,48,42,-54,-68,18,-28),
  array(24,-77,-74,52,52,50,-46,-62,28,-24),
  array(30,-76,-66,54,56,54,-36,-58,40,-20),
  array(36,-72,-68,58,60,60,-28,-52,50,-14),
  array(42,-70,-54,62,64,66,-18,-40,62,-8),
  array(48,-66,-46,66,68,70,-10,-28,72,-2),
  array(52,-62,-40,68,72,76,0,-22,80,4),
  array(58,-58,-32,70,74,78,4,-14,86,12),
  array(64,-54,-26,76,78,80,18,-10,90,18),
  array(70,-48,-20,80,80,84,26,-4,94,24),
  array(76,-44,-12,85,84,86,34,2,96,30),
  array(80,-36,-6,88,88,88,44,8,98,38),
  array(84,-32,2,90,90,90,50,14,99,46),
  array(88,-26,10,92,92,92,58,22,99,54),
  array(90,-18,22,95,94,94,66,30,99,60),
  array(91,-6,34,96,96,96,72,40,99,68),
  array(92,6,44,97,97,97,78,48,99,74),
  array(93,10,54,97,97,98,82,56,99,80),
  array(94,20,64,98,98,99,84,64,99,86),
  array(95,36,72,99,99,99,90,70,99,90),
  array(96,44,78,99,99,99,92,76,99,94),
  array(98,54,82,99,99,99,94,82,99,95),
  array(99,62,86,99,99,99,96,86,99,96),
  array(99,68,90,99,99,99,98,90,99,97),
  array(99,76,94,99,99,99,99,94,99,98),
  array(99,82,98,99,99,99,99,98,99,99),
  array(99,90,99,99,99,99,99,99,99,99),
  array(99,96,99,99,99,99,99,99,99,99),
  array(99,98,99,99,99,99,99,99,99,99),
  array(99,99,99,99,99,99,99,99,99,99),
  array(99,99,99,99,99,99,99,99,99,99),
  array(99,99,99,99,99,99,99,99,99,99));

     $PL = $otveti[0];
     $VZ = $otveti[1];
     $MM = 0; $MI=0; $V=0;
     for ($V=2;$V<=201;$V++) {
  	    if ($otveti[$V]==2) {
      	   $MM++;
        }
        $PROM[$V-1] = $TRAIT[$V-2][$otveti[$V]-1];
     }
     $MEM_P_T = 0;
     $SM=0;
     $SM += $PROM[1]; $SM += $PROM[8]; $SM += $PROM[15]; $SM += $PROM[17]; $SM += $PROM[42]; $SM += $PROM[46]; $SM += $PROM[52];
     $SM += $PROM[58]; $SM += $PROM[83]; $SM += $PROM[87]; $SM += $PROM[93]; $SM += $PROM[96]; $SM += $PROM[124]; $SM += $PROM[128];
     $SM += $PROM[131]; $SM += $PROM[138]; $SM += $PROM[165]; $SM += $PROM[169]; $SM += $PROM[173]; $SM += $PROM[176];
     IF ($PL==1) {
       IF ($VZ==1) {
         $MEM_AP = $MEN[$SM][0];
       }  
       IF ($VZ==2) {
          $MEM_AP = $BOYS[$SM][0];
       }
     } 
     IF ($PL==2) {
       IF ($VZ==1) {
          $MEM_AP = $WOMEN[$SM][0];
       }   
       IF ($VZ==2) {
          $MEM_AP = $GIRLS[$SM][0];
       }   
     } 
     $SM=0;
     $SM += $PROM[21]; $SM += $PROM[27]; $SM += $PROM[33]; $SM += $PROM[36]; $SM += $PROM[62]; $SM += $PROM[68]; $SM += $PROM[71];
     $SM += $PROM[78]; $SM += $PROM[101]; $SM += $PROM[106]; $SM += $PROM[113]; $SM += $PROM[116]; $SM += $PROM[141]; $SM += $PROM[148];
     $SM += $PROM[151]; $SM += $PROM[158]; $SM += $PROM[181]; $SM += $PROM[188]; $SM += $PROM[192]; $SM += $PROM[196];
     IF ($PL==1) {
       IF ($VZ==1) {
          $MEM_BP = $MEN[$SM][1];
       }   
       IF ($VZ==2) {
          $MEM_BP = $BOYS[$SM][1];
       }   
     } 
     IF ($PL==2) {
       IF ($VZ==1) {
          $MEM_BP = $WOMEN[$SM][1];
       }   
       IF ($VZ==2) {
          $MEM_BP = $GIRLS[$SM][1];
       }   
     } 
     $SM=0;
     $SM += $PROM[2]; $SM += $PROM[6]; $SM += $PROM[11]; $SM += $PROM[18]; $SM += $PROM[43]; $SM += $PROM[47]; $SM += $PROM[53];
     $SM += $PROM[56]; $SM += $PROM[81]; $SM += $PROM[86]; $SM += $PROM[91]; $SM += $PROM[97]; $SM += $PROM[122]; $SM += $PROM[130];
     $SM += $PROM[132]; $SM += $PROM[136]; $SM += $PROM[164]; $SM += $PROM[166]; $SM += $PROM[171]; $SM += $PROM[177];
     IF ($PL==1) {
       IF ($VZ==1) {
          $MEM_CP = $MEN[$SM][2];
       }
       IF ($VZ==2) {
          $MEM_CP = $BOYS[$SM][2];
       }   
     } 
     IF ($PL==2) {
       IF ($VZ==1) {
          $MEM_CP = $WOMEN[$SM][2];
       }   
       IF ($VZ==2) {
          $MEM_CP = $GIRLS[$SM][2];
       }   
     } 
     $SM=0;
     $SM += $PROM[22]; $SM += $PROM[26]; $SM += $PROM[32]; $SM += $PROM[40]; $SM += $PROM[61]; $SM += $PROM[67]; $SM += $PROM[73];
     $SM += $PROM[76]; $SM += $PROM[102]; $SM += $PROM[108]; $SM += $PROM[111]; $SM += $PROM[117]; $SM += $PROM[142]; $SM += $PROM[146];
     $SM += $PROM[153]; $SM += $PROM[156]; $SM += $PROM[184]; $SM += $PROM[186]; $SM += $PROM[191]; $SM += $PROM[197];
     IF ($PL==1) {
       IF ($VZ==1) {
          $MEM_DP = $MEN[$SM][3];
       }   
       IF ($VZ==2) {
          $MEM_DP = $BOYS[$SM][3];
       }   
     }
     IF ($PL==2) {
       IF ($VZ==1) {
          $MEM_DP = $WOMEN[$SM][3];
       }
       IF ($VZ==2) {
          $MEM_DP = $GIRLS[$SM][3];
       }   
     } 
     $SM=0;
     $SM += $PROM[3]; $SM += $PROM[7]; $SM += $PROM[12]; $SM += $PROM[16]; $SM += $PROM[41]; $SM += $PROM[48]; $SM += $PROM[51];
     $SM += $PROM[57]; $SM += $PROM[85]; $SM += $PROM[90]; $SM += $PROM[92]; $SM += $PROM[99]; $SM += $PROM[121]; $SM += $PROM[127];
     $SM += $PROM[134]; $SM += $PROM[137]; $SM += $PROM[162]; $SM += $PROM[168]; $SM += $PROM[175]; $SM += $PROM[179];
     IF ($PL==1) {
       IF ($VZ==1) {
          $MEM_EP = $MEN[$SM][4];
       }  
       IF ($VZ==2) {
          $MEM_EP = $BOYS[$SM][4];
       }   
     }
     IF ($PL==2) {
       IF ($VZ==1) {
          $MEM_EP = $WOMEN[$SM][4];
       }   
       IF ($VZ==2) {
          $MEM_EP = $GIRLS[$SM][4];
       }   
     }
     $SM=0;
     $SM += $PROM[23]; $SM += $PROM[29]; $SM += $PROM[31]; $SM += $PROM[38]; $SM += $PROM[65]; $SM += $PROM[66]; $SM += $PROM[72];
     $SM += $PROM[79]; $SM += $PROM[103]; $SM += $PROM[107]; $SM += $PROM[114]; $SM += $PROM[120]; $SM += $PROM[145]; $SM += $PROM[147];
     $SM += $PROM[154]; $SM += $PROM[159]; $SM += $PROM[185]; $SM += $PROM[187]; $SM += $PROM[195]; $SM += $PROM[199];
     IF ($PL==1) {
       IF ($VZ==1) {
          $MEM_FP = $MEN[$SM][5];
       }
       IF ($VZ==2) {
          $MEM_FP = $BOYS[$SM][5];
       }   
     } 
     IF ($PL==2) {
       IF ($VZ==1) {
          $MEM_FP = $WOMEN[$SM][5];
       }   
       IF ($VZ==2) {
          $MEM_FP = $GIRLS[$SM][5];
       }
     }
     $SM=0;
     $SM += $PROM[4]; $SM += $PROM[10]; $SM += $PROM[13]; $SM += $PROM[20]; $SM += $PROM[45]; $SM += $PROM[49]; $SM += $PROM[55];
     $SM += $PROM[60]; $SM += $PROM[82]; $SM += $PROM[89]; $SM += $PROM[95]; $SM += $PROM[100]; $SM += $PROM[123]; $SM += $PROM[126];
     $SM += $PROM[133]; $SM += $PROM[140]; $SM += $PROM[161]; $SM += $PROM[167]; $SM += $PROM[172]; $SM += $PROM[180];
     IF ($PL==1) {
       IF ($VZ==1) {
          $MEM_GP = $MEN[$SM][6];
       }
       IF ($VZ==2) {
          $MEM_GP = $BOYS[$SM][6];
       }   
     }
     IF ($PL==2) {
       IF ($VZ==1) {
          $MEM_GP = $WOMEN[$SM][6];
       }   
       IF ($VZ==2) {
          $MEM_GP = $GIRLS[$SM][6];
       }   
     }
     $SM=0;
     $SM += $PROM[24]; $SM += $PROM[30]; $SM += $PROM[35]; $SM += $PROM[37]; $SM += $PROM[63]; $SM += $PROM[70]; $SM += $PROM[74];
     $SM += $PROM[80]; $SM += $PROM[105]; $SM += $PROM[109]; $SM += $PROM[115]; $SM += $PROM[119]; $SM += $PROM[143]; $SM += $PROM[150];
     $SM += $PROM[152]; $SM += $PROM[157]; $SM += $PROM[182]; $SM += $PROM[189]; $SM += $PROM[194]; $SM += $PROM[198];
     IF ($PL==1) {
       IF ($VZ==1) {
          $MEM_HP = $MEN[$SM][7];
       }
       IF ($VZ==2) {
          $MEM_HP = $BOYS[$SM][7];
       }
     }
     IF ($PL==2) {
       IF ($VZ==1) {
          $MEM_HP = $WOMEN[$SM][7];
       }
       IF ($VZ==2) {
          $MEM_HP = $GIRLS[$SM][7];
       }
     }
     $SM=0;
     $SM += $PROM[5]; $SM += $PROM[9]; $SM += $PROM[14]; $SM += $PROM[19]; $SM += $PROM[44]; $SM += $PROM[50]; $SM += $PROM[54];
     $SM += $PROM[59]; $SM += $PROM[84]; $SM += $PROM[88]; $SM += $PROM[94]; $SM += $PROM[98]; $SM += $PROM[125]; $SM += $PROM[129];
     $SM += $PROM[135]; $SM += $PROM[139]; $SM += $PROM[163]; $SM += $PROM[170]; $SM += $PROM[174]; $SM += $PROM[178];
     IF ($PL==1) {
       IF ($VZ==1) {
          $MEM_IP = $MEN[$SM][8];
       }
       IF ($VZ==2) {
          $MEM_IP = $BOYS[$SM][8];
       }
     }     
     IF ($PL==2) {
       IF ($VZ==1) {
          $MEM_IP = $WOMEN[$SM][8];
       }

       IF ($VZ==2) {
          $MEM_IP = $GIRLS[$SM][8];
       }
     }
     $SM=0;
     $SM += $PROM[25]; $SM += $PROM[28]; $SM += $PROM[34]; $SM += $PROM[39]; $SM += $PROM[64]; $SM += $PROM[69]; $SM += $PROM[75];
     $SM += $PROM[77]; $SM += $PROM[104]; $SM += $PROM[110]; $SM += $PROM[112]; $SM += $PROM[118]; $SM += $PROM[144]; $SM += $PROM[149];
     $SM += $PROM[155]; $SM += $PROM[160]; $SM += $PROM[183]; $SM += $PROM[190]; $SM += $PROM[193]; $SM += $PROM[200];
     IF ($PL==1) {
       IF ($VZ==1) {
          $MEM_JP = $MEN[$SM][9];
       }
       IF ($VZ==2) {
          $MEM_JP = $BOYS[$SM][9];
       }
     }
     IF ($PL==2) {
       IF ($VZ==1) {
          $MEM_JP = $WOMEN[$SM][9];
       }
       IF ($VZ==2) {
          $MEM_JP = $GIRLS[$SM][9];
       }
     }
     IF ($PROM[198] == 2) {
        $MEM_BP = $MEM_BP.")";
     }
     IF ($PROM[22] == 1) {
        $MEM_EP = $MEM_EP.")";
     }
     IF ($PL==1) {
       IF ($VZ==1) {
          $MEM_MESSAGE = " М>18 ";
       }
       IF ($VZ==2) {
          $MEM_MESSAGE = " М14-18 ";
       }   
     }
     IF ($PL==2) {
       IF ($VZ==1) {
          $MEM_MESSAGE = " Ж>18 ";
       }
       IF ($VZ==2) {
         $MEM_MESSAGE = " Ж14-18 ";
       }
     }
     IF ($MM > 100) {
        $MEM_P_T = 1;
        $MEM_MESSAGE = $MEM_MESSAGE . " (M)>50% ";
     }   
     $StrRezultCalk = ( $MEM_MESSAGE."A: ".$MEM_AP."  B:".$MEM_BP."  C:".$MEM_CP."  D:".$MEM_DP."  E:".$MEM_EP."  F:".$MEM_FP."  G:".$MEM_GP."  H:".$MEM_HP."  I:".$MEM_IP."  J:".$MEM_JP );
  $array_result=array(
	'cols'=>array($MEM_AP,$MEM_BP, $MEM_CP, $MEM_DP, $MEM_EP, $MEM_FP, $MEM_GP, $MEM_HP, $MEM_IP, $MEM_JP),
	'mem'=> $MEM_MESSAGE
);	
     return array( ($StrRezultCalk), ($array_result));
} //end function


if ( $echo==1) 
{
 echo '<font color=#1F07F9>
<b>test функции OCA<b><br><br>
<pre>
  Пример расчета ответов теста
  ответы 1+1+200 значений,1-й пол (1,2),2-й возраст (1,2), блок из 200 - ответы в формате (1-нет,2-могобыть,3-нет)
  $otv = array(1,1,1,1,1,1,1,2,3,3,3,3,3,3,3,1,1,1,1,1,1,1,1,2,1,1,1,3,3,3,3,3,3,3,3,3,3,1,1,1,1,1,1,1,1,1,3,1,1,1,1,1,1,1,1,2,2,1,1,1,1,1,1,1,1,1,3,3,3,3,3,3,3,3,3,3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,3,3,2,3,3,3,3,1,1,1,3,3,3,3,3,3,3,3,3,3,3,3,3,3,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,3,3,3,3,3,3,3,3,3,3,1,1,1,1,3,3,3,1,3,1,3,1,3,1,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,3,3,3,1,1,3,3,1,1,3,3,1,1,3,3,3,1,1,1);
  $rezult = OcaCalk($otv);
</pre></font>';


  // Пример расчета ответов теста
  // ответы 1+1+200 значений,1-й пол (1,2),2-й возраст (1,2), блок из 200 - ответы в формате (1-нет,2-могобыть,3-нет)
  
  $otv = array(1,1,1,3,1,1,2,3,3,3,3,3,3,3,1,1,1,1,1,1,1,1,2,1,1,1,3,3,3,3,3,3,3,3,3,3,1,1,1,1,1,1,1,1,1,3,1,1,1,1,1,1,1,1,2,2,1,1,1,1,1,1,1,1,1,3,3,3,3,3,3,3,3,3,3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,3,3,2,3,3,3,3,1,1,1,3,3,3,3,3,3,3,3,3,3,3,3,3,3,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,3,3,3,3,3,3,3,3,3,3,1,1,1,1,3,3,3,1,3,1,3,1,3,1,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,3,3,3,1,1,3,3,1,1,3,3,1,1,3,3,3,1,1,1);
  $rezult = OcaCalk($otv);

  echo $rezult; //результаты на экран
  echo '<br><br>Расшифровка: М>18 - мужчина, возраст > 18 лет';
  echo '<br> дальше КОЛОНКА:РЕЗУЛЬТАТ, скобка справа цыфры - маник';
}
  
?>