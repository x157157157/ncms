<?php

	/**
	 * Created by PhpStorm.
	 * User: Юрий
	 * Date: 15.08.2016
	 * Time: 13:45
	 */

//var_dump(__LINE__);

	class _1c {
		/**
		 * событие срабатываемое при новой заказе
		 * @param $order_id_new ИД нового заказа
		 */
		public static function event_order_new( $order_id_new ){


			// todo хотелось бы отправку отдельным асинхронным способом но пока по лохански
			_1c::order_send($order_id_new);
		}



		//todo делаем отправитель всех неотправльенных заказов send_all()
		//todo делаем задание cron раз в пять минут

		/**
		 * эта функция отправляем все не отправленные заказы в 1с, и будет дёргатся кроном
		 */
		public static function orders_send_all() {

			$id_order_start = o('1c_start_order_id');  //стартовый номер отправки

			$pr = my_mysql_query(vl('SELECT id FROM ' . pref_db . 'orders
			WHERE  (id>23477) AND NOT (order_id_1c LIKE "sending") AND (order_id_1c ="err" OR order_id_1c="при" OR order_id_1c="")
			'));

			while ($mm = mysqli_fetch_array($pr)) {
				$id_arr[] = $mm[0];

			}
			//vl($id_arr);
			// $id_arr  массив номеров не отправленных заказов

			set_time_limit(360);
			$num_orders=0;
			foreach ( $id_arr as $id) {
				if (mydbget('orders', 'id', $id )['order_id_1c']!='sending') {
					mydbUpdate('orders', ['order_id_1c'=>'sending'], 'id', $id );
					_1c::order_send($id);
					$num_orders++;
				}

			}
			count($id_arr)==0 ? $otpravka='' : $otpravka='отправка';
			loger('', 'cron 1c', '', '', $otpravka, 'отправлено '.$num_orders.' из '.count($id_arr),  date('Y.m.d H:i:s' ));
		}


		/**
		 * Отправлялщик одного заказа в 1с
		 * @param $order_id_new ИД нового заказа
		 */
		public static function order_send( $order_id_new) {

			// получаем масссив нового заказа
			$order=mydbget('orders', 'id', $order_id_new ) ;

			// получаем сопоставления способов доставки статусам в 1с
			$statuses=explode_assoc( '=',o('1c_spdost2status'));

			$dost_code_arr=explode_assoc( '=',o('1c_spdost2dost_code'));


			$url_1c=o('1c_url');
			//$url_1c='http://ncms/1c_test_server';

			$login=o('1c_login');
			$pass=o('1c_pass');

			// создаём массив для отправки в 1с
			$data_out = fn('_1c::data_gen', $order, $statuses, $dost_code_arr );


			loger(uid(),'_1c::order_send',0, 0,0,get_defined_vars(),0);

			// отправляем
			return [get_defined_vars(),fn('_1c::data_send', $url_1c, $login, $pass , $data_out )];


		}

		/**
		 * _1c::data_send_test_1()
		 */
		public static function order_send_test_1() {
			//test_start();
			return _1c::order_send(1247);
			//test_stop();

		}

		public static function order_send_test_2() {
			//test_start();
			if ($_GET['order_id']) {

				_1c::order_send($_GET['order_id']);

				exit();
			}
			return smarty_tpl('1c_send_tpl', $p ) ;

			//test_stop();

		}

		public static function data_send( $url_1c, $login, $pass , $data_out   ){

			if( $curl = curl_init() ) {
				curl_setopt($curl, CURLOPT_URL, $url_1c);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
				curl_setopt($curl, CURLOPT_POST, true);
				curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode( $data_out, JSON_UNESCAPED_UNICODE ) );



				curl_setopt($curl, CURLOPT_HTTPHEADER, ['Authorization: Basic '.base64_encode($login.':'.$pass), 'Content-Type: text/plain'] );

				$response = curl_exec($curl);
				v($response);
				$order_1c=_1c::response_save($response);

				v($order_1c);  //номер заказа 1с
				v($data_out['number']);  // номер заказа с сайта

				$data['order_id_1c']=$order_1c;

				//записываем номер заказа 1с в БД orders
				$cid = mydbUpdate( 'orders', $data, 'id', $data_out['number']);

				$info= curl_getinfo($curl);

				curl_close($curl);

				$out=[
					'data_out'=>$data_out,
					'response'=>$response,
					'info'=>$info,
				];

				loger( uid(), '_1c::data_send', 0, 0, 0, $out, date('Y.m.d H:i:s')  );

				return $out;
			}

		}

		public static function response_save ($response) {
			if (substr($response, 3, 5)=='Order') {
				$order_1c = explode(' ', $response)[3];
			} else {
				$order_1c = 'err';
			}
			return $order_1c;

		}
		public static function data_send_test_1(){

			//$url_1c=o('1c_url');
			$url_1c='http://ncms/1c_test_server';
			$login=o('1c_login');
			$pass=o('1c_pass');

			return self::data_send( $url_1c, $login, $pass , $data_out );


		}


		/**
		 * тестовый сервер
		 */
		public static function test_server() {

			$cont= [
				'_POST'=>$_POST,
				'_HEADERS'=>getallheaders(),
				'php://input'=>file_get_contents('php://input'),
			];

			loger(false,'1c_test_server_req', 0, 0, 0 , print_r($cont, 1 ), 0 );
		}




		public static function data_gen($order, $statuses, $dost_code_arr) {

			$data=[
				'number'=>$order['id'],
				'date'=> date( 'YmdHis', $order['ordertime']),
				'status' => $statuses[ $order['spdost'] ],
				'client' => $client
			];

			// массив контрагента
			$client=[];
			if(  ($order['inn'] )  ) { // юрлицо

				$client['name']= $order['orgname'];
				$client['inn']=$order['inn'];

				if( strlen ($order['inn']==12 ) ) { // если ИП
					$client['type']='ИП';
				} else if( strlen ($order['inn'] ) == 10 ) { // OOO

					$client['type'] = 'Юрлицо';
				} else {
					$client['type'] = 'Физлицо';
				}
			} else {
				$client['name']= $order['family'].' '.$order['name'].' '.$order['patronymic'];
				$client['type']='Физлицо';
				$client['inn']='';
				//v($client['type']);


			}

			$client['phone']=  strtr( $order['phone'] , ['-'=>'',' '=>'','('=>'',')'=>''] ); // удалаем спецсимволы



			if (strpos($order['email'], '@')) {$client['email']=$order['email'];} else {$client['email']='';}
			$client['adress']= implode_notnul( ', ',
				[
					$order['country'],
					$order['postindex'],
					$order['region'],
					$order['city'],
					$order['adress'],
				]
			);


			$data['client']=$client;

			foreach (unserialize( $order['orderdocs'] ) as $k=>$item ) {
				$items_out[$k]['Item']= [
					'nametov'=>$item['tovname'],
					'artikul'=>$item['art'],
					'count'=>$item['num'],
					'price'=>floatval( $item['price']),
					'sum'=> floatval( $item['price']) * $item['num'],
					'unit'=> 'Шт',
				];
			}

			$data['items']=$items_out;

			$data['dost_code']=$dost_code_arr[ $order['spdost'] ];

			$data['allsum'] = $order['totalsum']; // todo выяснить с доставкой сумму или без, и можно ли точку а не запятую

			$data['dost_price'] = $order['pricedost'];

			$data['comment'] = $order['comment']; //

			return $data ;
		}

		public static function data_gen_test_1(){

			$order=mydbget('orders', 'id', '1246' ) ;

			$statuses=explode_assoc( '=',o('1c_spdost2status'));

			$result = fn('_1c::data_gen', $order, $statuses );

			return $result;

		}


	}