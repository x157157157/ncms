<? class dump {
	
	 public static function x() {
		
		if ( $_REQUEST['submode'] ) {
			return call_user_func(array( 'dump', $_REQUEST['submode'] ) );
		}
		
		$out=smarty_tpl('adm_dump');
		
		echo( $out ); 
	} 
	
	public static function restore () {
		
		$dump= $_REQUEST['dump'];
		
		$data=  ( file_get_contents(  'cont/dump/'. basename( $dump ) ) ) ;
		
		if  ( fileending( $dump )=='gz' )  $data = gzdecode ( $data ); 
		
		foreach (  explode( ";/* query */" ,  $data  )  as $qu ){
			//v($qu); exit();
			
			if (  strpos(  $qu, pref_db ) )mysqli_query( $GLOBALS['db_link'] , $qu ) ;
			
			//$err.=v( mysql_error() );
		} 
		if ($err) {
			return 'false[data]'.$err;
		} else {
			return 'ok';
		}
		
	
	}
	public static function files () {
		
		if ($handle = opendir('cont/dump') )
			{					
			while (false !== ($file = readdir($handle))) 			
				{				
					if (fileending($file)) $files[]=array('name'=>$file , 'size' => round( filesize('cont/dump/'.$file )/1000000, 1 ).'Мб' ) ;
				}				
			}			
			closedir($handle);
			
			rsort( $files );
			
			return smarty_tpl( 'adm_dump_files' , array('files'=>$files) );
	}
	 
	
	public static function delete () {
		unlink( vl( 'cont/dump/'.basename($_POST['dump']) ) ) ;
		return 'ok[data]'.dump::files() ;  
	}
	public static function create ($_is_last=FALSE) {
	
		//$bd_host='localhost';
		//$bd_user='root';
		//$bd_pass='';
		//$bd_name='mydb';
		$dump_dir = "cont/dump"; // директория, куда будем сохранять резервную копию БД
		$dump_name =  $_is_last ? 'last.sql' : date('Y.m.d_H-i-s').'.sql'; //имя файла
		$insert_records = 50; //записей в одном INSERT
		//$gzip = true; 		//упаковать файл дампа
		//$stream = true;		//вывод файла в поток

		//$link = mysql_connect($bd_host, $bd_user, $bd_pass) or die( "Сервер базы данных не доступен" );
		//$db = mysql_select_db($bd_name) or die( "База данных не доступна" );
		
		$res = my_mysql_query("SHOW TABLES") or die( "Ошибка при выполнении запроса: ".mysqli_error($GLOBALS['db_link']) );
		
		if ( !file_exists( $dump_dir  ) ) {
			mkdir( $dump_dir, 0777 );
			chmod($dump_dir,0777); 
		}
		


		
		while( $table = mysqli_fetch_row($res) )
		{
		$query="";
		    if ( strpos( ' '.$table[0], pref_db ) )
		    {
		        $fname=$dump_dir."/".$table[0].'.'.$dump_name;

		        $fp = fopen( vl($fname) , "w" );
		        if ( !$fp ) return ' Ошибка записи '.$fname;

				$res1 = my_mysql_query("SHOW CREATE TABLE ".$table[0]);
				$row1=mysqli_fetch_row($res1);
				$query="\nDROP TABLE IF EXISTS `".$table[0]."`;/* query */\n".$row1[1].";/* query */\n";
		        fwrite($fp, $query); $query="";
		        $r_ins = my_mysql_query('SELECT * FROM `'.$table[0].'`') or die("Ошибка при выполнении запроса: ".mysqli_error($GLOBALS['db_link']));
				if(mysqli_num_rows($r_ins)>0){
                    $query_ins = "\nINSERT INTO `".$table[0]."` VALUES ";
                    fwrite($fp, $query_ins);
                    $i=1;
                    while( $row = mysqli_fetch_row($r_ins) )
                    { $query="";
                        foreach ( $row as $field )
                        {
                            if ( is_null($field) )$field = "NULL";
                            else $field = "'".mysqli_escape_string( $GLOBALS['db_link'], $field )."'";
                            if ( $query == "" ) $query = $field;
                            else $query = $query.', '.$field;
                        }
                        if($i>$insert_records){
                                        $query_ins = ";/* query */\nINSERT INTO `".$table[0]."` VALUES ";
                                        fwrite($fp, $query_ins);
                                        $i=1;
                                        }
                        if($i==1){$q="(".$query.")";}else $q=",(".$query.")";
                        fwrite($fp, $q); $i++;
                    }
                    fwrite($fp, ";/* query */\n");
                }

				fclose ($fp);

				if (  file_put_contents( $fname.'.gz' , gzencode(   file_get_contents($fname) , 1 )  ) )  {

                        unlink($fname);

                }
		    }
		}
		
		//return 'ok[data]Создан дамп '.$fname.' ('. round( filesize($fname)/1000).'Кб)';  	 
		
		

		
		
		return 'ok[data]'.dump::files() ;  	 
		
		if($gzip||$stream){ $data=file_get_contents($dump_dir."/".$dump_name);
		$ofdot="";
		if($gzip){
			$data = gzencode($data, 9);
			unlink($dump_dir."/".$dump_name);
			$ofdot=".gz";
		}

		if($stream){
				header('Content-Disposition: attachment; filename='.$dump_name.$ofdot);
				if($gzip) header('Content-type: application/x-gzip'); else header('Content-type: text/plain');
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
				header("Pragma: public");
				echo $data;
		}else{
				$fp = fopen($dump_dir."/".$dump_name.$ofdot, "w");
				fwrite($fp,  gzencode($data, 2 ) );
				//fwrite($fp,   $data  );
				fclose($fp);
				
			}
		}
		
	}

    public static function content2files($folder, $ident = '')
    {

        $pr = my_mysql_query('SELECT * FROM  ' . pref_db . 'content WHERE rod=' . xs($ident));

        while ($row = mysqli_fetch_assoc($pr)) {
            $row = array_filter($row);
            if ($row['d81']) {
                $row['d81'] = array_filter( unserialize($row['d81']) ) ;
            }
            $row['cont_htm']=fgc($row['ident']);
            self::file_put_contents_auto_create_folders($folder . '/' . substr(  $row['ident'], 0 , 100 ) . '.yaml', yaml_encode($row));
            self::content2files($folder . '/' . $row['ident'], $row['ident']);
        }


    }

    public static function file_put_contents_auto_create_folders($path_file_name, $data)
    {

        $fileName = basename($path_file_name);

        $folders = explode('/', str_replace('/' . $fileName, '', $path_file_name));

        $currentFolder = '';
        foreach ($folders as $folder) {
            $currentFolder .= $folder . DIRECTORY_SEPARATOR;
            if (!file_exists($currentFolder)) {
                mkdir($currentFolder, 0755);
            }
        }
        return file_put_contents($path_file_name, $data);
    }

    function delete_files($dir)
    {
        foreach (glob($dir . '/*') as $file) {
            if (is_dir($file)) {
                self::delete_files($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dir);
    }

}

?>