<?

Class cart {
	
	public static function pricedost () {
		return 100;
	}
	
	public static function itogform_arr_user_noadress () {
			
			$post= array_intersect_key( $_POST , array( 'spdost'=>1, 'sposopl'=>1 ) ) ; // заполняем способ оплаты и доставки
			
			$post['email']=($GLOBALS['userdata']['email']);
			$post['phone']=($GLOBALS['userdata']['phone']);
			
			$adress_dop=fn('form_fill_all', 'adv-form', $post );
			
			
			
			$adress_filled=fn('form_fill_all', 'adress-form', $_POST );
			
			
			
			$post_add=$_POST;
			$post_add['uid']=uid();
			$post_add['is_last']=1;
			
			fn('mydbAddLine','adresses',$post_add);
			
			unset($adress_filled['type']);
			
			return  order('itogform_arr',  array_merge( $adress_filled, $adress_dop )  ) ;
	}
	
	public static function itogform_arr_user () {
		//v(__LINE__);
		$adress= vl( mydbget('adresses', 'id', $_POST['adr']) ) ; // находим адресс]
		
		if ( ($adress['uid'])==(uid()) ) {
			
			$post= array_intersect_key( $_POST , array( 'spdost'=>1, 'sposopl'=>1 ) ) ; // заполняем способ оплаты и доставки
			
			$post['email']=($GLOBALS['userdata']['email']);
			$post['phone']=($GLOBALS['userdata']['phone']);
			
			$adress_dop=fn('form_fill_all', 'adv-form', $post );
			
			unset($adress['type']);
			$adress_filled=fn('form_fill_all', 'adress-form', $adress );
			$adress_filled['email']=$GLOBALS['userdata']['email'];
			$adress_filled['phone']=$GLOBALS['userdata']['phone'];
			
			return  order('itogform_arr',  array_merge( array_filter( $adress_filled ), $adress_dop )  ) ;
			
		}
	}
	
	public static function itogform_arr () {
		
		$itogform_arr=fn( 'form_fill_all' , order('form') , $_POST );
		
		return order('itogform_arr', $itogform_arr ) ;
		
	}
	
	public static function is_cabinet_cart() {
		return o('kabinet') AND  cart::is_cart_oformit()  ;
	}

	public static function is_cart_oformit() {
		return ( curver('cart_oformit')=='adv' or curver('cart_oformit')>=30 AND o('kabinet') ) ;
	}

	public static function robokassa_gen( $data_arr ) {

		extract( $data_arr );

		$mrh_login = o('robo_login');
		$mrh_pass1 = o('robo_pass_1');
		$inv_desc = 'Оплата счёта магазина '.basehref.'/';
		$out_summ =$GLOBALS['cache']['cart']['totalsum'];
		// тип товара
		// code of goods
		$shp_item = 1;

		$in_curr = "QiwiR";
		$culture = "ru";

		$encoding = "utf-8";

		$crc = md5(  "$mrh_login:$out_summ:{$GLOBALS['cache']['cart']['inv_id']}:$mrh_pass1:Shp_item=$shp_item" );

		 // build URL
		if (o('robo_test')) {
			$serverurl="http://test.robokassa.ru/Index.aspx";
		} else {
			$serverurl='https://merchant.roboxchange.com/Index.aspx';
		}

		 $url = $serverurl."?MrchLogin=$mrh_login&". "InvId=$inv_id&OutSum=$out_summ&SignatureValue=$crc&Shp_item=$shp_item&IncCurrLabel=&Culture=$culture&Desc=$inv_desc";

		$out.=smarty_tpl('fullcart3_go2robokassa', array( 'url'=>$url ) );

		return $out;

	}

	/**
	 * @param $adresses Массив адресов текущего пользователя
	 * @param $post_add POST массив введённого адреса покупателем
	 * @param $fields список полей по которым идёт сравнение
	 * @return bool TRUE если найдёт хотя бы один существующий адрес, который совпадёт с введённым покупателем адресом
	 */
	public static function is_adress_exist( $post_add , $adresses, $fields ) {
		foreach ($adresses as $adress) {
			$eq=1;
			foreach (  $fields as $field ) {
				if (vl($adress[
					vl($field)]
					)!=
					vl($post_add[$field])
				){
					$eq=false;
				}
			}

			if($eq) return $adress['id'];

		}
	}


	public static function cur_cart_datas()  {
		foreach ( fn('cart::get_cur_cart_arr') as  $mm) {

			if ($mm['type']=='mod')	{
				$mrod=qvar('ident',$mm['rod']);
				autozapolnenie($mrod);
				$mm['name']=$mrod['d4'].' - '.$mm['name'];
			}

			$mm['weight'] = vl( floatval( $mm['sub']['weight'] ?: o('weight_default') ) ) ;

			$tovarline=smarty_tpl('cart_tovarline_email', $mm ); 	vl();
			$tovardocsline= fn('cart::tovardocsline', $mm ) ;	vl();
			$mmTovInCart=qvar('id',$mm['id']);vl();

			if ( o('tov_num_check') )
				{
					if ( $mmTovInCart['sub']['tov_cur'] >= $mm['num'] )
						{
							$mmTovInCart['sub']['tov_cur']=  $mmTovInCart['sub']['tov_cur']-$mm['num'];
							$mmTovInCart['sub']['tov_lock'] =  $mmTovInCart['sub']['tov_lock'] + $mm['num'];
							mydbUpdate('content',array('d81'=> serialize( $mmTovInCart['sub'] ) ), 'id', $mm['id'] );
						}
						else
						{
							// nedostatochno tovara
						}
				}

			$GLOBALS['cache']['cart']['tovar'].=$tovarline;
			$GLOBALS['cache']['cart']['tovardocs'][]=$tovardocsline;
			$GLOBALS['cache']['cart']['p_tov_summ']+=$mm['price']*$mm['num']*fn('cat_percent',$mm)/100 ;



			$GLOBALS['cache']['cart']['weight_all']+= vl( floatval( $mm['weight']) *$mm['num'] ) ;
			//$GLOBALS['cache']['cart']['group_totalprice'][$CCR]+=$mm['price']*$mm['num'];

			if ( o('fullcart_brandgroup') ){

				$CCR=CatCurRoot($mm['rod']); // корневой каталог

				$GLOBALS['cache']['cart']['group_tovar'][$CCR].=$tovarline;
				$GLOBALS['cache']['cart']['group_tovardocs'][$CCR][]=$tovardocsline;
				$GLOBALS['cache']['cart']['group_totalprice'][$CCR]+=$mm['price']*$mm['num'];;

			}


	        }

	        return array(
	        	'group_tovar'=> $GLOBALS['cache']['cart']['group_tovar'] ,
	        	'group_tovardocs'=> $GLOBALS['cache']['cart']['group_tovardocs'] ,
	        	'group_totalprice'=> $GLOBALS['cache']['cart']['group_totalprice']
	        );
	}
	public static function itogform($p)  {
		$arr=fn('order','itogform_arr');

		$arr_new['fio']['name']='Получатель';
		$arr_new['fio']['val']= implode_notnul( ' ', array($arr['family']['val'], $arr['name']['val'], $arr['patronymic']['val']) ) ;

		$arr_new['email']['name']='E-mail';
		$arr_new['email']['val']= $arr['email']['val'];

		$arr_new['phone']['name']='Телефон';
		$arr_new['phone']['val']= $arr['phone']['val'];

		$arr_new['adress']['name']='Адрес доставки';
		$arr_new['adress']['val']= implode_notnul( ', ', [
			$arr['country']['val'], $arr['postindex']['val'], $arr['region']['val'], $arr['city']['val'], $arr['adress']['val']
		]);


		unset ( $arr['name'], $arr['patronymic'], $arr['family'] ,  $arr['region'], $arr['country'], $arr['city'], $arr['adress'], $arr['postindex'], $arr['phone'], $arr['email']  );

		vl(order('itogform_arr', array_merge( $arr_new, $arr ) ), 'itogform_arr');

		return order( 'itogform_htm' , smarty_tpl( 'cart_itogform' ) );
	}

	public static function fields_all() {

		return ['country', 'spdost', 'type', 'orgname','family', 'name', 'patronymic', 'postindex', 'region', 'city', 'adress', 'inn', 'kpp', 'ogrn', 'bankrekv', 'sposopl' , 'card_num', 'promocode' ];
	}

	/**
	 * Обработчик отправки заказа покупателем (нажатие на кнопку "оформить заказ")
	 */
	public static function order_process()  {

		if( function_exists('session_register')) session_register("secret_number");

		// алиасы для простоты
		$ord =& $GLOBALS['cache']['cart'];
		$itogform_arr =& $GLOBALS['cache']['cart']['itogform_arr'];
		$insert_arr =& $GLOBALS['cache']['cart']['insert_arr'];
		$itogform_arr_mess =& $GLOBALS['cache']['cart']['itogform_arr_mess'];

		// идентификатор корня старой формы, внутри которой находятся некоторые поля - способ доставки и оплаты например
		$ord['form']='adv-form';

		// получаем массим контент объекта корня формы
		$ord['mmform']=qvar('ident', $ord['form'] );

		// todo проверка капчи... орхаизмы
		if (intval($_SESSION["secret_number"])<1000)  {  $_SESSION["secret_number"]=false; }

		//  определяем отправлять ли заказ
		if ( fn('cart::is_go_process') or $GLOBALS['longpage'][1]=='result')  {

			$_SESSION["secret_number"]=false; // сброс секретного числа для капчи

			fn('cart::cur_cart_datas'); // формировани списка товаров заказа

			// вычисляем $itogform_arr  (массив данных о заказчике ))
			{
				if ( fn('cart::is_cabinet_cart')  ) { // если-кабинетная-корзина-включена
					 	vl('',__LINE__.' если-кабинетная-корзина-включена ');

					 	if (  $_POST['spdost']=='self' ) { // Если самовывоз
							vl(' Если самовывоз');

							// оставляем из $_POST только нужные ключи
							$post= array_intersect_key( $_POST ,
												[
													'spdost'=>1,
													'sposopl'=>1,
													'comment'=>1,
												]
							) ;

							// получаем данные поля если чел авторизирован - из массива пользователя, иначе из введённых данных
							foreach ( explode(' ', 'name family patronymic email phone card_num promocode '.fn('cart::cart_add_fields') ) as $fld ){

								$post[$fld]= uid() ? $GLOBALS['userdata'][$fld] : $_POST[$fld]  ;
							}


							// подмена значений на текста в select и radio полях
							$itogform_arr= array_merge(
													fn('form_fill_all', 'adv-form', $post ) , // корень старой формы
													fn('form_fill_all', 'adress-form', $post ), // адресная форма (без страны, способа доставки и
													fn('form_fill_all', 'email_tel', $post ) , // email и телефон
													fn('form_fill_all', 'form_comment', $post ) // коментарий
												);


						} else if ( fn('cabinet::adresses_arr') AND 0) { // todo пока отключенный участок
							vl('',__LINE__.'  если есть уже хоть один адрес');

							// массив реквизитов из выбранного готового адреса
							$adress= vl( mydbget('adresses', 'id', $_POST['adr']) ) ; // находим адресс

							if ( ($adress['uid'])==(uid()) ) {

								// берём только способ оплаты и способ доставки из постзапроса
								//$post= array_intersect_key( $_POST , array( 'spdost'=>1, 'sposopl'=>1, 'comment'=>1 ) ) ; // заполняем способ оплаты и доставки
								$post= $_POST ;
								// берём мыло и емаил из кабинета
								$post['email']=($GLOBALS['userdata']['email']);
								$post['phone']=($GLOBALS['userdata']['phone']);

								// заполняем данные формы
								$adress_dop=fn('form_fill_all', 'adv-form', $post );
								unset($adress['type']);


								$adress_filled=fn('form_fill_all', 'adress-form', $adress );
								//$adress_filled['email']['val']=$GLOBALS['userdata']['email'];
								//$adress_filled['phone']=$GLOBALS['userdata']['phone'];

								// объединяем то, что пришло из поста и заполненного адреса
								$itogform_arr = array_merge( $adress_dop , array_filter( $adress_filled ) )   ;

							}

						} else { // если не самовывоз



							// массив реквизитов из формы адреса кабинета
							//$post= array_intersect_key( $_POST , array( 'spdost'=>1, 'sposopl'=>1, 'comment'=>1 ) ) ; // заполняем способ оплаты и доставки
							$post= $_POST ;

							// todo надо протестировать
							$post['email']= uid() ?   ($GLOBALS['userdata']['email']) : $post['email'] ;
							$post['phone']= uid() ? ($GLOBALS['userdata']['phone']) : $post['phone']  ;

							// заполнение полей spdost sposopl и comment
							$adress_dop=fn('form_fill_all', 'adv-form', $post );

							// заполняется поле адреса
							$adress_filled=fn('form_fill_all', 'adress-form', $_POST );

							//  надо понять, есть ли хоть один адрес у пользователя
							$adresses=fn('cabinet::adresses_arr');

							$post_add=$_POST;
							$post_add['uid']=uid();
							$post_add['is_last']=1;

							$id_adress_exist=fn('cart::is_adress_exist', $post_add, $adresses, cart::fields_all() );

							if(
									!$adresses // если нет адресов у текущего пользователя
										OR
									!$id_adress_exist // если не существует id ни одного идентичного адреса
							){
								mydbUpdate('adresses', ['is_last'=>''], 'uid', uid() );
								fn('mydbAddLine','adresses',$post_add); // создаём новый адрес
							} else {
								mydbUpdate('adresses', ['is_last'=>''], 'uid', uid() );
								mydbUpdate('adresses', ['is_last'=>'1'], 'id', $id_adress_exist );
							}



							// форируем массив для создания нового адреса



							unset($adress_filled['type']);

							$itogform_arr =  array_merge( $adress_dop , $adress_filled )   ;
						}

						// заполняем индивидуальные(ручные) поля
						foreach ( explode(' ', o('cart_add_fields')) as $add_field ) {
							if ($add_field) {

								$add_field_mm=qvar($add_field);


								$itogform_arr[$add_field]=[
									'name'=>( $add_field_mm['name'] ?: $add_field ),
									'val'=>$post[$add_field],
								];

							}
						}

				// // если кабинетная корзина ВЫкюлчена
				} else {

					// массив реквизитов из формы



					if ( fn('cart::is_cart_oformit') ) {


						//$post= array_intersect_key( $_POST , array( 'spdost'=>1, 'sposopl'=>1, 'comment'=>1 ) ) ; // заполняем способ оплаты и доставки
						$post= $_POST ;

						$post['email']=($GLOBALS['userdata']['email']);
						$post['phone']=($GLOBALS['userdata']['phone']);

						$dos_opl=fn('form_fill_all', 'adv-form', $post );

						$adress_filled=fn('form_fill_all', 'adress-form', $_POST );

						$email_phone=fn('form_fill_all', 'email_phone', $_POST );

						$itogform_arr=array_merge( $dos_opl, $email_phone , $adress_filled  );

					} else {
						$itogform_arr=fn( 'form_fill_all' , $ord['form'] , $_POST );
					}



				}
			}
			vl($itogform_arr);
			// формирование списка реквизитов заказчика
			{
				$arr=$ord['itogform_arr'];

				$arr_new['fio']['name']='Получатель';
				$arr_new['fio']['val']= implode_notnul( ' ', array($itogform_arr['family']['val'], $itogform_arr['name']['val'], $itogform_arr['patronymic']['val']) ) ;

				$arr_new['email']['name']='E-mail';
				$arr_new['email']['val']= $arr['email']['val'];

				$arr_new['phone']['name']='Телефон';
				$arr_new['phone']['val']= $arr['phone']['val'];

				$arr_new['adress']['name']='Адрес доставки';
				$arr_new['adress']['val']= implode_notnul( ', ', array ($arr['country']['val'], $arr['postindex']['val'], $arr['region']['val'], $arr['city']['val'], $arr['adress']['val']));


				unset ( $arr['name'], $arr['patronymic'], $arr['family'] ,  $arr['country'], $arr['region'], $arr['city'], $arr['adress'], $arr['postindex'], $arr['phone'], $arr['email']  );

				$itogform_arr_mess = vl( array_merge( $arr_new, $arr ) ) ;

				order( 'itogform_htm' , smarty_tpl( 'cart_itogform' ) );
			}


			// end

			//  цена доставки
			$ord['pricedost'] =  o('pricedost_fn') ? fn( o('pricedost_fn'), $ord ) : $GLOBALS['ivals']['sposopl'][$_POST['sposopl']] ;

			fn('cart::cur_summ');

			$ord['totalsum'] =$ord['cur_summ']+$ord['pricedost'];
			fn('cart::inv_id_new');


			// insert_arr


			if ($_POST['adr']) list( $adr )=fn('mydbGetSub', 'adresses', array('id'=>$_POST['adr'], 'uid'=>uid() ) );

			$insert_arr=array(
				'id'=>vl($ord['inv_id']),
				'sposopl'=>  vl($itogform_arr['sposopl']['val']),
				'spdost'=>  vl($itogform_arr['spdost']['val']),
				'comment'=>  vl($itogform_arr['comment']['val']),
				'ordertime'=>vl(time() ),
				'ordercontent'=>vl( $GLOBALS['cache']['cart']['mesage_admin'] ? : $GLOBALS['cache']['cart']['mesage']  ),
				'cur_summ'=>vl( round( $ord['cur_summ'], 2 ) ),
				'totalsum'=>vl( round( $ord['totalsum'], 2 ) ),
				'p_tov_summ'=>vl($ord['p_tov_summ']),
				'weight_all'=>vl( round( $ord['weight_all'], 2 )),
				'pricedost'=>vl($ord['pricedost'] ),
				'orderdocs'=>vl(serialize( $ord['tovardocs'] ))
			);


			// Если самостоятельное получение
			if ($_POST['spdost']=='self') {
				foreach ( explode(' ', 'name family patronymic email phone card_num promocode'.fn('cart::cart_add_fields') ) as $fld ){
					if ( uid() ){
						$insert_arr[$fld]= vl($GLOBALS['userdata'][$fld]);
					} else {
						$insert_arr[$fld]= vl($itogform_arr[$fld]['val']);
					}

				}
			} else {

				foreach ( explode(' ', 'name family patronymic country region city adress postindex email phone orgname inn kpp ogrn bankrekv card_num promocode '.fn('cart::cart_add_fields') ) as $fld ){
					$insert_arr[$fld]=vl($itogform_arr[$fld]['val'] ,__LINE__) ;
				}

			}

			$insert_arr['not_inadm_comment']=1;

			order('tovar_header', smarty_tpl('cart_tovarline_header_email') );

			order('post', smarty_tpl( 'cart_email_post' , array('not_inadm_comment'=>1) ) ) ;

			order('itog_full', smarty_tpl( 'itog_full' , $insert_arr ) );

			order('mesage', smarty_tpl( 'cart_zakaz_message' , $insert_arr ) );



			if ( qvar('ident','cart_zakaz_message_admin','id') ) {
				order('mesage_admin', smarty_tpl(  'cart_zakaz_message_admin' , $insert_arr ) );
				order('cart_zakaz_message_admin_post', smarty_tpl(  'cart_zakaz_message_admin_post' , $insert_arr ) );

			} else {
				order('cart_zakaz_message_admin_post', $ord['post'] );
			}

			$insert_arr['ordercontent'] = vl( $ord['mesage_admin'] ? : $ord['mesage']  );
			$insert_arr['ordercontent_user'] =  vl( $ord['mesage']  )  ;


			if ( o('mod_partner') ){
				partner::event_new_order( $insert_arr );
			}
			
			// вставка строки заказа в базу
			$order_id_new = fn('mydbAddLine', 'orders',$insert_arr); //

			// insert_arr

			// send email
			// если электрозаказ, то отправляем редиректом на внешнюю фотрму опталы ( UCS или robokassa

			setcookie('order_id_new', $order_id_new, time()+3600*24*3, "/" );

			if ( strpos( ' '.$_POST['sposopl'],'electro') )  {
					if  ( o('paysystem')=='ucs' ) {
						fn('ucspay::pay');
					} else {
						$out.=fn('cart::robokassa_gen');
					}
			} else {
				if ( o('mod_ga') AND $insert_arr['name']!=='test' ) {
					$out.=cart::ga_gen( mydbget('orders', 'id', $order_id_new) );
				}
				$out.=smarty_tpl( 'fullcart3_thanks'  );
			}

			// если установлен модуль кабинета
			if ( o( 'kabinet')  ) {
				fn('cabinet::cart_user_auto_create', $insert_arr ); // автоматически создаём нового пользователя
			}

			// отправка уведомления админам и покупателю
			fn('cart::order_email_sending');

            // hook
            smarty_tpl('hook_order');

			// отправка в 1с
			//fn('_1c::event_order_new', $order_id_new);

			// если включена функция дробления заказа по поставщику
			if (o('fullcart_brandgroup') ) { fn('cart::brandgroup_email'); }

		}

		if(!$debug AND $_POST['fname']!=='mytest'  AND  !$_POST['debug'] ) {

			fn('cart::clear');

		}
		return $out;
	}

	public static function clear()  {
		//mydbDelSub('cart', array('viz_sid'=>session_id() ) );
		mydbUpdate('cart', ['is_order'=>'1'] , ['viz_sid'=>session_id() ] );

		if ( uid() ) {
			mydbUpdate('cart', ['is_order'=>'1'] , ['uid'=>uid() ] );
		}
		unset( $_POST);    
	    	$_SESSION["secret_number"]='';

	    unset( $GLOBALS['cahe']['get_cur_cart_arr'] ) ;
	}
	
	public static function order_email_sending()  {
		$cart=vl($GLOBALS['cache']['cart'] , __LINE__ );

		// отправка покупателю
		email_to_admins( $cart['post'],$cart['mesage'], $cart['itogform_arr']['email']['val'] );

		// отправка админам
		email_to_admins( $cart['cart_zakaz_message_admin_post'], ( $cart['mesage_admin'] ?: $cart['mesage'] ) );
		
		order('email_sended', 1);
		return;
	}

	public static function cart_add_fields(){
		return o('cart_add_fields')? ' '.o('cart_add_fields') : '';
	}

	public static function brandgroup_email()  {
		foreach ( array_keys( $GLOBALS['cache']['cart']['group_tovar'] ) as $k ) {
			email_to_admins(
				basehref." Re:".$GLOBALS['cache']['cart']['post'],
				smarty_tpl( 
					'cart_zakaz_message', 
					array( 
						'inv_id'=>$GLOBALS['cache']['cart']['inv_id'].' '.$k , 
						'itogform'=>$GLOBALS['cache']['cart']['itogform'], 
						'tovar'=>$GLOBALS['cache']['cart']['group_tovar'][$k], 
						'zakaz_summ'=>$GLOBALS['cache']['cart']['group_totalprice'][$k], 
						'pricedost'=>$GLOBALS['cache']['cart']['pricedost'], 
						'totalsum'=>0 
					) 
				)
			);
		}
	}
	
	
	public static function adcart() {
	
			ob_clean();
			
			if ( vl($_REQUEST['tovars']) ) {
				foreach( explode( ';' , $_REQUEST['tovars'] ) as $tov ) {
					
					list($art,$num) = explode(':', $tov);
					//v($art);
					if ( $art AND $num ) {
						$_REQUEST['art']=$art;
						$_REQUEST['num_tov']=$num;
						
						tov();
						//v('ok');
					}
					
					
				}
				return;
			}
			else {
				fn('tov');		
			}
			
			
			exit;
		
	}

	public static function deltovcart() {
		    
				ob_clean();
				$quc='DELETE FROM `'.pref_db.'cart` WHERE `id`='.xs($_REQUEST['deltovcart']).'	  ';
				//v($quc);
				$pr=my_mysql_query($quc);	
				//v($GLOBALS['longpage']);
				//header('Location: '.basehref.'/'.$_POST['oldref']);
				if ($pr) echo ajaxout( '[fc]'.fullcart3table().'[fc]'.create_cart('auto').'[fc]' );				
				exit;
			
	}
			
	public static function cartclear() {

		
				$quc='DELETE FROM `'.pref_db.'cart`  WHERE `viz_sid`='.xs(session_id());
				$pr=my_mysql_query($quc);
				echo '[fc]deleted[fc]'.create_cart('auto').'[fc]';
				exit;
			
	}

	public static function renum() {
			
				
				ob_clean();
				//($_GET);
				$num_tov=$_REQUEST['num_tov'];
				$num_key=(array_keys($num_tov));
				foreach ($num_key as $art)
				{
					if ( ($num_tov[$art])>0 )
						{
							$quc='UPDATE `'.pref_db.'cart` SET `num`='.xs($num_tov[$art]).' 
							WHERE `viz_sid`='.xs(session_id()).'	AND `id`='.xs($art);
							v($quc);
							$pr=my_mysql_query(($quc));				
						}
						else
						{
							$quc='DELETE FROM `'.pref_db.'cart` WHERE `id`='.xs($art).'	  ';
							//v($quc);
							$pr=my_mysql_query($quc);	
						}				
					
				}
				echo ajaxout( '[fc]'.fn('fullcart3table').'[fc]'.create_cart('auto').'[fc]');				
				exit;
			
		//v($_REQUEST['SignatureValue']);
	}
	
	public static function result()  {
		ob_clean();
		
		if  ( o('paysystem')=='ucs' )
			{
				vl();
				$orderdata = ucspay::result();
				loger(0,'usc_result','','','',$orderdata,0);
				$my_crc=1;
				$crc=2;
				if ($orderdata['status']=='acknowledged')
					{
						$inv_id = $orderdata['order']['number'];
						$ucs_ok=1;
						
					}
				
				
			}
		else // tсли робокасса
			{
				// регистрационная информация (пароль #2)
				// registration info (password #2)
				$mrh_pass2 = o("robo_pass_2");
				
				//установка текущего времени
				//current date
				$tm=getdate(time()+9*3600);
				$date="$tm[year]-$tm[mon]-$tm[mday] $tm[hours]:$tm[minutes]:$tm[seconds]";
				
				// чтение параметров
				// read parameters
				$out_summ = $_REQUEST["OutSum"];
				$inv_id = $_REQUEST["InvId"];
				$shp_item = $_REQUEST["Shp_item"];
				$crc = $_REQUEST["SignatureValue"];
				
				$crc = strtoupper($crc);
				
				$my_crc = strtoupper(md5("$out_summ:$inv_id:$mrh_pass2:Shp_item=$shp_item"));
			}
		

		if ( ($ucs_ok OR $my_crc==$crc)  )
		{
			mydbAddLine( 'statusopl', array('id_order'=>$inv_id)  );
			
			$data_arr=array('statusopl'=>'ok');
			mydbUpdate( 'orders',$data_arr ,'id',$inv_id );						
			
			if ( !o('paysystem') ) echo "OK$inv_id\n";	
			
			$ord=mydbget('orders','id',$inv_id );
			
			email_to_admins('Новый оплаченный заказ №'.$inv_id,$ord['ordercontent'] );
			
			order('inv_id', $inv_id );    
			       
			email_to_admins(   smarty_tpl('cart_email_post' ) ,  $ord['ordercontent_user'] , $ord['email'] );
			fn('cart::result_hook', ['inv_id'=>$inv_id] );
			exit();
		}
		else 
		{	
			echo "bad sign\n";
		  	exit();		
		}
		
	}

	public static function success() {
		
		cart::clear();

		// отправка данных в ga
		if ( o('mod_ga') ) {

			$order=mydbget('orders', 'id', $_COOKIE['order_id_new'] );
			
			if (  o('mod_ga') AND $order['name']!=='test' ) {
				$out.=cart::ga_gen( $order );
			}
		}

		if  ( o('paysystem')=='ucs' )
			{
				return $out.ucspay::success();
			}
		else	
			{
				// регистрационная информация (пароль #1)
				// registration info (password #1)
				$mrh_pass1 = o("robo_pass_1");
				
				// чтение параметров
				// read parameters
				
				$out_summ = $_REQUEST["OutSum"];
				$inv_id = $_REQUEST["InvId"];
				$shp_item = $_REQUEST["Shp_item"];
				$crc = $_REQUEST["SignatureValue"];
				
				$crc = strtoupper($crc);
				
				$my_crc = strtoupper(md5("$out_summ:$inv_id:$mrh_pass1:Shp_item=$shp_item"));
				
				// проверка корректности подписи
				// check signature
				if ($my_crc != $crc)
				{
				  return "bad sign\n";
				  //exit();
				}
				else
				{
					return $out.fgc('success-opl');
				}
				
				

			}
	}

	public static function cur_summ(){

		foreach (  fn('cart::get_cur_cart_arr') as $mm) {
			$summ+=floatval( $mm['price'] )*$mm['num'];
		}

		//$quc='SELECT sum(`price`*`num`) FROM `'.pref_db.'cart` WHERE `viz_sid`='.xs(session_id()).'		';
		//   	$pr=my_mysql_query($quc);
		//   	$mm1=mysqli_fetch_array($pr);

		//return order( 'cur_summ', $mm1[0] );
		return order( 'cur_summ', $summ );
	}

	public static function get_cur_cart_arr($_sid=false){

	        if ( isset( $GLOBALS['cahe']['get_cur_cart_arr'] ) ) {
	            return $GLOBALS['cahe']['get_cur_cart_arr'];
            }

			$quc='SELECT 	
	    	 			c.*,
	    	 					a.id a_id,
	    	 				a.zakaz, 
						a.num, 
						a.num_opt, 
						a.price, 
						a.art,
	    	 			a.is_order,
	    	 					
						c.desc,
						c.name,
						c.rod,
						c.type,    
						c.id,
						c.id cid,
						c.ident,
						c.d16 ,
						c.d25 ,
						c.d27 ,
						c.d28 ,
						c.d81, 
						c.art tovart,
						c.art idart, 
						c.d23 codtov,
						c.price cur_price
						
			FROM `'.pref_db.'cart` a,`'.pref_db.'content` c 
			WHERE a.viz_sid='.xs( $_sid ? : session_id() ).' AND a.is_order<>1 AND a.art=c.ident	
			ORDER BY a.datetime ASC ';  
			
			
			$pr=my_mysql_query(($quc));
			while($mm=mysqli_fetch_assoc($pr)){
				
				setsub($mm);

				// проверяем включенность и наличие товара
				if ($mm['ok'] AND !$mm['sub']['nonal'] ) {

						$mm['price']=  price_actual(  [
						'price'=>$mm['d16'] ? $mm['price'] : $mm['cur_price'] ,
						'd27'=>$mm['d27']*( $mm['num_opt'] ? :1 ) ,
						'd28'=>$mm['d28'],
					]  ); // вычисление текущей цены, если нет опции

					$out[]=$mm;
				}

				
			}
			$GLOBALS['cahe']['get_cur_cart_arr']=$out;
			return $out;
			
	}

	public static function inv_id_new() {
		$pr= my_mysql_query(('SELECT max(id) ma FROM `'.pref_db.'orders`'));
		$mmid=mysqli_fetch_assoc($pr);
		return order('inv_id',  (($mmid['ma'])+1)>1010 ? $mmid['ma']+1 : 1010 );
	}
	
	public static function tovardocsline($mm) {
		$hr_mini=urlFromMnemo($mm['art']);
		$hr=basehref.'/'.hrpref.$hr_mini;
		return  array (
			'url'=>$hr,
			'tovname' => $mm['name'],
			'opt'=> implode(';',array( str_replace( '[val]', val, $mm['zakaz'] ), $mm['sub']['cart_desc'] ) ) ,
			'art'=> ($mm['tovart']  ? $mm['tovart'] : $mm['id']),
			'tid'=> ( $mm['id']) ,
			'ident'=> ( $mm['ident']) ,
			'codtov'=> $mm['codtov'] ,
			'price'=> $mm['price'].' '.val,
			'num'=>$mm['num']
			
		);
	}
	
	public static function is_go_process() {
		
		return  ( 
	 			(
	 				isset( $_POST['fname']) or $_POST['adr'] or $_POST['name'] or $_POST['web_form_submit'] // проверка введены ли
	 			)
	 			AND
	 			(
	 			 	(
						$_POST["secretcode"]==$_SESSION["secret_number"] AND intval($_POST["secretcode"]) > 0 	// проверка капчи
					)  
					OR
					$debug 
					OR
					antikapcha() // провека антикапци яваскриптовая
				)
				AND 
				self::get_cur_cart_arr() // массив товаров в корзине
						
		);
	}
	
	public static function fail(){	
		$inv_id = $_REQUEST["InvId"];
		return "<h2 style='text-align:center;'> Вы отказались от оплаты... а жаль :-(</h2><br/><br/>";
		//echo "You have refused payment. Order# $inv_id\n";
	}

	/** Function to return the JavaScript representation of a TransactionData object.
	*
	 *
	 */

	public static function getTransactionJs(&$trans) {
		
		return "
		ga('ecommerce:addTransaction', {
		  'id': '{$trans['id']}',
		  'affiliation': '".$_SERVER['SERVER_NAME']."',
		  'revenue': '{$trans['cur_summ']}',
		  ".( $trans['pricedost'] ? "'shipping': '{$trans['pricedost']}'," : '' )."
		 	'currency': 'RUB',
		});
		";
	}

		/**
		 * Function to return the JavaScript representation of an ItemData object.
		 */
	public static function getItemJs(&$transId, &$item) {

				return "
		ga('ecommerce:addItem', {
		  'id': '$transId',
		  'name': '{$item['tovname']}',
		  'sku': '{$item['art']}',
		  'category': '". cart::tov_rod_name( $item['tid']  )."',
		  'price': '".floatval($item['price'])."',
		  'quantity': '{$item['num']}'
		});
		";
	}

	public static function tov_rod_name($id_tov) {

		return mydbget('content', 'ident',
				mydbget('content', 'id',  $id_tov, 'rod' )
		)['name'];
	}

	public static function ga_gen($order_arr) {

		$out= "
		<script>
			window.onload= function() {
			  
				(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
				ga('require', 'ecommerce'); ";

				$out.= cart::getTransactionJs($order_arr);

				foreach ( unserialize($order_arr['orderdocs'])  as $item) {
					$out.= cart::getItemJs($order_arr['id'], $item);
				}

				$out.= " ga('ecommerce:send'); 
 
 			}
 		</script>";

		return $out;
	}
	
}
?>