<? class mycache {
	public static function find() {

		if ( fn('mycache::is_legalPage') ) {

			$key= fn('mycache::key_gen');
			return fn('mycache::get', $key);

		}


	}
	public static function is_legalPage() {
		$get= $_GET;
		unset( $get['p'] );
		if (
			o('caching')
				AND
			!$_POST
				AND
			!$_GET

		) {
			return TRUE;
		}
	}

	public static function key_gen( ){
		return ( is_mobile() ? 'mob_' : '_' ).(is_test() ? 'test_':'' ).url2mnemo( substr(  $_SERVER['REQUEST_URI'], 1, 2000) );
	}
	public function get( $key ){

		$cachefile='cont/cache/'.$key;
		$old=vl( time() - filemtime($cachefile) ) ;
		if ( file_exists( $cachefile ) AND ( $old < o('cache_expirie')*60 )  ) // выводим кеш
		{

			return  file_get_contents( vl( $cachefile ) ).( is_test()? '<!--c '.($old).'-->': '' );

		}
	}

	public static function  set (  $data ) {
		$key=fn('mycache::key_gen');
		$cachefile='cont/cache/'.$key;
		file_put_contents( $cachefile, $data );
	}


}