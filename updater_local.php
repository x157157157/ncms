<?php class updater_local {
	 
	public static function connect_repo() {
		return  ftp::connect( $GLOBALS['ftp_host'] , $GLOBALS['ftp_login'], $GLOBALS['ftp_pass'], $GLOBALS['ftp_dir'] );
	}
	
	 public static function update_files_mysql() {
	 	self::update_files();
	 	self::update_mysql();
	 }
	 
	 public static function update_mysql() {
	 	
	 	//dump::create(1);
        dump::delete_files( jscripts.'/install/dump' );
	 	dump::content2files(vl(jscripts.'/install/dump'));
	 	//dump::content2files('cont/dump_yaml');

	 	return 'Таблица content сохранена в yaml!';

        return;
	 	 
	 	$ftp=self::connect_repo();
	 	
	 	//v( ftp_nlist( $ftp, $GLOBALS['ftp_dir'].'/cont/dump' ) ) ;
	 	
	 	$file='cont/dump/last.sql.gz';
	 	  
	 	if (  ftp_put( $ftp,  $GLOBALS['ftp_dir'].'/'.$file , $file, FTP_BINARY ) ) {
	 		
	 		file_get_contents( 'http://newcms2.giperplan.ru/myadmin.php?&act=dump::restore&dump=last.sql.gz'.$GLOBALS['key_repo'] );
	 		
	 		echo '<div class="green">mysql загружен на удалённый репозиторий!</div>'  ;
	 	
	 	}
	 	
	 	echo ( smarty_tpl('adm_updater_local') );
	 	
	 } 
	 
	  
	 
	 public static function last_files_set( $offset=false ) {
	 	
	 	file_put_contents('cont/logs/last_update_files.log', time() + $offset*3600 );
	 	
	 }
	   
	 public static function last_files_get_show() {
	 	return;
	 	foreach (  self::last_files_get() as $file ) {
			$out.='<div class="oran">'.$file.'</div>';
		}
		
		if ( $out ) {
			return '<div class="green">Имеются файлы для отправки:</div>'.$out;
		}
		
	 }
	 
	 public static function last_files_get() {
	 	$files=[
	 			'allfun.php',
	 			'localfun.php',
	 			'mypage.php',
	 			'myadmin.php',
	 	];
	 	
	 	$files = array_merge( $files ,  get_files_folder( 'jscripts', 16, 'files' ) );
	 	
	 	$last_update_files=file_get_contents('cont/logs/last_update_files.log');	
	 	
	 	foreach ( $files as $file) {
			if (  ( filemtime( $file ) ) > $last_update_files  ) $new_files[]=$file;
			if ( $file=='bjscripts/cart.php' and 0) {
				echo $file.': '.filemtime( $file ).', last: '.$last_update_files.'<br/>'; 
			} 
		}	
		
		foreach (  mydb_query_all( 'SELECT * FROM '.pref_db.'updater_files WHERE 1 ') as $mm  ) {
			$not_uploaded[]=$mm['file'];
		} 
		
	 	$out= $not_uploaded ?  array_merge(  $new_files , $not_uploaded ) : $new_files ;
	 	//$out= $new_files  ;
	 	 
	 	 return $out;
	 }
	 
	 public static function update_folders() {
	 	//get_files_folder( 'jscripts', 16 ,1 );
	 }
	 
	 public static function put_repo($ftp, $file ) {
			return ftp_put( $ftp, $GLOBALS['ftp_dir'].'/'.$file , $file, FTP_BINARY );
	 }
	 public static function update_files() {
	 	
	 	$ftp=self::connect_repo();
	 	 
	 	foreach ( self::last_files_get() AS $file ){
	 		
	 		if ( self::put_repo($ftp, $file) ) {
	 			echo '<div class="green">Загружен '.$file.'</div>';
				mydbDelSub('updater_files',  ['file'=>$file ] )  ;
				$cont_uped++;
			} else {
				
				echo '<div class="red">Ошибка загрузки '.$file.'</div>';
				if ( !mydbget('updater_files',  ['file'=>$file ] ) ) {
					mydbAddLine('updater_files',  ['file'=>$file ] )  ;
				} 
				$cont_unuped++;
			}
		}
		
		if ( !$cont_unuped ) self::last_files_set();
		
		$out.='<div class="green">Загружено '.$cont_uped.' файлов</div>' ; 
		
		if ($cont_unuped) $out.='<div class="green">НЕ загружено '.$cont_unuped.' файлов</div>' ; 
	
		$out.=smarty_tpl('adm_updater_local');
		
		
		echo $out;
		
	 }
	 
	 public static function x() {
		
		if ( $_REQUEST['submode'] ) {
			return call_user_func(array( 'updater_local', $_REQUEST['submode'] ) );
		}
		
		$out=smarty_tpl('adm_updater_local');
		
		echo( $out );
	}

	// запускается из админки
    public static function import_admin_fromall()
    {


        $files = (  self::read__list_yaml_dumps(jscripts.'/install/dump/admin'));

        foreach ( $files as $f ) {

            self::import_once($f, 1 );

        }

        $files = (  self::read__list_yaml_dumps(jscripts.'/install/dump/fromall'));

        foreach ( $files as $f ) {

            self::import_once($f  );

        }

	}

    public static function import_content()
    {
        $files = v(  self::read__list_yaml_dumps(jscripts.'/install/dump'));

        foreach ( $files as $k=>$f ) {

            self::import_once($f  );

        }
	}

	// импорт в базу одного файла
    public static function import_once($file, $_is_replace = false  )
    {

        $mm = yaml_decode(file_get_contents($file));

        $mm['d81']=serialize($mm['d81']);
        vl($mm);

        $mm_old= qvar('ident', $mm['ident'] );

        if ($_is_replace OR  ( !$_is_replace AND !$mm_old['id'] ) ) {

            mydbDelSub('content', ['ident' => $mm['ident'] ] );
            if ( qvar('id', $mm['id'] )['id'] ) {
                unset ($mm['id']);
            }

            if (  mydbAddLine('content', $mm) AND !$_is_replace ) {
                echo 'Вставлен '.$mm['ident'].'<br>';
            }

        }

	}

    public static function read__list_yaml_dumps($folder)
    {
        $out=[];
        foreach ( scandir($folder) as $f ) {

            if ( fileending($f)==='yaml' ) {
                $out[] = $folder.'/'.$f;
            } elseif ( $f!=='.' AND $f!=='..' ) {

                $out = array_merge_null( $out,  self::read__list_yaml_dumps($folder.'/'.$f) );
                  //(v($folder.'/'.$f) ) ;
            }
        }

        return $out;

	}

    public static function check_content()
    {
        if ( ! mydb_query_all('SELECT id FROM '.pref_db.'content LIMIT 1') ) {
            echo 'Таблица content пустаа. Создать из репозитория? <br> ';
            echo '<a href="?mode=updater_local&submode=newcontent">СОЗДАТЬ</a> ';
        }

    }

    public static function newcontent()
    {

        newbase();

        truecss();

    }
	
}

?>