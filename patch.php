<?
Class patch
{
	// todo надо срачно поменять кодировку в таблице adresses
	public static function all ()
	{

		patch::options_up();

		if ( !p('num_opt') ) {
			my_mysql_query( ' ALTER TABLE  `'.pref_db.'cart` ADD `num_opt` int(11) NOT NULL  AFTER  `num` ' ) ;
			p('num_opt',1);
			echo 'num_opt ok!<br/>';
		}

		if ( !p('images_to_d26') )
		{

			patch::converturlimg_all();
			o('tov_img_loader','on');
			p('images_to_d26',1);


		}

		if ( !p('correct_cart_forms') )
		{

			patch::correct_cart_forms();

			p('correct_cart_forms',1);
			echo 'correct_cart_forms ok!<br/>';

		}

		if ( !p('order_time') )
		{

			patch::order_time();

			p('order_time',1);

			echo 'order_time ok!<br/>';

		}

		if ( !p('mysql2utf8') )
		{
			patch::mysql2utf8();
			patch::cont2utf_all();
			p('mysql2utf8',1);
		}

		if(!p('cont_htm'))
		{
			patch::cont2cont_htm();
			p('cont_htm',1);
		}

		if ( !p( 'fotoalbum_old2new') )
		{
			patch::fotoalbum_old2new ();
			patch::fotoalbum_img_old2new ();
			p( 'fotoalbum_old2new',1);
		}

		if ( !p( 'sub_sale_end_move') )
		{

			patch::fields_move ( 'content', 'tov', 'sub:sale_end', 'd28' );
			echo 'sub_sale_end_move!<br/>';
			p( 'sub_sale_end_move',1);

		}

		if ( !p( 'is_adresses_last add') ) {

			$data=[
				'ident'=>'is_adresses_last',
				'rod'=>'blockcart',
				'name'=>'Отображать ранее введённые адреса v1',
				'type'=>'chopt',
			];

			mydbAddLine('content',$data);

			echo 'is_adresses_last!<br/>';
			p( 'is_adresses_last add',1);

		}
		



		$event='mysql utf8 v2';
		if ( !p( $event ) )
		{
			// флаг удалённости заказа
			patch::mysql2utf8();

			echo $event.'!<br/>';
			p( $event,1);

		}

		$event='hleb_end_link_off add';
		if ( !p( $event ) )
		{
			// поле номера заказа, который прислался из 1с

			$data=[
				'ident'=>'hleb_end_link_off',
				'rod'=>'sys-opt',
				'name'=>'Последняя цепочка в хлебных крошках не должна быть ссылкой',
				'type'=>'chopt',
			];

			mydbAddLine('content',$data);

			echo $event.'!<br/>';
			p( $event,1);

		}

		$event='multi_lang add';
		if ( !p( $event ) )
		{
			// поле номера заказа, который прислался из 1с

			$data=[
				'ident'=>'multi_lang',
				'rod'=>'sys-opt',
				'name'=>'Перечень языков frontend',
				'type'=>'topt',
			];

			mydbAddLine('content',$data);

			echo $event.'!<br/>';
			p( $event,1);

		}

		$event='old migrat';
		if ( !p( $event ) )
		{
			// поле номера заказа, который прислался из 1с

			// Деревние миграции

			$qu='ALTER TABLE `'.pref_db.'cart` ADD `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST  ';
			if ( my_mysql_query($qu) ) echo 'Вставлен cart.id!<br/>'; //`idsub` int(11) NOT NULL auto_increment,

			$qu='ALTER TABLE `'.pref_db.'content` ADD INDEX `rod` ( `rod`(10) )';
			if ( my_mysql_query($qu) ) echo 'Вставлен индекс rod!<br/>';

			$qu='ALTER TABLE `'.pref_db.'content` ADD INDEX `d1` ( `d1`(20) )';
			if ( my_mysql_query($qu) ) echo 'Вставлен индекс d1!<br/>';

			$qu='ALTER TABLE `'.pref_db.'content` ADD INDEX `d2` ( `d2`(20) )';
			if ( my_mysql_query($qu) ) echo 'Вставлен индекс d2!<br/>';

			$qu='ALTER TABLE `'.pref_db.'content` ADD INDEX `d3` ( `d3`(20) )';
			if ( my_mysql_query($qu) ) echo 'Вставлен индекс d3!<br/>';

			$qu='ALTER TABLE `'.pref_db.'content` ADD INDEX `d4` ( `d4`(20) )';
			if ( my_mysql_query($qu) ) echo 'Вставлен индекс d4!<br/>';

			$qu='ALTER TABLE `'.pref_db.'subcat` ADD INDEX `subcat` ( `subcat`(10) )';
			if ( my_mysql_query($qu) ) echo 'Вставлен индекс subcat!<br/>';

			my_mysql_query(' ALTER TABLE  `'.pref_db.'cart` CHANGE  `price`  `price` VARCHAR( 11 ) NOT NULL ') ;   // cart.price


			echo $event.'!<br/>';
			p( $event,1);

		}


	}

	public static function max_num() {
		return mydb_query_all('SELECT MAX(num) ma FROM `'.pref_db.'content` ')[0]['ma'];
	}

	/** создавальщик контентобъектов опций из yaml файла*/
	public static function options_up(){
		$options_arr = Spyc::YAMLLoad(jscripts.'/admin_data/options.yaml');
		foreach ($options_arr as $rod=>$options) {
			foreach ($options as $ident=>$option) {

				// если есть такой контент объект
				if ( $mm = qvar($ident) ) {
					if ($mm['name']!==$option['name']) { // проверяем не поменялось ли имя

						mydbUpdate('content', ['name'=>$option['name']], 'id', $mm['id'] );

						echo 'option UPDATE: '.$option['name'].'('.$ident.')<br/>';

					}

				} else { // иначе вставляем новый
					$data=[
						'rod'=>$rod,
						'ident'=>$ident,
						'name'=>$option['name'],
						'type'=>$option['type'],
						'd1'=>$option['value'],
						'd5'=>$option['is_repo'],
						'd6'=>$option['repo_value'],
						'num'=>self::max_num()
					];

					mydbAddLine('content',$data);

					echo 'option ADD: '.$option['name'].'('.$ident.')<br/>';

				}

			}



		}
		return $array;
	}

	/**
	 * Создание поля в таблице базы
	 * @param $table - таблица
	 * @param $field => - создаваемое поле
	 * @param $type => - тип поля
	 * @param $after => - поле, поле которого надо создать
	 * @param bool $index_name
	 * @param bool $index_type
	 */
	public static function add_field ($table, $field, $type, $after,  $index_type=false ){
		// ADD INDEX (  `1c_order_id` )
		$index= $index_type ? ', ADD '.$index_type.' ( `'.$field.'` )' :  '' ;
		$qu='
			ALTER TABLE `'.pref_db.''.$table.'` 
			ADD `'.$field.'` '.$type.' NOT NULL 
			AFTER `'.$after.'`
			'.$index;

		my_mysql_query( $qu );
		return $qu;
	}

	public static function correct_all( $type , $fun , $id=false) {
		// rutr::correct_all( 'tov', 'move_d9' )
		$pr=my_mysql_query( 'SELECT * FROM '.pref_db.'content WHERE `type`=\''.$type.'\'  '.($id? ' AND id='.xs($id) : '' )    ) ;

		while ( $mm=mysqli_fetch_assoc($pr) )
		{
			setsub($mm);


			$mm= fn( $fun, $mm);

			$mm['d81']=serialize( $mm['sub'] );

			mydbUpdate( 'content',  $mm , 'id' , $mm['id']  );

			//v( $mm );

			echo $mm['id'].'<br/>';

		}
		return $out;
	}

	public static function parse_d30_copy_in_pack($mm) {

		//<strong>100 таб. <br><br>Товар сертифицирован</strong>
		// patch::correct_all( 'tov' , 'parse_d30_copy_in_pack' )

		$rep=[
			'<strong>'=>'',
			'</strong>'=>'',
		];

		$mm['sub']['in_pack']= trim(  explode('<br',  strtr(  $mm['d30'], $rep ) )[0] ) ;

		return $mm;
	}

	public static function pre_all () {

		$act='rename_user_leads';

		if ( !p($act) ) {

			mydbUpdate('content', ['d1'=>'user_events'], ['ident'=>'user_start'] );

			my_mysql_query( '   RENAME TABLE `'.pref_db.'user_leads` TO `'.pref_db.'user_events`;' ) ;

			p($act,1);

			echo $act.' ok!<br/>';

		}

	}

	public static function newbase_from_json(){
		// автосоздание

		// подгружаем файлы конфигурации базы, основной и локальный
		$base= baseConfigGet();

		foreach ( $base as $tablename=> $table )
		{

			create_mysql_table( $tablename );

			$cols=get_field_structure( $tablename ,FALSE);
			
			foreach ( $table as $fieldname => $field )
			{

				if ($fieldname=='tablename') continue;

				$field['fieldname']=$fieldname;
				// создаём поле
				mydb_field_create ($tablename, $fieldname_left, $fieldname, $field['type'], $field['lenght'], 0, $cols[$fieldname], 0 );

				// проверяем необходимость переименования поля
				if ($field['field_old']) mydb_field_move( $tablename, $field['field_old'] , $fieldname );

				//
				create_or_destroy_index2($tablename, $field);
				$fieldname_left=$fieldname;
			}

			$fieldname_left=FALSE;


		}
	}

	/**
	 * главная функция патча. выполняется при ?truecss=1
	 */

	public static function newbase($go=false)  //2
	{
		// nobase
		if ( !file_exists('cont/js/local.js') ) file_put_contents("cont/js/local.js",' ');

		// устранаем контент элементы с пустыми ident
		my_mysql_query('UPDATE `'.pref_db.'content` set ident=concat(\'id\',id) WHERE `ident`=\'\' ');




		$pr=my_mysql_query( 'SHOW COLUMNS FROM '.pref_db.'subcat' ) ;
		$mm=vl(mysqli_fetch_assoc($pr));

		// создание таблици subcat
		if	(!$mm or $go) {
			my_mysql_query(vl('CREATE TABLE IF NOT EXISTS `'.pref_db.'subcat` (
								  `idsub` int(11) NOT NULL auto_increment,
								  `attr` text NOT NULL,
								  `identsub` text NOT NULL,
								  `subcat` text NOT NULL,
								  PRIMARY KEY  (`idsub`),
								  KEY `attr` (`attr`(30),`identsub`(30),`subcat`(30))
								) ENGINE=InnoDB CHARSET=utf8  COLLATE utf8_general_ci '), 'create subcat');
			my_mysql_query('INSERT INTO `'.pref_db.'subcat` (`idsub`, `attr`, `identsub`, `subcat`) VALUES (NULL, \'+\', \'+\', \'+\');');

			echo "Таблица subcat создана!<br/>";

		}



		if (1) {

			patch::newbase_from_json();

		} else {
			/*// автосоздание

			$pr = my_mysql_query('SELECT * FROM ' . pref_db . 'content WHERE `type`=\'userdatabase\'  ');

			while ($mm = mysqli_fetch_assoc($pr)) {

				create_mysql_table($mm['d1']);

				$cols = get_field_structure($mm['d1'], FALSE);

				$qu = 'SELECT * FROM ' . pref_db . 'content WHERE `type`=\'basefield\' and `rod`=' . xs($mm['ident']) . '  
							ORDER BY `num` ASC  ';
				//v($qu);
				$pr2 = my_mysql_query($qu);
				while ($mm2 = mysqli_fetch_assoc($pr2)) {
					setsub($mm2);
					mydb_field_create($mm['d1'], $mm_old['d1'], $mm2['d1'], $mm2['d2'], $mm2['d4'], $mm2['d3'], $cols[$mm2['d1']], $mm['d7']);
					if ($mm2['sub']['d1_old']) mydb_field_move($mm['d1'], $mm2['sub']['d1_old'], $mm2['d1']);

					create_or_destroy_index($mm['d1'], $mm2);
					$mm_old = $mm2;
				}

				$mm_old = FALSE;


			}*/

		}

		// Автосоздание первичных таблиц

		$pr=my_mysql_query( 'SHOW COLUMNS FROM '.pref_db.'content' ) ;
		$mm=mysqli_fetch_assoc($pr);

		if ( !($mm) or ($go) )
		{

			// принудительная очистка таблиц
			/*if ($go)
			{
				my_mysql_query('DROP TABLE `'.pref_db.'cart`, `'.pref_db.'content`, `'.pref_db.'img`, `'.pref_db.'opros`, `'.pref_db.'params`, `'.pref_db.'subcat` ' );
			}*/


			$qu='
					
					CREATE TABLE IF NOT EXISTS `'.pref_db.'cart` (
					  `viz_sid` text NOT NULL,
					  `art` text NOT NULL,
					  `zakaz` text NOT NULL,
					  `price` int(11) NOT NULL,
					  `num` int(11) NOT NULL,
					  `num_opt` int(11) NOT NULL,
					  `datetime` datetime NOT NULL,
					  KEY `viz_sid` (`viz_sid`(10)),
					  KEY `art` (`art`(10)),
					  KEY `datetime` (`datetime`)
					) ENGINE=InnoDB CHARSET=utf8  COLLATE utf8_general_ci;
					
					CREATE TABLE IF NOT EXISTS `'.pref_db.'content` (
					  `id` mediumint(8) unsigned NOT NULL auto_increment,
					  `type` text NOT NULL,
					  `rod` text NOT NULL,
					  `ident` text NOT NULL,
					  `art` text NOT NULL,
					  `num` int(11) NOT NULL default \'0\',
					  `prio` text NOT NULL,
					  `name` text NOT NULL,
					  `ok` text NOT NULL,
					  `bbcode` text NOT NULL,
					  `nolink` text NOT NULL,
					  `nadcat` tinyint(1) NOT NULL,
					  `title` text NOT NULL,
					  `content` text NOT NULL,
					  `saveplace` text NOT NULL,
					  `desc` text NOT NULL,
					  `keyw` text NOT NULL,
					  `price` text NOT NULL,
					  `bok` text NOT NULL,';
			for ($x=1; $x<=100; $x++)
			{
				$qu.="`d{$x}` text NOT NULL,";
			}


			$qu.='PRIMARY KEY  (`id`),
					  KEY `ident` (`ident`(11)),
					  KEY `num` (`num`),
					  KEY `ok` (`ok`(5)),
					  KEY `price`(`price`(11)),
					 KEY `rod`(`rod`(11)),
					KEY `d1`(`d1`(11)),
					  KEY `type` (`type` (11))
					 
					) ENGINE=InnoDB CHARSET=utf8  COLLATE utf8_general_ci  ;
					
					
					
					
					CREATE TABLE IF NOT EXISTS `'.pref_db.'img` (
					  `id` int(11) NOT NULL auto_increment,
					  `album` text NOT NULL,
					  `file` text NOT NULL,
					  `name` text NOT NULL,
					  PRIMARY KEY  (`id`)
					) ENGINE=InnoDB CHARSET=utf8  COLLATE utf8_general_ci AUTO_INCREMENT=1 ;
					
					CREATE TABLE IF NOT EXISTS `'.pref_db.'opros` (
					  `id` int(11) NOT NULL auto_increment,
					  `viz_id` int(11) NOT NULL default \'0\',
					  `id_opros` int(11) NOT NULL default \'0\',
					  `id_ans` int(11) NOT NULL default \'0\',
					  `val` int(11) NOT NULL default \'0\',
					  PRIMARY KEY  (`id`)
					) ENGINE=InnoDB CHARSET=utf8  COLLATE utf8_general_ci AUTO_INCREMENT=1 ;
					
					CREATE TABLE IF NOT EXISTS `'.pref_db.'params` (
					  `name` text NOT NULL,
					  `value` text NOT NULL
					) ENGINE=InnoDB CHARSET=utf8  COLLATE utf8_general_ci ;
					
					
					
					INSERT INTO `'.pref_db.'params` (`name`, `value`) VALUES
					(\'count_viz\', \'0\');
					
					';
			$qu=explode(';',$qu);
			foreach ($qu as $q)
			{

				if (my_mysql_query($q)) echo 'Таблица создана...<br/>';
			}



			//echo '<meta http-equiv="Refresh" content="0; URL='.basehref."/myadmin.php?mode={$GLOBALS['mode']}&edit={$GLOBALS['edit']}&htitle={$GLOBALS['htitle']}\">";

		}

		if (1) {

			$fld[$mm['Field']]=1;

			while ($mm=mysqli_fetch_assoc($pr))
			{
				$fld[$mm['Field']]=1;
			}

			//v($fld);

			// автосоздание поля priceall
			if ( !isset( $fld['priceall']) )
			{
				/*my_mysql_query( ' ALTER TABLE  `'.pref_db.'content` ADD  `pricem` VARCHAR( 30 ) NOT NULL AFTER  `price`  ' ) ;
				my_mysql_query( 'ALTER TABLE `'.pref_db.'content` ADD INDEX `pricem` ( `pricem`(10) )' ); */
				// ALTER TABLE  `ncms__content` ADD  `pricem` FLOAT NOT NULL AFTER  `price` , ADD INDEX (  `pricem` )
				my_mysql_query( ' ALTER TABLE  `'.pref_db.'content` ADD  `priceall` FLOAT  NOT NULL AFTER  `price`  ' ) ;
				my_mysql_query( 'ALTER TABLE `'.pref_db.'content` ADD INDEX `priceall` ( `priceall` )' );

				echo '<br/>Вставлен  priceall!<br/>';
			}

			for ($x=1;$x<=100; $x++)
			{
				if ( !isset( $fld['d'.$x]) )
				{
					if ( my_mysql_query( ' ALTER TABLE  `'.pref_db.'content` ADD  `d'.$x.'` TEXT CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL AFTER  `d'.($x-1).'` ' ) )
					{
						echo 'Добавлено поле: content.d'.$x;
					}
				}

			}




		}



		// заполняем таблицу content из репозитория, если она пустая
		$pr=my_mysql_query( 'SELECT * FROM '.pref_db.'content  '    ) ;
		$mm=mysqli_fetch_assoc($pr);
		//exit();
		if  ( !$mm )
		{
			updater_local::import_content();
		}




		// вставка дополнительных полей:

		// 'ALTER TABLE `'.pref_db.'users` ADD `tel1` TEXT NOT NULL AFTER `uid` ';
		// 'ALTER TABLE `ncms__users` ADD INDEX ( `tel1` ( 11 ) )';

		// улаживание новостей

		$qu1='UPDATE `'.pref_db.'content` SET `type`=\'_news\' 
			WHERE `type`=\'news\' '  ;
		//v($qu1);
		my_mysql_query($qu1);

	}


	public static function correct_cart_forms () {
		//register-1
		mydbUpdate('content', array('d1'=>'phone') , array('rod'=>'register-1', 'd1'=>'tel1' ) );
		mydbUpdate('content', array('ident'=>'_autoreg') , array('rod'=>'register-1', 'd1'=>'autoreg' ) );
		mydbUpdate('content', array('d1'=>'adress') , array('rod'=>'adv-form', 'd1'=>'Adres580' ) );
		mydbUpdate('content', array('ident'=>'spdost_sel', 'd1'=>'spdost' ) , array('ident'=>'tform325-2' ) );
		mydbUpdate('content', array( 'd1'=>'email' ) , array('ident'=>'tform4460' ) );
		mydbUpdate('content', array('rod'=>'spdost_sel' ) , array('rod'=>'tform325-2' ) );

	}
	public static function order_time () {
		patch::fields_correct ( 'orders', 'order_time_field', 'ordertime' );
	}

	public static function order_time_field ($in) {
		$rep=array(
			','=>';',
			'.'=>';',
			':'=>';',
			'-'=>';',
		);



		$in= explode(';' , strtr($in,$rep) );

		if ( sizeof( $in )==6 ) {
			$in_cor=$in;
			if ( strlen( $in[2] )==4 ) {
				$in_cor[0]=$in[2];
				$in_cor[2]=$in[0];
			}

			return date2timestamp($in_cor);
		} else {
			return NULL;
		}
	}



	public static function fields_correct ( $table, $handler_fun, $_field_correct=false, $_id_only=false, $_search_arr_sec=false )
	{
		if ($_search_arr_sec) $where[]=mydb_srcharr2wh($_search_arr_sec);
		if ($_id_only ) $where[] = ' id='.xs($_id_only) ;

		if (!($where)) $where[]='1';

		$pr=my_mysql_query( ('SELECT id, '.$_field_correct.' FROM '.pref_db.$table.' WHERE '.implode(' AND ' , ($where)).'  
				 ' ) );

		while ( $mm=mysqli_fetch_assoc($pr) )
		{
			//v($mm);
			$data[$_field_correct]= patch::$handler_fun( $mm[$_field_correct] );
			if ( $data[$_field_correct]!==NULL ) {
				mydbUpdate( $table, $data, 'id', $mm['id'] );
			}

		}
	}

	public static function fields_move ( $table, $type, $field_from, $field_to, $id_only = false , $_handler_fun=FALSE )
	{

		$pr=my_mysql_query( 'SELECT id FROM '.pref_db.$table.' WHERE `type`= '.xs($type).'  
									'.($id_only? 'AND id='.xs($id_only) : '' ).'
				 ' );

		while ( $mm=mysqli_fetch_assoc($pr) )
		{
			$data=patch::field_move($table, $mm['id'] , $field_from, $field_to , $_handler_fun );
		}
	}
	public static function field_move ($table, $id, $field_from, $field_to )
	{
		$mm=mydbget($table, 'id', $id );
		setsub($mm);
        vl($mm);
		$ff= explode(':', $field_from);
		if ( $ff[0]=='sub' )
		{
			$in_field=$mm['sub'][ $ff[1] ];
			if ( is_array( $mm['sub'] ) )  {
			    unset( $mm['sub'][ $ff[1] ] );
            }
		}
		else
		{
			$in_field=$mm[ $ff[0] ];
			$mm[ $ff[0] ]= '' ;
		}



		$ft= explode(':', $field_to);
		if ( $ft[0]=='sub' )
		{
			$field_out=$mm['sub'][ $ft[1] ];
			$mm['sub'][ $ft[1] ] = $in_field;
		}
		else
		{
			$field_out=$mm[ $ft[0] ];
			$mm[ $ft[0] ] = $in_field ;
		}
		$mm['d81']=serialize($mm['sub']);

		if ( !$field_out AND $in_field ) mydbUpdate( $table, $mm , 'id', $mm['id'] );

	}
	public static function fotoalbum_old2new ()
	{
		$pr=my_mysql_query( 'SELECT * FROM `'.pref_db.'content` WHERE `ident` IN (SELECT album FROM `lar__img` GROUP BY `album` )' ) ;

		while ( $mm=mysqli_fetch_assoc($pr) )
		{

			v( 'old fotoalbum: '. $mm['ident'] );

			mydbAddLine('content', array('ident'=>$mm['ident'].'_alb', 'rod'=>$mm['ident'], 'type'=>'fotoalbum', 'num'=>( 100000+($x++) ) ) ) ;

			fpc ( $mm['ident'] ,'<p>[[['.$mm['ident'].'_alb'.']]]</p>'.fgc($mm['ident']) );

		}

	}

	public static function fotoalbum_img_old2new ()
	{
		$pr=my_mysql_query( ' SELECT * FROM `'.pref_db.'img`  ' ) ;

		while ( $mm=mysqli_fetch_assoc($pr) )
		{

			//v( 'old fotoalbum: '. $mm['ident'] );
			if ( qvar('ident', $mm['album'] ) )
			{
				$filename=filename( $mm['file'] );
				$ident=$mm['album'].'_alb_'.$filename;
				$newfile=$ident.'.jpg';

				mydbAddLine('content',
					array(
						'ident'=>$ident,
						'rod'=>$mm['album'].'_alb' ,
						'type'=>'imgalbum',
						'num'=>( 200000+($x++) )

					)
				) ;
				//http://www.largof.ru/cont/album/ekonom-klass3.jpg
				//http://www.largof.ru/cont/album/big/ekonom-klass3.jpg

				// http://newcms2.giperplan.ru/cont/img/cat/mid/index-1-chrysanthemum.jpg
				// http://newcms2.giperplan.ru/cont/img/cat/big/index-1-chrysanthemum.jpg

				copy('cont/album/'.$mm['file'], 'cont/img/cat/mid/'.$newfile );
				copy('cont/album/big/'.$mm['file'], 'cont/img/cat/big/'.$newfile );

			}


		}

	}

	public static function mysql2utf8 ()
	{

		$pr=my_mysql_query( 'SHOW FULL TABLES  LIKE \''.pref_db.'%\''  ) ;

		while ( $mm=mysqli_fetch_array($pr) )
		{

			my_mysql_query( 'ALTER TABLE '.$mm[0].' CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci' ) ;
			v($mm[0]);
		}

		echo 'mysql2utf8 pached!<br/>';
	}

	public static function cont2utf_all()
	{

		$handle = opendir ( 'cont' ) ;
		{
			while ( $fileindir = readdir($handle) )
			{
				if (  !is_dir('cont/'.$fileindir  )  )
				{
					patch::cont2utf($fileindir);
					//v($fileindir);
				}
			}
		}
	}

	public static function cont2utf($fileindir)
	{
		if ( !file_exists('cont/htm') and !mkdir('cont/htm', 0777) ) echo 'Ошибка! cont/htm - не создан!<br/>';
		if ( !file_exists('cont/htm/old') and !mkdir('cont/htm/old', 0777 ) ) echo 'Ошибка! cont/htm/old - не создан!<br/>';

		echo ($fileindir);
		echo ( file_put_contents( ( 'cont/htm/'.$fileindir ), iconv('windows-1251','utf-8', file_get_contents('cont/'.$fileindir ) )  ) ) ? ' conv' : ' errconv' ;
		echo ( rename( 'cont/'.$fileindir, 'cont/htm/old/'.$fileindir ) ? 'bkup' : 'err_bkp' )  ;
		echo '<br/>';

	}

	public static function cont2cont_htm()
	{

		if ( file_exists('cont/htm') ) return;

		mkdir('cont/htm');

		$handle = opendir ( 'cont' ) ;
		{
			while ( $fileindir = readdir($handle) )
			{

				if (  !is_dir('cont/'.$fileindir  )  )
				{
					rename( 'cont/'.$fileindir, 'cont/htm/'.$fileindir );
				}

			}
		}
		echo 'cont -> cont/htm ok! ' ;
	}

	public static function images_to_d26_all ()
	{
		$pr=my_mysql_query( 'SELECT * FROM '.pref_db.'content WHERE `type`=\'tov\'  '    ) ;

		while ( $mm=mysqli_fetch_assoc($pr) )
		{
			mydbUpdate( 'content', patch::images_to_d26($mm), 'id', $mm['id'] );
		}
		echo 'images_to_d26 pached!<br/>';
	}
	function images_to_d26( $mm )
	{
		for ($x=26; $x<=45; $x++)
		{
			if ( oror( fileending( $mm['d'.$x] ),  'jpg','png','gif' ) )
			{
				$images[]=$mm['d'.$x];
				$mm['d'.$x]='';
			}
			else
			{
				break;
			}
			$mm['d26']=serialize( $images );

		}
		return $mm;
	}

	function converturlimg_all()
	{
		$pr=my_mysql_query( ( 'SELECT * FROM '.pref_db.'content  WHERE `type`=\'tov\'   '     )) ;
		echo 'Обновление адресов картинок... <br/>';
		while ( $mm=mysqli_fetch_assoc($pr) )
		{
			patch::converturlimg($mm);

		}
		echo 'Завершено!';

	}
	function converturlimg($mm)
	{
		unset($img, $x,$up);

		for ($x=0; $x<15 ; $x++)
		{

			$filename= !$x ? $mm['ident'] :   $mm['ident'].'_'.num2str($x-1,2);

			if ( file_exists( img.'/cat/big/'.($filename.'.jpg') )   )
			{
				$img[]= ($filename.'.jpg');
				$next=1;
			}
			else
			{
				$next=0;
			}

		}



		//v( $img );

		foreach ($img as $k=>$v)
		{
			$up['d'.($k+25)]=$v;
		}

		if ($img)
		{

			mydbUpdate('content',  patch::images_to_d26($up),'id',$mm['id'] );
			echo $mm['id'].' ';
		}
	}

	/**
	 * конвертация струкруры базы из контент объектов ветки admin в мнемонический файл
	 */
	public static function mysql_admin_structure_2_file() {


		$pr=my_mysql_query( 'SELECT * FROM '.pref_db.'content WHERE `type`=\'userdatabase\'  '    ) ;

		while ( $mm=mysqli_fetch_assoc($pr) )
		{

			$out[$mm['d1']]['tablename']=$mm['name'];

			$qu='SELECT * FROM '.pref_db.'content WHERE `type`=\'basefield\' and `rod`='.xs($mm['ident']).'  
							ORDER BY `num` ASC  ' ;

			$pr2=my_mysql_query( $qu );
			while ( $mm2=mysqli_fetch_assoc($pr2) )
			{
				setsub( $mm2);
				// $table_name, $field_left, $field_name, $type, $lenght, $keyed, $exist_key
				//mydb_field_create ($mm['d1'], $mm_old['d1'], $mm2['d1'], $mm2['d2'], $mm2['d4'], $mm2['d3'], $cols[$mm2['d1']], $mm['d7'] );

				$out[$mm['d1']][$mm2['d1']]= array_filter( [
					'name'=>$mm2['name'],
					'field'=>$mm2['d1'],
					'field_old'=>$mm2['sub']['d1_old'],
					'type'=>$mm2['d2'],
					'lenght'=>$mm2['d4'],
					'index_type'=>$mm2['d5'],
					'index_lenght'=>$mm2['d3'],
					'num'=>$mm2['d8'],
					'fun_in'=>$mm2['d9'],
					'fun_out'=>$mm2['d10'],
					'dopindex'=>$mm2['d6'],
					'dopindex1'=>str_replace( '/' , '.' , $mm2['d11']),
					'dopindex2'=>str_replace( '/' , '.' , $mm2['d12']),
					'dopindex3'=>str_replace( '/' , '.' , $mm2['d13']),
					'dopindex4'=>str_replace( '/' , '.' , $mm2['d14']),
					'dopindex5'=>str_replace( '/' , '.' , $mm2['d15']),
					'dopindex6'=>str_replace( '/' , '.' , $mm2['d16']),


				] );

			}

		}

		$out= json_encode($out, JSON_PRETTY_PRINT |  JSON_UNESCAPED_UNICODE ) ;
		file_put_contents(jscripts.'/admin_data/base.json',$out);

	}


	public static function mysqlStructure2json($rod,$mainField, $file=false,$opts=false) {


		$pr=my_mysql_query( 'SELECT * FROM '.pref_db.'content WHERE `rod`='.xs($rod) ) ;

		while ( $mm=mysqli_fetch_assoc($pr) )
		{

			$qu='SELECT * FROM '.pref_db.'content WHERE `rod`='.xs($mm['ident']).'  
							ORDER BY `num` ASC  ' ;

			$pr2=my_mysql_query( $qu );
			while ( $mm2=mysqli_fetch_assoc($pr2) )
			{
				setsub( $mm2);

				$out[ $mainField ]= array_filter( $mm2 );

				$out += self::mysqlStructure2json($mm['ident'], $mainField );

			}

		}

		if ( $file ) {
			$out= json_encode($out, JSON_PRETTY_PRINT |  JSON_UNESCAPED_UNICODE ) ;
			file_put_contents(  $out);
		}


	}
}
?>