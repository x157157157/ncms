<?
Class imgcor
	{
	
		
		public static function image_test()
			{
				ob_clean();
				header('Content-Type: image/png');
				$im= imagecreatefrompng( 'cont/img/tpl/renum.png');
				
				//imagesavealpha($thumb,true);
				//imagealphablending($thumb, false);					
				imagesavealpha($im,true);
				imagealphablending($im, false);	
				image_corsize($im, -40 , 0 , 30  ); 
				imagepng ( $im  ) ;
				ob_flush();
				exit;
			}

		public static function save($in_file, $out_file,$size_offset)
			{
				           
				$im= imagecreatefrompng( $in_file );
					 			      
				imagesavealpha($im,true);
				imagealphablending($im, false);	
				imgcor::gen($im, $size_offset ); 
				imagepng ( $im, $out_file  ) ;
				imagedestroy( $im );
			}

		public static function gen( &$im, $size_offset=false )
			{
				if (!$size_offset) return;
				
				list($width_offset , $_height_offset , $_pointx, $_pointy  ) = trimarr(  explode(',', $size_offset ) );
				 
				//v($size_offset);   
				
				// if (!$width_offset) return;
				 
				
				$w=imagesx($im);
				$h=imagesy($im);
				
				$newim=imagecreatetruecolor (  $w+$width_offset ,  $h );
				
				imagesavealpha($newim,true);
				imagealphablending($newim, false);					
				
				$cor=$w%2;
				
				if ( $width_offset < 0 )
					{
						if ($_pointx)
							{
								$wl=$_pointx;
								$wr=$w-$wl+$width_offset;    
							}
						else
							{
								$wl= ($w+$width_offset-$cor)/2;
								$wr= $wl +$cor ;
							}
						
						
						// левая часть
						imagecopy (  $newim ,  $im ,  0 , 0 , 0 , 0  , $wl , $h );
						// правая часть
						imagecopy (  $newim ,  $im ,  $wl , 0 , $w - $wr , 0 , $wr , $h );
						
						$imold=$im;
						$im=$newim;
						imagedestroy($imold);
						
					}
				elseif ($width_offset > 0)
					{
						if ($_pointx)
							{
								$wl=$_pointx;
								$wr=$w-$wl;
							}
						else
							{
								$wl= ($w-$cor)/2;
								$wr= $wl +$cor ;
							}
						
						// левая часть
						imagecopy (  $newim ,  $im ,  0 , 0 , 0 , 0  , $wl , $h );
						// правая часть
						imagecopy (  $newim ,  $im ,  $w + $width_offset  - $wr  , 0 , $w - $wr , 0 , $wr , $h );
						// повторяшка
						for ($x=1; $x<=$width_offset ; $x++)
							{
								imagecopy (  $newim ,  $im ,  $wl + $x -1   , 0 ,  $wl-1 , 0 , 1 , $h );  
							}
						
						$imold=$im;
						$im=$newim;
						imagedestroy($imold);
					}
				
				
				// Копирует часть src_im в dst_im, начиная с координат x, y src_x, src_y с шириной src_w и высотой src_h. Скопированная часть помещается на координаты dst_x и dst_y.		
			}

	 	public static function imagelinethick($image, $x1, $y1, $x2, $y2, $color, $thick = 1)
			{
			    /* этот способ применим только для ортогональных прямых
			    imagesetthickness($image, $thick);
			    return imageline($image, $x1, $y1, $x2, $y2, $color);
			    */
			    if ($thick == 1) {
			        return imageline($image, $x1, $y1, $x2, $y2, $color);
			    }
			    $t = $thick / 2 - 0.5;
			    if ($x1 == $x2 || $y1 == $y2) {
			        return imagefilledrectangle($image, round(min($x1, $x2) - $t), round(min($y1, $y2) - $t), round(max($x1, $x2) + $t), round(max($y1, $y2) + $t), $color);
			    }
			    $k = ($y2 - $y1) / ($x2 - $x1); //y = kx + q
			    $a = $t / sqrt(1 + pow($k, 2));
			    $points = array(
			        round($x1 - (1+$k)*$a), round($y1 + (1-$k)*$a),
			        round($x1 - (1-$k)*$a), round($y1 - (1+$k)*$a),
			        round($x2 + (1+$k)*$a), round($y2 - (1-$k)*$a),
			        round($x2 + (1-$k)*$a), round($y2 + (1+$k)*$a),
			    );
			    imagefilledpolygon($image, $points, 4, $color);
			    return imagepolygon($image, $points, 4, $color);
			}

		public static function drawtest($result)
			{
				
				$padding=array( 94 , 56 , 142 , 67 );
				
				list($w, $h ) = getimagesize(jscripts."/oca/full-blank-test.jpg");

				$im = imagecreatefromjpeg ( jscripts.'/oca/full-blank-test.jpg' );
				$red = imagecolorallocate ($im, 254, 0, 0);
				//          ( 650 - 31 -            22             ) /
				
				$scaleX=$w - $padding[1] - $padding[3];
				$scaleY=$h - $padding[0] - $padding[2];
				
				$stepX=$scaleX/9;
				$stepY=$scaleY/99;
				
				for ( $x=0 ; $x<=8 ; $x++ )
					{
						imgcor::imagelinethick($im, 
								
								$padding[3]+$stepX*$x , 
								$padding[0] + $scaleY* (  ( 100-intval($result[$x] ) )/200 ),  // 0 
								$padding[3]+$stepX*($x+1) ,
								$padding[0] + $scaleY* (  ( 100-intval($result[$x+1]))/200 ),  // 0 
								$red, 4 )  ;
							
					}
				 
				
				$fileout=rand(10000000000,99999999999).'.png';
				if ( !file_exists('cont/result') ) mkdir('cont/result');
				imagepng ( $im, 'cont/result/'.$fileout );
				return $fileout;
				
			}
	}
?>