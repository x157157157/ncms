<?

Class cabinet{
	
	public static function user_leads_get_view($email) {
		foreach ( cabinet::user_leads_get($email) as $u_lead ) {
			$out.= '<div>'.$u_lead.'</div>' ;
		}
		return $out;
	}
	
	public static function user_leads_get($email) {
		if (!$email) return;
		$rep=[basehref=>'','&'=>' ', '?'=>' '];
		foreach ( mydbGetSub('user_events', ['email'=>$email ] )  as $u_lead ) {
			if (  !$u_lead['event'] OR ( $u_lead['event']=='pageload' AND $u_lead['subevent']=='coming')  ) {
				$out[]= urldecode( strtr(  $u_lead ['referer'], $rep ) ) ;
			}
		}		
		return $out;
	}
	public static function user_del($id=FALSE) {
		$id= $id ? $id : $_POST['id'];
		
		mydbDelSub('users', ['id'=>$id] );
		return 'ok[data]<script> $("#obta'.$id.'").remove(); </script> '; 	
	}	
	public static function user_data_init( &$data  ) {
		$data['pass_val']=  rand() ;
		$data['pass']=cabinet::pass_hash( $data['pass_val'] ) ;
		$data['crc_email']=cabinet::crc_email($data['email'], $data['pass_val']  );	
		$data['act_href']= cabinet::act_href( $data['email'], $data['crc_email'] );
		$data['regtime']=time();
	}
	
	public static function cart_user_auto_create( $data  ) {
		
		if (   qvar('ident','cart_user_auto_create_email','id')  and !mydbget('users', 'email', trim( $data['email'] ) )   )  {
			
			cabinet::user_data_init( $data );
			
			$data['status_reg']=1;
			$data['type_reg']='cart_auto_create';
			unset($data['id']);
			$uid_new = fn('mydbAddLine', 'users',  $data);

			$post=  smarty_tpl( 'cart_user_auto_create_email_post' , array('not_inadm_comment'=>1) )  ; 
			
			$message =  smarty_tpl( 'cart_user_auto_create_email' , $data ) ;
			
			email_to_admins( $post, $message, $data['email'] );
			
			// create adress
			$post= $_POST ;
							
			$post['email']=($data['email']);
			$post['phone']=($data['phone']);
			
			$adress_dop=fn('form_fill_all', 'adv-form', $post );
			
			$adress_filled=fn('form_fill_all', 'adress-form', $post );
			
			$post_add=$post;
			$post_add['uid']= $uid_new  ;
			$post_add['is_last']=1;
			
			fn('mydbAddLine','adresses',$post_add);
			
			
		}
		
	}
	
	public static function pass_hash( $pass  ) {
		return md5(md5(trim($pass)));
	}
	
	public static function act_href( $email , $crc_email  ) {
		return bh().'/'.hrpref.'register--activate?autorizlogin='.o('autoriz-login1').'&login='.$email.'&crc='.$crc_email;
	}
	
	public static function crc_email( $email , $pass  ) {
		return md5( $_POST['email'].md5(md5($pass)).o('cryptcode') );	
	}
	
	public static function wishlist( ) {
		if ( !uid() ) return;
		
		$wl =  vl( array_flip(  explode(' ', $GLOBALS['userdata']['wishlist']) ) );
		
		if (  $_POST['add']  ) {
			
			$add=vl( array_filter(  explode(' ',$_POST['add'] ) ));
			
			if ( sizeof( $add) >1 ) {
				if ( isset(  $wl[ $add[0] ]  ) ) { 
					
					$err= '[data]added[data]';
			
				}
				
				foreach ( $add as $k=>$v ) {
					if ( !isset( $wl[$v] ) ) $dop_add[]=strip_tags($v);
				} 
				
				$wl_new =  vl( str_replace('  ', ' ',  vl(implode(' ',$dop_add)).' '.$GLOBALS['userdata']['wishlist'] ) ) ; 
				
				$wl_new=vl(array_flip(array_flip( explode(' ', $wl_new ))));
				
			} else {
				
				if ( isset(  $wl[ $_POST['add'] ]  ) ) { 
					return '[data]added[data]'; 
				} else {
					$wl_new =  vl(str_replace('  ', ' ',  strip_tags($_POST['add']).' '.$GLOBALS['userdata']['wishlist'] ) ) ; 
				}
			}
			
			
		} else if ( $_POST['del'] ) {
			
			vl($wl[ vl($_POST['del'] ) ]);
			
			unset(  $wl[$_POST['del'] ]  ) ;	
			
			$wl_new=vl(implode(' ', array_flip($wl) ) );
		}
		
		if ( fn('mydbUpdate', 'users',  array('wishlist'=>  $wl_new  ), 'id', uid()  ) ) { return '[data]ok[data]'; } ;	
		
	}
	public static function changepass( ){
		
		if( !uid() ){ return o('page_noauth'); }
		
			$mu = $GLOBALS['userdata'];
			
			if($_POST){
				if( ($_POST['newpass'] and $_POST['newpass'] === $_POST['newpassre']) ){
					$_POST['pass'] = (md5(md5($_POST['newpass']))) ;
					$post=strip_tags_arr($_POST);
					mydbUpdate( 'users',$post, 'id', uid());
					$mu = mydbget('id',uid());
					$out .= '<span class="red">Сохранено!</span>';
					//return $out;
				}
				elseif($_POST['newpass']){
					$out .= '<span class="red">Пароли не совпадают!</span>';
				}
				
			}

			foreach($mu as $k=>$v){
				$GLOBALS[$k] = $v;
			}

			$out.=smarty_tpl('cabinet_changepass');
			
			return $out;
			
	}
	
	public static function main($sendform,$_stop_fielfs = false){
		
		if( !uid() ){ return o('page_noauth'); }
			
			$mu = $GLOBALS['userdata'];
			if($_POST){
				
				$post=strip_tags_arr($_POST);

				if (inadm()) {
					
				} else {
					$post= array_intersect_key( $post , [ 
						'family'=>1, 
						'name'=>1,
						'patronymic'=>1,
						'phone'=>1,
					] );
				}
				
				mydbUpdate('users',$post ,'id',uid());
				$mu = mydbget('id',uid());
				$out .= '<span class="red">Сохранено!</span>';
			}

			foreach($mu as $k=>$v){
				$GLOBALS[$k] = $v;
			}
			
			$out .= '<form action="" method="post" onsubmit="return checkmyform(this)" enctype="multipart/form-data">'.fn('form','cabinet-form')
			.'<center>'.ibut('save_but').'</center></form>';
			
			
			
			return $out;
	}
	
	public static function orders (){
		
		if( !uid() ){ return o('page_noauth'); }
		
		$u=$GLOBALS['user'];
		
		$pr=my_mysql_query( ( 'SELECT * FROM '.pref_db.'orders WHERE email='.xs( $u['email'] ).'  ORDER BY ordertime DESC ' ) ) ;
		
		while ( $mm=mysqli_fetch_array($pr) ) {
			
			$mm['ordertime']= date( 'd.m.Y' , $mm['ordertime'] - tz*3600 ) ;
			
			$orders[]=htmlspecialchars_arr($mm);
		}
			//$order_sub
		
		return smarty_tpl( 'cabinet_orders', array('orders'=>$orders) );
		
	}
	
	
	public static function orderEdit (){
		
		if( !uid() ){ return o('page_noauth'); }	
		if ( $_REQUEST['id'] and $mm_ord=fn( 'mydbGetSub', 'orders', [ 'id'=>$_REQUEST['id'], 'email'=>$GLOBALS['userdata']['email'] ] )[0] ) {
			
			if ( $_POST ) {
						$post=strip_tags_arr( $_POST );
						mydbUpdate('orders', $post, 'id', $_GET['id']);
						$mm_ord=mydbget('orders', 'id', $_GET['id']);
					} 
					
			
			if ($mm_ord) {
				
				
				//v($mm_ord);
				$mm_htm=htmlspecialchars_arr($mm_ord);
				
				
				$out.=smarty_tpl('cabinet_orderdata' , array('mm_htm'=>($mm_htm)) );
				
				$orders = ( unserialize( $mm_ord['orderdocs'] ) );
				
				foreach ($orders as $k=>$ord) {
					
					
					$ord['ident']=$ord['ident']? $ord['ident'] : url2mnemo( str_replace( basehref.'/' , '' , $ord['url']  ) ) ;
					
					//v($ord);
					 
					if ( !$ord['tid'] ) {
						
						$mm_tov=(mysqli_fetch_array( my_mysql_query(( 
							'SELECT * FROM '.pref_db.'content WHERE 
								( 	art = '.xs( $ord['art'] ).' 
									OR 
									id = '.xs( $ord['art']).'
								) 
									AND
								ident = '.xs(  $ord['ident'] ).'
							
							LIMIT 1 ' 
						) ) )  );
						//exit();
						
					} else {
						
						$mm_tov=qvar( 'id', $mm_ord['tid'] );
						
						if ( $mm_tov['ident']==$ord['ident']) $ord['mm_tov']= $mm_tov ;
					}
					$orders[$k]=$ord;
				} 
				
				$out.=smarty_tpl('cabinet_order_tovars' , array('orders'=>$orders) );
				
				//exit();
				
				if ( inadm() ) {
					reglobalArray( $mm_ord );
					$out.=smarty_tpl('cabinet_order_edit'  );	
				}
				
				return $out;
				
			}
		}
				
	}
	public static function set_last_adress($id) {
		mydbUpdate('adresses', array('is_last'=>0), 'uid', uid() );
		mydbUpdate('adresses', array('is_last'=>1), 'id', $id );
	}
	

	public static function adresses_arr () {
		if ( uid() ) {
			$pr=my_mysql_query(( 'SELECT * FROM '.pref_db.'adresses WHERE uid='.xs( uid() ).'  ORDER BY is_last DESC , `id` DESC ' ) ) ;
			while ( $mm=mysqli_fetch_assoc($pr) ) {
				$out[]=$mm;
			}
			
			return $out;
		}
		
	}
		
	public static function adresses (  ) {
		
		if( !uid() ){ return o('page_noauth'); }
		
		foreach ( self::adresses_arr() as $mm  ) {
			$mm=htmlspecialchars_arr($mm);
			$p1['adresses'][]=$mm;
		}
		//debug_start();
		return smarty_tpl( 'cabinet_adresses', $p1 );
		//debug_stop();
	}
	
	public static function adressEdit ($_act){
		
		if( !uid() ){ return o('page_noauth'); }
		$return_url=$_GET['return']=='cart' ? 'cart' : 'myadresses';
		if ( $_REQUEST['id']=='new' ) {
		
			if ($_POST and antikapcha() )
				{
					$_POST['uid']=uid();
					
					
					$post= strip_tags_arr( $_POST );
					
					$newid=mydbAddLine('adresses', $post );
					cabinet::set_last_adress($newid);
					$out .= '<span class="red">Сохранено!</span>';
					header("Location: ".basehref.'/'.$return_url );
					exit;
				
				}
			$out.= smarty_tpl('cabinet_adress_form');
			
		} else if ( $_REQUEST['id'] ) {
				
			$mm_adr = (mydbGetSub('adresses', array( 'id'=>$_REQUEST['id'], 'uid'=>uid() ) ) );
			
			if ($mm_adr AND $_act=='del') {
				
				mydbDelSub('adresses',array('id'=>$_POST['id']) ) ; 
				return 'ok'; 
				
			} else if ($mm_adr) {
				
				if ($_POST and antikapcha() ){
					
					$post= strip_tags_arr( $_POST );
					
					mydbUpdate('adresses', $post , 'id' , $mm_adr[0]['id'] );
					cabinet::set_last_adress($_REQUEST['id']);
					$mm_adr = (mydbGetSub('adresses', array( 'id'=>$_REQUEST['id'], 'uid'=>uid() ) ) );
					
					$out .= '<span class="red">Сохранено!</span>';
					header("Location: ".basehref.'/'.$return_url );
					exit;
					
				}
				
				foreach($mm_adr[0] as $k=>$v){
					$GLOBALS[$k] = $v;
				}
				$out.= smarty_tpl('cabinet_adress_form');	 
			}
		}		
		
		return $out;
		
	}
	
	public static function fogot () 
	{
		
		
		if ( $_POST['email'] or $_REQUEST['id']  )
			{
				$log=$_POST['email'] ? $_POST['email'] : $_REQUEST['id'];
				
				$u=mydbget('users', ( $_POST['email'] ? 'email' :'id'  ) ,$log);	
				$crc=md5(  $u['id'].substr($u['pass'],0,7).o('cryptcode') );
				$crcok= ($crc)== $_REQUEST['crc'] ? true : false ; 				
			}		
		
		if( ($_POST['email'])  )
			{				
				if  ( $u ) {
					$header= "MIME-Version: 1.0\r\n";		
					$header.= "Content-type: text/html; charset=UTF-8\r\n";
					$from=email_from();
					
					$p['hr']=bh().'/fogotpass--?mode=chpass&crc='.$crc.'&id='.$u['id'];
					//$message='Для создания нового пароля вам необходимо перейти по ссылке <a href="'.$hr.'" >'.$hr.'</a>' ;

					//if ( !( ( $u['email']   ),'Восстановление пароля' , $message , $header ) ) $out='Ошибка отправки' ;
					//Восстановление пароля для '.str_replace('http://','', basehref)
					
					if (  email_to_admins( smarty_tpl('fogot_email/post', $p), smarty_tpl('fogot_email/message', $p), $_POST['email'] ) ) {
							$out.='<center><h2>На указанный email '.( $login ?   'при регистрации ' : '' );
							$out.='отправлено письмо для восстановления</h2> 
									<p>Письмо, как правило, приходит менее чем через 5 минут. </p><p>При отсутствии письма, проверяйте папку "спам"</p></center>';				
					} else {
							$out.='<span class="red">Ошибка отправки</span>' ; 
							$out.=smarty_tpl('fogot_form');
					}
					
	
				} else {
					$out.='<center><h2 class="red">Указанного email нет в системе</h2></center>';
					$out.=smarty_tpl('fogot_form');
				}
				
				
			}
		
		elseif ($_POST['newpass'] AND $crcok  ) 
			{
				$err.= strlen($_POST['newpass'])<8  ? '<div class="red">В пароле должно быть не менее 8 символов</div>' : '' ; 
				$err.= ( isset($_POST['newpassvalid']) AND $_POST['newpassvalid'] !==$_POST['newpass'] )   ? '<div class="red">Пароли должны совпадать</div>' : '' ; 				
				if ( $err )
					{
						$out.=$err;
						$_GET['mode']='chpass';
					}
					else
					{
						mydbUpdate('users', array('pass'=> md5( md5( $_POST['newpass'] ) ) ), 'id', $_POST['id']  );
						autoriz($_POST['id']);
						$p['url']=bh().'/';
						$out = smarty_tpl('cabinet_newpass_saved',$p);

					}
				
			}
			
		if( $_GET['mode']=='chpass' AND $crcok  )
			{
				
				$out.=smarty_tpl	(	'cabinet_fogot_chpass' , 
									array(
										'crc'=>$crc, 
										'u'=>$u
										) 
								);
								
			}
		if ($_REQUEST['login'] and !$crcok)
			{
				$out.='Неверный проверочный код';
			}
		
		
		
		if (  !$_GET AND !$_POST   )
			{
				
				$out.=smarty_tpl('cabinet_fogot');
				
			}
		
		return $out;
	}
	

}

?>