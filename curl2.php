<?php
/**
 * Created by PhpStorm.
 * User: Юрий
 * Date: 10.07.2018
 * Time: 20:03
 */

class curl2
{
    public static function post ( $url, $params ) {

		$curl = curl_init();

		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query( $params) );
        curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
        curl_setopt($curl, CURLOPT_VERBOSE, true);

        $verbose = fopen('cont/logs/curl.log', 'w+');
        curl_setopt($curl, CURLOPT_STDERR, $verbose);

		$out = curl_exec($curl);
		curl_close($curl);
		return $out;

    }

    public static function post2($url, $params)
    {
        $postdata = http_build_query(
            $params
        );

        $opts = [
            'http' =>
            [
                'method'  => 'POST',
                'header'  => 'Content-Type: application/x-www-form-urlencoded',
                'content' => $postdata
            ]
        ];

        $context  = stream_context_create($opts);

        return  file_get_contents($url, false, $context);
    }
}

