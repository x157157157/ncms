<?
class ucspay {
	
	
	
	public static function pay ($p)
		{
			 require_once( jscripts.'/lib/orderv2.php' );

			    /* логин */
			    $login = o('robo_login');
			    /* пароль */
			    $password =o('robo_pass_1');
			    /* номер магазина */
			    $shopId = o('shopid');
			    /* адрес шлюза */
			  
			    $url = o('robo_test') ? 'https://tws.egopay.ru/order/v2/' : 'https://ws.egopay.ru/order/v2/' ;


			    /* номер заказа */
			    
			    $orderId = $GLOBALS['cache']['cart']['insert_arr']['id'] ;
			    /* сумма заказа */
			    $orderAmount = $GLOBALS['cache']['cart']['totalsum'] ;

			    $itemPNR = $orderId;

			    /* адрес для возврата при успешной оплате */
			    $urlOk = basehref.'/cart--success';
			    /* адрес для возврата при отказе от оплаты */
			    $urlFault = basehref.'/cart--fail';


			    $soapClient = new orderv2(null, array('location' => $url,
			                                          'uri'      => basehref,
			                                          'login'    => $login,
			                                          'password' => $password,
			                                          'trace'    => 1,
			                                          'features' => SOAP_SINGLE_ELEMENT_ARRAYS,
			                                          'connection_timeout' => 12));   

			    $order = new OrderID(); 
			    $order->shop_id = $shopId;
			    $order->number = $orderId;       
			    
			    $cost = new Amount();
			    $cost->amount = $orderAmount;
			    $cost->currency = 'RUB';

			    $customer = new CustomerInfo();
			    $customer->name = $GLOBALS['cache']['cart']['name'].' '.$GLOBALS['cache']['cart']['family'];
			    $customer->email = $GLOBALS['cache']['cart']['email'];
			    $customer->phone = $GLOBALS['cache']['cart']['tel'] ;

			   $description = new OrderInfo();
			   // $description->paytype = 'card';

			    $itemCost = new Amount();
			    $itemCost->amount = $orderAmount;
			    $itemCost->currency = 'RUB';

			   /* $item = new OrderItem();
			    $item->typename = $itemTypeName;
			    $item->number = $itemPNR;
			    $item->amount = $itemCost;
			    $item->host = $itemHost;

			    $items = new SoapVar(array(
			                                new SoapVar($item, SOAP_ENC_OBJECT, null, null, 'OrderItem'),
			                        ), SOAP_ENC_OBJECT);
			    $description->items = $items;*/

			    $language = new PostEntry();
			    $language->name = 'Language';
			    $language->value = 'ru';

			    $cardtype = new PostEntry();
			    $cardtype->name = 'ChoosenCardType';
			    $cardtype->value = 'VI';

			    $returnUrlOk = new PostEntry();
			    $returnUrlOk->name = 'ReturnURLOk';
			    $returnUrlOk->value = $urlOk;

			    $returnUrlFault = new PostEntry();
			    $returnUrlFault->name = 'ReturnURLFault';
			    $returnUrlFault->value = $urlFault;

			    $request = new register_online();     
			    $request->order = $order;
			    $request->cost = $cost;
			    $request->customer = $customer;
			    $request->description = $description;

			    $postdata = new SoapVar(array(
			                                    new SoapVar($language, SOAP_ENC_OBJECT, null, null, 'PostEntry'),
			                                    new SoapVar($cardtype, SOAP_ENC_OBJECT, null, null, 'PostEntry'),
			                                    new SoapVar($returnUrlOk, SOAP_ENC_OBJECT, null, null, 'PostEntry'),
			                                    new SoapVar($returnUrlFault, SOAP_ENC_OBJECT, null, null, 'PostEntry'),
			                            ), SOAP_ENC_OBJECT);     

			    $request->postdata = $postdata;
			if (0)
				{
					
				
					echo '$soapClient:'."<br/>\n";
					echo '<textarea rows="15" style="width:100%;">';
					 print_r($soapClient); 
					echo '</textarea><br/><br/><br/>';
					 
					echo '$request:'."<br/>\n";
					echo '<textarea  rows="15"  style="width:100%;">'; 
					print_r($request); 
					echo '</textarea><br/>';

			 	}
				
			ucspay::register($soapClient, $request );
	
		}
		
	public static function register($soapClient, $request)
		{
			if ($GLOBALS['cache']['ucs']['num'] <=2 )
				{
					try {
			  	
					        $info = $soapClient->register_online($request);
						header('Location: ' . $info->redirect_url . '?session=' . $info->session);
						
						exit();
					       return;
					    } catch (SoapFault $fault) {
					  	
					        echo '<!-- SOAP error: ', $fault->faultcode, '-', $fault->faultstring, ' -->';
						loger(0,'SOAP error','', $fault->faultcode. '-'. $fault->faultstring, '', print_r($soapClient,1)."\r\n".print_r($request,1) );
						sleep(1);
						$GLOBALS['cache']['ucs']['num']++;
						ucspay::register($soapClient, $request );
						
					    } catch (Exception $fault) {
					        echo '<!-- PHP Exception: ', $fault->__toString(). ' -->';
						
					    }
				}
			
			
		}
		
	public static function success ()
		{
			return fgc('success-opl'); 
		}
		
	public static function result ()
		{
		    register_shutdown_function(function () {
                ucspay::end();
            });
			 /* запускаем сервер */
            //return fn('ucspay::res2');

			/* в wsdl в soap:address указан адрес данного файла */
            try {
                vl();

                $post = file_get_contents('php://input');

                //$server = new SoapServer('jscripts/ucsserver.wsdl'); vl();
                /* привязываем класс, отвечающий за обработку */
                //$server->setClass('ucsserver'); vl(1);
                //$server->handle(); vl(2);
                //vl( json_encode_pretty($server) );
                
                $xml = self::soap_xml_decode($post);
    
                loger(0, 'ucs_result', '', '', '', $post, $xml  ) ;

                return  $xml ;


            } catch (Exception $e) {
                vl( $e->getMessage() );
                loger ( '','ucs_error', __LINE__, '','', $e->getMessage(), '' );

            }

			
		}

    public static function soap_xml_decode($post)
    {
        $clean_xml = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $post );
        $xml = ( simplexml_load_string($clean_xml) );
        return  json_decode(json_encode( $xml->Body->notify ), true); ;

	}	
		
    public static function res2() {
            vl();
            //$server = new SoapServer('jscripts/ucsserver.wsdl'); vl();
            /* привязываем класс, отвечающий за обработку */
            //$server->setClass('ucsserver'); vl(1);
            //$server->handle(); vl(2);

            vl(3 );
    }

    public static function end()
    {

        $err=vl(error_get_last());

    }
		
}


?>