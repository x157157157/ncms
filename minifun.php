<?


function inadm ()
{
    return $_COOKIE['okauth']==md5($GLOBALS['adminlogin'].$GLOBALS['adminpass'] );
}

function is_test() {
    return   ( inadm() AND $_COOKIE['is_test'] ) or $_GET['is_test']=='1' ;
}

function file_theme_name($file_name) {
    theme('theme_start');

    foreach ( $GLOBALS['theme']['d3'] as $folder ) {

        $folder=(trim($folder));

        if ($folder) {
            if (  file_exists( ($folder.'/'.$file_name )) ) {

                return $folder.'/'.$file_name  ;
            }
        }
    }
    vl( $GLOBALS['theme']['d3'], 'error_find_file');
}

function theme( $ident, $_context=false ) {
	/*
		Возвращает идентификатор или путь или файл в зависимости от перебивки в контент объекте текущей темы
		По сути идут тупая подстановка старого текста на новый в зависимости от указаний в текущей теме
		1) контент объекты подмена идентификатора, например был указан hmenu_tpl , а тема его перебила на hmenu_tpl_mobile
		2) Полные пути для подмены нужно указывать для следующих файлов 
			а) любые класссы. Пример jscripts/hmenu.php jscripts/tpl/mobile/hmenu.php
			б) главные шаблоны _all и _body (если они не указаны в текущем "Дизайне скелета" в 
				"Идентификатор html head шаблона" "Идентификатор body шаблона")
				например jscripts/tpl/_all.php jscripts/tpl/_all_new.php
				 
	*/
	if ( ! isset( $GLOBALS['theme'] ) ) {

		$find=[ 'rod'=>'themes', 'ok'=>'on' ];
		
		if ( !is_mobile() AND !is_test()  ) {
		 	$find['d2']='desktop';
		}else if ( !is_mobile() AND is_test()  ) {
		 	$find['d2']='desktop_test';
		}else if ( is_mobile() AND !is_test()  ) {
		 	$find['d2']='mobile'; 
		}else if ( is_mobile() AND is_test()  ) {	 	
		 	$find['d2']='mobile_test';
		}
		 
		 $theme = mydbGetSub('content', $find )[0]  ;
		
		foreach ( explode( "\n", $theme['d1'] ) as $rep ) {
			$r= explode(' ' , trim(  ($rep) ) );
			$theme['rep'][ $r[0] ]=$r[1];
		}
		$d3=[core, local_core];
		foreach ( explode( "\n", $theme['d3'] ) as $folder ) {
			$d3[]=local_core.'/'.trim($folder);
		}
		$theme['d3'] = $d3;
		$GLOBALS['theme']=($theme);
	}
	
	$out= ( $GLOBALS['theme']['rep'][$ident] ? : $ident) ;
	
	//vl( $ident.' -> '. $out ,  'theme_'.$_context );
	
	return $out;		
	
}
function  mydbGetSub($table_name, $search_arr_sec , $_field_out=false , $_or_mode_sec=false )
{
    if ( $_field_out )
    {
        $fld=is_array($_field_out) ? implode(',', $_field_out) :$_field_out;
    }
    else
    {	$fld='*';
    }
    $qu='SELECT '.$fld.' FROM `'.pref_db.$table_name.'` 
		WHERE '.mydb_srcharr2wh($search_arr_sec,$_or_mode_sec).'  ORDER BY `id` ASC ';
    //v($qu);
    $pr=my_mysql_query($qu);
    while ( $mm=mysqli_fetch_assoc($pr) )
    {
        $out[] = ($_field_out and !is_array($_field_out) )  ? $mm[$_field_out] : $mm ;
    }
    return $out;
}
function mydb_srcharr2wh ($search_arr_sec,$_or_mode_sec=false)
{
    $bool=$_or_mode ? 'OR' : 'AND';
    foreach ($search_arr_sec as $k=>$v )
    {
        $where[]= '`'.( $k? $k : '' ).'`='.xs($v);
    }
    return implode(' '.$bool.' ',$where);
}
function main_fun( $file_name ) {
	return  ( is_test() AND $GLOBALS['test_fun'][$file_name] ) ? $GLOBALS['test_fun'][$file_name]  : $file_name;  
}

function is_mobile() {
	//return  $GLOBALS['is_mobile'];
	return substr( $_SERVER['HTTP_HOST'], 0,2 )=='m.';  
}



function global_get ($key) {
	return $GLOBALS['globals_vars'][$key];
}

function global_set ($key, $value=false) {
	$GLOBALS['globals_vars'][$key]=$value;
}
function global_add ($key, $value=false) {
	$GLOBALS['globals_vars'][$key][]=$value;
}

function global_unset (){
	unset( $GLOBALS['globals_vars'] );
}

function is_testing() {
	return   $GLOBALS['is_testing'];
}
function test_start() {
	$GLOBALS['is_testing']=true;
}
function test_stop() {
	$GLOBALS['is_testing']=false;
}

function array_alloy_keys($array, $allowed_keys_arr ) {
	
	foreach ($allowed_keys_arr as $k) {
		$keys_arr[$k]=1;
	}
	
	return array_intersect_key( $array , $keys_arr ) ; 
	
}

function array_merge_null( $arr1, $arr2 ) {
	
	if ( is_array( $arr1 ) AND is_array($arr2) ) {
		return array_merge($arr1,$arr2);
	} else {
		return is_array( $arr1 ) ? $arr1 : $arr2 ;
	} 	
}

function mycache_get( $key, &$keymd5_ , $_ttl=FALSE )
	{
		$keymd5_ = md5($key);
		switch ( constant('cachetype') ) 
			{			
				case 'file':
					//	ищем файл $keymd5 который свежее чем $ttl				
					if ( filemtime(( 'cont/cache/'.$keymd5_ ))+$_ttl > time() )
						{
							return file_get_contents( ('cont/cache/'.$keymd5_)) ;
						}
					
				break;
				case 'memc':
					//
				break;
				case 'mysql':
					//
				break;
				case 'apc':
					//
				break;
			}
	} 

function mycache_set( $keydm5, &$out_ , $_ttl=false )
	{
		switch ( constant('cachetype') ) 
			{			
				case 'file':
					//												
					file_put_contents('cont/cache/'.$keydm5, $out_ );
				break;
				case 'memc':
					//
				break;
				case 'mysql':
					//
				break;
				case 'apc':
					//
				break;
			}
	} 


function create_admin_menu ($rod,$ur) //2
		{
		@$inlim=func_get_arg(2);
		@$ev=func_get_arg(3);
		
		$mrod= qvar('ident', $rod );
		
		global $menu, $edit, $rod_edit,$rod_edit2, $put;
		//v($put);
		if ($put[0]=='admin') {$lenspis=1500;}
			else {$lenspis=1500;}
		if ($GLOBALS['lim'] && $inlim)
			{
			$start=$GLOBALS['lim']*$lenspis-$lenspis;
			}
		if ($ev=='singl' or $ev=='minisingl')
			{
				$qu='SELECT * FROM '.pref_db.'content 
				WHERE `id`="'.$rod.'"  ORDER BY `num` ASC, `ident` DESC LIMIT '.intval($start).', '.$lenspis;
				//v($qu);
			}
			else
			{				
				//v(reg());
				if( $mrod['type']=='bnews' )
					{
						$order='d1 DESC, ';
					}
				elseif($mrod['ident']== 'tov_values')
					{
						$order = 'd4 ASC, ';
					}
				else
					{
						$order='';
					}
				
				
				$qu='SELECT SQL_CALC_FOUND_ROWS * FROM '.pref_db.'content 
				WHERE rod="'.$rod.'" AND `d100`<='.reg().'  ORDER BY '.$order.' `num` ASC, `ident` DESC LIMIT '.intval($start).', '.$lenspis ;
			}
		
		$prebase=my_mysql_query( $qu) ;
		
		
		$prenum=my_mysql_query('SELECT FOUND_ROWS()') ;
		$aln=mysqli_fetch_array($prenum);
		
		
		
		if ( ($aln[0]> $lenspis) & (!$inlim) ) // esli mnogo podpunktov
			{
			for ($x=1;$x<(1+$aln[0]/$lenspis);$x++ )
				{
				$m['type']='zagl';
				$m['id']=-1;
				$m['name']=$x;
				$m['ident']=$rod;
				$pref='+';
				if ( $m['ok'])
					{ $col="#009900"; }
					else
					{ $col="#0000FF"; }
					
				if ( $x==$GLOBALS['lim'] )
						{
						$rod_edit=$m['rod'];
						$rod_edit2=$m['ident'];
						$ukaz1 = "<strong>";
						$ukaz2 = "</strong>";
						//$ukaz1='font-weight:bold'
						$subm=true;
						}
						else
						{ $ukaz1=$ukaz2 = "";$subm=false;
						}
				$idpar=$GLOBALS['id'];
				$pref="<a  class='aj' href='javascript:adm(-{$idpar},{$ur},{$x})'>+</a>";
				
				?>
				<div class='t-<?=$m['type'].( $m['sub']['nonal']?' not_av': '' ); ?>' id='lim<?=$idpar.'-'.$x; ?>'><span class='context'><?=$pref; ?>
                <?=$ukaz1; ?><a  style="color:<?=$col; ?>; " href="?id=<?=$GLOBALS['m']['id'] ?>&mode=-1&lim=<?=$x; ?>"><?=lang($m['name']); ?></a><?=$ukaz2; ?></span>
					<?
				
					if ( ($subm)  & ($ev!=='singl') )
						{
						create_admin_menu ($m['ident'],$ur,$GLOBALS['lim']+1);
						}
					?>
					</div>
				<?
				}
			}
			else
			{
			
			while ($m=mysqli_fetch_assoc($prebase) )// затем начинаем перебирать каждый пункт и делать выборку по каждому (рекурсивно)
				{
				setsub($m);
				if ( $m['id']==$GLOBALS['id'] )
						{
						$rod_edit=$m['rod'];
						$rod_edit2=$m['ident'];
						//$ukaz1 = "<strong>";
						//$ukaz2 = "</strong>";
						$act=' act';
						}
						else
						{ $ukaz1=$ukaz2 =$act ="";
						}
				if ( $m['ok'])
					{ $col="#009900"; }
					else
					{ $col="#0000FF"; }
			
				if (  qvar('rod', $m['ident'] ) )
					{
			
						$pref="<a  class='aj' href='javascript:adm({$m['id']},{$ur},0)'>+</a>";
			
					}
					else
					{
					$pref="";
					}
				
				if (reg()>=5) $fields=qqu("`type`='contenttype' and `d1`=".xs($m['type']));
				unset($field,$field2);
				if ($m[$fields['d2']] ) $field[]=urlFromMnemo( $m[$fields['d2']] );
				if( $m[$fields['d5']] ) $field[]=$m[$fields['d5']];
				@$field=implode('~', $field);					
				
				if (  $field ) $field2=' ('.substr(htmlspecialchars( $field ).')',0,35);
								
				$hhtitle=urlencode ($m['name'].$field2);
				
				if (!$field2 & !$m['name']) $m['name']='('.$m['ident'].')';
				
				if ($inlim)
					{
					$lim='&lim='.$GLOBALS['lim'];					
					}
					
				if ( @strpos(' spis checkbox', $m['type']) )
					{
						$ankor=hrpref.'{'.$m['d1'].'}';
					}
					elseif ($m['type']=='mpunkt')
					{						
						$ankor=hrpref.'{'.qvar('ident',$m['rod'],'d1').'}'."='".$m['d1']."'"	;
					}					
					else
					{						
						$ankor=hrpref.urlFromMnemo($m['ident']);
					}
				
					
				if ($ev!=='minisingl')
					{
					?>				
					<div class='t-<?=$m['type'].( $m['sub']['nonal']?' not_av': '' ); ?>' id='<?=$m['id']; ?>'>
					<? } ?>
				
				<span id='ss<?=$m['id']; ?>' class='context<?=$act.( $m['sub']['nonal']?' not_av': '' ); ?>'>
				
				<span class='context0'>
					<a title="Контекстное меню"   class="context2" id='c<?=$m['id']; ?>' style="color:<?=$col; ?> " href="?mode=0<?=$lim; ?>&id=<?=$m['id'] ?>"><img src='/<?=jscripts; ?>/tpladmin/spacer.gif'></a><a title="Редактор"  class="context3" id='r<?=$m['id']; ?>'    style="color:<?=$col; ?> " href="?mode=2<?=$lim; ?>&id=<?=$m['id'] ?>"><img src='/<?=jscripts; ?>/tpladmin/spacer.gif'></a></span><?=$pref; ?><?=$ukaz1; ?><a class='svoystva'  style="color:<?=$col; ?> " id='e<?=$m['id']; ?>' href="?&id=<?=$m['id'] ?>&num=<?=$m['num'] ?><?=$lim; ?>&mode=1&ankor=<?=$ankor; ?>"><?=lang($m['name']).$field2; ?><?=( $m['d88'] ? '<b title="'.$m['d88'].'">*</b>' : '' ) ; ?></a><?=$ukaz2; ?>&nbsp;
				
				 
				
				&nbsp;<span class="ancor"><a href="<?=$ankor; ?>"><?=$m['name']; ?></a></span></span>
				<? 
				if ($ev!=='minisingl')
					{
					
					if (  ( $put[$ur]==$m['ident'] and  !$inlim  ) or $_COOKIE['openid'][$m['id']] )
						{
						//v($_COOKIE['openid'][$m['id']]);
						create_admin_menu ($m['ident'],$ur+1);
				
						}
					?>
					</div>
					<?
					}
				
				}
			}
			
		
		} 



function urlFromMnemo($ident)
{
	return strtr($ident , array_flip($GLOBALS['repurl'] ? $GLOBALS['repurl'] : $GLOBALS['repurl_main'] ) );
}

function reglobal()
	{
	$keys=array_keys($_REQUEST);
	define('ruscode',strlen($_POST['ruscode']));
	//if ($_POST['ajax']==1 and ruscode==12)
		//{
			//v($_REQUEST);
		//	$_REQUEST=w1251($_REQUEST);
			//v($_REQUEST);
			
	//	}
	
	foreach ($keys as $k)
				{
					if ( isset( $_COOKIE[$k]) AND isset($_POST[$k]) ) {
						$GLOBALS[$k]= $_POST[$k];
					} elseif ( isset( $_COOKIE[$k]) AND isset( $_GET[$k] ) )  {
						$GLOBALS[$k]= $_GET[$k];
					} else {
						$GLOBALS[$k]= $_REQUEST[$k];
					}
				}
		
	
	
	}
	
function fn( $fn_name, $_arg1=false, $_arg2=false, $_arg3=false, $_arg4=false, $_arg5=false, $_arg6=false, $_arg7=false, $_arg8=false, $_arg9=false, $_arg10=false, $_arg11=false ,$_arg12=false){
	
	
	$must=substr($fn_name,0,1)=='@';	
	$fn_name=theme( substr($fn_name, ( $must ? 1 : 0 ),200) );

	$start_time=microtime(1);
		
	if ( inadm() ) {
		
		for ($x=1;$x<=5;$x++) {
			$name_var='_arg'.$x;
			if ($$name_var) $params[$name_var] = $$name_var  ;
		}
		
		vl( $params, $fn_name.' params' );
		$GLOBALS['bedug_ur']++;
		
	}
	
	$fn_name_class = explode ( '::', $fn_name );
	
	if ( $fn_name_class[1] ) {
		if ( function_exists( $fn_name_class[0].'_'.$fn_name_class[1].'_local' ) ) {
			
			$fn_name_out=$fn_name_class[0].'_'.$fn_name_class[1].'_local';
			if ( !function_exists($fn_name_out) ) $err=1; 
			
		} else {
			
			$fn_name_out= array( $fn_name_class[0], $fn_name_class[1] );
			if ( !method_exists($fn_name_out[0], $fn_name_out[1] ) ) $err=1; 
		
		}
	}
	else {
		$fn_name_out=function_exists($fn_name.'_local') ? $fn_name.'_local' : $fn_name;
		if ( !function_exists($fn_name_out) ) $err=1; 
	}
	
	if ($err AND !$must) {
		$return=vl('Неизвестная функция '.$fn_name , $fn_name.' return');
		
	} else {
		
		$return= vl( call_user_func( $fn_name_out ,  $_arg1, $_arg2, $_arg3, $_arg4, $_arg5, $_arg6, $_arg7, $_arg8, $_arg9, $_arg10, $_arg11, $_arg12), $fn_name.' return',
			[
					'time_exec'=>microtime(1) - $start_time
			]) ;
		
	}
	
	$GLOBALS['bedug_ur']--;
	return  $return ;
	
}



function vt($wd,$_hidden=false,$_textarea=false) {
	return v($wd,0,1); 
}



function v($wd,$_hidden=false,$_textarea=false)
{
	if( is_array($_hidden) ) { $h=$_hidden; unset($_hidden); extract($h);  }
	
	//var_dump($_textarea);
	
	if ( !inadm() AND !$_hidden ) return $wd;
	
	
	$debug = debug_backtrace(0,3);
	// var_dump($debug);
	
	$line= $debug[2]['function'].'('.$debug[1]['line'].')/'.$debug[1]['function'].'('.$debug[0]['line'].")\r\n";	
	
	if(1)
	{
		if ($_hidden ) echo '<div style="display:none;">';		
		if (  ( ( is_array($wd) or is_object($wd) ) and $_POST['ajax']!==1) or $_textarea)
		{
			
			?><textarea  style="width:100%;height:200px;" >
			<?
			echo($line);
			var_dump($wd); 
			?></textarea><?
		}
		else
		{
			echo($line);
			var_dump($wd);
		}
		echo '<br/>';
		if ($_hidden) echo '</div>';
	}
	return $wd;
} 
             
function find_rod($ident)
{
	//global $put;
	/* $prebase=my_mysql_query( 'SELECT `ident`,`rod` FROM `'.pref_db.'content` WHERE ident='.xs($ident).'  ORDER BY `num` ASC ' ) ;
	$mm=mysqli_fetch_assoc($prebase);*/
	$mm=qvar('ident',$ident);
	//echo $mm['ident']." - ".$mm['rod'].'<br/>';
	if ( ($mm) )
	{
		//echo $mm[ident]."<br/>";
		$GLOBALS['put2'][]=$mm['ident'];
		$GLOBALS['put'][]=$mm['ident'];
		$GLOBALS['pardata']=$mm;
		find_rod($mm['rod']);
	}
}
	
function vl( $in=false, $_context=false, $_context_arr=false ) {

	if ( is_debug() AND ($_REQUEST['mode'])!=='log' AND !$_POST['admajax'] ) {
		if ( !$GLOBALS['vl_logger'] ) {
			$sufix=debug_suf();

			rename( 'cont/logs/debug_'.$sufix.'.1.txt', 'cont/logs/debug_'.$sufix.'.2.txt' );
			rename( 'cont/logs/debug_'.$sufix.'.txt', 'cont/logs/debug_'.$sufix.'.1.txt' );

			$GLOBALS['vl_logger'] = fopen('cont/logs/debug_'.$sufix.'.txt', 'w');

		}
		//v('vl'.$GLOBALS['vl_num']++);
		 $_con=explode(' ',trim( $_context ));
		 
		$debug = debug_backtrace( );
		if ( $_con[1]=='params' ) {
			$line=  $debug[1]['line'] ;	
		} else if ( $_con[1]=='return' ) {
			
		} else {
			$line=  $debug[0]['line'] ;	
		}


		//exit();
		$cv=preg_replace('/[^a-zа-яё]+/iu', '_', $_context);
		
		if ( inadm() AND (  $_COOKIE[ $cv ] OR $_COOKIE['show_all'] ) )  {
			fwrite( $GLOBALS['vl_logger'] , serialize( array(
				'date'=> date('H:i:s'),
				'context'=>trim($_context),
				'context_arr'=>serialize($_context_arr),
				'data'=>  var_export( $in, 1 )   ,
				'line'=>$line,
				'bedug_ur'=>$GLOBALS['bedug_ur']
			)).'[vl_logger_sep]' );
		}


	}
	
	return $in;
}

function debug_suf(){
	return  $_GET['debug']  ? : ( $_COOKIE['debug'] ? : $_COOKIE['is_test'] );
}

function is_debug() {
	if (
			!( $_GET['xhprof']?:$_COOKIE['xhprof'] )
		AND
			(
					( inadm() AND $_COOKIE['show_all'] )
				OR
					in_array( debug_suf() , $GLOBALS['debug_keys']??[])
			)
		AND
			!$GLOBALS['stop_debug']
	)
	{ return TRUE;}
}

function w1251($str)
	{	
			if (is_array($str))
				{
					$keys=array_keys($str);
					foreach($keys as $k)
						{
							$nstr[$k]=w1251($str[$k]);
							//v($nstr[$k]);
						}
					return $nstr;
				}
			else
				{					
					return iconv('utf-8', 'windows-1251//IGNORE', $str);					
				}
	}
	
function utf($str)
	{
		if (is_array($str))
				{
					$keys=array_keys($str);
					foreach($keys as $k)
						{
							$nstr[$k]=utf($str[$k]);
							//v($nstr[$k]);
						}
					return $nstr;
				}
			else
				{					
					return iconv  ('windows-1251', 'utf-8' ,  $str);					
				}
	}
	
function lang($text)

	{
	$text=explode('++',$text);
	$out=$text[$GLOBALS['langs'][ lngsuf ] ];
	if ($out)
		{	return $out;}
		else
		{ return $text[0];}
		
	
	}
	




function reg()
	{
		if (!defined('adreg')) 
			{
				define('adreg', intval($_COOKIE['adreg']) );
			}	
		return adreg;
	}
	

function qqu($find_data,$field=false,$_cache=false)
	{
		if ( $GLOBALS['cache'][$find_data.$field] and $_cache  )
			{
				return $GLOBALS['cache'][$find_data.$field];
			}
		else
			{	
		
				$qu="SELECT * FROM `".pref_db."content` WHERE $find_data limit 0,1 ";		
				$pr=my_mysql_query($qu);		
				if ($pr )		
					{		
					$out=mysqli_fetch_assoc($pr);		
					if ( ($field=='*') | ($field=='') )		
						{		
							$GLOBALS['cache'][$find_data.$field]= $out;		
						}		
					else		
						{		
							$GLOBALS['cache'][$find_data.$field]= $out[$field];		
						}					
					}
				setsub($out);
				 return $GLOBALS['cache'][$find_data.$field];
									
			}
	}		



function my_mysql_query($qu)
	{
		if ($_COOKIE['debug_all_qu']) {

            file_put_contents('cont/logs/qu_all.log', json_encode_pretty($qu).PHP_EOL, $GLOBALS['querys'] ? FILE_APPEND : false );
        }

		$GLOBALS['querys']++;
		//$GLOBALS['quu'].=$qu."\r\n\r\n";
		
		$out=mysqli_query($GLOBALS['db_link'],$qu);
		if ( !$out and  $GLOBALS['cache']['sqlerr']  )
			{
			 v(mysqli_errno() . ": " . mysqli_error() . "\n в запросе:".$qu ) ;
			}
			else
			{
				return $out;
			}
			
	}
	

function myauth()//2
	{
	//session_start();
	global $adminlogin, $adminpass ;
	//v($_SESSION);
	//v($_COOKIE['okauth']);
	//v(md5($adminlogin.$adminpass.$_SERVER['REMOTE_ADDR'] ) );
	

	if ( ($_REQUEST['login']==$adminlogin) & ($_REQUEST['pass']==$adminpass )  )

		{
		setcookie('okauth', md5($adminlogin.$adminpass),time()+3600*24*30 );
		define('adm',1);
		define('inadm',1);
		}
		
	elseif ($_COOKIE['okauth']==md5($adminlogin.$adminpass) ) //.$_SERVER['REMOTE_ADDR']

		{
		define('adm',1);
		define('inadm',1);
		//v(1);
		}
	else
		{
		define('inadm',false);
		?><body style="background:none;">

        <link href="/<?=jscripts; ?>/admin.css" rel="stylesheet" type="text/css">

       <div style="position:absolute; top:150px; left:0px;width:100%">

            <div id="auth">

            <form id="form1" method="post" action="">

             

                <input id='login' type="text" name="login"  /><br />

             

                <input type="password" name="pass" id="password" /><br />

                <input type="submit" name="submit" value=" " id='submit'/>

            

            </form>

            </div> 

        </div></body>

        <?

        exit;

		}

	

	}
 
function qvar($field_find,$val_find=FALSE )
	{
		@$field=func_get_arg(2) ; 
		@$nocache=func_get_arg(3) ;
		//v($field_find);
		if (1) {
			if ( is_numeric($field_find) and (($val_find)===FALSE)) {return qvar('id', theme($field_find));}
		}
		if( ($val_find)===FALSE )  return qvar('ident',theme($field_find)) ? :  qvar('id', theme($field_find)) ;		
		
		//$val_find=theme( $val_find );
		//замена {{ выдерание }} высчитывание  
		// { } подстановка, слеширование и по бокам кавычки
		if( isset($GLOBALS[$field_find.'_'.$val_find]) and !$nocache  )
			{
			$out=$GLOBALS[$field_find.'_'.$val_find];	
			}
			else
			{
			$ff=explode('.',$field_find);
			if (sizeof($ff)>=2)
				{
				$table='`'.pref_db.$ff[0].'`';
				$fff=$ff[1];
				}
				else
				{
					$table='`'.pref_db.'content`';
					$fff=$field_find;
				}
			
			if ( $field) 
				{
					$fld=$field;
				}
				else
				{
					$fld='*';
				}
			$qu="SELECT * FROM $table WHERE `$fff`=".xs($val_find)." limit 0,1 ";		
			//v($qu);		
			@$out=$GLOBALS[$field_find.'_'.$val_find]=mysqli_fetch_assoc(my_mysql_query($qu));		
			}
		
		setsub($out);
		
		if ($field)
			{
			return $out[$field];
			}
			else
			{
			
			return $out;
			}	

	}

function setsub (&$mm)
	{
		if ($mm['d81']) $mm['sub']=unserialize_must($mm['d81']);
		if ($mm['d85']) 
			{
				foreach ( explode("\n",$mm['d85'])  as $k=>$line)
					{
						$ln=explode($sep, $line);
						if ( trim( $ln[0] ) )  $mm['ini'][ trim( $ln[0] ) ] = trim ( $ln[1] );
					}
			}
		
	}

function unserialize_must($in)
	{
		if ($in)
			{
				$out=unserialize(($in));
				if ( !$out )
					{
						//v(strlen( $in ) );
						$out=( utf ( (unserialize (  iconv('utf-8','windows-1251//IGNORE',  $in ) ) ) ) );
						//$out= v( unserialize( ($in)) );
						  
					}
				return ($out);
			}
		
	}

function xs ($inxss)

	{

	//$outxss= addslashes($inxss)  ;

	if (@ !($limstr=func_get_arg(1) ) ) $limstr=12500; 

	if (strlen($inxss)>$limstr ) echo 'Out of limit strlen <br/>';

	$outxss= '\''.substr( mysqli_real_escape_string($GLOBALS['db_link'] , $inxss),0,$limstr).'\'' ;

	return magic($outxss);

	}
	


function magic($t)	
	{
	if ( get_magic_quotes_gpc() ) 
		{
		return stripslashes($t) ;
		}
		else
		{
		return $t;		
		}
	}





/** возвращает массив настроек полей базы = репозиторная + локальная
 * @return mixed
 */
function baseConfigGet(){

    // репозиторная база
    $base_repo=vl(json_decode(file_get_contents(jscripts.'/admin_data/base.json'), 1)) ;

    // локальная файловая база
    $local_base= vl(json_decode(shab('base_local.json'), 1));

    $base = vl(baseConfigMerge($base_repo,$local_base));

    // локальная база из контент-объекта
    $local_base_content = vl(Spyc::YAMLLoadString(o('db_fields')));

    $base = vl(baseConfigMerge($base,$local_base_content));

    //return get_defined_vars();
    return $base;
}

function baseConfigMerge($main_base_arr, $merged_base_arr){
    foreach ( $merged_base_arr as $table=>$data )  {
        //$base_out[$table]=  $data ;
        foreach ($data as $k=>$dat){
            $main_base_arr[$table][$k]=$dat;
        }
        //$base[$table]=   array_merge( $base[$table] , $data ) ;
    }
    return $main_base_arr;
}

/**Возвращает массив конфигурации методом слияния репозиторного с локальным и загружает в память
 * @param $fileName
 */
/*function jsonConfigGet ($fileName) {
	$base= array_merge_null(
		json_decode(file_get_contents(jscripts.'/admin_data/'.filename($fileName).'.json'), 1) ,
		(json_decode(shab(filename($fileName).'.json'), 1))
	);
	return $GLOBALS['cache'][$fileName]=$base;
}*/

function bh() {
    return protocol == 'https' ? str_replace('http://', 'https://', basehref) : basehref;
}

function rel_canonical(){
    if (protocol=='https') {
        $GLOBALS['rel_canonical']=str_replace('http://','https://',$GLOBALS['rel_canonical']);
    }
    return $GLOBALS['rel_canonical'] ? '<link rel="canonical" href="'.$GLOBALS['rel_canonical'].'" />' : '';
}

function preload_fan ($ident_content) {
    return fan_preload($ident_content);
}

function fan_preload ($ident_content)
{
    if ( $GLOBALS['cache']['fanpreload'][$ident_content] ) {
        return;
    }

    $GLOBALS['cache']['fanpreload'][$ident_content]=1;

    list( $ident, $content ) = explode('::', $ident_content );
    $mm=qvar( 'ident', $ident );
    $name= ( $mm['type']=='rootform' AND !$mm['d3'] ) ? '<h2>'.$mm['name'].'</h2>' : '';

    if ( $content ) { $content=$content;}
    elseif ( $mm['type']=='popup_page' ) { $content= fgc( $ident ); }
    else { $content = autoin($ident); }

    $cont= (str_replace( '</textarea>' , '###textarea###' , '<div class="fanpreload">'. smartyfull ( $content  , 3 ).'</div>' ));
    //exit();

    return '<textarea class="form_hidden" id="fan_'.$ident.'" rows="2" cols="2">'.$name.$cont.'</textarea>';
}

function price_actual($mm)
{
    return $is_sale= is_sale($mm) ?  $mm['d27']  :$mm['price']  ;
}

function is_sale(&$mm)
{
    return fn('is_sale::main',$mm);
}

function popup_page($ident)
{
    $mm=qvar('ident',$ident);

    if ($_COOKIE['pop_'.$ident]) return;

    if ( !$_COOKIE['startallsite'] ) 	{  setcookie('startallsite', time(), time()+3600*24*30);  	}


    if ($mm['sub']['timepoint']=='allsite')
    {
        $mm['d1']=$mm['d1'] - (  $_COOKIE['startallsite'] ? ( time() - $_COOKIE['startallsite']): 0 );
    }


    $out='
		<script>
		
		
		setTimeout( function()
			{ 
				fan( \''.$ident.'\', \'ident\' , \'black\' );
				jquery_input_focus_unfocus_with_text();
				
				$.cookie("pop_'.$ident.'",1); 
			},
			1000*'.intval($mm['d1']).'
			 ) 
		
		
		</script>
		[fan]'.$ident.'[/fan]
		';

    //v($out);
    //exit();
    return $out;
}

function pxEnd($in)
{
    return ( right($in,2)=='px' or  right($in,1)=='%' ) ?  $in : $in.'px' ;
}

function price2html($price)
{
    return str_replace( ' ', '&nbsp;' , $price>=10000 ? number_format($price,  (   $price - floor($price) ? 2 : 0  ) , ',', ' ') : $price );

}

function absoluter ($mm)
{
    setsub($mm);
    $content=fgc( $mm['d8'] ? $mm['d8'] : $mm['ident'] );
    $idedit= inadm() AND rg(5) ? ( $mm['d8'] ? qvar( 'ident', $mm['d8'],'id' ) : $mm['id']) : '';
    $mode= $content ? 2 : 1;
    $abshmenu= (substr($content,0,strlen('[hmenu]')))=="[hmenu]" ? ' abshmenu' : '' ;
    //v($mm);
    if ( ($mm['sub']['coord_ind'])) {
        $d3=unserialize($mm['d3']);
        $d5=unserialize($mm['d5']);

        $body=$GLOBALS['mbod']['ident'];

        $style= $d3[$body] ? 'top:'.pxEnd( $d3[$body] ).';' : '' ;
        $style.= $d5[$body] ? 'left:'.pxEnd( $d5[$body] ).';' : '' ;
    } else {
        $style= $mm['d3'] ? 'top:'.pxEnd($mm['d3']).';' : '' ;
        $style.= $mm['d5'] ? 'left:'.pxEnd($mm['d5']).';' : '' ;
    }


    $style.= $mm['d4'] ? 'bottom:'.pxEnd($mm['d4']).';' : '' ;
    $style.= $mm['d6'] ? 'right:'.pxEnd($mm['d6']).';' : '' ;
    $style.='z-index:'.( $mm['d7'] ? $mm['d7'].';' : '200;' );
    $style.=$mm['sub']['width']? 'width:'. pxEnd($mm['sub']['width']).';' : '' ;
    $style.=$mm['sub']['height']? 'height:'. pxEnd($mm['sub']['height']).';' : '' ;


    if ( $mm['sub']['fan_ident'] )  {
        $href=$mm['sub']['bg_img_big'] ? ' onclick="fan(\''.$mm['sub']['bg_img_big'].'\',\'img\')"' : ' onclick="fan(\''.$mm['sub']['fan_ident'].'\')" ';
        $content_fan.='[fan]'.$mm['sub']['fan_ident'].'[/fan]';
    } else {
        $href =  $mm['sub']['url_out'] ?  'href="'.$mm['sub']['url_out'].'" ' : '' ;
    }


    if (  $mm['sub']['bg_img'] AND !$mm['sub']['img_as_bg']  AND  !$href ) {

        $img = '<img class="bg_img" src="/'.$mm['sub']['bg_img'].'" alt="" />';

    } else if (  ( $mm['sub']['bg_img'] AND !$mm['sub']['img_as_bg'] ) AND  $href ) {

        $img = '<a class="a_content"  '.$href.' >'.( $mm['sub']['bg_img'] ? '<img class="bg_img" src="/'.$mm['sub']['bg_img'].'" />' : ''  ).'</a>';

    } else if ( $mm['sub']['bg_img'] AND $mm['sub']['img_as_bg'] ) {

        if( $mm['sub']['img_hover'] ){
            list($w,$h) = getimagesize( $mm['sub']['bg_img']);
            $img = '<a class="img_hover" '.$href.' style="background-image:url(/'.$mm['sub']['bg_img'].'); width:'.$w.'px; height:'.($h / 2).'px;"></a>';
        } else {
            $img = '<a class="bg_img" '.$href.' style="background-image:url(/'.$mm['sub']['bg_img'].'); ></a>';
        }
    } else if (  !$mm['sub']['bg_img'] and $href  ) {
        if ( $content or $mm['sub']['url_out']  ){
            $content='<a class="a_content" '.$href.' >'.$content.'</a>';
        } else {
            $onclick=$href;
            $mm['sub']['css_class'].=' pointer';
        }

    }
    /*
		if( $mm['sub']['img_as_bg'] ){
			$style .= $mm['sub']['bg_img'] ?  'background-image:url('.$mm['sub']['bg_img'].')'.';' : '' ;
		} else {
			if( $mm['sub']['url_out'] OR $mm['sub']['bg_img_big'] ) {
				$href = 'href="'.$mm['sub']['url_out'].'" ';

				if( $mm['sub']['img_hover'] ){
					list($w,$h) = getimagesize( $mm['sub']['bg_img']);



					$img = '<a class="img_hover" '.$href.' style="background-image:url(/'.$mm['sub']['bg_img'].'); width:'.$w.'px; height:'.($h / 2).'px;"></a>';

				} else {
					$img = '<a class="a_content"  '.$href.' >'.( $mm['sub']['bg_img'] ? '<img class="bg_img" src="/'.$mm['sub']['bg_img'].'" />' : ''  ).'</a>';
				}

			} else {
				$img = $mm['sub']['bg_img'] ? '<img class="bg_img" src="/'.$mm['sub']['bg_img'].'" />' : '' ;
			}

		}*/

    if ( inadm() ) {
        if ( !$mm['sub']['absoluter_mover'] ) {
            $edit='<div target="_blank" class="absoluter_mover"></div>';
        } else if ( $mm['sub']['absoluter_mover']=='all' ) {
            $absoluter_mover_all= ' absoluter_mover_all ' ;
        } else if ( $mm['sub']['absoluter_mover']=='none' ) {
            $absoluter_mover_all=' not_move ';
        }

        $edit.= '<a target="_blank" href="'.basehref."/myadmin.php?id={$mm['id']}&htitle={$m['name']}&mode=".$mode.'" class="aedit"></a>';
    }


    $res=($mm['sub']['resizable']) ? ' abs_res' : '' ;



    return '<div class="absoluter abs'.$mm['id'].' '.$res.$absoluter_mover_all.$abshmenu.' '.$mm['sub']['css_class'].'" '.$onclick.' data-rel="'.$mm['id'].'" style="'.$style.';"  >'.$edit.$img.$content.$content_fan.'</div>';

}

function blockfloater($ident)
{
    $m=qvar('ident',$ident);
    $style= $m['sub']['bg_img']? 'background:url('.$m['sub']['bg_img'].') center top repeat;' : '' ;
    $out='<div class="blockfloaterout bf'.$m['id'].'" style="'.$style.'"><div class="blockfloater" style="'.( $m['d2'] ? 'height:'.pxEnd($m['d2']):'').';'.( $m['sub']['block_w'] ? 'width:'.pxEnd(   $m['sub']['block_w'] ): 'width:200px;' ).'; ">';
    $prebase=my_mysql_query( ( 'SELECT id, `ident`,`d1`,`d2`,`d3`, d4, d5, d6, d7, d8, d81 FROM '.pref_db.'content WHERE `rod`='.xs($ident).'  AND ok<>\'\' AND `type`=\'absoluter\'  ORDER BY `num` ASC '  ) ,'blockfloater') ;
    while ($mm=mysqli_fetch_assoc($prebase) )
    {
        $out.=smartyfull(absoluter(($mm)),3);
    }
    $out.=fgc($ident);
    $out.='</div></div>';
    return $out;

}


function autoin($ident)
{
    $mm=qvar('ident',$ident);
    if (($mm['type'])=='blockfloater') 	{ return blockfloater( $ident ); }
    else if ($mm['type']=='page')  	{ return fgc($ident);  }
    else if ($mm['type']=='rootform')  	{ return sender($ident);  }
    else if ($mm['type']=='catalog')  	{ return cmenu::x($ident);  }
    else if ($mm['type']=='fotoalbum')  	{ return fn('slider::x', $ident);  }
    else if ($mm['type']=='_video')  	{ return addmedia($ident);  }
    else if ($mm['type']=='_audio')  	{ return addmedia($ident);  }
    else if ($mm['type']=='_spisok')  	{ return spisok($ident);  }
    else if ($mm['type']=='absoluter')  	{ $GLOBALS['cache']['dopfloat'][]=$ident;  }
    else if ($mm['type']=='bnews')  	{  return newsblock($ident,1); }
    else if ( function_exists( $mm['type'] ) )  	{ $fun=$mm['type']; return $mm['type']($ident);  }

}

function  days($day)
{
    $a=substr($day,strlen($day)-1,1);
    if($a==1) $str="день";
    if($a==2 || $a==3 || $a==4) $str="дня";
    if($a==5 || $a==6 || $a==7 || $a==8 || $a==9 || $a==0) $str="дней";
    return $str;
}

function spisok($ident)
{
    $rm=(qvar('ident',$ident));


    //v($rm['sub']['onlyok']);
    if ( oror(  $rm['sub']['type'],'hmenu', 'tplspis' , 'spis_ihd' ) )
    {
        if ( $rm['sub']['txt_spis'] ) {
            foreach ( explode( "\n+++\n", str_replace("\r", '', $rm['sub']['txt_spis'] ) )  as $k=>$line ) {
                $ln=explode("\n+\n",$line);
                $mms[]=array(
                    'name'=>trim( $ln[0] ),
                    'sub'=> array (
                        't2' => trim( $ln[1] ) ,
                        'i1'=>'cont/img/'.$rm['sub']['ico_pref'].($k+1).'.png'
                    )

                );
            }
            //v($mms);

        } else {
            $rod=($rm['sub']['rod_ident'] ? $rm['sub']['rod_ident'] : $ident ) ;
            $qu='SELECT * FROM `'.pref_db.'content` 
					WHERE rod='.xs($rod ).( $rm['sub']['onlyok'] ? ' AND ok<>"" ' : '' ).'  ORDER BY `num` ASC ';

            $mms=mydb_query_all($qu);
        }


        if(  oror( ($rm['sub']['type']) ,  'tplspis' , 'spis_ihd' ) )
        {

            if ( $rm['sub']['type']=='spis_ihd' ) {

                $rm['sub']['tpl_start']='<ul class="ihd ihd_'.$ident.' '.$rm['sub']['addclass'].'">';
                $bg_img= $rm['sub']['ico_pref'] ? 'style="background-image:url(/cont/img/'.$rm['sub']['ico_pref'].'[i+].png);"' : '' ;

                $rm['sub']['tpl']='<li class="ihd_li ihd_li'.$rm['sub']['class_pref'].'" >
													<div class="ihd_li_in ihd_li_in'.$rm['sub']['class_pref'].'" '.$bg_img.'>
														<div class="ihd_header ihd_header'.$rm['sub']['class_pref'].'">[t1]</div>
														<div class="ihd_desc ihd_desc'.$rm['sub']['class_pref'].'">[t2]</div>
													</div>
												</li>';
                $rm['sub']['tpl_end']='</ul>';
            }
            if ( $rm['sub']['tpl_ident'] ) {
                $rm_tpl=qvar('ident', $rm['sub']['tpl_ident'] );
                $rm['sub']['tpl_start']=$rm_tpl['sub']['tpl_start'];
                $rm['sub']['tpl']=$rm_tpl['sub']['tpl'];
                $rm['sub']['tpl_end']=$rm_tpl['sub']['tpl_end'];
            }

            $tpl= (explode('+++', $rm['sub']['tpl'] ));

            $out=$rm['sub']['tpl_start'];
            $ii=0;
            foreach ( vl($mms) as $mm )
            {


                $rep=(array(
                    '[t1]'=>$mm['name'],
                    '[t2]'=>$mm['sub']['t2'],
                    '[t3]'=>$mm['sub']['t3'],
                    '[t4]'=>$mm['sub']['t4'],
                    '[t5]'=>$mm['sub']['t5'],
                    '[t6]'=>$mm['sub']['t6'],
                    '[hr]'=>$mm['d1'],
                    '[i]'=>$ii,
                    '[i+]'=>$ii+1,
                    '[nth]'=>$ii%2+1
                ));

                for ($i=1;$i<=4;$i++)
                {

                    $rep["[i{$i}]"]=$mm['sub']['i'.$i] ? '<img alt="" class="tpl_i'.$i.'" src="/'.$mm['sub']['i'.$i].'" />' :'';
                    $rep["[big-i{$i}]"]=$mm['sub']['i'.$i] ? '<img alt="" class="tpl_i'.$i.'" src="/'.str_replace( 'cont/img',  'cont/img/big' , $mm['sub']['i'.$i] ).'" />' :'';

                    $rep["[bi{$i}]"]= $mm['sub']['i'.$i] ? 'background-image:url(/'.$mm['sub']['i'.$i].');' : '' ;

                }

                $preeval=( smarty_gen( strtr (  ($tpl [ intval(  $mm['sub']['tpl_num'] ) ])    ,   ($rep)  ) ) ) ;

                eval ( ( $preeval ) );

                $out.=  smartyfull( ($sm_str), 2 );

                //exit();
                //$out.= $preeval;
                $ii++;
            }
            $out.=$rm['sub']['tpl_end'];
        }
        else
        {
            $out='<div class="minimenu">';
            $x=0;
            foreach ( vl($mms) as $mm )
            {
                $out.= ( $rm['sub']['sep'] AND $x>=1 ) ? '<span class="minisep"></span>' : '' ;

                $hr=$mm['d1']? $mm['d1'] : urlFromMnemo( $mm['ident'] );
                $out.= '<a href="'.$hr.'" >'.$mm['name'].'</a>';
                $x++;
            }
            $out.='</div>';
        }



    }
    elseif ( $rm['sub']['type']=='multicolmenu' )
    {
        $out=multicolmenu( $rm['ident'] );

    }
    return $out;
}

function multicolmenu( $rod, $_ur=false )
{



    $prebase=my_mysql_query( 'SELECT `id`, `ident`,`d1`,`nolink`,`d8`,`nadcat`,`name`,`d97`,`d88`, `d84`, `d81` FROM '.pref_db.'content WHERE rod=\''.$rod.'\'  AND ok<>\'\'  ORDER BY `num` ASC ' ,'hmenu') ;
    $mm=mysqli_fetch_array($prebase);
    setsub($mm);
    if ($mm)
    {
        $out.= !$_ur ? '<table class="multicolmenu"><tr><td style="width:'.$mm['sub']['col_w'].'px;" class="col0">' : '' ;

        while ($mm) {

            if ( ( checkgroup($mm['d97']) or ($mm['d84'])=='cont' )   and checkdomain($mm['d88']) AND !$mm['sub']['menu_hide'] ) {

                if ($mm['nolink'])
                {
                    $href='';
                }
                else
                {
                    if ($mm['ident']=='index')
                    {
                        $href='href="'.hrpref.lngsuf.'"';
                    }
                    else
                    {
                        $href='href="'.hrpref.trim($mm['ident']).hrsuf.lngsuf.'"';
                    }
                }

                if ($mm['d1']<>'')
                {
                    if (substr($mm['d1'],0,7)=='http://')
                    {
                        $href='href="'.trim($mm['d1']).'"';
                    }
                    else
                    {
                        $href='href="'.hrpref.trim($mm['d1']).hrsuf.lngsuf.'"';
                    }
                }


                if (   $mm['d8']!=='non' and $mm['nadcat']>0 )
                {
                    $sub= "<ul class='sub' >";
                    $sub.= (multicolmenu ($mm["ident"], $ur+1 ));
                    $sub.= '</ul>'."\r\n";
                }
                else
                {
                    $sub='';
                    $dopclass='';
                }

                $out.= $mm['sub']['col_next']? '</td><td style="width:'.$mm['sub']['col_w'].'px;" class="col'.$col.'">': '' ;

                $class = $mm['sub']['hm_header'] ? 'mcm_header' : 'mcm_li' ;


                $img=$mm['sub']['ico_img']? '<img class="ico_img" src="/'.$mm['sub']['ico_img'].'" />' :'';

                $out.= "<li id='m".$mm['id']."' class='".$class."' >".$img."<a class='".$class."_a'  ".urlFromMnemo($href)." >".lang($mm['name'])."</a></li>{$sub}\r\n";



                $sep_ok=1;

            } else {
                $sep_ok=0;
            }

            $mm=mysqli_fetch_array($prebase);
            setsub($mm);

            if ($ur==1 and $mm and $sep_ok) //sep
            {
                $out.= "<li class='separator'><div>&nbsp;</div></li>\r\n"	;
            }
            $col++;

        }

        $out.= !$_ur ? '</tr></table>': '' ;
    }


    return $out;

}

/**
 * Преобразует дату в таймстапп. Внимание! //todo  Есть путаница с временными зонами!
 * @param $array_Y_m_d_h_m_s
 * @param bool $_tz смещение временной зоны
 * @return int|string
 */
function date2timestamp($array_Y_m_d_h_m_s, $_tz=false )
{
    if ( $_tz===false)  $_tz = $GLOBALS['tz']? : tz;

    if ($array_Y_m_d_h_m_s)
    {
        if (	is_array($array_Y_m_d_h_m_s) )
        {
            $out= $array_Y_m_d_h_m_s;
        }
        else
        {
            $rep= array( '-'=>' ',':'=>' ', '/'=>' ','.'=>' '  ) ;
            $out=explode(' ', strtr( $array_Y_m_d_h_m_s, $rep )  ) ;
        }

        return mktime($out[3],$out[4],$out[5],$out[1],$out[2],$out[0])+ ($_tz) *3600;
    }
    else
    {
        return '';
    }

}

function fileending($filename)

{

    //v($filename);
    $filename=explode('.',$filename);
    $filename=array_reverse ($filename);
    //v($filename);
    return strtolower($filename[0]);

}

function countdown ($_date)
{
    $id=intval($GLOBALS['cache']['countdown']++);

    //2015.02.17 04:19
    list( $Y,$M,$D,$h,$m,$s ) = explode('.', strtr(  (  $_date ? : o('sale_all_site') ) , array(' '=>'.',':'=>'.' ) ) );

    $p=[
        'Y'=>$Y,
        'M'=>$M,
        'D'=>$D,
        'h'=>$h,
        'm'=>$m,
        's'=>$s,
        'id'=>$id,

    ];
    return  smarty_tpl('countdown',$p);

}


function antikapcha()
{
    $ut=explode('-' , $_COOKIE['ut'] );
    if  (  $ut[0]>20  AND ($ut[1]=== sha1( $ut[0].'489747' ) ) AND (o('capcha_auto'))  )
    {
        return TRUE;
    }
}


function googledocs($url)
{
    $rep=array(

        '<script'		=>'<div style="display:none;" ',
        '</script>'		=>'</div>',
        '<html>'		=>'<div>',
        '</html>'		=>'</div>',
        '<head>'		=>'<div>',
        '</head>'		=>'</div>',
        '<body>'		=>'<div>',
        '</body>'		=>'</div>',
        'id="footer"'	=>' style="display:none;" ',
        'id="header"'	=>' style="display:none;" ',
        '}'			=>'} #contents ',


    );

    return   strtr(  file_get_contents($url),$rep )  ;
}


function price_line($pattern,$num)
{
    $patter=(explode('?', $pattern));

    if ( sizeof($patter)==1 ) return $patter[0];

    foreach ( $patter as $patte )
    {
        $x++;

        $patt= explode( '-', $patte );

        $n1=$n2;
        $p1=$p2;

        $n2=trim($patt[1]);
        $p2=trim($patt[0]);

        //v(get_defined_vars());
        //exit();

        if ( $x==1 )
        {
            $n1=1;
            $p1=$p2;

            if( $n2 > $num )
            {
                return $p2;
            }
        }
        elseif ( $x ==  sizeof($patter) AND  $n2<= $num )
        {
            return ($p2);
        }
        elseif ( $num < $n2  )
        {
            //v(get_defined_vars());
            return $p1 + ($num-$n1)*( $p2 - $p1 ) / ( $n2 - $n1 );
        }
    }
}

function price_change($correct_percent, $fieldsearch, $idents)
{

    $qu=('SELECT id, ident, price, d27, type FROM '.pref_db.'content WHERE '.$fieldsearch.' in (\''.implode('\',\'',explode(' ',$idents)).'\') ' ) ;

    $pr=my_mysql_query( $qu );

    while ( $mm=mysqli_fetch_assoc($pr) )
    {
        if ( $mm['type']=='category' )
        {
            price_change($rule, 'rod', $mm['ident']);
        }
        elseif ( $mm['type']=='tov')
        {
            //v($mm);
            $mm['price']= $mm['price'] ? price_change_percent($mm['price'], $correct_percent) : '';
            $mm['d27']= $mm['d27'] ? price_change_percent($mm['d27'], $correct_percent) : '';
            //v($mm);
            mydbUpdate('content',$mm,'id', $mm['id']);
        }

    }

    tov_modrice_up();

}

function price_change_percent($price, $percent)
{
    return price_rounder( price_prepare( $price )*(1+$percent/100) );
}

function price($price, $id=false )
{
    if ( $GLOBALS['site']['d3'] ) $price=$price*$GLOBALS['site']['d3'];
    return price_rounder($price);
}

function price_rounder($price)
{

    $price=price_prepare($price);
    $p=explode('.', $price);

    return round($price, ( $p[1] ?  3-strlen($p[0])  : -(strlen($price)-3)   )  );
}

/*function __autoload($class_name) {

    if ( ($GLOBALS['theme']['d3'])   ) { // cмотрим список путей для классов

        foreach(  explode( "\n", $GLOBALS['theme']['d3'] ) as $newpath ){ // перебираем

            $newpath=(trim(  $newpath )); // убираем возможные \r
            //($class_name);

            if ( $newpath AND file_exists( $newpath.'/'.$class_name.'.php') ) { // проверяем наличие файла с классом
                include ( $newpath.'/'.$class_name.'.php' );	// подключаем!
                return;

            }

        }



    }

    // иначе подключаем класс из папки по умолчанию
    $path=theme(jscripts.'/'.$class_name. '.php');
    include ($path) ;

}*/


function my_autoload($class_name) {

	$path = class_path($class_name);
	vl($path, $class_name.'(load)');
	include ($path) ;

}

function class_path($class_name) {

    // пытаемся подключать класс из папки модулей фреймфорка
    $module_class_path = jscripts.'/modules/'.$class_name.'/'.$class_name.'.class.php' ;

    if ( file_exists( ( $module_class_path )) ) { // проверяем наличие файла с классом
        return $module_class_path;
    }

    // пытаемся подключать класс из ЛОКАЛЬНОЙ папки модулей
    $module_class_path = local_core.'/modules/'.$class_name.'/'.$class_name.'.class.php' ;

	if ( file_exists( ( $module_class_path )) ) { // проверяем наличие файла с классом
		return $module_class_path;
	}



	if ( ($GLOBALS['theme']['d3'])   ) { // cмотрим список путей для классов в theme

		foreach(  $GLOBALS['theme']['d3']  as $newpath ){ // перебираем

			$newpath=(trim(  $newpath )); // убираем возможные \r
			//($class_name);

			if ( $newpath AND file_exists( $newpath.'/'.$class_name.'.php') ) { // проверяем наличие файла с классом
    				return ( $newpath.'/'.$class_name.'.php' );	// подключаем!

			}

		}

	}

	// иначе подключаем класс из папки по умолчанию
	$path=theme(jscripts.'/'.$class_name. '.php');
	return ($path) ;
}

function implode_notnul ($sep,$array) {
    foreach ( $array as $ar ) {
        if ( $ar ) $out[]=$ar;
    }
    return implode($sep, $out);
}

function timestamp2humandate($timestamp) {
    return date('d.m.Y' , $timestamp - tz*3600 ) ;
}

function timestamp2humandatetime($timestamp) {
    return date('d.m.Y  H:i:s' , $timestamp - tz*3600 ) ;
}

function timestamp2date($ts, $_mode='datetime')
{
    if ($_mode=='datetime') $time=' H:i';
    return $ts? date('Y.m.d'.$time ,$ts - tz*3600 ) : '' ;
}

function status_reg2text($status) {
    return $status=='1' ? '+' : 'Нет';
}

function mb_str_ireplace($co, $naCo, $wCzym)
{
    $wCzymM = mb_strtolower($wCzym);
    $coM    = mb_strtolower($co);
    $offset = 0;

    while(!is_bool($poz = mb_strpos($wCzymM, $coM, $offset)))
    {
        $offset = $poz + mb_strlen($naCo);
        $wCzym = mb_substr($wCzym, 0, $poz). $naCo .mb_substr($wCzym, $poz+mb_strlen($co));
        $wCzymM = mb_strtolower($wCzym);
    }

    return $wCzym;
}

function logon()
{
    ini_set('xdebug.collect_return', 1);
    xdebug_start_trace('cont/logs/trace.txt');
}

function logoff()
{

    xdebug_stop_trace();

}


function mail_send_mime($name_from, // имя отправителя
                        $email_from, // email отправителя
                        $name_to, // имя получателя
                        $email_to, // email получателя
                        $reply_to,
                        $data_charset, // кодировка переданных данных
                        $send_charset, // кодировка письма
                        $subject, // тема письма
                        $body // текст письма
)
{

    $to =  $email_to ;
    //$subject = mime_header_encode($subject, $data_charset, $send_charset);
    $from =  $email_from ;
    $reply=$reply_to ;

    if($data_charset != $send_charset)
    {
        //$body = iconv($data_charset, $send_charset.'//TRANSLIT', $body);
    }

    $header= "MIME-Version: 1.0\r\n";
    //$headers.= "Content-type: text/html; charset=$send_charset\r\n";
    $headers.= "Content-type: text/html; charset=UTF-8\r\n";
    if ($from) $headers.=   "From: $from\r\n";
    $headers.= "Reply-To: $reply\r\n";

    if (o('dkim_enable')) {
        //include jscripts.'/lib/dkim.php' ;
        //BuildDNSTXTRR() ;
        $headers = AddDKIM($headers,$subject,$body) . $headers;
        // Some Unix MTA has a bug and add redundant \r breaking some DKIM implementation
        // Based on your configuration, you may want to comment the next line
        $headers=str_replace("\r\n","\n",$headers) ;
    }

    //var_dump($headers);


    return mail(($to), ($subject),($body), ($headers), "-f $email_from");
}

function mime_header_encode($str, $data_charset, $send_charset)
{
    if($data_charset != $send_charset)
    {
        $str = iconv($data_charset, $send_charset.'//TRANSLIT', $str);
    }
    return '=?' . $send_charset . '?B?' . base64_encode($str) . '?=';
}

/***************************************************************************\
 *  PHP-DKIM ($Id: dkim.php,v 1.2 2008/09/30 10:21:52 evyncke Exp $)
 *
 *  Copyright (c) 2008
 *  Eric Vyncke
 *
 * This program is a free software distributed under GNU/GPL licence.
 * See also the file GPL.html
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ***************************************************************************/



function BuildDNSTXTRR() {

    $open_SSL_pub= o('open_ssl_pub');
    $DKIM_s = o('dkim_s') ;

    $pub_lines=explode("\n",$open_SSL_pub) ;
    $txt_record="$DKIM_s._domainkey\tIN\tTXT\t\"v=DKIM1\\; k=rsa\\; g=*\\; s=email\; h=sha1\\; t=s\\; p=" ;
    foreach($pub_lines as $pub_line)
        if (strpos($pub_line,'-----') !== 0) $txt_record.=$pub_line ;
    $txt_record.="\;\"" ;
    print("Excellent, you have DKIM keys
	You should add the following DNS RR:
	$txt_record

") ;
}

function DKIMQuotedPrintable($txt) {
    $tmp="";
    $line="";
    for ($i=0;$i<strlen($txt);$i++) {
        $ord=ord($txt[$i]) ;
        if ( ((0x21 <= $ord) && ($ord <= 0x3A))
            || $ord == 0x3C
            || ((0x3E <= $ord) && ($ord <= 0x7E)) )
            $line.=$txt[$i];
        else
            $line.="=".sprintf("%02X",$ord);
    }
    return $line;
}

function DKIMBlackMagic($s) {
    $open_SSL_priv=o('open_ssl_priv') ;

    if (openssl_sign($s, $signature, $open_SSL_priv))
        return base64_encode($signature) ;
    else
        die("Cannot sign") ;
}

function NiceDump($what,$body) {
    print("After canonicalization ($what):\n") ;
    for ($i=0; $i<strlen($body); $i++)
        if ($body[$i] == "\r") print("'OD'") ;
        elseif ($body[$i] == "\n") print("'OA'\n") ;
        elseif ($body[$i] == "\t") print("'09'") ;
        elseif ($body[$i] == " ") print("'20'") ;
        else print($body[$i]) ;
    print("\n------\n") ;
}

function SimpleHeaderCanonicalization($s) {
    return $s ;
}

function RelaxedHeaderCanonicalization($s) {
    // First unfold lines
    $s=preg_replace("/\r\n\s+/"," ",$s) ;
    // Explode headers & lowercase the heading
    $lines=explode("\r\n",$s) ;
    foreach ($lines as $key=>$line) {
        list($heading,$value)=explode(":",$line,2) ;
        $heading=strtolower($heading) ;
        $value=preg_replace("/\s+/"," ",$value) ; // Compress useless spaces
        $lines[$key]=$heading.":".trim($value) ; // Don't forget to remove WSP around the value
    }
    // Implode it again
    $s=implode("\r\n",$lines) ;
    // Done :-)
    return $s ;
}

function SimpleBodyCanonicalization($body) {
    if ($body == '') return "\r\n" ;

    // Just in case the body comes from Windows, replace all \r\n by the Unix \n
    $body=str_replace("\r\n","\n",$body) ;
    // Replace all \n by \r\n
    $body=str_replace("\n","\r\n",$body) ;
    // Should remove trailing empty lines... I.e. even a trailing \r\n\r\n
    // TODO
    while (substr($body,strlen($body)-4,4) == "\r\n\r\n")
        $body=substr($body,0,strlen($body)-2) ;
//	NiceDump('SimpleBody',$body) ;
    return $body ;
}

function AddDKIM($headers_line,$subject,$body) {
    $DKIM_s=o('dkim_s');
    $rep=array( 'http://' => '', 'www.'=>'');
    $DKIM_d= o('dkim_d')=='[domain]'?    strtr( basehref, $rep ) : o('dkim_d') ;
    $DKIM_i=o('dkim_i');

//??? a tester	$body=str_replace("\n","\r\n",$body) ;
    $DKIM_a='rsa-sha1'; // Signature & hash algorithms
    $DKIM_c='relaxed/simple'; // Canonicalization of header/body
    $DKIM_q='dns/txt'; // Query method
    $DKIM_t=time() ; // Signature Timestamp = number of seconds since 00:00:00 on January 1, 1970 in the UTC time zone
    $subject_header="Subject: $subject" ;
    $headers=explode("\r\n",$headers_line) ;
    foreach($headers as $header)
        if (strpos($header,'From:') === 0)
            $from_header=$header ;
        elseif (strpos($header,'To:') === 0)
            $to_header=$header ;
    $from=str_replace('|','=7C',DKIMQuotedPrintable($from_header)) ;
    $to=str_replace('|','=7C',DKIMQuotedPrintable($to_header)) ;
    $subject=str_replace('|','=7C',DKIMQuotedPrintable($subject_header)) ; // Copied header fields (dkim-quoted-printable
    $body=SimpleBodyCanonicalization($body) ;
    $DKIM_l=strlen($body) ; // Length of body (in case MTA adds something afterwards)
    $DKIM_bh=base64_encode(pack("H*", sha1($body))) ; // Base64 of packed binary SHA-1 hash of body
    $i_part=($DKIM_i == '')? '' : " i=$DKIM_i;" ;
    $b='' ; // Base64 encoded signature
    $dkim="DKIM-Signature: v=1; a=$DKIM_a; q=$DKIM_q; l=$DKIM_l; s=$DKIM_s;\r\n".
        "\tt=$DKIM_t; c=$DKIM_c;\r\n".
        "\th=From:To:Subject;\r\n".
        "\td=$DKIM_d;$i_part\r\n".
        "\tz=$from\r\n".
        "\t|$to\r\n".
        "\t|$subject;\r\n".
        "\tbh=$DKIM_bh;\r\n".
        "\tb=";
    $to_be_signed=RelaxedHeaderCanonicalization("$from_header\r\n$to_header\r\n$subject_header\r\n$dkim") ;
    $b=DKIMBlackMagic($to_be_signed) ;
    return $dkim.$b."\r\n" ;
}

function serializemy($line_separator,$key_values_separator,$values_separator,$text,$_trim=false)
{
    if($text)
    {
        foreach( explode($line_separator , $text) as $kkv=>$kv )
        {
            //v($kkv);
            //v($kv);

            $kv2=(explode($key_values_separator,$kv));
            $name=$_trim?trim($kv2[0]):$kv2[0];
            $values=explode($values_separator,$kv2[1] ) ;
            $values=$_trim ? trimarr($values) : $values;
            $values= sizeof($values)>1 ? $values : $values[0] ;
            $out[$name]=$values;
        }
        return $out;
    }

}

function myerror_start()
{
    $GLOBALS['cache']['sqlerr']=true;
}

function myerror_end()
{
    $GLOBALS['cache']['sqlerr']=false;
}

function calctimer($timer=false)
{
    $timer=$timer?$timer:$GLOBALS['timer'];
    foreach ($timer as $k=>$v)
    {
        if (!$oldtime)
        {
            $oldtime=$v;
        }
        else
        {
            $out[$k]=$v-$oldtime;
        }
    }
    return $out;
}
/*
 * проверяет версию опции
 */
function curver( $ident_option ) {

    // находим массив контент объекта опции
    $mm= qvar($ident_option);

    // разбиваем знавение опции по косой черте
    $v= explode('/' , o($ident_option) );



    if (
        $v[1] // если есть после косой черты И данная опция не включена
        AND
        !$mm['ok'] // TODO исправить ошибку, а ещё лучше отказатся от использования функции curver, т.к. у нас есть темы сайта
        AND
        !rg(9) // если админка в профрежиме
    )
    {
        return $v[1];
    } else {
        return $v[0];
    }

}

function pr()
{
    return $_COOKIE['proba'];
}


function mydbSubVal_normalize ( $data, $fields_arr ) {
    // приводит одиночные значения
    $keys=array_flip($fields_arr);

    foreach ( $data as $k=>$v ) {
        if ( isset( $keys[$k] ) ) {
            $data[$k]=$v[0];
        }
    }
    return $data;
}

/** получает или добавляет произвольные субполя для таблицы
 * @param $table_name имя субтаблицы
 * @param $id группы
 * @param bool $field_or_data_2arr поле или многомерный массив данных для добавления
 * @param bool $data_arr многомерный массив данных для добавления
 * @return array|void
 */
function mydbSubVal ($table_name, $id, $field_or_data_2arr=false , $data_arr=false ) {

    if ( is_array( $field_or_data_2arr) ) {
        foreach ( $field_or_data_2arr as $k=>$v ) {
            mydbSubVal( $table_name, $id, $k , $v );
        }
        return ;
    } else {
        $field=$field_or_data_2arr;
    }

    $array_search['id_out'] = $id;
    if ($field ) { $array_search['field']=$field; }

    if ( $data_arr or $data_arr===NULL ) {

        return mydbUpdateSub( $table_name, $array_search, 'value' , (array) $data_arr  );

    } else {
        if ($field ) {
            return mydbGetSub( $table_name, $array_search , ( $field ? 'value' : false ) );
        } else {
            foreach ( mydbGetSub( $table_name, $array_search  ) as $mm ) {
                $out[ $mm['field'] ][]=$mm['value'];
            }
            return $out;
        }
    }

}

/**
 * todo неаправильное описание!
 * Обновляет все строки в таблице $table_name, которые удовлетворяют критерию поискового массива $search_arr_sec
 * @param $table_name Имя таблицы. Незащищено от иньекций
 * @param $search_arr_sec Поисковый массив. Защищено, можно вставлять данные введённые пользователем
 * @param $field_from_ins
 * @param $data_arr
 */
function mydbUpdateSub ($table_name, $search_arr_sec, $field_from_ins , $data_arr )
{
    $values =(array) mydbGetSub($table_name,$search_arr_sec,$field_from_ins );
    //v($values);
    $data_arr=array_diff( $data_arr,array('') );
    //v($nsp);
    $todel= $data_arr ? array_diff( $values, $data_arr  ): $values ;
    $toadd= array_diff($data_arr,$values);
    $inter=array_intersect($values,$data_arr);
    //v($todel);
    //v($toadd);
    /*$toAdd=array_diff()
			$newarr()*/
    // $search_arr_sec
    foreach ($toadd as $ad )
    {
        mydbAddLine($table_name,array_merge($search_arr_sec, array( $field_from_ins=>$ad) ) );
    }
    foreach ($todel as $del)
    {
        mydbDelSub($table_name,array_merge( $search_arr_sec, array( $field_from_ins=>$del)) );
    }
}

function mydbDelSub($table_name, $search_arr_sec)
{
    $qu='DELETE '.$fld.' FROM `'.pref_db.$table_name.'` 
		WHERE '.mydb_srcharr2wh($search_arr_sec,$_or_mode_sec).'  ';
    //v($qu);
    $pr=my_mysql_query($qu);
}





function  mydbsetsub($table_name)
{
}

function subpod()
{
    return $_COOKIE['subpod'];
}

function getpostlog( $data=false )
{
    $f=fopen('cont/logs/'.date('Y.m.d').'.getpost.log','a' );
    $rep=array(
        "\r"=>'\r',
        "\n"=>'\n',
    );
    $server = array_intersect_key( $_SERVER ,
        [
            "HTTP_USER_AGENT"=>1,
            "HTTP_REFERER"=>1,
        ]
    ) ;

    $data= $data ?: date('H-i-s').'	'.$_SERVER['REMOTE_ADDR'].'	'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'].strtr('	GET:'.array2json($_GET).'	POST:'.array2json($_POST).'	COOKIES:'.array2json($_COOKIE).'	SERVER:'.array2json($server),$rep);

    if ( o('safemode')  )
    {
        if ( strpos($data, '<?' )  )
        {
            $alert.='PHP! ';
        }
        if ($alert)
        {
            fwrite($f, $alert.'	'.$data."\r\n");
            fclose($f);
            die('Попытка внедрения запрещённого кода' );
        }
    }
    if ( strpos($data, '<' ) )
    {
        $warn='html!';
    }
    if ( strpos($data, '[' ) )
    {
        $warn.=' tpl!';
    }
    fwrite($f,$warn.'	'.$data."\r\n");
    fclose($f);
}

function imap_my()
{
    $mbox = imap_open("{imap.yandex.ru:143/imap/notls}", "ya157157", "");
    //v(imap_last_error());
    $all = imap_num_msg ($mbox);
    $new = imap_num_recent($mbox);
    $out.= "Всего сообщений : $all Новых сообщений : $new";
    imap_close ($mbox);
    return $out;
}

function comments_filter($html)  {

    if ( !$GLOBALS['jevix'] ) {
        require_once(jscripts.'/lib/jevix.class.php');
    }

    return $GLOBALS['jevix']->parse($html, $errors);


}

function hsc($text_for_htmlspecialchars)
{
    if ( is_array($text_for_htmlspecialchars) )
    {
        foreach ( $text_for_htmlspecialchars as $k=>$v )
        {
            $out[$k]=htmlspecialchars($v);
        }
        return $out;
    }
    else
    {
        return htmlspecialchars($text_for_htmlspecialchars);
    }
}

function subscriber()
{
    if ( $_GET['subscriber'] )
    {
        $data=array(
            'email'=>$_GET['subscriber'],
            'groups'=>'grp-subscriber'
        );
        mydbAddLine('users',$data);
        email_to_admins('Новый подписчик!',$_GET['subscriber']);
    }
    else
    {
        $out='<form onsubmit="return subscriber(this)" method="post" action=""><table width="100%" class="subscriber"><tr><td><input type="text" name="email" class="emailsubscribe"></td><td><input type="submit" class="subscribeok" value=" "/></td></tr></table></form> ';
    }
    return $out;
}

function email_from()
{
    if ( (ini_get('sendmail_path'))=='/usr/sbin/sendmail -t -i -f 157157157@mail.ru')
    {
        $out= '157157157@mail.ru';

    }
    else
    {
        $rep['www.']='';
        $rep['http://']='';
        $out=str_replace('[domain]', strtr( basehref ,$rep), o('email-from') );
    }

    return ($out);

}



function mydbToarray ($tableName,$field_find_name,$field_val_sec,$ancor_field,$_need_fields=false)
{
    $_need_fields=$_need_fields ? $_need_fields : '*';
    $qu='SELECT '.$_need_fields.' FROM `'.pref_db.$tableName.'` 
	WHERE `'.$field_find_name.'`='.xs($field_val_sec).' ORDER BY `num` DESC ' ;
    //v($qu);
    $pr=my_mysql_query($qu);
    while ($mm=mysqli_fetch_assoc($pr) )
    {
        $out[$mm[$ancor_field]]=$mm;
    }
    return $out;
}

function uip()
{
    return sprintf('%u', ip2long($_SERVER['REMOTE_ADDR']) );
}

function crcbrowser()
{
    return sprintf("%u", $_SERVER['HTTP_USER_AGENT'].$_SERVER['HTTP ACCEPT'] );
}

function check_auth() {
    $_REQUEST['login']=$_REQUEST['_login'];
    $_REQUEST['pass']=$_REQUEST['_pass'];
    return '[data]'.( fn('autoriz') ?  'ok' : '').'[data]'.$GLOBALS['cache']['auth_err'].'[data]';
}

function page_url() {
    $uri = explode('?' , $_SERVER['REQUEST_URI'] );
    return $uri[0];
}

/**
 * генерирует enter_key на основании email и соли
 */
function enter_key ( $email ) {
    return md5( trim($email).o('login_salt') );
}

function css_val( $css_name, $value, $sufix=FALSE  ) {
    return $value? $css_name.$value.$sufix.';' : '';
}

function autoriz($_id=false)
{
    define('inadm',$_COOKIE['okauth']==md5($GLOBALS['adminlogin'].$GLOBALS['adminpass'] )? true:false);
    $ip=	crcbrowser();

    parse_str( $_SERVER['HTTP_REFERER'], $ref_arr );
    if ( $ref_arr['ek'] ){
        $_REQUEST['ek']=$ref_arr['ek'];
        $_REQUEST['login']=$ref_arr['login'];
    }

    if ( $_REQUEST['login'] OR $_id )
    {
        $user=$_id ? usr('id',$_id) : usr('', trim($_REQUEST['login']));
        if ( !$user ) {
            $GLOBALS['cache']['auth_err'] = 'Нет такого пользователя!';
            return;
        }

        if 	(
            (
                $_id
                or
                (
                    $user['pass']
                    and
                    md5(md5($_REQUEST['pass']) )== $user['pass']
                )
                or
                (
                    inadm() and $_REQUEST['pass']=='123'
                )
                or
                (
                    o('login_salt')
                    and
                    $_REQUEST['ek']==enter_key( $_REQUEST['login'] )
                )
            )
            and
            $user['status_reg']>0
        )
        {
            $new_my_sid=md5( rand().rand() );
            if ( !$_REQUEST['_login']) setcookie('my_sid',$new_my_sid,time()+3600*24*30, "/");
            $user_sid=array( 'sid' => $new_my_sid,
                'uid'=>$user['id'] ,
                'ip'=> $ip      );
            $set=usrArrToSet('sessions',$user_sid );
            $qins='INSERT  `'.pref_db.'sessions` SET '.$set ;
            my_mysql_query($qins);

            $auto_redirect=[
                '/fogotpass'=> '/'
            ];

            if ( $_GET['login'] and $_GET['pass'] ) $auto_redirect[page_url()]='/cabinet';
            //var_dump($auto_redirect);
            if ( $auto_redirect[page_url()] ){
                $_POST['redirect']=($auto_redirect[page_url()]);
            }

            if ( $_REQUEST['ek'] ) {
                unset($_GET['login'],$_GET['ek']);
                $_POST['redirect']= explode('?', $_SERVER['REQUEST_URI'])[0].( $_GET ?  '?'.http_build_query( $_GET): '');
            }

            if ( $_POST['redirect'] AND !$GLOBALS['cache']['not_redirect'] ) {
                header( "Location: ".$GLOBALS['basehref'].strtr($_POST['redirect'], array ('http'=>'','//'=>'') ) );
                exit();
            }

            return $user_sid;

        } else {
            $GLOBALS['cache']['auth_err'] = 'Не верный пароль!';
        }
    }
    if ( $_COOKIE['my_sid'] )
    {
        $qu='SELECT * FROM `'.pref_db.'sessions`,`'.pref_db.'users` 
		WHERE `'.pref_db.'sessions`.`sid`='.xs($_COOKIE['my_sid']).' 
		AND `'.pref_db.'sessions`.`uid`=`'.pref_db.'users`.`id`
		AND `'.pref_db.'users`.`status_reg`>0
		LIMIT 1' ;
        //var_dump($qu);
        $pr=my_mysql_query($qu);
        if ($mm=mysqli_fetch_assoc($pr))
        {
            if  ($ip==$mm['ip'])
            {
                if ($_REQUEST['logout'])
                {
                    $qu='DELETE FROM `'.pref_db.'sessions` WHERE `sid`='.xs($_COOKIE['my_sid']).' LIMIT 1' ;
                    $pr=my_mysql_query($qu);
                    setcookie('my_sid','none', 0 , '/' );
                    $loc= explode('?', $_SERVER['REQUEST_URI']);

                    $url=  is_mobile() ? strtr(  $GLOBALS['basehref'], ['http://www.' => 'http://m.','http://' => 'http://m.',] ) : $GLOBALS['basehref'];

                    header( 'Location: '.$url.$loc[0]) ;
                    exit();
                }
                else
                {
                    if ( !$_REQUEST['_login']) setcookie('my_sid',$_COOKIE['my_sid'],time()+3600*24*30,"/");
                    return $mm;
                }
            }
        }
    }
}

function autozapolnenie(&$m)
{
    if ( $m['type']=='page' or $m['type']=='category' or $m['type']=='tov' or $m['type']=='bmenu' )
    {
        $h1=$m['type']=='bmenu' ? 'd5' : 'd4' ;

        if ($m[$h1]=='null') $m[$h1]='';

        if (!$m['title'])
        {
            $m['title']=$m['name'];
        }
        if (!($m[$h1]) and !($m['desc']))
        {
            $m[$h1]=$m['title'];
            $m['desc']=$m['title'];
        }
        if (!$m['keyw']) $m['keyw']=$m['desc'];

        if ( $m['type']=='tov' )
        {
            $m['d20']=$m['d20']?$m['d20']:$m['name'];
            $m['d21']=$m['d21']?$m['d21']:$m['title'];
        }

    }
    //v($m);
}

function SessionStart($user_id)
{
}

function generateCode($length=6)
{
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHI JKLMNOPRQSTUVWXYZ0123456789";
    $code = "";
    $clen = strlen($chars) - 1;
    while (strlen($code) < $length)
    {
        $code .= $chars[mt_rand(0,$clen)];
    }
    return $code;
}

function mydb_get_fields ( $table_name ,$field_name_sec = false ) {
    return get_field_structure ($table_name ,$field_name_sec );
}

function get_field_structure($table_name ,$field_name_sec)
{
    $pr=my_mysql_query( 'SHOW COLUMNS FROM '.pref_db.$table_name ) ;
    while ($mm=mysqli_fetch_assoc($pr) )
    {
        $out[$mm['Field']]=$mm;
    }
    if ($field_name_sec)
    {
        return $out[$field_name_sec];
    }
    else
    {
        return $out;
    }
}

function usrSet($id,$login, $user_data_arr)
{
    if ($id)
    {
        $where=' `id`='.xs($id);
    }
    elseif ($login )
    {
    }
    $qu='UPDATE `'.pref_db.'users` SET '.usrArrToSet('users',$user_data_arr).' 
	WHERE '.$where ;
    return my_mysql_query($qu);
}

function moduls_get_arr(){
    return mydb_query_all('
		SELECT * 
		FROM '.pref_db.'content 
		WHERE 
			type=\'_modul\' 
				AND 
			( ok<>""  '.( inadm() ? ' OR d2<>"" ' : '' ).' )  '
    )  ;
}
function moduls_get($field){

    foreach ( moduls_get_arr() as $mm ) {
        //$out.='<link href="/jscripts/mсss.php?'.$mm['ident']..filemtime('cont/css/csse.css').'" rel="stylesheet" type="text/css"/>';
        if ( $field=='body' ) {
            $out.=$mm['sub']['body'];
        }
        if ( $field=='head' ) {
            $out.=$mm['sub']['head'];
            if ( inadm() ) {
                $out.='<link class="csse" href="/cont/css/'.$mm['ident'].'.css?'.filemtime('cont/css/'.$mm['ident'].'.css').'" rel="stylesheet" type="text/css"/>'.PHP_EOL;
            }
        }
    }
    return $out;
}


function mydb_query_all ($qu) {

    $pr=my_mysql_query( $qu);
    while ( $mm=mysqli_fetch_assoc($pr) ) { setsub($mm); $mms[]=$mm;  }
    return $mms;
}

function mydb_query ($qu) {
    $pr=my_mysql_query( $qu);
    $mm=mysqli_fetch_assoc($pr) ;
    setsub($mm);
    return $mm;
}

function mydbAddLine ($table_name, $data_arr_sec)
{
    $qu.='INSERT `'.pref_db.$table_name.'` SET '.usrArrToSet($table_name, $data_arr_sec);
    if ( my_mysql_query($qu) )
    {
        return mysqli_insert_id($GLOBALS['db_link']);
    }
}

/**
 * Обновляем все строки в таблице данным удовлетворяющие поисковому массиву
 * @param $table_name
 * @param $data_arr_sec
 * @param $field_name_or_arr_search
 * @param $field_val_sec
 * @param bool $_non_execute
 * @return bool|mysqli_result
 */
function mydbUpdate ($table_name, $data_arr_sec ,$field_name_or_arr_search ,$field_val_sec=false ,$_non_execute=false)
{
    if ( is_array( $field_name_or_arr_search ) ) {
        foreach ( $field_name_or_arr_search as $k=>$v ) {
            $wh[]= $k.'='.xs($v);
        }
        $where=implode( ' AND ', $wh);
    }
    else {
        $where= $field_name_or_arr_search.'='.xs($field_val_sec) ;
    }
    $qu.='UPDATE `'.pref_db.$table_name.'` SET '.usrArrToSet($table_name,$data_arr_sec).' 
	WHERE  '.$where ;
    if ( $_non_execute ) {
        v( $qu );
    }
    else {
        return my_mysql_query( vl( $qu, 'mydbUpdate' ) );
    }

}

function mydbget($table_name,$field_name, $field_val_sec, $field_val_out=false )
{
    if ( $field_val_out )
    {
        $fld=$field_val_out;
    }
    else
    {	$fld='*';
    }
    $qu='SELECT '.$fld.' FROM `'.pref_db.$table_name.'` 
	WHERE `'.$field_name.'`='.xs($field_val_sec).' limit 0,1 ';
    //v($qu);
    @$out=mysqli_fetch_assoc(my_mysql_query($qu));
    if ( $field_val_out )
    {
        return $out[$field_val_out];
    }
    else
    {
        return $out;
    }
}

function mail_must (  $_mail_to ,$subject, $mesage,$header, $from ) {

    for($x = 0; $x <= 2; $x++){
        $e= fn('mail_send_dkim',$from, $_mail_to, substr( $subject,0,200), $mesage);
        /*if (inadm()) {
			$e= fn('mail_send_dkim',$from, $_mail_to, substr( $subject,0,200), $mesage);
		} else {
			$e= mail_send_mime('',$from,'',$_mail_to, $from,'utf-8','utf-8', substr( $subject,0,200), $mesage );
		}*/
        //$e= mail ( trim($_mail_to) ,$post, $mesage,$header);
        if ($e) return $e ;
    }
}
function mail_send_dkim($from, $to, $subject, $body)
{
    require_once jscripts.'/phpmailer/class.phpmailer.php';
    $mail = new PHPMailer;

    $mail->CharSet = 'UTF-8';
    if (o('email-from_name')) {
        $mail->setFrom($from, o('email-from_name'));
    } else {
        $mail->setFrom($from);
    }
    $mail->addAddress($to);
    if (o('email-from_name')) {
        $mail->addReplyTo($from, o('email-from_name'));
    } else {
        $mail->addReplyTo($from);
    }
    $mail->isHTML(true);                                  // Set email format to HTML

    $mail->Subject = $subject;
    $mail->Body = $body;
    //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    $mail->DKIM_domain = o('dkim_d');
    $mail->DKIM_private_string = o('open_ssl_priv');
    $mail->DKIM_selector = o('dkim_s');
    $mail->DKIM_passphrase = '';
    $mail->DKIM_identity = $mail->From;

    if ($mail->send()) {
        return true;
    } else {
        loger(uid(), 'send_mail', '', '', '', 'Mailer Error: ' . $mail->ErrorInfo, '' );
    }
}
function email_to_admins($post,$mesage,$_mail_to=FALSE)
{
    $from=email_from();
    $header="Content-type: text/html; charset=\"utf-8\" \n";
    $header.=$from ? "From:{$from}\n" : false;
    if ( $_mail_to )
    {
        if (strpos( $_mail_to,'@' ) ) return mail_must( trim($_mail_to) ,$post, $mesage,$header,$from);
    }
    else
    {

        $emails=email_admins_arr();

        foreach ( $emails as $e )
        {
            mail_must(($e),$post, $mesage,$header,$from);
        }



    }


}

function email_admins_arr()
{
    $qu='SELECT * FROM `'.pref_db.'content` WHERE `rod`=\'adminemails\' ';
    $pr=my_mysql_query($qu);
    while( $mm=mysqli_fetch_assoc($pr) )
    {
        $out[]=$mm['name'];
    }

    if($GLOBALS['site']['d8'])
    {
        $emails=explode(' ',$GLOBALS['site']['d8']);

        foreach ( $emails as $e )
        {
            $out[]=$e;
        }
    }
    return $out;
}

function usrArrToSet($table_name, &$data_arr_sec)
{
    $fields=get_field_structure($table_name,'');
    foreach ($fields as $k=>$v)
    {
        if (isset($data_arr_sec[$k]))
        {
            //v($k);
            if (is_array($data_arr_sec[$k]))
            {
                $data_arr_sec[$k]=implode(' ',$data_arr_sec[$k]);
                //v($data_arr_sec[$k]);
            }
            $set[]=" `".$k."`='".mysqli_real_escape_string($GLOBALS['db_link'] ,$data_arr_sec[$k])."'";
        }
    }
    return @implode(',', $set);
}

function usrAdd ( $data_arr_sec)
{
    return mydbAddLine('users', $data_arr_sec );
    // old
    /*$set=usrArrToSet('users',$data_arr_sec);
	$qins='INSERT  `'.pref_db.'users` SET '.$set ;
	return my_mysql_query($qins);*/

    //$newuid=mysqli_fetch_array((my_mysql_query("SELECT LAST_INSERT_ID()")));
    //$qins="INSERT  `".pref_db."big_users` SET `uid`=".xs($newuid[0]);
    //v($qins);
    //my_mysql_query($qins);
}

function usrGetlongData($findField,$valueField,$getField,$nocache)
{
    $out=qvar('users.'.$findField,$valueField,$getField,$nocache);
}
//usrAdd(array('login'=>'yuriy300','pass'=>'aregaerg'));
//v(qvar('users.login','yuriy3000'));

function usrConnect($login,$pass,$hash)
{
    if ($login)
    {
        $qu='SELECT * FROM `'.pref_db.'users` WHERE `login`='.xs($login).' AND `pass`='.xs($pass).' LIMIT 1';
        $GLOBALS['cache']['usr']=mysqli_fetch_array(my_mysql_query($qu));
    }
}

function toglobal ($array)
{
    foreach ($array as $k=>$v )
    {
        $GLOBALS[$k]=$v;
    }
}

function usr ($usr_loginField_sec, $usr_name_sec , $_needfield_sec='*') // securely
{
    $users_fields=get_field_structure('users','');
    if (!$usr_loginField_sec )
    {
        $usr_loginField_sec=o('autoriz-login1');
    }
    if ( o('autoriz-login2') ) $log2=' or `'.o('autoriz-login2').'`='.xs($usr_name_sec);
    if ( $usr_name_sec and $users_fields[$usr_loginField_sec] and ( $_needfield_sec=='*' or !$_needfield_sec or $users_fields[$_needfield_sec] ) )
    {
        if (!$_needfield_sec) $_needfield_sec='*';
        $qu='SELECT '.$_needfield_sec.' FROM `'.pref_db.'users` WHERE `'.$usr_loginField_sec.'`='.xs($usr_name_sec).$log2.' LIMIT 1';
        //v($qu);
        $out=mysqli_fetch_assoc(my_mysql_query($qu));
        //v($out);
        //v($_needfield_sec);
        if ($_needfield_sec and $_needfield_sec!=='*')
        {
            return $out[$_needfield_sec];
        }
        else
        {
            return $out;
        }
    }
}

function keywords()
{
    if (@$GLOBALS['m']['keyw'])
    {
        $out=$GLOBALS['m']['keyw'];
    }
    else
    {
        @$out=fgc('keywords');
    }
    return lang($out);
}

function auth_block()
{
    if ($GLOBALS['user'])
    {
        $rep['[email]']=$GLOBALS['userdata']['email'];
        $rep['[login]']=$GLOBALS['userdata']['login'];
        $rep['[logout]']='<input type="submit" name="logout" value=" " class="logout" />';
        $out='<form action="" method="post" id="blockauthform">'.strtr(shab('blockauthenterok'),$rep).'</form>';
    }
    else
    {
        $rep=array(
            '[login]'=>'<input type="text" name="login" id="login" />',
            '[pass]'=>'<input type="password" name="pass" id="pass" />',
            '[ok]'=>'<input type="submit" name="ok" value=" " class="okauth" />',
            '[reg]'=>hrpref.'register',
            '[fogot]'=>hrpref.'fogotpass'
        );
        $out='<form action="" method="post" id="blockauthform">'.strtr(shab('blockauthenter'),$rep).'</form>';
    }
    return $out;
}

function view_album($_ident=false) //2
{
    $m=$_ident ? qvar('ident' , $_ident ) : $GLOBALS['m'];
    if ( $_ident or $m['d2'] or strpos(' category tov _news',$m['type'] ) )
    {
        if ($m['d6']=='list1')
        {
            $atr='class="group" rel="group"';
        }
        else
        {$atr='class="imgbig"';
        }
        /* старый фотоальбом
			$pr=my_mysql_query( 'SELECT * FROM '.pref_db.'img WHERE album="'.$m['ident'].'"  ' ) ;
			while ( !$_ident and $mm=mysqli_fetch_assoc($pr) )
			{
				//v($mm);
				$truealb=true;
				//$out.='<div class="dalb" align="center">';
				$out.=block('blockfoto','<a '.$atr.' href="'.album.'/big/'.$mm['file'].'" ><img src="'.album.'/'.$mm['file'].'" /></a>');																			  			//$out.='</div>';
			} */

        //v($m['ident']);
        $identalb=$_ident ? $_ident : qqu(' rod='.xs($m['ident']).' and `type`=\'fotoalbum\' ','ident'  );
        //v($identalb);
        $mmalb=qvar('ident',$identalb);
        $pr=my_mysql_query( 'SELECT * FROM '.pref_db.'content WHERE rod="'.$identalb.'" and `type`=\'imgalbum\'  ORDER BY `'.( $mmalb['d7']=='adm' ? 'num' : 'ident' ).'` ASC ' ) ;
        while ($mm=mysqli_fetch_assoc($pr) )
        {
            //v($mm);
            $truealb=true;
            $atr='class="group" rel="group"';
            //$out.='<div class="dalb" align="center">';
            $out.=block('blockfoto','<a '.$atr.' href="'.img.'/cat/big/'.$mm['ident'].'.jpg" ><img src="'.img.'/cat/mid/'.$mm['ident'].'.jpg" /></a>
			<div class="fotoname">'.$mm['name'].'</div>
			');																			  			//$out.='</div>';
        }

        if ($truealb) return '<table class="album album-'.$identalb.'" autoScale="'.($mmalb['d8'] ? 'true' : '' ).'"><tr><td>'.$out.'</td></tr></table>';
    }
}

function htmlspecialchars_arr($mm) {
    foreach ( $mm as $k=>$v ) {
        $mm[$k]=htmlspecialchars($v);
    }
    return $mm;
}

function strip_tags_arr($mm) {
    foreach ( $mm as $k=>$v ) {

        $mm[$k]=is_array($v) ? strip_tags_arr($v) : strip_tags($v);

    }
    return $mm;
}

function hmenu($rod, $pref=false, $ur=false ) {
    if (o('hmenu_umenu_new')) {
        return menu2::hmenu($rod,$pref,$ur);
    } else {
        return hmenu::main($rod,$pref,$ur);
    }

}

function nmenu($ident) //2
{
    $lim=intval($GLOBALS['longpage'][1])*o('news-num');
    $pr=my_mysql_query ( ( "SELECT SQL_CALC_FOUND_ROWS * FROM ".pref_db."content WHERE 
	`rod`=".xs($ident)."  AND `ok`='on'   
	ORDER BY `d1` DESC LIMIT ".$lim.",".o('news-num')  ) ) ;
    $prenum=my_mysql_query('SELECT FOUND_ROWS()') ;
    $aln=mysqli_fetch_array($prenum);
    $out='<div id="newsmenu">';
    while ($mm=mysqli_fetch_assoc($pr) )
    {
        if ( $mm['type']=='_news')
        {
            if (!$GLOBALS['news-tpl']) $GLOBALS['news-tpl']=shab('news-tpl-mini');
            $img='cont/img/cat/min/'.$mm['ident'].'.jpg';
            $hr=hrpref.urlFromMnemo($mm['ident']).hrsuf.lngsuf;
            $rep=array(
                '[date]'	=>	'<span class="newsdate" >'.date_for_news($mm['d1']).'</span>',
                '[name]'		=>	'<b><a href="'.$hr.'" >'.lang($mm['name']).'</a></b>',
                '[href]'		=>	$hr,
                '[minidesc]'	=>	lang($mm['d14']),
                '[img]'		=>	( file_exists($img) ? '<img class="newsmimg" src="/'.$img.'" />' : '&nbsp;' ),
                '[podrob]'		=>	( lang($mm['d14']) ? '<a href="'.$hr.'" >'.lang('Подробнее...++Докладнiше...++Detail...').'</a>' : '' )
            );
            $out.=strtr($GLOBALS['news-tpl'],$rep);
        }
    }
    $out.='</div>';
    //листалка
    $alpages=floor($aln[0]/o('news-num'));
    if ($alpages>=1)
    {
        $out.='<div id="nlistalka">';
        for ($x=0;$x<=$alpages;$x++)
        {
            $name=$x+1;
            if ($GLOBALS['longpage'][1]==$x)
            {
                $out.="<span>$name</span>";
            }
            else
            {
                if ($x>=1)
                {
                    $numpage='--'.$x;
                }
                $hr='href="'.hrpref.urlFromMnemo($GLOBALS['m']['ident']).$numpage.hrsuf.lngsuf.'"';
                $out.="<a $hr>$name</a><span class='msp'> </span>";
            }
        }
        $out.='</div>';
    }
    return $out;
}

function vta($var)
{
    v('<textarea cols="50" rous="10">'.$var.'</textarea>');
}

function blockauth()  //2
{
    $ud=$GLOBALS['user'];
    $class='blockauth';
    if ($ud)
    {
        $header='Личная панель';
        $mesage='<span>Добрый день, <br/><b>'.$ud['name'].'</b><br/><br/><a  class="ina"  href="/forum/login.php?logout=true&sid='.$_COOKIE['phpbb2mysql_sid'].'&redirect=..'.$_SERVER['REQUEST_URI'].'--">Выйти</a></span>';
    }
    else
    {
        $header='Авторизация';
        $mesage='<span><b> <a class="ina"  href="/forum/profile.php?mode=register">Регистрация</a><br/>
		</b>
		<form action="/forum/login.php?mode=login" method="post">
		<label class="alab" for="username">Логин: <input type="text" name="username" class="mainauth" size="10" title="Username" /></label>
		<br /> <label class="alab"  for="password">Пароль: <input type="password" name="password" class="mainauth" size="10" title="Password" /></label>
		<input type="hidden" name="redirect" value="..'.$_SERVER['REQUEST_URI'].'--" />
		<br/><a class="ina" href="/forum/profile.php?mode=sendpassword">Забыли пароль?</a><br />
		<input class="voyti" type="submit" name="login" value="  Войти  " />
		</form></span>
		';
    }
    $class='blockauth';
    $out="<div class='{$class}'><div class='$class-1'><table class='m1-L'><tbody><tr><td class='m1-s'><a class='m1-a'>$header</a></td></tr></tbody></table>
	</div><div class='$class-2'>$mesage
	</div><div class='$class-3'/></div></div>";
    return $out;
}

function block($class1,$cont)
{
    @$classblock=func_get_arg(2);
    @$blockname=lang( func_get_arg(3) );
    /*	$out="<div class='{$class1}'><div class='{$class1}-1'>$blockname</div><div class='{$class1}-2'>";
	$out.=$cont;
	$out.="</div><div class='{$class1}-3'></div></div>\r\n";*/
    $blockset= qvar('ident',$class1,'d6') ;
    $bname= $blockname ? "<div class='bname'>$blockname</div>" : '' ;
    if ( $blockset =='fix' )
    {
        $out='<table class="'.$class1.' '.$classblock.'" ><tr><td class="'.$class1.'-2">'.$cont.'</td></tr></table>';
    }
    elseif ( $blockset=='reznoop')
    {
        $out='<div class="'.$class1.' '.$classblock.'" ><div class="'.$class1.'-1"><div class="'.$class1.'-15">'.$bname.'<div class="'.$class1.'-2">'.$cont.'</div></div></div></div>';
    }
    else
    {
        $out="<table class='{$class1} {$classblock}'><tr><td class='{$class1}-1'>".( $bname )."</td></tr><tr><td class='{$class1}-2'>";
        $out.=$cont;
        $out.="</td></tr><tr><td class='{$class1}-3'></td></tr></table>\r\n";
    }
    return $out;
}

function create_rod ($rod,$ur)
{
    global $menu, $edit, $rod_edit, $m;
    $prebase=my_mysql_query( 'SELECT * FROM '.pref_db.'content WHERE rod='.xs($rod).'  ORDER BY `num` ASC ' ) ;
    //var_dump ($prebase);
    $mm=mysqli_fetch_array($prebase);
    while ($mm & ($mm['type']!=='prod') )// затем начинаем перебирать каждый пункт и делать выборку по каждому (рекурсивно)
    {
        ?>
        <option  <? if ($mm['ident']==$m['rod']) echo "selected"; ?> value="<?=$mm['ident'] ?>"><?=str_repeat(". ",$ur-1).$mm['name']; ?> (<?=$mm['ident']; ?>)</option>
        <?
        create_rod ($mm['ident'],$ur+1);
        $mm=mysqli_fetch_array($prebase);
    }
}

function create_full_menu( $rod )
{
    global $m,$put;
    @$ur=func_get_arg (1);
    @$pref=func_get_arg (2);
    if (!$ur)
    {
        $prebase=my_mysql_query( 'SELECT * FROM my__content WHERE ident="'.$rod.'"  AND ok="1"  ORDER BY `num` ASC ' ) ;
        $mm=mysqli_fetch_array($prebase);
        $pref=$mm['d1'];
        $ur=1;
    }
    $prebase=my_mysql_query( 'SELECT * FROM my__content WHERE rod="'.$rod.'"  AND ok="1"  ORDER BY `num` ASC ' ) ;
    $out="";
    $mm=mysqli_fetch_array($prebase);
    if ($mm)
    {
        $out.= "[{$pref}m{$ur}{$ur}]";
        while ($mm)// затем начинаем перебирать каждый пункт и делать выборку по каждому (рекурсивно)
        {
            //вывод строки   str_repeat("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;",$ur).
            if ($mm['type']=='vlink')
            {
                $out.= "[{$pref}m{$ur}]<a class=\"{$pref}m{$ur}\" href=\"".trim($mm['d1']).'">'.$mm['name']."</a>[/{$pref}m{$ur}]";
            }
            elseif ($mm['type']=='part')
            {
                $out.= "[{$pref}m{$ur}]<span class=\"{$pref}m{$ur}\" >".$mm['name']."</span>[/{$pref}m{$ur}]";
            }
            elseif ($mm['type']=='add')
            {
                $out.= "[{$pref}m{$ur}]<span class=\"{$pref}m{$ur}\" >".file_get_contents( 'admin/content/'.$mm['ident'].'.htm')."</span>[/{$pref}m{$ur}]";
            }
            elseif ($mm['type']=='name-img')
            {
                $tp="{$pref}m{$ur}";
                $hr=trim($mm['ident']);
                $n=$mm['name'];
                $out.= "<div class=\"$tp\" style=\"float:left\"> [$tp]<table ><tr><td align=\"left\" valign=\"top\"><a class=\"ma\" href=\"$hr\" >$n</a></td></tr><tr><td><a  href=\"$hr\" ><img border=\"0\" src=\"admin/content/imgcon/{$hr}.png\"/></a></td></tr></table>[/$tp]</div>";
            }
            elseif ($mm['type']=='_news')
            {
                $tp="{$pref}m{$ur}";
                $hr=trim($mm['ident']);
                $n=$mm['name'];
                $out.= "<div class=\"$tp\" style=\"float:left\"> [$tp]<table ><tr><td align=\"left\" valign=\"top\"><a class=\"ma\" href=\"$hr\" >$n</a></td></tr><tr><td><a  href=\"$hr\" ><img border=\"0\" src=\"admin/content/imgcon/{$hr}.png\"/></a></td></tr></table>[/$tp]</div>";
            }
            else
            {
                $out.= "[{$pref}m{$ur}]<a class=\"{$pref}m{$ur}\" href=\"".trim($mm['ident']).'">'.$mm['name']."</a>[/{$pref}m{$ur}]";
            }
            $out.=create_full_menu ($mm["ident"],$ur+1,$pref);
            $mm=mysqli_fetch_array($prebase);
        }
        $out.= "[/{$pref}m{$ur}{$ur}]";
        return $out;
    }
}

function Clear_array($array)// обрезание пробелов
{
    $c=sizeof($array);
    $tmp_array=array();
    for($i=0; $i<$c; $i++)
    {
        if (!(trim($array[$i])==""))
        {
            $tmp_array[]=$array[$i];
        }
    }
    return $tmp_array;
}

function closetags($html)
{
    #put all opened tags into an array
    preg_match_all("#<([a-z]+)( .*)?(?!/)>#iU",$html,$result);
    $openedtags=$result[1];
    #put all closed tags into an array
    preg_match_all("#</([a-z]+)>#iU",$html,$result);
    $closedtags=$result[1];
    $len_opened = count($openedtags);
    # all tags are closed
    if(count($closedtags) == $len_opened)
    {
        return $html;
    }
    $openedtags = array_reverse($openedtags);
    # close tags
    for($i=0;$i < $len_opened;$i++)
    {
        if (!in_array($openedtags[$i],$closedtags))
        {
            $html .= '</'.$openedtags[$i].'>';
        }
        else {
            unset($closedtags[array_search($openedtags[$i],$closedtags)]);
        }
    }
    return $html;
}

/** Модель хлебных крошек
 * @param $art
 * @return string
 */
function hleb_krosh ($art) {
    return create_navigation($art);
}

function create_navigation ($art)
{
    @$sub=func_get_arg(1); // флаг надуровня
    $mm=qvar('ident',$art); // массив текущего контентобъекта

    //
    if ($art=='cat')  $mm='';

    // флаг включенности каталожного фильтра
    $is_fullsearch = o('fullsearch_ver')=='1'   ;

    // вычисляем товарную группу
    $tovgroup = par_tovcat( $mm, 'd6');

    if (  strpos( ' tov category page _news bnews bmenu',  $mm['type']) and $mm['ident']!=='index' )
    {
        $pass = (
            (
                $is_fullsearch
                AND
                $tovgroup
                AND
                (
                    (
                        $mm['type']=='category'
                        AND
                        $mm['d6']
                    )
                    OR
                    (
                        $mm['type']!=='category'
                    )
                )


            )
            OR
            (
                (  $mm['type']!=='bmenu' )
                OR
                (   $mm['type']=='bmenu'  AND $mm['sub']['bmenu_in_hk'] )
            )
        )
            ? TRUE: false;



        //if ($mm['ok'] AND $pass) $GLOBALS['hlebk'][]='&nbsp;<a href="/'.hrpref.urlFromMnemo($mm['ident']).lngsuf.'" >'.lang($mm['name']).'</a>&nbsp;';
        $p=[
            'href'=>'/'.hrpref.urlFromMnemo($mm['ident']).lngsuf,
            'name' => lang( $mm['name'] ) ,
        ];

        if ($mm['ok'] AND $pass) {

            $GLOBALS['hlebk'][]=smarty_tpl('hleb_krosh_tpl/'.(
                ( !$GLOBALS['hlebk'] AND o('hleb_end_link_off') )? 'first_nolink' : 'sub_krosh'
                ), $p);

        }

        create_navigation($mm['rod'],1);
        //v($mm['type']);
        //if ( ((bool)$pod)  )	$out=$pod.$out;
    }
    if ($sub==1)
    {
        return $out;
    }
    else
    {
        //$GLOBALS['hlebk'][]='<a href="/'.hrpref.lngsuf.'" >Главная</a>&nbsp;';
        $GLOBALS['hlebk'][]= smarty_tpl( 'hleb_krosh_tpl/main_krosh' ); //'<a href="/'.hrpref.lngsuf.'" >Главная</a>&nbsp;';

        $sep=o('sep_hk') ?: smarty_tpl( 'hleb_krosh_tpl/sep_hk' ) ;

        return implode (' '.($sep? $sep: '&rsaquo;' ).' ',array_reverse($GLOBALS['hlebk']));
    }
}

function price_auto_cop($in)
{
    return $in  > floor( $in ) ?   sprintf("%.2f" , $in ) : $in ;
}

function mb_lcfirst($text) {
    return mb_strtolower(mb_substr($text, 0, 1, 'utf-8'), 'utf-8') . mb_substr($text, 1, 10000 , 'utf-8');
}
function mb_ucfirst($text) {
    return mb_strtoupper(mb_substr($text, 0, 1, 'utf-8'), 'utf-8') . mb_substr($text, 1, 10000 , 'utf-8');
}

function price_prepare($in)
{
    return str_replace( array(',',' '), array('.',''), trim($in) );
}

function create_cart($newcart = false)
{

    if ($newcart or !o('altercart')) {
        /*$quc='SELECT count(`art`),sum(`price`*`num`) FROM `'.pref_db.'cart` WHERE `is_order`<>1 AND `viz_sid`='.xs(session_id()).'		';
		//v($quc);
		$pr=my_mysql_query($quc);
		$mm=mysqli_fetch_array($pr);
		//v($mm);
		$num=intval($mm[0]);*/
        $cart_tovs=fn('cart::get_cur_cart_arr');
        $num = sizeof($cart_tovs);
        if ($num > 1 or $num == 0) {
            $okonch = 'й';

        } else {
            $okonch = 'е';
        }

        if ($num == 0 or $num > 4) {
            $tov = 'товаров';
        } elseif ($num == 1) {
            $tov = 'товар';
        } else {
            $tov = 'товара';
        }

        $hr = substr($_SERVER['REQUEST_URI'], 1);
        //$price= price2html( price_auto_cop( $mm[1] )) ;

        $rep = array(
            '[num]' => $num,
            '[naim]' => 'наименовани' . $okonch,
            '[tov]' => $tov,
            '[sum]' => (fn('cart::cur_summ') ?: 0),
            '[val]' => val
        );

        if ($num > 0) {
            $hr = 'href="' . hrpref . 'cart--' . $GLOBALS['m']['ident'] . '"';
        } else {
            $hr = '';
        }

        //v( intval($mm[0]) ) ;
        //v(  shab('blockcart-tpl-no-tov') );

        if (intval($num) > 0 and !o('altercart')) {
            if (o('blockcart-tpl-oldtpl')) {
                $sep = o('fullsearch_ver') ? '/' : '--';
                $out = "<div align='center' class='cartmini'>
					<a href='" . hrpref . "cart" . $sep . $GLOBALS['m']['ident'] . "'> <big><b>В вашей корзине:</b></big><br/> <br />
					<b>$num</b> наименовани{$okonch}<br />
					на сумму <br/>
					<b>$price&nbsp;" . val . "</b> </a>
					</div>";
            } else {

                $cart_tpl = (!intval($num) and shab('blockcart-tpl-no-tov')) ? shab('blockcart-tpl-no-tov') : shab('blockcart-tpl');
                $out = strtr('<div class="cartmini' . ($num > 0 ? ' tovincart' : '') . '"><a ' . $hr . '>' . $cart_tpl . '</a></div>', $rep);
            }

            return block('blockcart', $out);
        } elseif (o('altercart')) {
            //v(9);

            //v(tpl('blockcart-tpl'));
            $cart_tpl = (!intval($num) and shab('blockcart-tpl-no-tov')) ? shab('blockcart-tpl-no-tov') : shab('blockcart-tpl');
            return strtr('<div id="cartmini" class="' . ($num > 0 ? 'tovincart' : '') . '"><a ' . $hr . '>' . $cart_tpl . '</a></div>', $rep);
        }
    }
    /*}*/
}

function cartmini(){

	$cart_tovs=fn('cart::get_cur_cart_arr');
	$num = sizeof($cart_tovs);

	if ($pnum > 1 or $num == 0) {
		$okonch = 'й';

	} else {
		$okonch = 'е';
	}

	if ($num == 0 or $num > 4) {
		$tov = 'товаров';
	} elseif ($num == 1) {
		$tov = 'товар';
	} else {
		$tov = 'товара';
	}


	$naim= 'наименовани' . $okonch;

		$sum = (fn('cart::cur_summ') ?: 0);


	if ($num > 0) {
		$hr = 'href="/cart"';
	} else {
		$hr = '';
	}



	return smarty_tpl('cartmini',  compact('hr', 'num', 'sum', 'tov' ) );

}

function fullcart()
{
    if ($GLOBALS['clear']) //ochistka korzini
    {$quc='DELETE FROM `'.pref_db.'cart` WHERE `viz_sid`='.xs(session_id()).'		';
        //v($quc);
        $pr=my_mysql_query($quc);
        //v($GLOBALS['longpage']);
        header('Location: '.basehref.'/'.$_POST['oldref']);
        exit;
    }
    if ($_POST['renum'] or $_POST['gozakaz']) // pereschet
    {
        $num_tov=$_POST['num_tov'];
        $num_key=array_keys($num_tov);
        foreach ($num_key as $art)
        {
            if ( !$_POST['del'][$art])
            {
                $quc='UPDATE `'.pref_db.'cart` SET `num`='.xs($num_tov[$art]).' 
				WHERE `viz_sid`='.xs(session_id()).'	AND `art`='.xs($art);
                //v($quc);
            }
            else
            {
                $quc='DELETE FROM `'.pref_db.'cart`  WHERE `viz_sid`='.xs(session_id()).'	AND `art`='.xs($art);
            }
            $pr=my_mysql_query($quc);
        }
    }
    //v($_REQUEST['SignatureValue']);
    if ($_POST['gozakaz'] or $_REQUEST['SignatureValue'])
    {
        if (o('form-zakaz')=='adv-form')
        {
            $out=tpl('newzakazcart');
        }
        else
        {
            $out=tpl('zakazcart');
        }
    }
    else
    {
        $quc='SELECT sum(`price`*`num`) FROM `'.pref_db.'cart` WHERE `viz_sid`='.xs(session_id()).'		';
        //v($quc);
        $pr=my_mysql_query($quc);
        $mm1=mysqli_fetch_array($pr);
        $quc='SELECT '.pref_db.'cart.zakaz,'.pref_db.'cart.num, '.pref_db.'cart.price, '.pref_db.'content.desc,'.pref_db.'content.name,'.pref_db.'cart.art,'.pref_db.'content.type,'.pref_db.'content.rod   FROM `'.pref_db.'cart`,`'.pref_db.'content` WHERE `'.pref_db.'cart`.`viz_sid`='.xs(session_id()).' AND `'.pref_db.'cart`.`art`=`'.pref_db.'content`.`ident`	ORDER BY `datetime` ASC ';
        $pr=my_mysql_query($quc);
        $mm=mysqli_fetch_array($pr);
        $out=nsmartpl('fullcart',1,$rep);
        while ( $mm )
        {
            //v($mm);
            if ($mm['type']=='mod')
            {
                $mrod=qvar('ident',$mm['rod']);
                autozapolnenie($mrod);
                $mm['name']=$mrod['d4'].' - '.$mm['name'];
                //v($mrod);
            }
            $rep=array(
                '$name'		=>	lang($mm['name']).': '.$mm['zakaz'],
                '$desc'		=>	$mm['desc'],
                '$price'	=>	$mm['price'],
                '$art'		=>	$mm['art'],
                '$num'		=>	$mm['num']
            );
            $out.=nsmartpl('fullcart',2,$rep);
            $mm=mysqli_fetch_array($pr);
        }
        $rep=array(
            '$fprice'	=>	$mm1['sum(`price`*`num`)'] ,
            '$hr'		=>	(o('cart-vozvrat')?o('cart-vozvrat'): $GLOBALS['longpage'][1].'--'.$GLOBALS['longpage'][2].'--'.$GLOBALS['longpage'][3])
        );
        $out.=nsmartpl('fullcart',3,$rep);
    }
    return $out;
}
function ajaxout($out)
{
    //$GLOBALS['cache']['utf'] = $GLOBALS['cache']['utf'] ? 1 :
    header("Content-Type: text/html; charset=utf-8");
    return   $out ;
    //return   iconv('windows-1251','utf-8',  $out );
    //return iconv('cp1251','utf-8',$out);
}

function profiler_fn_out( $function_name, $out ) {

}
function profiler_fn_in( $function_name, $args ) {

}


function fullcart3() {

    cart_request_hand();

    if ( oror($GLOBALS['longpage'][1], 'merchant', 'success', 'fail') AND antikapcha()   )
    {

        //$out=tpl('newzakazcart3');
        $out= (smarty(jscripts.'/tpl/fullcart3merchant.php',FALSE));
    }
    else
    {

        $fullcart3table=fullcart3table();
        $form= (smarty(jscripts.'/tpl/fullcart3merchant.php',FALSE));
        $out = $fullcart3table ? nsmartpl('fullcart3tpl',1,$rep).$fullcart3table.nsmartpl('fullcart3tpl',5,$rep).$form : 'Пусто...' ;
    }


    return $out;
}

function gozakaz_form() {

}



function fullcart3table()
{
    return fn('fullcart3table_smarty');

    $mm1=fn('cart::cur_summ');

    $mm_tovs=fn('cart::get_cur_cart_arr');

    if($mm_tovs)
    {
        $out.= nsmartpl('fullcart3tpl',2,$rep)  ;
        foreach ( $mm_tovs as $mm )
        {
            //v($mm);

            setsub($mm);

            if ($mm['type']=='mod')
            {
                $mrod=qvar('ident',$mm['rod']);
                autozapolnenie($mrod);
                $mm['name']=$mrod['d4'].' - '.$mm['name'];
                //v($mrod);
            }

            $price=price2html( price_auto_cop( $mm['price'] ));

            $weight=$mm['sub']['weight'] ? : o('weight_default') ;

            $rep=array(
                '$name'		=>	lang($mm['name']).': ',
                '$desc'		=> 	strtr( $mm['zakaz'], array('\;'=>';', ';'=>';') ) ,
                '$cart_desc'	=> 	strtr( $mm['sub']['cart_desc'], array('\;'=>';', ';'=>';') ) ,
                '$price'	=>	$price,
                '$price_all_tov'	=>	$price*$mm['num'] ,
                '$art'		=>	$mm['art'],
                '$idart'		=>	($mm['idart']) ? $mm['idart'] : $mm['cid'] ,
                '$num'		=>	$mm['num'],
                '$id'		=>	$mm['a_id'],
                '$img'		=>	noimage( 'cont/img/cat/mid/'.$mm['d25'] ) ,
                '$codtov'		=>	$mm['codtov'],
                '$weight'		=>	$weight,
                '$maxvalue'	=>	$mm['sub']['tov_cur'],
                '$href'	=> urlFromMnemo($mm['art'])

            );

            $weight_all+=floatval( $weight ) * $mm['num'];

            $line=nsmartpl('fullcart3tpl',3,$rep);

            if (o('fullcart_brandgroup')) { // группировка
                $group[CatCurRoot($mm['rod'])].=$line;
            } else {
                $group[]=$line ;
            }

            $mm=mysqli_fetch_array($pr);
        }

        $out.=implode('', $group);

        $rep=array(
            '$fprice'	=> 	price2html( price_auto_cop( $mm1 ) ),
            '[weight_all]'	=> 	vl( $weight_all ) ,
            '$hr'		=>	(o('cart-vozvrat')?o('cart-vozvrat'): $GLOBALS['longpage'][1].'--'.$GLOBALS['longpage'][2].'--'.$GLOBALS['longpage'][3])

        );

        $out.=nsmartpl('fullcart3tpl',4,$rep);

        return smarty(  str_replace(  '[val]', (val) , $out) ,1 );
    }

}

function fullcart3table_smarty()
{

    $fprice=fn('cart::cur_summ');

    $mm_tovs=fn('cart::get_cur_cart_arr');

    if($mm_tovs)
    {
        //$out.= nsmartpl('fullcart3tpl',2,$rep)  ;
        foreach ( $mm_tovs as $mm )
        {
            //v($mm);

            setsub($mm);

            if ($mm['type']=='mod')
            {
                $mrod=qvar('ident',$mm['rod']);
                autozapolnenie($mrod);
                $mm['name']=$mrod['d4'].' - '.$mm['name'];
                //v($mrod);
            }

            $price= price_auto_cop( $mm['price'] );

            $weight=$mm['sub']['weight'] ? : o('weight_default') ;

            $p['tovs'][]=array_merge( $mm ,[
                'name'		=>	lang($mm['name']).': ',
                'desc'		=> 	strtr( $mm['zakaz'], array('\;'=>';', ';'=>';') ) ,
                'cart_desc'	=> 	strtr( $mm['sub']['cart_desc'], array('\;'=>';', ';'=>';') ) ,
                'price'	=>	price2html($price),
                'price_all_tov'	=>	price2html(  $price*$mm['num'] ) ,
                'art'		=>	$mm['art'],
                'idart'		=>	($mm['idart']) ? $mm['idart'] : $mm['cid'] ,
                'num'		=>	$mm['num'],
                'id'		=>	$mm['a_id'],
                'id_с'		=>	$mm['id'],
                'img'		=>	noimage( 'cont/img/cat/mid/'.$mm['d25'] ) ,
                'codtov'		=>	$mm['codtov'],
                'weight'		=>	$weight,
                'maxvalue'	=>	$mm['sub']['tov_cur'],
                'href'	=> urlFromMnemo($mm['art']),
                'min_val'	=> ( $mm['d19']>0 and o('tov_num_min') )  ? ($mm['d19']) : '1' ,

            ]);

            $weight_all+=floatval( $weight ) * $mm['num'];

            //$line=nsmartpl('fullcart3tpl',3,$rep);

            if (o('fullcart_brandgroup')) { // группировка
                $group[CatCurRoot($mm['rod'])].=$line;
            } else {
                $group[]=$line ;
            }

            $mm=mysqli_fetch_array($pr);
        }

        $out.=implode('', $group);



        $p['fprice'] 		= 	price2html( price_auto_cop( $fprice ) );
        $p['weight_all']	=	vl( $weight_all );
        $p['hr']			=	(o('cart-vozvrat')?o('cart-vozvrat'): $GLOBALS['longpage'][1].'--'.$GLOBALS['longpage'][2].'--'.$GLOBALS['longpage'][3]);
        $p['spdost']		=$_COOKIE['cart_spdost'];
        if (o('pricedost_fn') ) {
            $p['pricedost'] = fn( o('pricedost_fn'), $p );

            if ( $p['pricedost'] or $p['pricedost']===0) {
                $p['is_pricedost']=true;
            }
            $p['price_all']	=  price2html( floatval( $p['pricedost'] ) + floatval( $fprice ) );
        }

        //$out=nsmartpl('fullcart3tpl',4,$rep);
        //return smarty(  str_replace(  '[val]', (val) , $out) ,1 );
        return  str_replace(  '[val]', (val) , smarty_tpl( 'fullcart3tpl_smarty',  $p ) );
    }

}

function json2array($json){

    if(get_magic_quotes_gpc()){
        $json = stripslashes($json);
    }
    $json = substr($json, 1, -1);

    $json = str_replace(array(":", "{", "[", "}", "]"), array("=>", "array(", "array(", ")", ")"), $json);

    @eval("\$json_array = array({$json});");

    return $json_array;

}

function cat_percent($mm){
    $mm=(qvar( 'ident', $mm['rod'] ));
    if ( $mm['type']=='category' ) {
        return $mm['sub']['p_percent']!=='' ? $mm['sub']['p_percent'] : cat_percent( $mm);
    } else {
        return (o('p_percent'));
    }
}

function par_tovcat($mm, $field, $_subfield=false ) {

    if ( oror(  $mm['type'], 'category', 'tov' )  ) {
        //v( $mm );
        return ( $_subfield ? $mm[$field][$_subfield] : $mm[$field]  ) ? :  par_tovcat( qvar('ident',$mm['rod'] ), $field, $_subfield );
    }

}

function CatCurRoot($rod){
    $mm=qvar('ident',$rod);
    $mm_rod=qvar('ident', $mm['rod']);
    return $mm_rod['type']=='bmenu' ? $mm['ident'] : CatCurRoot($mm['rod']);

}

function description()
{
    if (@$GLOBALS['m']['desc'])
    {
        $out=$GLOBALS['m']['desc'];
    }
    else
    {
        @$out=fgc('description');
    }
    return lang($out);
}



function fgc($ident)
{

    $ident=strtr($ident,$GLOBALS['repurl'] );
    //v($ident);
    return @file_get_contents(cont.'/htm/'.trans_ident($ident).'.htm');
}

function fpc($ident,$data)
{

    $ident=strtr($ident, $GLOBALS['repurl'] );
    //v($ident);
    return @file_put_contents(cont.'/htm/'.trans_ident($ident).'.htm',$data);
}

function foot_smartpl ($template)
{
    $tpl=explode('-t-',fgc($template));
    return $tpl[3];
}

function find_rod_cat($ident)
{
    $mm=qvar('ident',$ident);
    if ($mm)
    {
        //echo $mm[ident]."<br/>";
        $put[]=$mm['ident'];
        find_rod($mm['rod']);
    }
}



function myheader()
{
    //v($GLOBALS['mbod']);




    if ($GLOBALS['mbod']['d25'])
    {
        if (! $out=fgc('header') )  $out.='&nbsp;';
        if ($GLOBALS['mbod']['d23']) $hw3='<td id="hw3">&nbsp;</td>';
        if ($GLOBALS['mbod']['d24']) $hw4='<td id="hw4">&nbsp;</td>';
        $out='<table id="inheader2"><tr><td id="hw1">'.$out.'</td>'.$hw3.$hw4.'</tr></table>'
        ;
    }
    else
    {
        $out=fgc('header');
    }
    return '[headerout]'.$out;
}

function loc301($loc) {

    header("HTTP/1.1 301 Moved Permanently");

    $basehref= $GLOBALS['protocol']=='https' ? str_replace('http://','https://', $GLOBALS['basehref'] ) : $GLOBALS['basehref'] ;

    if ( substr( $loc, 0, 4 )=='http' or substr( $loc, 0, 2 )=='//' ) {
        $loc= $GLOBALS['protocol']=='https' ? str_replace('http://','https://', $loc ) : $loc ;

    } else if ( substr( $loc, 0, 1 )=='/' ) {
        $loc=$basehref.$loc;
    } else {
        $loc=$basehref.'/'.$loc;
    }
    $header="Location: ".$loc;
    if (is_testing()) {
        return $header;
    } else {
        header ($header);
        exit;
    }
}

function trans_ident($ident)
{
    $arr=$GLOBALS['site']['newident'];
    if ( ($arr[$ident]) )  {
        return $arr[$ident];
    } else {
        return $ident;
    }
}

function trans_mm()
{

}

function headerout()
{
    foreach( vl($GLOBALS['cache']['dopfloat'],__LINE__) as $ident )
    {
        $df[]=xs($ident);
    }
    $df=implode(', ', $df);

    $qu='SELECT * FROM '.pref_db.'content 
			WHERE   
			( 	`rod` in ( \'parent\', '.($df ? $df.',':'') .xs($GLOBALS['mbod']['ident']).($GLOBALS['site']['d5'] ? ', '.xs($GLOBALS['site']['d5']) : '' ). ' ) 
				'.($df ?  '				
					OR 
				`ident` in ('.$df.') ' : '' ).'
			)
			AND  `type`=\'absoluter\' AND ok=\'on\'   ORDER BY `num` ASC ';

    $prebase=my_mysql_query(($qu)) ;
    while ($mm=mysqli_fetch_array($prebase) )			{

        $floater= ($mm['d2'])=='header' ? 'headerpreout' : 'footerpreout' ;

        $$floater = $$floater.absoluter($mm);

    }

    $GLOBALS['footerpreout']='<div id="footerpreout">'.$footerpreout.'</div>';

    return ($headerpreout);
}

function head_smartpl ($template)
{
    $tpl=explode('-t-',fgc($template));
    $x=1;
    while ( @$add=func_get_arg ($x)  )
    {
        $rep[$add]=func_get_arg ($x+1);
        $x=$x+2;
    }
    //v($x);
    if ( $x==1)
    {
        return $tpl[1];
    }
    else
    {
        return strtr($tpl[1],$rep);
    }
}

function i( $t, $name, $add_opt=FALSE, $add3_opt=false,$add4_opt=false,$mm_opt=false)
{

    if ( o('ifun_new') AND !constant('adminpanel') ) return ifun::i( $t, $name, $add_opt, $add3_opt, $add4_opt, $mm_opt );

    @$add=func_get_arg (2);
    @$add3=func_get_arg (3);
    @$add4=func_get_arg (4);
    @$mm=func_get_arg(5);
    if ( $t=='t' )
    {
        $t='text';
        $out= "<input type=\"$t\" name=\"$name\" id=\"$name\" value=\"".htmlspecialchars($GLOBALS[$name])."\" style='width:300px' />  ";
        $out='<table align="center"><tr><td align="right" width="50%">'.$add3.': </td><td align="left" valign="midle" width="50%">'.$out.'</td></tr></table>';
    }
    else if ( $t=='ch')
    {
        $t='checkbox';
        if  ( $GLOBALS[$name] or (!$_POST and $mm['d7']) )
        {
            $ch='checked="checked"';
        }
        else
        {
            $ch='';
        }
        //V($GLOBALS['name']);
        if (!$mm['d9']) $sep=':';
        $out= "<input rel='".$mm['d12']."' id='$name' type=\"$t\" name=\"$name\" $ch $add  />  ";

        $width_all=  $mm['d3'] ?  $mm['d3']  : '100%' ;
        $width_name=  ($mm['d4']) ? $mm['d4']  :  '50%'  ;

        $out='<table  width="'.$width_all.'"><tr><td align="right" width="'.$width_name.'" class="namecheck"><label for="'.$name.'">'.lang($add3).helpop($mm).formprice($mm['d12']).$sep.'</label></td><td  align="left" class="inputcheck">'.($out).'</td></tr></table>';
    }
    else if ( $t=='r')
    {
        $t='radio';
        if  (@$GLOBALS[$name]==$add)
        {
            $ch='checked';
        }
        else
        {
            $ch='';
        }
        $out= "<input type=\"$t\" name=\"$name\" value=\"$add\" id=\"$add\" $ch $add3 />  ";
        $add3=$add4;
        $out='<table><tr><td align="left" colspan="2">'.$out.'<label id="'.$add.'"> - '.$add3.'</label></td></tr></table>';
    }
    elseif ($t=='s')
    {
        $out='<table><tr><td align="center" colspan="2" ><b>'.$name.'</b></td></tr></table>';
    }
    elseif ($t=='o')
    {
        $out="<select name=\"$name\" $add >";
        $qu= 'SELECT * FROM '.pref_db.'content WHERE rod="'.xss($add4).'"  AND ok="1"  ORDER BY `num` ASC ';
        $prebase=my_mysql_query($qu) ;
        //v($GLOBALS[m][type]);
        echo "<br/>";
        while ($m=mysqli_fetch_array($prebase) )
        {
            $out.='<option value="'.$m[ident].'" ';
            //v($GLOBALS[m][type]);
            //v($m['type']);
            if ( $GLOBALS[m][type]==$m['ident'] ) $out.="selected" ;
            $out.=" >{$m['name']}</option>	"	;
        }
        $out.='/<select>';
        $out='<table><tr><td align="right">'.$out.'</td><td align="left"> - '.$add3.'</td></tr></table>';
    }
    elseif ($t=='oo')
    {
        $out="<select name=\"$name\" $add >";
        $qu= 'SELECT * FROM '.pref_db.'content WHERE rod="'.xss($add4).'"   ORDER BY `num` ASC '; //AND ok="1"
        $prebase=my_mysql_query($qu) ;
        //v($GLOBALS[m][type]);
        echo "<br/>";
        while ($m=mysqli_fetch_array($prebase) )
        {
            $out.='<option value="'.$m[d1].'" ';
            //v($GLOBALS[m][type]);
            //v($m['type']);
            if ( $GLOBALS[$name]==$m['d1'] ) $out.="selected" ;
            $out.=" >{$m['name']} ({$m['d1']})</option>	"	;
        }
        $out.='/<select>';
        $out='<table><tr><td align="right">'.$out.'</td><td align="left"> - '.$add3.'</td></tr></table>';
    }
    elseif ($t=='f')
    {
        $out= "<input type=\"file\" ".($mm['d7'] ? 'multiple class="multiple"' : '' )." style='width:100%;' name=\"$name\" $ch $add />  ";
        if ($mm['d11']=='top' )
        {
            $out='<table width="100%"><tr><td align="center" class="input_file_name">'.$add3.': </td></tr><tr><td align="left"   class="input_file">'.$out.'</td></tr></table>';
        }
        else
        {
            $out='<table><tr><td align="right" width="50%">'.$add3.': </td><td align="left" width="50%">'.$out.'</td></tr></table>';
        }

    }
    return $out;
}

function select_fields($ident) //2
{
    $qu= 'SELECT `ident`,`d1`,`d2`,`type`,`name`,`d99` FROM '.pref_db.'content WHERE rod='.xs($ident).' AND `d100`<='.reg().'  ORDER BY `num` ASC ';
    $pr=my_mysql_query($qu) ;
    $m=qvar('ident', $ident);
    @$add=func_get_arg(1);
    if ( ($m['d1']) & ($m['d2']) )
    {
        if ($add)
        {
            $out.=$m[$add].'	';
        }
        else
        {
            $out.=$m['d2'].'~'.$m['d1']."\r\n	";
        }
    }
    elseif ($add=='send')
    {
        $out.=$m['name'].'~'.$m['d1']."\r\n";
    }
    while ($mm=mysqli_fetch_assoc($pr))
    {
        if ($mm['type']=='formlink')
        {
            $identsel=$mm['d1'];
        }
        else
        {
            $identsel=$mm['ident'];
        }
        if ($GLOBALS['vvv'][0])
        {
            $GLOBALS['vvv'][1][]= $mm['type'];
            $GLOBALS['vvv'][2][]=$identsel;
        }
        if  (is_ok_opt($mm['d99']) )
        {
            $out.=select_fields($identsel,$add);
        }
    }
    return $out;
}

function helpop(&$mm)
{
    if ($mm['d95'])
    {
        if ($mm['d94'])
        {
            $a='<a href="'.$mm['d94'].'" target="_blank" />'   ;
            $aa='</a>';
        }
        return '<span class="help" >'.$a.'<img class="helpimg" src="/jscripts/tpladmin/help.png"/>'.$aa.'<div class="minihelp">'.htmlspecialchars($mm['d95']).'</div><div class="bighelp">'.$mm['d94'].'</div></span>';
    }
}

function select_user()
{
    $out= '<input id="fanvalue" name="fanvar" value="kuku585" type="hidden"/> <div class="outsorter"> <table class="sorter"><thead>
	<tr>
	<th></th>
	<th></th>
	<th>Фамилия</th>
	<th>Имя</th>
	<th>Логин</th>
	<th>Группы</th>
	</tr>
	</thead><tbody><tr><td>1</td><td><input name="x[]" value="yuriy3000" type="checkbox"/> </td><td>Клюшников</td><td>Юрий</td><td>yuriy3000</td><td>Администраторы, Менеджеры, Техники, Клиенты</td></tr><tr><td>2</td><td><input name="x[]" value="ya157157" type="checkbox"/> </td><td></td><td>Юрий</td><td>ya157157</td><td>Техники</td></tr><tr><td>3</td><td><input name="x[]" value="general@novatel.ru" type="checkbox"/> </td><td>Царьков</td><td>Сергей</td><td>general@novatel.ru</td><td>Администраторы</td></tr></tbody></table></div>';
    return modaller($out);
}

/**
 * @param $uid
 * @param $event
 * @param $ident
 * @param $alert
 * @param $sobs
 * @param $cont
 * @param $oldcont
 * @return int|string
 */
function loger($uid, $event, $ident, $alert, $sobs, $cont, $oldcont)
{
    $log=array
    (
        'uid'=>$uid,
        'event'=>$event,
        'ident'=>$ident,
        'alert'=>$alert,
        'sobs'=>$sobs,
        'cont'=> is_array($cont) ? print_r($cont,1) : $cont ,
        'oldcont'=>is_array($oldcont) ? print_r($oldcont) : $oldcont,
        'ip'=>uip(),
        'microtime'=>microtime(1) // todo надо сделать удобоваримый вывод времени, либо ещё дублировать поле, либо сделать отдельный интерфейс в админке с поиском
    );
    return mydbAddLine('log',$log);
}

function modaller($out)
{
    return "<div id='modaller' style='margin:auto;'><div id='inmod' style='max-height:300px;width:600px;overflow:auto'>$out</div></div>		<div id='fanfoot'><a id='apply' href='javascript: apply();' >Выбрать</a>  <a id='cancel' href='javascript: $.fancybox.close();' >Отменить</a> </div>";
}
function formedit($id)
{
    if ( inadm() AND   rg(5)  )
    {
        return '<a class="pointer formedit" '.( !adminpanel ? 'href="/myadmin.php?&id='.$id.'&mode=1" target="_blank"' : 'onclick="chpage('.$id.',1)"').'  ><img border="0" src="/'.core.'/tpladmin/document-edit.png"/></a>';
    }
}
function i_t($variable, $name, $width_all, $width_name,$rows,$x ,$mm)
{
    // func_get_arg()
    if( o('ifun_new')  AND !constant('adminpanel') ) return ifun::i_t($variable, $name, $width_all, $width_name,$rows,$x ,$mm);

    global $nothiden;
    $exp=explode('[', $variable);
    if ($mm['d13']) // внутренняя подсказка
    {
        $alt=' title="'.$mm['d13'].'" ';
        $hint=' hinted';
    }
    if ($exp[1]) // если перепенная - элемент массива
    {
        $exp[1]=substr($exp[1],0,strlen($exp[1])-1);
        $var=$GLOBALS[$exp[0]][$exp[1]] ;
    }
    else
    {
        $var=$GLOBALS[$variable];
    }

    if ( $mm['sub']['is_form_tpl'] ) {
        return smarty_tpl( array('tpl_content'=>$mm['sub']['is_form_tpl']), $mm );
    }

    if ($rows and $mm['type']!=='rangeslider') //если textarea
    {
        $rows_var=substr_count($var,"\n") +2 ;
        $rows=$rows > $rows_var ? $rows : $rows_var ;
        $out= "<textarea type='text' rows='$rows' name='$variable' style='width:100%' class='$hint' $alt>".htmlspecialchars($var)."</textarea>
		".($nothiden?"<input type='hidden' name='ps[]' value='$variable'	/>":'' );
    }
    else
    {
        //v($var);
        if ($mm['type']=='rangeslider')
        {
            $type='hidden';
            if($GLOBALS[$mm['d1']])
            {
                $curval=explode('-',$GLOBALS[$mm['d1']]);
            }
            else
            {
                $curval=array( $mm['d5'], $mm['d6'] );
            }
            $curvalues=intval($curval[0]).','.intval($curval[1]);
            $val= 'от '.intval($curval[0]).' до '.intval($curval[1]).' '.$mm['d8'] ;
            $valinp= intval($curval[0]).'-'.intval($curval[1]);
            $out.='<div class="rangediv" id="sp'.$mm['ident'].'" >'.$val.'</div>' ;
            $out.='<div id="rs'.$mm['ident'].'"></div>';
            $out.='<script>
			$(function()
			{
				$( "#rs'.$mm['ident'].'" ).slider({
					range: true,
					min: '.($mm['d5']?$mm['d5']:0).',
					max: '.$mm['d6'].',
					step: '.($mm['d7']?$mm['d7']:1).',
					values: [ '.$curvalues.' ],
					slide: function( event, ui )
					{
						$( "#sp'.$mm['ident'].'" ).html(  "от " + ui.values[ 0 ] +  " до " + ui.values[ 1 ] + " '.$mm['d8'].'");
						$( "#'.$mm['ident'].'" ).val(ui.values[ 0 ] +"-"+ui.values[ 1 ] );
					}
					});
				$( "#sp'.$mm['ident'].'" ).html("'.$val.'");
				$( "#'.$mm['ident'].'" ).val( "'.$valinp.'" );
			}
			);
			</script>';
        }
        elseif ($mm['d8'])
        {
            $type='password';
        }
        elseif ($mm['d12']=='select_user')
        {
            $type='hidden';
            $varspan='<span>'.implode('</span> <span>', explode(' ',$var) ).'</span>';
            $out.='<div class="inserter"><table width="100%"><tr><td width="16"><a class="selectajax" href="/ajax--?action='.$mm['d12'].($mm['d96']?'_'.$mm['d96']:'').'&ident='.$mm['ident'].'&var='.$mm['d1'].'" ><img src="/jscripts/tpladmin/add_group.png" /></a></td><td class="spanins">'.$varspan.'</td></tr></table></div>';
        }
        elseif( $mm['d10'] )
        {
            $type='hidden';
            if ( function_exists($mm['d10']) ) $out.=$mm['d10']($mm,$var);
        }
        else
        {
            $type='text';
        }

        $out.= "<input type='$type'  id='$variable' name='$variable' {$alt} class='{$hint}' value='".
            ( $variable=='ident' ? urlFromMnemo( $var ) :  htmlspecialchars($var,ENT_QUOTES, 'cp1251')  ).
            "' style='width:100%' /> 
		".($nothiden?"<input type='hidden' name='ps[]' value='$variable'	/>":'' );
    }
    $edit= formedit( $mm['id']);
    $tf=$GLOBALS['m']['type'];
    if (!$width_all) $width_all='100%';
    if (!$width_name) $width_name='200px';
    if ($mm['d5'] and $mm['type']!=='rangeslider' )
    {
        $need=' need';
        $red='<span class="red">*</span>';
    }
    $namealign=$mm['d9']?$mm['d9']:'left';
    if  ($mm['d7']=='r')
    {
        $place1="<td align='left' width='$width_name' > - $name</td>";
    }
    elseif($mm['d7']=='t')
    {
        $place2="<td   class='it_name' align='$namealign' style='padding-bottom:0;' valign='top'>".lang($name).$red.helpop($mm).":</td></tr><tr>";
    }
    elseif ($variable=='ident' and  strpos( ' page category tov _news' ,  $tf ) )
    {
        $name=basehref.'/'.hrpref;
        $place2="<td align='left' width='1' style='border-right:0;'  class='it_name'>Адрес страницы: <b>$name</b></td>";
    }
    else
    {
        $place2= "<td class='it_name' align='$namealign' valign='".( $mm['type']=='rangeslider' ? 'bottom':'top' )."' width='$width_name'>".lang($name).$red.helpop($mm).":</td>"  ;
    }
    if ($mm['d98']) $mm['d98'] = str_replace('=','--',$mm['d98']) .' hidden';
    if (adm) $alt=$mm['d2'].'~'.$mm['d1'];
    $out="<div  title='$alt' class='i_t{$need} {$mm['d12']}'><table align='center' width='$width_all' >
	<tr>
	".($name ? $place2 : '' )."
	<td align='left' valign='top' class='it_input'>$out".( $mm['sub']['comment']? '<div class="input_comment">'.$mm['sub']['comment'].'</div>' : '' ).( $mm['type']!=='rangeslider'? $edit:'')."</td> 	
	$place1
	
	</tr>
	</table></div>";
    return $out;
}



function get_post_form_arr()
{
    foreach ($GLOBALS['inames'] as $k=>$v)
    {
        $val=$_POST[$k];
        if ($k and !$_POST['ihid'][$k] )
        {
            $out[$k]=$val;
        }
    }
    return $out;
}

function mb_ucasefirst($str)
{

    if ( mb_strlen($str)>0)
    {
        $str = mb_strtoupper( mb_substr( $str, 0,1 ) ).mb_substr(  $str, 1, 1000 ) ;
        return $str;
    }
}



function actions_front ($act) {

    if ( strpos( ' opros cabinet_adressdel cabinet_wishlist check_auth' , $act ) ) {

        if ($act=='cabinet_adressdel') { return cabinet::adressEdit('del'); }
        if ($act=='cabinet_wishlist') { return fn('cabinet::wishlist'); }

        else { return $act(); }

    } else if ( inadm() ) {
        return "Неизвестная функция ".$act ;
    }
}

function labels($mm)
{
    foreach( ($mm['sub']['labels']) as $lname )
    {
        $lmm=( qvar('ident', 'label_'.$lname ) );
        $out.='<div class="label_one label_'.$lname.'">'.( $lmm['sub']['text_name_on'] ? $lmm['name'] : '' ).'</div>';
    }
    if ($out) return '<div class="labels"><div class="inlabels">'.($out).'</div></div>';
    //mydbGetSub()
}

function i_r($variable, $name, $width_all, $width_name, $ident, $valfield=FALSE, $arg6=FALSE,$arg7=FALSE,$arg8=FALSE)
{
    // $out.=i_r($mm['d1'], $mm['name'],  $mm['d3'], $mm['d4'] , $id_spis ,$mm['d9'],$mm['d5'] ,$mm['d6'],$mm);
    //	5

    if ( o('ifun_new')  AND !constant('adminpanel') ) return ifun::i_r($variable, $name, $width_all, $width_name, $ident, $valfield, $arg6, $arg7, $arg8);

    $valfield = $valfield ? : 'd1';
    // func_get_arg
    $mm=$arg8;
    if (!$width_rro) $width_all='100%';
    if (!$width_name) $width_name='50%';

    if($mm['d2']='sub')
    {
        $vars=array_flip(  $GLOBALS[$variable] );
    }
    else
    {
        if ( is_array( $GLOBALS[$variable] ) ) {

            $vars=array_flip( $GLOBALS[$variable] );

        } else {

            $vars=array_flip( explode(' ', $GLOBALS[$variable]) );

        }
    }

    if (  $ident{0}=='=' ) {

        eval ( '$labels_all'.$ident.';' );
        vl($labels_all);

    } else {

        $ident=explode( ' ', $ident );
        $rod = sizeof( $ident ) > 1 ? '( rod='.xs($ident[0]).' or rod='.xs( $ident[1] ).' ) ' : ' rod='.xs($ident[0]) ;

        if ($arg7) $ok=' AND `ok`<>\'\' ';
        $qu= 'SELECT `name`,`'.$valfield.'`,`d12`,`d99`, `d98`,`d97`,`d96`,`d95`,`d94` FROM '.pref_db.'content WHERE '.$rod.$ok.'  AND `d100`<='.reg().'    ORDER BY `num` ASC ';
        $prebase=my_mysql_query( vl($qu)) ;
        while ( $m=mysqli_fetch_assoc($prebase) ) {
            $labels_all[]=$m;
        }
    }


    foreach ( vl($labels_all) as $m ) {

        $m[0]=$m['name'];
        $m[1]=$m[$valfield];
        vl($m);
        $dep=formDep2class($m['d98']);
        if (  is_ok_opt($m['d99']) ) {
            if  ($name==='Опции для репозитория:') {
                $m[1]=$m[1];
            }
            $file=jscripts.'/tpladmin/t-'.$m[1].'.png';


            if (file_exists($file) and $m[1])
            {
                $img = "<img src='$file'/>";
            }
            else
            {
                $img='';
            }
            if ($mm['d8']=='multi')
            {
                //v($mm);
                //v($GLOBALS['m']);
                $label="<label><input  type='checkbox' name='{$variable}[]' value='{$m[1]}'  ";

                if ( isset($vars[$m[1]]) ) $label.='checked="checked"' ;

                if ( ($arg6) )
                {
                    $val="({$m[1]})";
                }
                $label.=" />$img {$m[0]} $val </label>";
            }
            else
            {
                $label="<label><input  type='radio' name='$variable' value='{$m[1]}' rel='".$m['d12']."' ";
                if ( $GLOBALS[$variable]==$m[1] ) $label.='checked="checked"' ;
                if ( ($arg6) )
                {
                    $val="({$m[1]})";
                }
                $label.=' />'.$img.' '.mb_ucasefirst($m[0]).' '.($val).' '.( $mm['d16'] ? formprice(($m['d12'])) : '' ) .'</label>';
            }
            $out.="<div  $dep[0] $dep[1] rel='radio_label' >$label</div> ";
            if ($m['d12'])
            {
                $out.='<input type="hidden" name="ival['.$variable.']['.$m[1].']" value="'.$m['d12'].'" />';
                $GLOBALS['ivals'][$variable][$m[1]]=$m['d12'];
            }
            $GLOBALS['ivalnames'][$variable][$m[1]]=$m['name'];
        }

    }
    if($mm['d8']=='radiofieldset')
    {
        $out="
			<fieldset class='i_r' style='width:".$width_all."' ><legend align='".(($mm['d10'])?$mm['d10']:'right')."'><b> $name".helpop($mm).": </b></legend>
			<table  align='center' style='width:100%'  id=\"$variable\" rel='rad'>
			<tr>			
			<td  align='left' valign='middle' class='i_r_labels'>$out</td>
			</tr>
			</table></fieldset>";
    }
    else
    {
        $out="
			<table class='i_r' align='center'  style='width:".$width_all."' id=\"$variable\" rel='rad'>
			<tr>
			<td style='width:".$width_name."' align='".(($mm['d10'])?$mm['d10']:'right')."'><b> $name".helpop($mm).": </b></td>
			<td  align='left' valign='middle' class='i_r_labels'>$out</td>
			</tr>
			</table>";
    }

    return $out;
}

function formprice($mmd12)
{
    $out=($mmd12) ? ' <b>('.$mmd12.' [val])</b>':'' ;
    return $out;
}

function i_sel($name,$var)
{
    return i_select ($name, $var, $var, 'name', 'd1','');
}

function i_select ($name, $var, $ident, $name_field, $value_field ,$attribute, $arg6=FALSE, $arg7=FALSE, $arg8=FALSE )
{
    if ( o('ifun_new')  AND !constant('adminpanel') ) return ifun::i_select ($name, $var, $ident, $name_field, $value_field ,$attribute, $arg6, $arg7, $arg8 );

    if ($arg7) $ok=' AND `ok`<>\'\' ';
    $mm=$arg8;
    // func_get_arg()
    $out="<select name=\"$var\" id=\"$var\" $attribute  rel='sel' style='width:100%;'>";
    $subid=explode(':', $mm['d7']);
    if ($subid[0]=='autosearch')
    {
        $qu='SELECT value v,value vv FROM `'.pref_db.'subcontent` WHERE folder='.xs($GLOBALS['cache']['tovgroupp']).' and field='.xs(trim( $name )).' GROUP BY value ORDER BY VALUE ASC';
    }
    else if ($subid[0]=='subsearch')
    {
        $qu='SELECT value v,value vv FROM `'.pref_db.'subcontent` WHERE folder='.xs($subid[1]).' and field='.xs(trim( $subid[2]?$subid[2]:$name )).' GROUP BY value ORDER BY VALUE ASC';
    }
    else
    {
        $qu= 'SELECT `'.$name_field.'`,`'.$value_field.'`, `d98`, `d99`, `d12` FROM '.pref_db.'content WHERE rod='.xs($ident).$ok.'  AND `d100`<='.reg().' ORDER BY `num` ASC '; //AND ok="1"
    }
    $prebase=my_mysql_query($qu) ;
    if( $mm['d15'] ){
        $out.= '<option value=""  rel="opt" >'.($subid[0]=='subsearch'?'Не важно':'').'</option>'	;
    }

    $folder=explode(':',$mm['d7']);

    if ( $folder[0]=='folder' )
    {
        $file=folder2array_asort( ($folder[1] ));
        $file_key=(array_keys( $file ) );
    }
    $x=0;

    while ($m=mysqli_fetch_array($prebase) or ($file_key[$x]) ) {


        if ( is_ok_opt($m['d99'])  ) {

            $dep=formDep2class($m['d98'])  ;

            if(  $file_key[$x]) {
                $m[1]=$folder[1].'/'.$file_key[$x];
                $m[0]=$file_key[$x];
            }

            $out.='<option value="'.$m[1].'" '."  $dep[0] $dep[1] rel='opt' ";
            //v($GLOBALS[m][type]);
            //v($m['type']);
            if ( $GLOBALS[$var]==$m[1] ) $out.="selected" ;
            if ( ($arg6 ) )
            {
                $val="({$m[1]})";
            }
            if ( $mm['d16'] ){
                $price=' - '.$m['d12'].' [val]';
            }
            else {
                $price='';
            }

            $out.=' >'.mb_ucasefirst($m[0].' '.$val.$price).'</option>'	;
            if ($m['d12'])
            {
                $postout.= '<input type="hidden" name="ival['.$var.']['.$m[1].']" value="'.$m['d12'].'" />';
                $GLOBALS['ivals'][$var][$m[1]]=$m['d12'];
            }
            $GLOBALS['ivalnames'][$var][$m[1]]=$m['name'];

            $x++;

        }

    }

    $out.='</select>';
    $width_all=$mm['d3']?$mm['d3']: '100%';
    $width_name=$mm['d4']?$mm['d4']:'50%';

    if ($mm['d17'] and $mm['type']!=='rangeslider' )
    {
        $need=' class="need"';
        $red='<span class="red">*</span>';
    }

    $sep= !$mm['sub']['not_sep'] ?  ':' : '';

    $out='
	<table class="i_select" width="'.$width_all.'" '.$need.'>
	<tr> 
	<td class="i_select_name" align="'.(($mm['d10'])?$mm['d10']:'right').'" width="'.$width_name.'" align="right">'.$name.$red.$sep.'</td>
	<td class="i_select_input" align="left"  align="left" style="position:relative;"> '.$out.$postout.formedit($mm['id']).'</td>
	</tr>
	</table>';
    return $out;
}

function imgex($file_exists)
{
    if (file_exists($file_exists) )
    {
        return "<img src='$file_exists'/><br/>";
    }
    else
    {
        return '';
    }
}

function invideo($_rod=false)
{
    if($_rod)
    {
        $m['ident']=$_rod;
    }
    else
    {
        global $m;
    }
    $qu= 'SELECT * FROM '.pref_db.'content WHERE `rod`='.xs($m['ident']).' 
	AND (`type`=\'audio\' or `type`=\'video\')
		ORDER BY `num` ASC ';
    $pr=my_mysql_query($qu) ;
    while ($mm=mysqli_fetch_assoc($pr))
    {
        $out.=addmedia($mm);
    }
    return $out;
}

function addmedia ( $mm )
{
    if ( !is_array( $mm) ) $mm=qvar('ident',$mm);
    if ($mm['type']=='_video')
    {
        $out.="<br/><center><object width='{$mm['d2']}' height='{$mm['d3']}'>
				<embed src='/jscripts/lib/uppod.swf' 
				type='application/x-shockwave-flash' 
				allowscriptaccess='always' 
				allowfullscreen='true' 
				wmode='opaque'				flashvars='comment={$mm['name']}&amp;st=/cont/files/video-diz.txt&amp;file={$mm['d1']}' bgcolor='#ffffff' width='{$mm['d2']}' height='{$mm['d3']}'></embed></object></center><br/>";
    }
    elseif ($mm['type']=='_audio')
    {
        $out.="<br/><center><object id='audioplayer1352' width='300' height='40'><embed 	
				src='/jscripts/lib/uppod.swf' 	
				type='application/x-shockwave-flash'
				allowscriptaccess='always' 
				wmode='opaque' 
				flashvars='comment={$mm['name']}&amp;st=/cont/files/audio-diz.txt&amp;file={$mm['d1']}' 
				bgcolor='#ffffff' width='300' height='40'></embed></object></center><br/>";
    }
    return $out;
}

function langswich()
{
    $hr=$GLOBALS['page']=='index' ? '' : $GLOBALS['page']  ;
    $tpl=shab('langswich_tpl');
    $rep['[hr]']=$hr;
    $rep['[lngsuf]']=lngsuf;
    //v($tpl);
    return strtr($tpl,$rep);
}


function 	main_body($_is_ajax=false)
{
    // $_is_ajax указываем если аяктовый запрос либо хотим принудительно отрубить движковые ссылки на редактирование
    /*
	 * прокидываем $m (контент-массив текущей страницы)
	 *
	 */
    global $m,
           $allrep; // массив всеобщей замены перед выводом

    //  TODO что это?
    $GLOBALS['cache']['startbody']='body';
    vl( $m ); // логируем


    if (!checkgroup(($m['d97'])) or !(checkdomain($m['d88']))  ) {
        return  o('err_auth') ? o('err_auth') : '<h2> У вас нет прав доступа к данной странице</h2>';
    }

    if ( inadm() and !$_is_ajax ) {

        $GLOBALS['inadm']=1;

        // вясняем id подкатегорий фильтра, либо оставляем id текущей страницы
        $allrep['[idedit]']=$allrep['[idedit]']? $allrep['[idedit]'] : $m['id'];

        // генерируем ссылку на редактирование
        $hr=basehref."/myadmin.php?id=[idedit]&htitle={$m['name']}&mode=";

        // TODO вынести в tpl
        $bod.="<div id='outcadmenu'><div id='cadmenu'>
		<a href='{$hr}1'>Свойства</a> ";
        $bod.="<a href='{$hr}0'>Настройка</a> ";
        $bod.="<a href='{$hr}2'>Редактор</a> ";
        $bod.="</div></div>";
        if (rg(9)) $bod.='<div id="coord"></div>';

        /**
         * сохранялка координат плавающих элементов
         */
        $bod.='<div id="savepage" style="display:none;"><form method="post" action="" onsubmit="return"><input type="hidden" name="dragged" id="dragged"><input type="hidden" name="curbody" id="curbody"></form><input id="savepage_but" onclick="savepage()" type="submit" name="savepage" value="Сохранить"></div>';
    }
    else
    {
        $GLOBALS['inadm']=0;
    }
    if ( !o('navigatmenu') // если не активирована опция отключения хлебных крошек
        and
        $m['ident']!=='index' // если данная страница не является главной
        and
        !$_is_ajax ) // если данная страница небыла вызвана через ajax
    {
        //вывод шаблона пост вставки хлебных крошек крошек
        $bod.=smarty_tpl('hleb_krosh_tpl',['role'=>'main_wrap']);

        // генерируер хлебные крошки и назначает на ленивый вывод
        $allrep['[navigat]'] = fn('create_navigation',$m['ident']);

    }
    // TODO расшифровать (Ю)
    if ( !( (bool) $m['d10'])
        |
        ( (bool)$GLOBALS['userdata']['session_logged_in'] )
    )
    {
        // если страница корзина
        if ($m['ident']=='cart')
        {
            // разбиваем на верхнюю и нижнюю части контент
            $exp=explode('<!-- pagebreak -->',fgc($m['ident'].$GLOBALS['lng']) );
            $bod.=$exp[0];

            // вызов обработчика ( контроллера)  корзины
            $bod.=  smarty_tpl('cart_controller') ;

            // выводим нижнюю (вторую) часть текста
            $bod.='<div style="clear:both;">'.$exp[1].'</div>';
        }
        elseif ($m['type']=='page'   )
        {
            if ($m['d4'] AND $m['d4']!==' ') { $allrep['[h1]']='<h1>'.lang($m['d4']).'</h1>'; $bod.='[h1]'; }
            $bod.=fgc($m['ident'].$GLOBALS['lng']);
            if ($m['d3']) $bod.=invideo();
            $bod.= view_album();
            if ( ( $m['d7']==1 ) |( $m['d7']=='prev' )    )
            {
                $bod.= cmenu::x($m['ident']);
            }
        }
        elseif ( ($m['type']=='category') OR  $m['type']=='bmenu' )
        {
            if ($GLOBALS['art'])
            {
                $_POST['price']=qvar('ident',$GLOBALS['art'],'price');
                //$_POST['num_tov']=1;
                tov( $GLOBALS['art'] );
            }
            $allrep['[h1]']=(isset( $allrep['[h1]'])? $allrep['[h1]'] : '<h1>'.(lang( $m['type']=='bmenu' ? $m['d5'] :  $m['d4'])).'</h1>');
            $bod.='[h1]';  //<span class="desc">'.lang($m['desc']).'</span>';
            $exp=explode('<!-- pagebreak -->',fgc($m['ident'].$GLOBALS['lng']) );
            $bod.= view_album();
            $cmenu_ident=$m['sub']['cmenu_ident'] ? $m['sub']['cmenu_ident'] : $m['ident'];

            // hide cmenu for block menu page
			$is_show_cmenu =  $m['type']!=='bmenu' OR ($m['type']=='bmenu' AND !o('bmenu_cmenu_hide'));

			if (o('fullsearch_ver') and $m['d6'] )
            {
                $allrep['[exp0]']= $allrep['[exp0]'] ?  smartyfull($allrep['[exp0]'],1)  : smartyfull( ($exp[0]) , 1 ) ;
                $allrep['[exp1]']= $allrep['[exp1]'] ?  smartyfull($allrep['[exp1]'],1)  : smartyfull( ($exp[1]) , 1 ) ;

                if ( !$_GET['p'] ) {
                    $wrap1='[exp0]';
                    $wrap2='[exp1]';
                }

                    $bod .= $is_show_cmenu ? $wrap1 . cmenu2::x($cmenu_ident) . $wrap2 : '';
            }
            else
            {
                if ($GLOBALS['longpage'][1])
                {
                    $bod.= cmenu::x($cmenu_ident);
                }
                else
                {
                    $bod.=$exp[0];
                    $bod.= $is_show_cmenu ? cmenu::x($cmenu_ident,$exp[0]) : '';
                    $bod.='<div style="clear:both;">'.$exp[1].'</div>';
                }
            }


        }
        elseif ($m['type']=='tov' )
        {
            $bod.=tov($m['ident']);
        }
        elseif ($m['type']=='_news')
        {
            $allrep['[h1]']='<h1>'.lang($m['d4']).'</h1>'; $bod.='[h1]';
            $img='cont/img/cat/mid/'.$m['ident'].'.jpg';
            $bod.=( file_exists($img) ? '<a class="imgbig" href="/cont/img/cat/big/'.$m['ident'].'.jpg"><img class="newsmimg" src="/'.$img.'" /></a>' : '' );
            $bod.=fgc($m['ident'].$GLOBALS['lng']);
            $bod.= view_album();
        }
        elseif ($m['type']=='bnews')
        {
            $bod.= nmenu($m['ident']);
        }
    }
    else
    {
        $bod.= fgc('goauth'.$GLOBALS['lng']);
    }
    //v(extractor('<img class="imgbox"','>',$bod) );
    //$GLOBALS['cache']['startbody']='';


    if( $GLOBALS['repcont'] )
    {
        return strtr( $bod, $GLOBALS['repcont'] ).$GLOBALS['contentfooter'];
    }
    else
    {
        return  $bod.$GLOBALS['contentfooter'];
    }

}

// TODO нужна справка по данной
function nsmartpl($template,$num_part,$rep_array=FALSE )
{
    $tpl=(explode('-t-',tpl($template)));

    //v($num_part);
    //v($tpl[$num_part]);

    if ( !$rep_array )
    {
        return $tpl[$num_part];
    }
    else
    {
        return strtr($tpl[$num_part],$rep_array);
    }
}

/**
 * Получение значения опции
 * Данное значение берётся из контент-объекта по его идентификатору
 */
function o($ident,$value_d1='null')
{
    if ( $value_d1!=='null')	{
        $data['d1']=$value_d1;
        mydbUpdate('content',$data,'ident',$ident );
        unset($GLOBALS['cache'][$ident]);
    } else {
    	if (defined($ident) AND constant($ident)) {
    		return constant($ident);
		}
        $ident=theme($ident);
        if (!isset($GLOBALS['cache'][$ident])) {

            $mm=( qvar('ident',$ident) ) ;
            if ($mm) {

                rego( $mm );
            } else {
            	$GLOBALS['cache'][$ident]=false;
			}
            //v();
        }

        //if($mm['d1']!==$GLOBALS['cache'][$ident])  v( $mm );

        return ($GLOBALS['cache'][$ident]);
    }
}

function rego($mm)
{
    $ident=$mm['ident'];
    //vl($mm);
    if ( $mm['type']=='_modul' ) {

        $GLOBALS['cache'][$ident] =  $mm['ok'] ? : ( inadm() ? $mm['d2'] : false )  ;

    } elseif ( $mm['d5']=='on' and  is_rep ) {
        //v('repval:'.$ident);
        $GLOBALS['cache'][$ident] =  ($mm['d6']);
    } else  {
        $GLOBALS['cache'][$ident] =  $mm['d1']  ;
    }
}

function p ($name,$value='null',$folder='null')
{
    if ( $value!=='null' ) {
        $data['value']=$value;
        $data['folder']=$folder=='null' ? '': $folder;
        if ( mydbget('nparams','name',$name,'id') ) {
            mydbUpdate('nparams',$data,'name',$name );
        } else {
            $data['name']=$name;
            mydbAddLine('nparams',$data);
        }
    } else {
        return mydbget('nparams','name',$name,'value');
    }
}



function opros($name_opros)
{
    global $page;
    //v($_COOKIE);
    $name_opros= $name_opros ? $name_opros : $_POST['name_opros'] ;
    //$prebase=my_mysql_query( 'SELECT * FROM '.pref_db.'content WHERE ident='.xs($name_opros).' ORDER BY `num` ASC ' ) ;
    $mop=qvar('ident',$name_opros);// mysqli_fetch_array($prebase);
    if ($_POST and @$_POST['id_opros']==$mop['id'])
    {
        if (isset($_POST['id_ans']) )
        {
            my_mysql_query("INSERT INTO `".pref_db."opros` (`id_opros`,`id_ans` , `viz_id`)
				VALUES ('".intval($_POST['id_opros'])."','".intval($_POST['id_ans'])."', '".intval($GLOBALS['viz_id'])."') ;	");
        }
        if (isset($_POST['ans']))
        {
            //v($_POST['ans']);
            $ans=array_keys($_POST['ans']);
            foreach ($ans as $value)
            {
                //echo "Value: $value<br/>\n";
                my_mysql_query("INSERT INTO `".pref_db."opros` (`id_opros`,`id_ans` , `viz_id`)
					VALUES ('".intval($_POST['id_opros'])."','".intval($value)."', '".intval($GLOBALS['viz_id'])."') ;	");
            }
        }
        setcookie('id_opros['.$mop['id'].']',1,time()+3600*24*30);

        $id_opros[$mop['id']]=1;

    }
    @$id_opros= $id_opros ? $id_opros : $_COOKIE['id_opros'] ;
    if (@$id_opros[$mop['id']]) //построение
    {
        //v($mop['id']);
        $vsego=mysqli_num_rows(my_mysql_query('SELECT id FROM '.pref_db.'opros WHERE id_opros="'.$mop['id'].'"  ','vsego') );
        $prebase=my_mysql_query('SELECT * FROM '.pref_db.'content WHERE rod='.xs($name_opros).'   ORDER BY `num` ASC ');
        $m=mysqli_fetch_array($prebase);
        $out='<div class="oprosname">'.$mop['name'].'</div>';//<h3>Опрос</h3>
        $vsego1=mysqli_num_rows(my_mysql_query('SELECT id FROM '.pref_db.'opros WHERE id_opros="'.$mop['id'].'" GROUP BY viz_id ','vsego1') );
        $vsego2=mysqli_num_rows(my_mysql_query('SELECT id FROM '.pref_db.'opros WHERE id_opros="'.$mop['id'].'"  ','vsego2') );
        if ($mop['bok'])
        {
            $vsego=$vsego1;
        }
        else
        {
            $vsego=$vsego2;
        }
        $out.="<p style='text-align:center;'>Всего голосов - $vsego </p><br/>";
        while ($m)
        {
            $ans= mysqli_num_rows(my_mysql_query('SELECT id FROM '.pref_db.'opros WHERE id_opros="'.$mop['id'].'" AND id_ans='.$m['id'],'ans1929') );
            if ($mop['bok'])
            {
                @$percent=intval((100*$ans/$vsego1));
            }
            else
            {
                @$percent=intval((100*$ans/$vsego2));
            }
            $out.="<p>$m[name] $ans ($percent%)<br/>";
            $out.='<table width="90%"  height="10" border="0" cellpadding="0" cellspacing="0"><tr><td width="'.($percent+1).'%" class="colopr spacer">&nbsp;</td><td class="spacer">&nbsp;</td></tr></table></p><br/>';
            $m=mysqli_fetch_array($prebase);
        }
    }
    else
    {
        $prebase=my_mysql_query( 'SELECT * FROM '.pref_db.'content WHERE rod='.xs($name_opros).'   ORDER BY `num` ASC ' , '1949') ;
        $m=mysqli_fetch_assoc($prebase);
        $out='<div class="oprosname"><span>'.$mop['name'].'</span></div>';
        $out.='<form class="answer" method="post" action="" onsubmit="return opros_set(this)">';
        $out.='<input type="hidden" name="name_opros" value="'.$name_opros.'" />';
        while ($m)
        {
            if ($mop['bok'])
            {
                $out.='<div><label><b><input type="checkbox" name="ans['.$m['id'].']"/></b> '.$m['name'].'</label></div>';//checkbox"
            }
            else
            {
                $out.='<div><label><b><input type="radio" name="id_ans" value="'.$m['id'].'"/></b> '.$m['name'].'</label></div>';
                //<input type="" name="RadioGroup1" radio" id="RadioGroup1_0" />
            }
            $m=mysqli_fetch_array($prebase);
        }
        $out.='<center>'.ibut('otvetit').'</center>';
        $out.='<input type="hidden" name="id_opros" value="'.$mop['id'].'" />';
        $out.='</form>';
    }
    return $out;
}

function par($mm, $field) //2
{


    if (  ( ($mm[$field]=='' ) | (substr($mm[$field],0,6)=='parent') ) & ($mm['rod']!=='') )
    {

        $mm2=qvar('ident',$mm['rod']);
        $out=par($mm2,$field);
    }
    else
    {
        $out=$mm[$field];
    }
    //v($out);
    return $out;
}

function prefup($ident)
{
    $ident=explode('-',$ident);
    $kon=$ident[count($ident)-1];
    if ( is_numeric($kon))
    {
        $ident[count($ident)-1]=$kon+1;
    }
    else
    {
        $ident[count($ident)-1]=$kon."-1";
    }
    $ident=implode('-',$ident);
    return $ident;
}

function q($query)
{
    $add2=func_get_arg(1);
    $t=subst($query);
    if ($t='.')
    {
        $fff='ident';
    }
    elseif ($t='#')
    {
        $fff='id';
    }
    $qu='SELECT * FROM `'.pref_db.'content` WHERE `$fff`='.$val_find.' limit 0,1 ';
    $out=mysqli_fetch_array(my_mysql_query($qu));
    return $out[$field];
}

function utm_exists(){
    foreach($_GET as $k=>$v) {
        if ( substr( $k, 0 , 3)=='utm' ) return true;
    }
}

function ev($event_ip_uid_email_phone_viz_id_in_url_referer_ts) {
    if ( is_array($event) ) {
        extract($event);
    } else {
        $event=$event_ip_uid_email_phone_viz_id_in_url_referer_ts;
    }

    $referer=str_replace( basehref, '', $_SERVER['HTTP_REFERER'] );
    $referer_domain= explode( '/' , $_SERVER['HTTP_REFERER'] )[2];
    $site_domain= explode( '/' , basehref )[2];
    $is_utm_exists=utm_exists();

    if (!$referer AND !$is_utm_exists  ) {
        return;

    }

    if (
        (
            $referer_domain
            AND
            $referer_domain!==$site_domain
        )
        or
        (
            $is_utm_exists AND !$referer
        )

    )

    {
        $subevent=  'coming' ;
    }

    if ( $event=='pageload' AND $subevent!=='coming' AND !o('user_hit_log') ) {
        return;

    }
    $extime_gen=extime_gen();
    $data=[
        'event'=>$event,
        'subevent'=>$subevent,
        'ip'=>$_SERVER['REMOTE_ADDR'],
        'uid'=>uid(),
        'email'=> $email ?: $GLOBALS['userdata']['email'],
        'phone'=> $email ?: $GLOBALS['userdata']['phone'],
        'viz_id'=> $_COOKIE['viz_id'],
        'in_url'=> $in_url ? : $_SERVER['REQUEST_URI'],
        'referer'=> $referer ,
        'extime'=>$extime_gen['extime'],
        'extime_raw'=>$extime_gen['extime_raw'],
    ];

    return mydbAddLine ( 'user_events', $data );
}
function extime_gen(){
    foreach (  $GLOBALS['extime'] as $k=>$v ) {

        if ( $x ) {
            $t = round ( ( $v-$v_old ) * 1000 ) ;
            $extime_raw.=$k.':'.$t.PHP_EOL;
            $extime+=$t;
        }

        $v_old=$v;
        $x++;
    }
    return ['extime_raw'=>$extime_raw,'extime'=>$extime ];
}

/**
 * Выясняет, соответствует ли текущий уровень админки указанному уровню
 * Пример
 * Чел в админке врубил продвинутый уровень ( chreg=5), но мы вызвали rg(9), котрое вернёт false. Таким образом мы можеш скрывать какието части админки или функции для менее продвинутых пользователей
 * @param $reg
 * @return bool
 */
function rg($reg)
{
    if ( intval( reg )<= intval( $_COOKIE['adreg'] ) )
    {
        return true;
    } else {
        return false;
    }
}

function formsec($form_ident)
{
    if ( !$GLOBALS['cache'][$form_ident]['secform'] )
    {
        $form=form($form_ident);
        $GLOBALS['cache'][$form_ident]['secform']['ivals']=$GLOBALS['ivals'];
        $GLOBALS['cache'][$form_ident]['secform']['ivalues']=$GLOBALS['ivalues'];
        $GLOBALS['cache'][$form_ident]['secform']['ifiles']=$GLOBALS['ifiles'];
    }
    return $GLOBALS['cache'][$form_ident]['secform'];
}

function price_sec_val($form_ident,$form_html_var)
{
    $formsec=formsec($form_ident);
    $out=$formsec['ivals'][$form_html_var][$_POST[$form_html_var]] ;
    //ob_get_clean();
    //v($GLOBALS['cache'][$form_ident]['secform']);
    //$out=ob_get_clean();
    return $out;
}

function right($text,$int_right)
{
    $ww=strlen($text);
    return substr($text,$ww-$int_right);
}

function smarty($sourse,$_istext=false, $_params_arr=false  )// выщитывалка
{
    global $mero, $head,$menu,$body,$footer,$stk, $header, $meta, $m, $num,$mag2, $p, $mm  ;



    if ( $_istext==1 )
    {
        $in=$sourse;
    }
    else
    {
        $in = file_get_contents ($sourse) ;
    }

    if (1) {

        eval( smarty_gen ($in) );

        return $sm_str;

    } else {
        /*$in = '$str=""; ?>'.$in."<?" ;
		$strtemp = str_replace( "?>" , "\n".'$str.'."= <<<EOD\n", $in );
		$strtemp = str_replace( "<?=" , "\n"."EOD;"."\n".'$str.=' , $strtemp );
		$out = str_replace( "<?" ,  "\n"."EOD;"."\n" , $strtemp );

		//smarty_gen ($in);
		eval($out);
		//$str=$out;
		//return $sm_str;
		return $str;*/
    }
}

function smarty_gen($in)// выщитывалка
{
    return  str_replace(
        array(
            '?>',
            '}}',
            '<?=' ,
            '{{' ,
            '<?'
        ),
        array(
            "\n".'; $sm_str.'."= <<<EOD\n",
            "\n".'; $sm_str.'."= <<<EOD\n",
            "\n"."EOD;"."\n".'; $sm_str.=' ,
            "\n"."EOD;"."\n".'; $sm_str.=' ,
            "\n"."EOD;"."\n"
        ),
        '$sm_str=""; ?>'.$in."<?"
    );
}

function sender ($sendform=false,$email=FALSE)
{
    $GLOBALS['cache']['sendform']=($sendform);
    $GLOBALS['cache']['mailto']=$email;
    //v($GLOBALS['cache']);
    return smarty(jscripts.'/tpl/send.php',0);
}

function theme_file($file_name) {

    $GLOBALS['bedug_ur']++;


    list( $tpl_file, $new_role ) = explode('/', theme($tpl_file) );

    $role=$new_role ? : $role;

    $tpl_file=theme($tpl_file);


}

function smarty_tpl( $tpl_file, $p=false, $tpl_content=FALSE, $tpl_dir=false ) {


    if (is_array($tpl_file)) extract($tpl_file);

    $tpl_dir=jscripts.'/tpl';

    extract($p);


    vl( $p, 'tpl/'.$tpl_file.' params'  );
    $GLOBALS['bedug_ur']++;

    // theme: cart_table/head cart_table_mob/head
    // $tpl_file: cart_table/foot

    // theme: cart_table cart_table_mob
    // $tpl_file: cart_table/foot


    list( $tpl_file, $new_role ) = explode('/', theme($tpl_file) );

    $role=$new_role ? : $role;

    $tpl_file=theme($tpl_file);

    if ( $tpl_content ){
        $in=$tpl_content;
    }
    elseif ( $in=shab( $tpl_file ) ) {
    }
    else {
        $in = file_get_contents( file_exists( $tpl_dir.'/'.$tpl_file.'_local.php' ) ? $tpl_dir.'/'.$tpl_file.'_local.php' : $tpl_dir.'/'.$tpl_file.'.php' ) ;
        $GLOBALS['sh_'.$tpl_file]=$in;
    }

    vl( $in, 'tpl/'.$tpl_file.' tpl'  );

    if ( !$in ) {
        $return = ('Пустой шаблон '.$tpl_file.'/'.$role );
    } else {

        $GLOBALS['bedug_ur']++;
        eval( (smarty_gen(  (inadm() AND !$not_inadm_comment ) ? '<!-- START '.$tpl_file.'/'.$role.' -->'.$in.'<!-- END '.$tpl_file.'/'.$role.' -->' : $in )  ) );
        $GLOBALS['bedug_ur']--;

        if ( !$sm_str )  {
            $return = ('Ошибка в шаблоне '.$tpl_file.'/'.$role );
        } else {
            $return = vl( str_replace( '<!-- sm_check -->','', $sm_str) , 'tpl/'.$tpl_file.'/'.$role.' out'  ); ;
        }


    }
    $GLOBALS['bedug_ur']--;
    return $return;
}

function addfogot ()
{
    return smarty(jscripts.'/tpl/fogot.htm',0);
}

function order($key, $_value=false ) {
    if ( $_value ) {
        $GLOBALS['cache']['cart'][$key]=$_value;
    }

    return $GLOBALS['cache']['cart'][$key] ;

}

function addregister ()
{
    return smarty(jscripts.'/tpl/register.htm',0);
}

function super_explode($line_separator,$sel_separator,$str)
{
    $prostr=explode($line_separator,$str);
    foreach ($prostr as $p)
    {
        $out[]=explode($sel_separator,$p);
    }
    return $out;
}

function super_explode_n($sep,$str)
{
    $prostr=explode("\r\n",$str);
    for ($x=0;$x< sizeof($prostr);$x++)
    {
        $out[$x]=explode($sep,$prostr[$x]);
    }
    return $out;
}


function explode_assoc($sep,$str) {
    return explode_ass($sep,$str);
}

function explode_ass($sep,$str)
{

    foreach ( explode("\n",$str)  as $k=>$line)
    {
        $ln=explode($sep, $line);
        if ( trim( $ln[0] ) )  $out[ trim( $ln[0] ) ] = trim ( $ln[1] );
    }
    return $out;
}

function sv()
{
    $unshow = "AUTH_TYPE DOCUMENT_ROOT GATEWAY_INTERFACE GLOBALS HTTP_ACCEPT HTTP_ACCEPT_CHARSET HTTP_ACCEPT_LANGUAGE HTTP_CACHE_CONTROL HTTP_COOKIE HTTP_COOKIE2 HTTP_COOKIE_VARS HTTP_ENV_VARS HTTP_GET_VARS HTTP_HOST HTTP_POST_FILES HTTP_POST_VARS HTTP_PRAGMA HTTP_SERVER_VARS HTTP_USER_AGENT HTTP_VIA HTTP_X_FORWARDED_FOR PATH PHP_AUTH_PW PHP_AUTH_USER PHP_SELF QUERY_STRING REMOTE_ADDR REMOTE_PORT REMOTE_USER REQUEST_METHOD REQUEST_TIME REQUEST_URI SCRIPT_FILENAME SCRIPT_NAME SERVER_ADDR SERVER_ADMIN SERVER_NAME SERVER_PORT SERVER_PROTOCOL SERVER_SIGNATURE SERVER_SOFTWARE VHOST_DOCUMENT_ROOT VHOST_TARGET_UID VHOST_TARGET_USERNAME _COOKIE _ENV _FILES _GET _POST _REQUEST _SERVER argc argv dump HTTP_REFERER base_db host_db pass_db user_db";
    $unssh=explode(" ",$unshow);
    $unssh=array_flip($unssh);
    $glob_keys=array_keys($GLOBALS);
    //v($unssh);
    //$glob_keys=array_flip($glob_keys);
    //v($glob_keys);
    //$diff=array_diff($unssh,$glob_keys);
    sort($glob_keys);
    //v($diff);
    ?>
    <table   cellpadding="5">
        <?
        foreach ($glob_keys as $gl)
        {
            if (!isset($unssh[$gl]))
            {
                ?>
                <tr>
                    <td><b><?=$gl ?></b></td>
                    <td><? var_dump($GLOBALS[$gl]) ?></td>
                </tr>
                <?
            }
        }
        ?>
    </table>
    <?
}

function val2id($valls, $tgrp, $tovfld, $_url=false )
{
    $valls=explode(';',$valls);
    foreach ($valls as $val)
    {
        $val=(trim($val));
        $tovfldarr=tovsetsfields(tovgroupsets_get('d1',$tgrp));
        if ($val!=='')
        {

            $val=str_replace(array('ё'),array('е'), $val );
            $grpfld=$tgrp.':'.$tovfld;

            $mm=mydbGetSub('content',array( 'd4'=>$grpfld,  'd3'=>$val) );

            if ( $mm )
            {
                $out[]=  $mm[0]['d1'];
            }
            else
            {
                /*$plast=p('tov_val_id_last')+1;
                                p( 'tov_val_id_last', $plast );*/
                $plast=rand(1000000,9999999);

                $pr= my_mysql_query('SELECT max(num) maxnum FROM `'.pref_db.'content`');
                $mn=mysqli_fetch_array($pr);

                $urlpref=($tovfldarr[$tovfld]['d5']!=='num' AND $tovfldarr[$tovfld]['d13'] ) ? $tovfldarr[$tovfld]['d13'].'-' : '';

                $repurl=[
                    '+sl+'=>'-',
                    '/'=>'-',
                    ','=>'-',
                    '\\'=>'-',
                ];

                mydbAddLine('content',array(
                        'rod'=>'tov_values',
                        'd1'=>$plast,
                        'd2'=> strtr( $urlpref.($_url ? $_url : newident('',$val,1)), $repurl),
                        'd3'=>$val,
                        'd4'=>$grpfld,
                        'ident'=>'tgrp_'.$tgrp.'_'.$tovfld.'_'.$plast,
                        'type'=>'tovval',
                        'num'=>$mn[0]+1
                    )
                ) ;
                $out[]= $plast;

            }
        }
    }
    return implode(',',$out);

}

function val_fromid($ids, $_full=false, $_field=false)
{
    foreach ( explode(',',$ids ) as $id )
    {
        if (($id))
        {
            $id=trim($id);
            $mm=(mydbGetSub('content',array( 'type'=>'tovval',  'd1'=>$id ) ) ); //tgrp_vr_col_8
            if ($mm[0]['d1'])
            {

                $mm[0]['name'] =$mm[0]['name'] ? $mm[0]['name']  : $mm[0]['d3'];

                $out[]= $_field ?  $mm[0][$_field] : $mm[0];


            }
            else
            {
                $out[]= '[id'.$id.']';
            }
        }
    }

    return $_field ? implode('; ', $out) : $out ;
}

function minimodparser($shab_minimod, $mm=false)
{
    // Размер:30,32,34--1000,36,40; Цвет--500; Высота -100--200
    $x=0;
    if ($shab_minimod)
    {
        $s1=explode(';',$shab_minimod);
        foreach ( ($s1) as $s2)
        {
            $x++;
            $s3=(explode(':', trim($s2)));	// Cвойство:ВАриант

            if ($s3[1])
            {
                $out[$x][0]=  trim($s3[0]) ;
                $s4=( trimarr( explode(',', $s3[1] ) )); //перечень значений
                $xx=0;
                foreach ($s4 as $s5)
                {
                    $xx++;
                    $s6=(explode('--',($s5)));
                    $out[$x][1][$xx][0]=trim($s6[0]);
                    $out[$x][1][$xx][1]=trim($s6[1]);

                }
            }
            else
            {
                $s4=trimarr( explode('--', ($s3[0] )) ); //перечень значений
                $out[$x][0]=  trim($s4[0]) ;
                $out[$x][2]=  trim($s4[1]) ;
            }
        }
        return $out;
    }
}

function minimodparser_assoc($shab_minimod )
{
    // Размер:30,32,34--1000,36,40; Цвет--500; Высота -100--200
    $x=0;
    if ($shab_minimod)
    {
        $shab_mini=explode(';',$shab_minimod);
        foreach ( $shab_mini  as $shab_min )
        {
            $x++;
            $shab=(explode(':', trim($shab_min )));	// Cвойство:ВАриант

            if ($shab[0] ) // key
            {

                $vals=( trimarr( explode(',',$shab[1] ) )); //перечень значений

                foreach ($vals as $val)
                {
                    $out[ trim($shab[0]) ][]=trim($val);
                }
            }

        }
        return $out;
    }
}

function parsetable_assoc ($in)
{
    $shab_mini=explode("\n", $in   );

    foreach ( $shab_mini  as $shab_min )
    {
        $shab=( explode('=', trim($shab_min )));	// Cвойство:ВАриант

        if ($shab[0] ) // key
        {
            $out[ trim($shab[0]) ]=trim($shab[1]);

        }

    }
    return $out;
}

function cron( $fn, $params_arr=false, $prioriter_int=0, $ts_start=FALSE, $msleep=FALSE ) {
    mydbAddLine('cron',array(
        'fn'=>$fn,
        'params'=> $params_arr ? serialize( $params_arr ) : NULL ,
        'prioritet'=>$prioriter_int,
        'ts_start'=>$ts_start,
        'msleep'=>$msleep
    ));
}

function minimod($shab_minimod, $mm=false )
{
    //  Размер:30,32,34,36,40;Цвет:Белый,Красный
    if  ( ($shab_minimod) )
    {
        $s1 = fn ('minimodparser', $shab_minimod, $mm);
        //$x=0
        //v($s1);
        foreach ($s1 as $k=>$s2)
        {
            $s2[2]=   price_auto_cop( ($s2[2]) );  // коректировка отображения цены

            if ( $s2[0] and $s2[1])
            {
                $rs=( substr( trim($s2[0]), 0 , 3 )  );

                $rs=	($rs=='[r]' or $rs=='[s]' ) ? $rs : false;

                if($rs) $s2[0]=str_replace(array('[r]','[s]'),'', $s2[0] );

                if(  $rs=='[r]' or ( $rs!=='[s]' and o('tov-spis-radio') )  )
                {

                    $labels='';
                    $xx=0;
                    foreach ($s2[1] as $kk=>$s3)
                    {
                        // <option value="Чебурашка">Чебурашка</option>
                        $s3[1]= o('tov_noprice') ? ''  : $s3[1];
                        $idLabel='L'.$k.$kk;
                        $labels.='<tr><td  width="1" class="minimod_radio"><input id="'.$idLabel.'" '.(!$labels?'checked="checked"':'').' type="radio" name="opts['.$k.']" rel="'.$s3[1].'" value="'.($kk).'"/></td><td><label for="'.$idLabel.'">'.trim($s3[0]).'</label></td><td width="1">'.($s3[1]?'&nbsp;&nbsp;<b>'.$s3[1].'&nbsp;[val]</b> ':'&nbsp;').'</td></tr>';
                    }
                    $out.='<tr><td class="minimod_name_radio"><b>'.mb_ucasefirst($s2[0]).':</b></td></tr>
					<tr><td colspan="2" ><table style="margin-left:0;">'.$labels.'</table></td></tr>
					<tr><td colspan="2" class="lastr">&nbsp;</td></tr>';
                }
                else
                {
                    $labels='';
                    $xx=0;
                    list($optionname_opt,$optionname_name)= explode('#',$s2[0]);

                    if( $optionname_opt=='inselect' )
                    {
                        $labels.='<option  value="">'.$optionname_name.'</option>';
                    }
                    foreach ($s2[1] as $kk=>$s3)
                    {
                        // <option value="Чебурашка">Чебурашка</option>
                        $s3[1]= o('tov_noprice') ? ''  : $s3[1];

                        $labels.='<option rel="'.$s3[1].'" value="'.($kk).'">'.$s3[0].($s3[1]?' +'.$s3[1].' [val]':'').'</option>';
                    }
                    $out.='<tr><td class="minimod_select_name"> '.$s2[0].'</td><td class="minimod_select"><select name="opts['.$k.']">'.$labels.'</select></td></tr>
					<tr><td colspan="2" class="lastr">&nbsp;</td></tr>';
                }
            }
            elseif( $s2[0]  )
            {
                $s2[2]= o('tov_noprice') ? ''  : $s2[2];
                $out.='<tr>
							<td  class="optcheck">
								<table width="100%">
									<tr>
										<td width="1" >
											<input type="checkbox" name="opts['.$k.']" id="opts'.$k.'"/>
											<input type="hidden" id="popts'.$k.'" name="popts['.$k.']" value="'.$s2[2].'"/>
										</td>
										<td width="1">'.($s2[2]?'&nbsp;&nbsp;<b>'.$s2[2].'&nbsp;[val]</b>&nbsp;&nbsp;':'').'</td>
										<td class="minimod_checkbox_name">'.mb_ucasefirst($s2[0]).'</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="2" class="lastr">&nbsp;</td></tr>';
            }
            $x++;
        }
        return '<table class="minimod">'.$out.'<tr id="itogo"><td colspan="2"></td></tr></table>';
    }
}

function trim_value( &$value )
{
    $value= trim($value);
}

function filename($filename)
{

    $filename=explode('.',$filename);
    array_pop($filename);
    if (sizeof($filename)-1)
    {
        return implode('.',$filename  );
    }
    else
    {
        return $filename[0];
    }

}


function trimarr (&$arr)
{
    array_walk_recursive(  $arr , 'trim_value' );
    return $arr;
}

function ibut($ident,$name=false)
{
    $mm=qvar('ident', $ident);


    $val=$mm['sub']['but_text'] ? ($name? $name: ( $mm['sub']['nameout'] ?   $mm['sub']['nameout'] :   $mm['name']  )  ) : ' ' ;

    return '<input  class="'.$ident.' '.( $mm['sub']['class'] ? $mm['sub']['class'] : 'ibut_fonts_stand' ).'" type="submit" name="web_form_submit" value="'.$val.'" />';
}

function fonts($mm, $adfont=false )
{

    $font=array(
        'arial'=>'Arial, Helvetica, sans-serif',
        'verdana'=>'Verdana, Helvetica, sans-serif',
        'times'=>'Times New Roman, Times, serif',
        'courier'=>'Courier New, Courier, monospace',
        'georgia'=>'Georgia, Times New Roman, Times, serif',
        'geneva'=>'Geneva, Arial, Helvetica, sans-serif',
        'Tahoma'=>'Tahoma, Geneva, sans-serif',
        'Arial Black'=>'"Arial Black", Gadget, sans-serif',
        'Palatino Linotype'=>'"Palatino Linotype", "Book Antiqua", Palatino, serif',
        'Lucida Sans Unicode'=>'"Lucida Sans Unicode", "Lucida Grande", sans-serif',
        'MS Serif'=>'"MS Serif", "New York", serif',
        'Lucida Console'=>'"Lucida Console", Monaco, monospace',
        'Comic Sans MS'=>'"Comic Sans MS", cursive'
    );

    if (is_array($mm))
    {
        $out= $mm['sub']['adfont'] ? $mm['sub']['adfont'] : $font[$mm['d9'] ];
    }
    else
    {
        $out=  $adfont ? $adfont :  $font[$mm] ;
    }
    return str_replace('font-family:','',$out);
}

function object2array($object) {
    if (is_object($object)) {
        foreach ($object as $key => $value) {
            $array[$key] = object2array ($value);
        }
    }
    else {
        $array = $object;
    }
    return $array;
}

function tov($ident){
	return fn('tov::main',$ident);
}

/**
 * Подменяет $_POST значения на текстовые (для людей) значения из select и radio полей
 *
 * @param $form_ident Идентификатор корня формы, в которой находятся нужные поля
 * @param $post  масиив присланный методом POST
 * @return array
 */
function form_fill_all ($form_ident, $post ) {

    $getform=form( $form_ident ) ;

    foreach ($GLOBALS['inames'] as $k=>$v) {
        if ( $GLOBALS['ivalnames'][$k][$post[$k]] )	{
            $val=$GLOBALS['ivalnames'][$k][$post[$k]]; // значение подставленное вместо POST значения для seleсt или radio
        } else {
            $val=$post[$k]; // значение непосредственно присланное их формы пользователем
        }
        // $itogform_arr['sposopl']=['name'=>'Способ оплаты', 'val'=>'Наличными пр иполучении' ]
        if ( isset( $post[$k] ) AND $val ) $itogform_arr[$k]= [ 'name'=>$v, 'val'=>$val, 'post_val'=> $post[$k] ];

        //if ($v) $itogform[]= '<b>'.$v.':</b> '.$val.'<br>';
    }
    return $itogform_arr;
}

function form_fill ( $select_ident, $post_val ) {
    if ( qvar('ident',$select_ident,'type')=='spis'  ) {
        list($mm)= mydbGetSub('content',array('rod'=>$select_ident, 'd1'=>$post_val ) );
        return $mm['name'];
    } else {
        return $post_val;
    }

}

function othertov($mmd8, $ident, $tov_tpl_opts ) {
    //v($mc_border_shab);
    //
    if ($mmd8)  {


        foreach ( explode(' ', str_replace( '%20', ' ', $mmd8 ) ) as $oth ) {

            $mm= fn('mydb_query', 'SELECT * FROM '.pref_db.'content WHERE type=\'tov\' AND (  id='.xs($oth).' OR ident='.xs($oth).' ) ' ) ;


            if ( $mm['ident']!==$ident and $mm['ok'] AND $mm['type']=='tov'  AND !$mms[$mm['id']]  ) {
                $out.=fn('tovmc::x', $mm,    $tov_tpl_opts  );
                $mms[$mm['id']]=1;
            }
        }
        return $out;
    }
}


function tabmod($mm, $tovgrsets)
{
    if( $mm['sub']['tabmod'] )
    {
        $fld= ( (super_explode_n('::', $tovgrsets['sub']['tabmodfields']) )  );

        $out.='<tr>';
        foreach ( $fld as $fk=>$fv)
        {
            $out.='<th class="th_'.$fv[0].'">'.$fv[1].'</th>';
            $fields[$fv[0]]=$fv[1];
        }
        $out.='<th class="outcart">&nbsp;</th></tr>';

        $complect=$tovgrsets['sub']['complect_name'] ? "\r\n".$tovgrsets['sub']['complect_name'] : '' ;

        foreach( ( super_explode_n('	',  str_replace('++','	', $mm['sub']['tabmod'].$complect ) )) as $mod  )
        {
            $outtd='';
            $oktr=0;
            trimarr($mod);
            $tr_od=$tr_od ? '' : 'tr_od' ;

            foreach ( $mod as $mk=>$mv)
            {

                if ( substr( $mv, 0, 2 )=='%%' )
                {
                    $outtd.='<td class="mod_header" colspan="'.(sizeof($fld)+1).'">'.substr($mv,2,10000).'</td>';
                    $oktr=1;
                    $header_line=1;
                    break;
                }

                if( trim($mv ))
                {

                    $oktr=1;
                    $header_line=0;

                    if ( $fld[$mk][0]=='price' ) {

                        $preprice=price_prepare($mv);
                        if ( !$price_ot  or $price_ot > $preprice )  $price_ot = $preprice ;
                        $mv=price2html($mv);

                    }

                    $outtd.='<td class="'.$fld[$mk][0].'">'.$mv.'</td>';
                }


            }
            if ( $oktr )
            {
                $out.='<tr class="mod'.($mmk+1).' '.$tr_od.'">';
                $out.=$outtd;
                if (!$header_line)
                {
                    $out.= $fields['num'] ?   '<td class="outnum"><input class="num" name="tabnum['.($mmk+1).']" value="'.($mm['d19']?$mm['d19'] : '1' ).'" /></td>' : '' ;
                    $out.= '<td class="outcart"><a class="adcart" onclick="adcartbut('.$mm['id'].','.($mmk+1).')" ></a></td>'  ;
                    //$out_arr=
                }

                $out.='</tr>';
                $mmk++;
            }

        }

        return array ( '<table class="tabmod">'.$out.'</table>', price2html($price_ot),  $mmk ) ;
    }
}

function collection( $tovgrsets, $mm )
{
    if ( $tovgrsets['d12'] )
    {
        //узнаём поля схожести
        $sets=  explode(';', $tovgrsets['d12']) ;
        // Составляем запрос
        $tovsets= fn ('minimodparser_assoc', $mm['d9'], $mm );

        foreach ( $sets as $k=>$v )
        {
            $dopfrom.=', '.pref_db.'subcontent s'.($k+2);
            $tid.=' AND  s1.tid = s'.($k+2).'.tid ';
            $value.='AND s'.($k+2).'.value in(\''.(implode("','",  $tovsets[$v]  )).'\') ';
        }

        $qu=(' 
						 		SELECT SQL_CALC_FOUND_ROWS * 
								FROM '.pref_db.'content
								WHERE
								id IN (
										SELECT  s1.tid 
										FROM '.pref_db.'subcontent s1 '.$dopfrom.'
										WHERE 
										s1.folder =  '.xs($tovgrsets['d1']).'
										'.$tid.'
										'.$value.'
									)
								AND ok<>\'\'
								ORDER BY priceall ASC 
								LIMIT 50 '	);

        //array_search()
        $pr=my_mysql_query( ($qu) );
        while ($mm2=mysqli_fetch_assoc($pr))
        {
            //v($mm2['ident']);
            if ( $mm2['ident']!==$mm['ident']   )
            {
                $othshab=shab('tov_tpl_big_other');
                $imurl2=$mm2['ident'];

                $imurl2=  o('tov_img_loader')  ? filename($mm2['d25'])  :  $mm2['ident'];

                $minimgoth="<img src='/cont/img/cat/min/{$imurl2}.jpg'/>";
                if( !$mm2['price'] )
                {
                    $minprice =fn('minimodparser',$mm2['d16'], $mm2 );
                    $mm2['price']=($minprice[1][1][1][1] );
                }
                $rep=array(
                    '[name]'	=>	lang($mm2['name']),
                    '[minidesc]'	=>	$mm2['d14'],
                    '[minimg]'	=>	$minimgoth,
                    '[price]'	=>	pricer( $mm2['priceall'] ) ,
                    '[href]'	=>	hrpref.trim( urlFromMnemo($mm2['ident'])).hrsuf.lngsuf
                );
                $out.=strtr($othshab,$rep) ;
            }
        }

        return $out;

    }

}

function uid()
{
    return $GLOBALS['user']['uid'];
}

function filemtime_file($file)
{
    return $file.'?'.filemtime($file);
}

function watermark_file( $in_file )
{
    if  (  !o('img-mark') ) return ;


    if ( !file_exists('cont/img/cat/bigorig') )
    {
        if ( !mkdir('cont/img/cat/bigorig') ) v('Каталог не создан!');
    }

    if( !file_exists( 'cont/img/cat/bigorig/'.$in_file ) )
    {
        if ( !rename('cont/img/cat/big/'.$in_file,'cont/img/cat/bigorig/'.$in_file) ) {} ;
    }


    /*if ( 	!v(filemtime( 'cont/img/cat/big/'.$in_file ) < v(filemtime('cont/files/img-mark-file.png'))
				OR
			v(filemtime( 'cont/img/cat/big/'.$in_file ))<v(filemtime( 'cont/img/cat/bigorig/'.$in_file ) ) ) ) return;*/

    //v($in_file);

    if ( 	(
            (filemtime( 'cont/img/cat/big/'.$in_file ))
            <
            (filemtime('cont/files/img-mark-file.png'))
        )
        OR
        (
            (filemtime( 'cont/img/cat/big/'.$in_file ))
            <
            (filemtime( 'cont/img/cat/bigorig/'.$in_file ) )
        )
    ) {

        //v('resize!');
        //v('ok');
        if ( fileending($in_file)=='jpg' ){
            $thumb = imagecreatefromjpeg ( 'cont/img/cat/bigorig/'.$in_file );
        } else {
            $thumb = imagecreatefrompng ( 'cont/img/cat/bigorig/'.$in_file );
            imagesavealpha($thumb,true);
            imagealphablending($thumb,TRUE);
        }
        //Читаем картинку
        list($wm,$hm) =getimagesize('cont/files/img-mark-file.png');

        $rounder=imagecreatefrompng('cont/files/img-mark-file.png');

        imagealphablending($rounder,TRUE);


        $w=imagesx($thumb);
        $h=imagesy($thumb);

        imagealphablending($thumb,TRUE);

        if (o('img-mark-valign')=='top')
        {
            $yd= 0;
        }
        elseif (o('img-mark-valign')=='bottom')
        {
            $yd= $h - $hm;
        }
        else
        {
            $yd= $h/2 - $hm/2;
        }

        if (o('img-mark-valign')=='left')
        {
            $xd= 0;
        }
        elseif (o('img-mark-valign')=='right')
        {
            $xd= $w - $wm;
        }
        else
        {
            $xd= $w/2 - $wm/2;
        }


        imagecopy (($thumb), $rounder, $xd, $yd, 0, 0, $wm, $hm); // 1
        if ( fileending($in_file)=='jpg' ){
            imagejpeg ( $thumb, 'cont/img/cat/big/'.$in_file  , 95 );
        } else {
            imagepng ( $thumb, 'cont/img/cat/big/'.$in_file  );
        }
    }
}




function noimage($filename,$_is_bool_out=false)
{

    $ofs=substr($filename,0,1 )=='/' ?  $ofs=1 : 0 ;

    if( $_is_bool_out)
    {
        $out = file_exists( substr($filename,1 ) ) ? TRUE: FALSE;
    }
    else
    {
        $out = ( file_exists( substr($filename,$ofs ) ) AND is_file( substr($filename,$ofs) ) ) ? $filename :  ($ofs ? '/': '' ).'cont/files/tov_mini_noimg.png';
    }


    return  $out ;
}

function tov_modrice_up($_id=false)
{

    $qu='SELECT * FROM '.pref_db.'content WHERE type=\'tov\'  '. ($_id ? ' AND id='.xs($_id) : '' )  ;
    $pr=my_mysql_query($qu) ;
    echo 'Обновлено:<br/>';
    while ($mm=mysqli_fetch_assoc($pr))
    {
        $minprice = fn('minimodparser',$mm['d16'], $mm) ;
        $priceall=( !$mm['price'] AND  ($minprice[1][1][1][1] )) ? $minprice[1][1][1][1] : $mm['price'];
        mydbUpdate('content',array('priceall'=>$priceall),'id',$mm['id']);
        echo ' '.$mm['id'];
    }
}


function catcur($cur_ident, $allnum)
{
    if ($allnum)
    {
        $doptov=200;
        // $allnum = количество ячеек в cтроке вычисляется от получения ширины
        $m=qvar('ident',$cur_ident);
        // cписок предыдущих nL=(n-1)/2 cтраниц
        $nL=($doptov*2 + $allnum-1)/2;
        $colls='ident, name';
        $qu='SELECT '.$colls.' FROM '.pref_db.'content WHERE rod='.xs($m['rod']).' 
		AND `num`< '.xs($m['num']).'
		AND `ok`<>\'\' ORDER BY `num` DESC limit '.$nL ;
        $pr=my_mysql_query($qu) ;
        while ($mm=mysqli_fetch_assoc($pr))
        {
            $x++;
            $cat[]=$mm;
        }
        $cat=array_reverse($cat);
        $cat[]=$m; // текущий товар
        // cписок следующих nR=n-1-nL
        $nR=$doptov + $allnum - sizeof($cat);
        $qu='SELECT '.$colls.' FROM '.pref_db.'content WHERE rod='.xs($m['rod']).' 
		AND `num`> '.xs($m['num']).'
		AND `ok`<>\'\' ORDER BY `num` ASC limit '.$nR ;
        $pr=my_mysql_query($qu) ;
        while ($mm=mysqli_fetch_assoc($pr))
        {
            $y++;
            $cat[]=$mm;
        }
        // вывод
        $sizecat=sizeof($cat);
        foreach ($cat as $c)
        {
            $z++;
            $xhiden=$x-($allnum-1)/2; // 4-(7-1)/2 //($allnum-1)/2
            $xhidenold=$xhiden<0 ? $xhiden: 0;
            $xhiden=$xhiden<0 ? 0 : $xhiden;
            $yhiden=$y-($allnum-1)/2; // 4-(7-1)/2 //($allnum-1)/2
            $yhidenold=$yhiden<0 ? $yhiden : 0 ;
            $yhiden<0 ? $yhiden = 0 : $yhiden;
            $hidenclass= ($z<=($xhiden + $yhidenold) or $z>($sizecat-$yhiden-$xhidenold ) )  ? 'hidem ' : '' ;
            $src= $hidenclass ? 'rel': 'src';
            $vizclass=$hidenclass ? '': 'viz';
            $out.='<td class="'.$vizclass.$hidenclass.( $c['ident']==$m['ident'] ? ' curtov':'' ).'" ><a href="'.urlFromMnemo($c['ident']).'"><img title="'.$c['name'].'" '.$src.'="/cont/img/cat/min/'.$c['ident'].'.jpg"  width="'.o('microtovimg_w').'" height="'.o('microtovimg_h').'" /></a></td>';
        }
        return '<table id="arow"><tr><td class="arow"><a onclick="wheelmicrotov(1,1)" id="microleft"></a></td><td><table id="microtov"><tr>'.$out.'</tr></table></td><td class="arow"><a id="microright" onclick="wheelmicrotov(1,-1)" ></a></td></tr></table>';
    }
}

function catcurclassic($cur_ident, $allnum)
{
    if ($allnum)
    {
        $m=qvar('ident',$cur_ident);
        // cписок предыдущих nL=(n-1)/2 cтраниц
        $nL=($doptov*2 + $allnum-1)/2;
        $colls='ident, name, d25';
        $qu='SELECT '.$colls.' FROM '.pref_db.'content WHERE rod='.xs($m['rod']).' 								
		AND `ok`<>\'\' ORDER BY `num` ASC ' ;
        $pr=my_mysql_query($qu) ;
        while ($mm=mysqli_fetch_assoc($pr))
        {
            $x++;
            $src='src';
            unset($class);
            if( $mm['ident']==$m['ident'])
            {
                $class='curtov';
                $init=$x-($allnum-1)/2;
            }
            $img=o('tov_img_loader') ? filename( $mm['d25'] ) : $mm['ident'] ;
            $out.='<li class="'.$class.'" ><a href="'.urlFromMnemo($mm['ident']).lngsuf.'"><img title="'.$mm['name'].'" '.$src.'="'.noimage('/cont/img/cat/min/'.$img.'.jpg').'"  width="'.o('microtovimg_w').'" height="'.o('microtovimg_h').'" /></a></li>
			';
        }
        $out= '<ul id="jcar" class="jcarousel-skin-tango" >'.$out.'</ul>
		<script type="text/javascript">
		jQuery(document).ready(function()
		{
			jQuery(\'#jcar\').jcarousel({
				// Configuration goes here
				scroll:'.$allnum.',					
				start:'.($init?$init:1).'
				});
		}
		);
		</script>';
        $quad=o('microtovimg_w')+4;
        $width=	$allnum*($quad+10);
        $out.='<style>
		.jcarousel-skin-tango .jcarousel-container-horizontal {  width: '.$width.'px;
		}
		.jcarousel-skin-tango .jcarousel-clip-horizontal {  width: '.$width.'px; height: '.($quad+6).'px;
		}
		.jcarousel-skin-tango .jcarousel-item {     width: '.$quad.'px;     height: '.($quad+6).'px;
		}
		</style>	';
        return  $out;
    }
}

function pricer($pr, $_bigtov=false)
{
    if ( o('tov_noprice') ) return '';

    if ( !is_price  )
    {
        $price= '<a class="getprice" href="javascript:prigl();">Узнать цену</a>';
    }
    else
    {
        $mm['price']=(price_auto_cop( floatval( price_prepare ($pr) ) ) );

        if($mm['price']>0)
        {
            $price=($_bigtov? '<span class="bigtovts">'.( o('price_tsena') ? o('price_tsena') : 'Цена: </span>')  : '').'<span class="mainprice">'.price2html($mm['price']).'</span> [val]';
        }
        elseif ( $pr!=='  ' AND ( $mm['price']=='--' or ( !$mm['price'] and o('no_price') ) )  )
        {
            $price='<span class="no_price">'.o('no_price_text').'</span>';
        }
        else
        {
            $price='';
        }
    }
    return ($price);
}

function view_tov_set($tovset)
{
    foreach ( explode(';',($tovset)) as $tovse )
    {
        $tovs=trimarr(explode(':',$tovse));
        if($tovs[0] and $tovs[1])
        {
            $out[]='<td class="tovset1"><b>'.$tovs[0].': </b></td><td class="tovset2">'.$tovs[1].'</td>';
        }
    }
    if ($out) return '<table class="tovset"><tr>'.implode('</tr><tr>',$out).'</tr></table>';
}

function view_tov_set2($tovset, $tovfld )
{
    //v($tovset);
    foreach ( explode(';',($tovset)) as $tovse )
    {

        $tovs=trimarr(explode(':',$tovse));

        if($tovs[0] and $tovs[1] AND $tovfld[$tovs[0]]['d9'] )
        {
            $name= $tovfld[$tovs[0]]['d3'] ? $tovfld[$tovs[0]]['d3'] : $tovfld[$tovs[0]]['name'];

            if($tovfld[$tovs[0]]['d5']=='num')
            {
                $val=  $tovs[1].' '.$tovfld[$tovs[0]]['d6'];
            }
            else
            {
                $val =  val_fromid( $tovs[1], true, 'name' ).($tovfld[$tovs[0]]['d6']?  ' '.$tovfld[$tovs[0]]['d6'] : '' );
            }

            $out[]='<td class="tovset1"><b>'. $name.': </b></td><td class="tovset2">'.$val .'</td>';
            $ts_name[$tovs[0]]=$name;
            $ts_val[$tovs[0]]=$val;
        }
    }
    if ($out) return array( '<table class="tovset"><tr>'.implode('</tr><tr>',$out).'</tr></table>', $ts_name, $ts_val );
}

function tov_set_mini_view($tovset, $tovfld )
{
    //v($tovset);
    foreach ( explode(';',($tovset)) as $tovse )
    {

        $tovs=trimarr(explode(':',$tovse));

        if($tovs[0] and $tovs[1] AND $tovfld[$tovs[0]]['d8'] )
        {
            $name= $tovfld[$tovs[0]]['d3'] ? $tovfld[$tovs[0]]['d3'] : $tovfld[$tovs[0]]['name'];

            if($tovfld[$tovs[0]]['d5']=='num')
            {
                $val=  $tovs[1].'&nbsp;'.$tovfld[$tovs[0]]['d6'];
            }
            else
            {
                $val =  val_fromid( $tovs[1], true, 'name' ).($tovfld[$tovs[0]]['d6']?  '&nbsp;'.$tovfld[$tovs[0]]['d6'] : '' );
            }

            $out[]='<span class="tsm-'.$tovs[0].'" ><b>'. $name.': </b> '.$val.'</span>';
        }
    }
    if ($out) return '<div class="tovset_mini">'.implode( ( o('tovset_mincart_br') ? '<br/>' : '; ' ) ,$out).'</div>';
}

function tpl ($ident)
{
    return file_get_contents(jscripts.'/tpl/'.($ident).'.htm');
}

function shab($ident)
{
    /*
		возвращает исходник шаблона без его просчёта
		в качестве источника испльзуеся поиск шаблона в следующей последовательности:
    
		1) идёт поиск в папках поиска

    	2)	идёт поиск контент-объекта type shab (шаблон), с учётом перебивки в theme

    	3) если находит, то проверяет размещение указанное в этом шаблоне
			-если указанная база, то дёргает с редактора (физически это размещается в mysql строке)
			-если указывается диск,
				- то дёргается указанный путь, либо папка по умолчанию - cont/htm
				- В качестве имени используется $ident.'.htm'

		4) если не найден, то ищется в папке jscripts/tpl
			- В качестве имени используется $ident.'.php'

	*/
    //return ;
    $ident= theme($ident);
    if ($GLOBALS['sh_'.$ident])
    {
        return $GLOBALS['sh_'.$ident];
    }
    else
    {
    	$file_ending= !strpos($ident, '.') ? '.htm' : '';

    	if ($file_path = file_theme_name(($ident.$file_ending) )) {

			$GLOBALS['sh_'.$ident]=file_get_contents( ($file_path) );

            return $GLOBALS['sh_'.$ident];

		} else {
    		$mm=qvar('ident',$ident);
			if ( $mm ) {
                if ($mm['d15'] == 'disc') {
                    $folder = $mm['d16'] ?: cont . '/htm';
                    $GLOBALS['sh_' . $ident] = file_get_contents($folder . '/' . $ident . '.htm');

                } else {
                    $GLOBALS['sh_' . $ident] = $mm['content'];
                    vl('from ident=' . $mm['ident'], '(load)');
                }
            }
		}

        return $GLOBALS['sh_'.$ident];
    }
}

function translit( $cyr_str)
{
    $tr = array(
        "Ґ"=>"G","Ё"=>"YO","Є"=>"E","Ї"=>"YI","І"=>"I",
        "і"=>"i","ґ"=>"g","ё"=>"yo","№"=>"-","є"=>"e",
        "ї"=>"yi","А"=>"A","Б"=>"B","В"=>"V","Г"=>"G",
        "Д"=>"D","Е"=>"E","Ж"=>"ZH","З"=>"Z","И"=>"I",
        "Й"=>"Y","К"=>"K","Л"=>"L","М"=>"M","Н"=>"N",
        "О"=>"O","П"=>"P","Р"=>"R","С"=>"S","Т"=>"T",
        "У"=>"U","Ф"=>"F","Х"=>"H","Ц"=>"TS","Ч"=>"CH",
        "Ш"=>"SH","Щ"=>"SCH","Ъ"=>"","Ы"=>"YI","Ь"=>"",
        "Э"=>"E","Ю"=>"YU","Я"=>"YA","а"=>"a","б"=>"b",
        "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"zh",
        "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
        "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
        "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
        "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"",
        "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
        "&"=>"-"," "=>"-","\""=>"-","?"=>"-q-","!"=>"-","#"=>"-","№"=>"-",
        '"'=>'',"'"=>'',':'=>'-',"\r"=>'-',','=>'-','«'=>'-','»'=>'-', '’'=>''
    );
    $out=str_replace(array('--','---'),array('-','-') , strtr(trim($cyr_str),$tr) );
    return str_replace('--','-', $out );
}

function url2mnemo($ident)
{
    return strtr($ident,$GLOBALS['repurl']);
}

function oro ($_val1, $_val2, $_val3=FALSE ) {
    return $_val1 ? $_val1 : ( $_val2 ? $_val2 :  $_val3  );
}
function mor ( &$_va1 , $_val2 ){
    if ($_val2) $_va1 = $_val2;
}
function oror($sample,$_val1,$_val2)
{
    $arr=func_get_args() ;
    unset( $arr[0] );
    return in_array( $sample, $arr );
}

function truecss()
{

    @$must=func_get_arg(0);
    if( ! (bool) fgc('local.css') & file_exists(cont.'/css/local.css') )
    {
        file_put_contents(cont.'/htm/local.css.htm', file_get_contents(cont.'/css/local.css') );
    }


    if (!file_exists('cont/logs') ) mkdir('cont/logs');



    if (
        filemtime(cont.'/css/all.css')<filemtime(jscripts.'/css.php')
        |
        (filemtime(cont.'/css/all.css')<filemtime(cont.'/htm/local.css.htm') )
        |
        (filemtime(cont.'/css/all.css')<filemtime(cont.'/htm/tinymce.css.htm') )
        |
        ((bool)$must) )
    {
        // для всех

        if (adminpanel) patch::pre_all();

        $rep=array('!!!'=>'!important');
        $GLOBALS['cssbr']='all';

        smarty(jscripts.'/css.php',0);

        file_put_contents('cont/css/'.local_core.'_all.css', strtr( smarty(jscripts.'/css.php',0), $rep ) );
        // для админов в публичной части

        $GLOBALS['cssbr']='all_admin';
        file_put_contents('cont/css/'.local_core.'_all_admin.css', strtr( smarty(jscripts.'/css.php',0), $rep ) );

        // для ИЕ6
        $GLOBALS['cssbr']='ie6';

        file_put_contents(jscripts.'/render/css/IE6.css',strtr(smarty(jscripts.'/css.php',0), $rep ) );
        // для ИЕ7-8
        $GLOBALS['cssbr']='ie78';
        file_put_contents(jscripts.'/render/css/IE78.css',strtr(smarty(jscripts.'/css.php',0), $rep ) );
        // для admin
        $GLOBALS['cssbr']='adm';
        file_put_contents(jscripts.'/render/css/admin.css',strtr(smarty(jscripts.'/css.php',0), $rep ) );


        if (adminpanel) newbase();
        if (adminpanel) patch::all();
    }
    // 'cont/css/'.local_core.'_
    if   ( filemtime('cont/css/'.local_core.'_csse.css') < filemtime(cont.'/htm/csseditor.htm')  or $must )
    {
        file_put_contents( jscripts.'/render/css/csse.css' , fgc('csseditor')  );
        foreach(  moduls_get_arr() as $mm ) {
            file_put_contents( jscripts.'/render/css/'.$mm['ident'].'.css', $mm['sub']['css'] );
        }
    }
}

function smartyfull($all,$level)
{
    for ( $leveltpl = 1 ; $leveltpl<=$level ; $leveltpl++)
    {
        foreach ( $GLOBALS['tplspis'] as $tpls)
        {

            $tpl=str_replace( 'tpl_' , '',  $tpls );

            //v(strpos( $all, $tplspis) );

            if ( $tpls == 'autoin' ){
                $tpl_find ='[[[';
            }else if ( $tpls == 'tpl_abtest' ) {
                $tpl_find ='{{{';
            } else {
                $tpl_find = '['.($tpl).']';
            }




            if ( strpos( $all,  ($tpl_find) )  )
            {

                if ($GLOBALS['fulltpl_'.$tpls])
                {


                    @$ftpl= explode( "|", shab ($tpls ) );
                }
                else
                {
                    @$ftpl= explode( "|",  $tpl  );
                }

                //v($tpl_find);
                //v( $all );

                if ( ($tpls) == 'autoin'  or  $tpls == 'tpl_abtest' ) {

                    //@$all = ( strtr( $all , array("[[["=>$ftpl[0],"]]]"=> $ftpl[2], '{{{'=>$ftpl[0] , '}}}'=> $ftpl[2]  ) ) ) ;
                    @$all = str_replace( array("[[[","]]]","{{{","}}}"), array($ftpl[0],$ftpl[2],$ftpl[0],$ftpl[2]), $all);

                    //if  ( $tpls == 'tpl_abtest' ) { v($ftpl);  exit();  }

                } else {

                    @$all = str_replace(array("[{$tpl}]","[/{$tpl}]","[//{$tpl}]","[///{$tpl}]"), array($ftpl[0],$ftpl[2],$ftpl[4],$ftpl[6]), $all);

                }


                // уровень 2


            }
        }

        $all=smarty($all,1);
    }
    return $all;
}
function ab_curver() {
    return $_REQUEST['utm_cv'] ? $_REQUEST['utm_cv'] : 1 ;
}
function abtest($data) {

    $ver=ab_curver();
    list($dat,$content)=explode(':',$data);

    list ($allver, $show) = explode('#',$dat );

    $show= (   array_flip( str_split ($show ) ) );
    $curver=  1+ ($ver-1) % $allver;

    if ( isset( $show[$curver]) ) return $content;

}

function is_ok_opt($opt_of_element)
{
    //v(substr($opt_of_element,0,1));

    if ( substr( $opt_of_element, 0,1) == '=')
    {
        //v('$out'.$opt_of_element);
        eval ( ('$out'.$opt_of_element.';'));

        return $out;
    }
    else
    {
        if ( (!$opt_of_element or o($opt_of_element) )
            or
            ( substr($opt_of_element,0,1)=='!' and !o(substr($opt_of_element,1))  ) )
        {
            //v(1);
            return TRUE;
        }
    }


}

function formDep2class($dep_d98,$mm=FALSE) // вставка  класса зависимости
{
    if ($dep_d98)
    {

        // {Prod1056}='1' и {Vid364346}='1'

        $rep=array( '} {' => '\') && vl(\'', '{'=>'vl(\'', '}'=>'\')',' или '=>' || ', ' и '=>' && ',' or '=>' || ', ' and '=>' && ', '='=>'==');
        $rep2=array(  '!'=>'','{'=>'', '}'=>'',' или '=>';', ' и '=>';',' or '=>';', ' and '=>';', '='=>':' ,  '>'=>':', '>'=>':'  );

        $group=strtr($dep_d98,$rep);
        $class = ' form_hidden';
        $out[0] =  (' style="display:none;" class="obform '.$class.' '.$mm['sub']['addclass'].'"');

        $dob=(explode(';',strtr($dep_d98,$rep2) ));
        foreach ($dob as $d)	{
            $dd=explode(':',$d);
            if ($dd[0]) $depob.=trim($dd[0]).';';
        }

        $out[1] = 'group="'.$group.';dp=\''.$depob.'\'"' ;
    }
    else
    {
        $out[0] = (' class="obform '.($mm['d96']?'_'.$mm['d96']:'').' '.$mm['sub']['addclass'].'"');
    }
    return $out;
}

function reglobalArray($array)
{
    foreach($array as $k=>$v)
    {
        $GLOBALS[$k]=$v;
    }
}

function form($ident,$_ur=false,$_nothiden=false)  //2
{
    if (!$_ur )
    {
        $cache= mycache_get($ident, $keymd5, 3600 );
        if ( $cache )
        {
            //v($keymd5);
            $ival=unserialize( mycache_get($keymd5.'ival', $keymd5_, 3600 ));
            reglobalArray($ival)	;
            //v($ival);
            /*v($GLOBALS['ivalnames']);
			v($GLOBALS['ivals']);
			v($GLOBALS['inamescoll']);
			v($GLOBALS['inames']);
			*/
            return $cache;
        }
    }
    $ur=$_ur;
    static $nothiden;
    $nothiden=$_nothiden?$_nothiden:$nothiden;
    $mm=qvar('ident',$ident);
    $GLOBALS['inames'][$mm['d1']]=$mm['name'] ? $mm['name'] : $mm['d13'] ; //cached!
    $GLOBALS['inamescoll'][$mm['d2']]=$mm['name']; //cached!
    static $preinline;
    if (is_ok_opt($mm['d99']) and checkgroup($mm['d97'])   )
    {
        if (adminpanel) $alt=$mm['d2'].'~'.$mm['d1'];
        $dep=formDep2class($mm['d98'],$mm);
        $preout.=($mm['d14'] and !$preinline )?'<table align="left" class="inlineform"><tr>':'';
        $preout.= ($preinline or $mm['d14']) ? '<td valign="bottom" class="inlinetd">':'';
        $preout.="<div id='d-{$mm['d1']}' title='$alt' $dep[0] $dep[1] ";
        if ($mm['type']=='spis')
        {
            if 	($mm['d7'])
            {
                $id_spis=$mm['d7'];
            }
            else
            {
                $id_spis=$ident;
            }
            if (  substr( $mm['d8'],0,5)=='radio' or $mm['d8']=='' )
            {
                $preout.=' rel="rad" ';
                $out.=i_r($mm['d1'], $mm['name'],  $mm['d3'], $mm['d4'] , $id_spis ,$mm['d9'],$mm['d5'] ,$mm['d6'],$mm);
            }
            elseif ( ($mm['d8']=='multi') )
            {
                $preout.=' rel="mul" ';
                $out.=i_r($mm['d1'], $mm['name'],  $mm['d3'], $mm['d4'] , $id_spis ,$mm['d9'],$mm['d5'] ,$mm['d6'],$mm);
            }
            else
            {
                if ( !($mm['d9']) )  $mm['d9']='d1';
                $out.=i_select ($mm['name'], $mm['d1'], $id_spis, 'name', $mm['d9'] ,'',$mm['d5'],$mm['d6'],$mm);
            }
        }
        elseif ($mm['type']=='formlink')
        {
            $out.=form($mm['d1'],$ur+1);
        }
        elseif ($mm['type']== 'ftext' or $mm['type']=='rangeslider'  )
        {
            $out.=i_t( $mm['d1'], $mm['name'],  $mm['d3'], $mm['d4'] ,$mm['d6'], $mm['d5'], $mm );
        }
        elseif ($mm['type']=='checkbox')
        {
            $out.=i("ch",$mm['d1'],'',$mm['name'],'',$mm);
            if ($mm['d12'])
            {
                $out.=$nothiden? '<input type="hidden" name="ival['.$mm['d1'].'][on]" value="'.$mm['d12'].'" >':'';
                $GLOBALS['ivals'][$mm['d1']]['on']=$mm['d12'];
            }
        }
        elseif ($mm['type']=='file')
        {
            $out.=i('f', $mm['d1'],'',$mm['name'],'',$mm);
            $GLOBALS['ifiles'][$mm['d1']]= array_diff($mm,array(''));
            if ( !$mm['d4'] and $mm['d5'] )
            {
                if ($mm['d8'])
                {
                    $suf='_'.$mm['d8'] ;
                }
                else
                {
                    $suf='';
                }
                if ( $mm['d3']=='tpl'  ) // and file_exists($filename)
                {
                    $path='cont/img/tpl/';
                }
                elseif ($mm['d4']=='cat')
                {
                    $path='cont/img/cat/mid/';
                }
                elseif ($mm['d3']=='tpladmin')
                {
                    $path=jscripts.'/tpladmin/';
                }
                if ($mm['d9'])
                {
                    $identname=$mm['d9'];
                }
                else
                {
                    $identname='ident';
                }
                $filename=$path.$mm['d10'].$GLOBALS['m'][$identname].$suf.'.'.$mm['d5'].'?'.rand();
                $out.='<img class="imgform" src="'.$filename.'"  /> ';
            }
        }
        elseif ($mm['type']=='submit-button')
        {
            if ( $mm['sub']['named'] ) {
                $name=$mm['name'];
            } else {
                $size=getimagesize('cont/img/tpl/'.$mm['ident'].'.png');
                $style= "background: url(/cont/img/tpl/{$mm['ident']}.png);width:{$size[0]}px;height:{$size['1']}px;";
            }

            $out.= smarty_tpl('submit-button', array('mm'=>$mm, 'size'=>$size) );

        }
        elseif ($mm['type']=='color')
        {
            $out.="<div class='color'>".i_t( $mm['d1'], $mm['name'],  $mm['d3'], $mm['d4'] ,$mm['d6'], $mm['d5'] ,$mm).'</div>';
        }
        elseif ($mm['type']=='rootform')
        {
            if ( $ur>=1 )
            {
                $out.=$mm['name']? '<h3>'.$mm['name'].'</h3>' : '';
            }
            $ok= $mm['d11'] ? ' AND `ok`<>\'\' ' : FALSE;
            $qu= 'SELECT `ident` FROM '.pref_db.'content WHERE rod='.xs($ident).' AND `d100`<='.reg().$ok.'   ORDER BY `num` ASC ';
            $pr=my_mysql_query($qu) ;
            while ($mm2=mysqli_fetch_assoc($pr))
            {
                $out.=form($mm2['ident'],$ur+1);
            }
        }
        if ($mm['d98'])
        {
            $hidden='1';
        }
        else { $hidden='';
        }
        //v($mm['d1']);
        $out=$preout.'>'.$out.($nothiden? '<input type="hidden" name="ihid['.$mm['d1'].']" id="ihid-'.$mm['d1'].'" value="'.$hidden.'" /><input type="hidden" name="ps2[]" value="'.$mm['d1'].'"	/>':'').'
		</div>';
        $out.= ($preinline or $mm['d14']) ? '</td>':'';
        $out.= ($preinline and !$mm['d14']) ? '</tr></table><div style="clear:both;"></div>':'';
        $preinline=$mm['d14'];
    }
    if(!$_ur)
    {
        $ival=array(	'ivalnames'=>$GLOBALS['ivalnames'],
            'ivals'=>$GLOBALS['ivals'],
            'inamescoll'=>$GLOBALS['inamescoll'],
            'inames'=>$GLOBALS['inames']);
        mycache_set(md5($keymd5.'ival'),serialize($ival),3600);
        mycache_set($keymd5, $out, 3600);
    }
    return $out; //
}

function array2json($arr)
{
    $parts = array();
    $is_list = false;
    if (!is_array($arr)) return;
    if (count($arr)<1) return '{}';
    //Find out if the given array is a numerical array
    $keys = array_keys($arr);
    $max_length = count($arr);
    if(($keys[0] == 0) and ($keys[$max_length] == $max_length))
    {
        //See if the first key is 0 and last key is length - 1
        $is_list = true;
        for($i=0; $i<count($keys); $i++)
        {
            //See if each key correspondes to its position
            if($i != $keys[$i])
            {
                //A key fails at position check.
                $is_list = false; //It is an associative array.
                break;
            }
        }
    }
    foreach($arr as $key=>$value)
    {
        if(is_array($value))
        {
            //Custom handling for arrays
            if($is_list) $parts[] = array2json($value); /* :RECURSION: */
            else $parts[] = '"' . $key . '":' . array2json($value); /* :RECURSION: */
        } else {
            $str = '';
            if(!$is_list) $str = '"' . $key . '":';
            //Custom handling for multiple data types
            if(0 /*is_numeric($value)*/) $str .= $value; //Numbers
            elseif($value === false) $str .= 'false'; //The booleans
            elseif($value === true) $str .= 'true';
            //else $str .= '"' . addslashes($value) . '"'; //All other things
            else
            {
                //$value=htmlspecialchars($value);
                //$value=mysqli_real_escape_string($GLOBALS['db_link'] ,$value);
                $rep=array(
                    "\r"=>'\r',
                    "\n"=>'\n',
                    '"'=>'\\"'
                    //"'"=>"\\'"
                );
                $value=strtr($value,$rep);
                //$value=addslashes($value);
                $str .= '"' .$value. '"';
            }
            //All other things
            // :TODO: Is there any more datatype we should be in the lookout for? (Object?)
            $parts[] = $str;
        }
    }
    $json = implode(',',$parts);
    if($is_list) return '[' . $json . ']';//Return numerical JSON
    return '{' . $json . '}';//Return associative JSON
}

function json_safe_encode($var)
{
    return json_encode(json_fix_cyr($var));
}

function json_encode_pretty ($array){
    return json_encode($array, JSON_PRETTY_PRINT |  JSON_UNESCAPED_UNICODE );
}

function json_fix_cyr($var)
{
    if (is_array($var))
    {
        $new = array();
        foreach ($var as $k => $v)
        {
            $new[json_fix_cyr($k)] = json_fix_cyr($v);
        }
        $var = $new;
    }
    elseif (is_object($var))
    {
        $vars = get_object_vars($var);
        foreach ($vars as $m => $v)
        {
            $var->$m = json_fix_cyr($v);
        }
    } elseif (is_string($var))
    {
        $var = iconv('cp1251', 'utf-8', $var);
    }
    return $var;
}

function smenu($rod)
{
    global $m;
    @$ur=func_get_arg (1);
    if ( !$ur)
    {
        $out='<ul  id="MenuBarVer" class="MenuBarVertical">'.smenu($rod,1).'</ul>';
        //print_r ($put);
        return $out;
    }
    else
    {
        $pref='s';
        $prebase=my_mysql_query( 'SELECT * FROM '.pref_db.'content WHERE rod="'.$rod.'"  AND ok<>\'\' AND (`type`=\'category\' OR `type`=\'page\') ORDER BY `num` ASC ' ) ;
        $mm=mysqli_fetch_assoc($prebase);
        if ($mm)
        {
            $class1=$pref.($ur);
            $ur1=$ur-1;
            //echo $class;
            while ($mm)// затем начинаем перебирать каждый пункт и делать выборку по каждому (рекурсивно)
            {
                $out.="<li class='m{$class1}'>";
                $ident=$mm['ident'];
                $name=$mm['name'];
                if ($mm['nolink'])
                {
                    $hr='';
                }
                else
                {
                    if ($mm['d1'])
                    {
                        $link=$mm['d1'];
                    }
                    else {$link=$mm['ident'];
                    }
                    $hr='href="'.hrpref.$link.hrsuf.lngsuf.'"';
                }
                $out.= "<table class='{$class1}-t'><tr><td><a class='{$class1}-a' {$hr}>".lang($mm['name'])."</a></td></tr></table>\r\n"	;
                $subout=smenu ($mm["ident"],$ur+1,$pref);
                if ($subout)
                {
                    $out.='<ul>'.$subout.'</ul>';
                }
                $out.="</li>\r\n";
                $mm=mysqli_fetch_array($prebase);
            }
            return $out;
        }
    }
}

function date_for_news($date_str)
{
    $rep=array(
        ':'=>'.',
        ' '=>'.'
    );
    $d=explode( '.' , strtr( $date_str, $rep) );
    return $d[2].'.'.$d[1].'.'.$d[0];
}

function newsblock($ident,$_hor=false) //2
{
    $mmall=qvar('ident',$ident);
    $numnews=$mmall['d9'];

    if ( intval($numnews)==0) $numnews=3;
    $pr=my_mysql_query( "SELECT SQL_CALC_FOUND_ROWS * FROM ".pref_db."content WHERE 
	`rod`=".xs(($ident)  )."  AND `ok`<>''   
	ORDER BY `d1` DESC LIMIT 0,".$numnews  ) ;
    $mmall['name']=lang($mmall['name']);
    //v($numnews);
    //$out=($_hor ? '<div class="bnews_hor"><div class="bnews_hor_name">'.lang($mmall['name']).'</div><table>': '');

    while ($mm=mysqli_fetch_assoc($pr) )
    {
        $mm['hr']='href="'.hrpref.urlFromMnemo($mm['ident']).hrsuf.lngsuf.'"';
        $mm['date']=date_for_news($mm['d1']);
        $mm['name']=lang($mm['name']);
        $mm['ndesc']=$mm['d2'] ? '<div class="ndesc">'.$mm['d2'].'</div>' : '';

        $news[]=$mm;

    }

    $mmall['hr']=hrpref.urlFromMnemo($ident).hrsuf.lngsuf;

    //$out.= $_hor ? '</table>'.$all.'</div>' : $all  ;
    //debug_start();
    return smarty_tpl( $_hor ? 'news_tpl_anons_hor' : 'news_tpl_anons', array ('news'=>$news, 'mmall'=>$mmall  ) ) ;
    //debug_stop();
}

function checkgroup ($fld_d97)
{
    if ( !$fld_d97 )
    {
        return true;
    }
    if( $GLOBALS['user'] AND !is_array($GLOBALS['userdata']['groups'])   )
    {
        $GLOBALS['userdata']['groups']=explode(' ',$GLOBALS['userdata']['groups']);
        $GLOBALS['userdata']['groups'][]='grp-autoriz';
    }
    if ( array_intersect( explode(' ',$fld_d97),  $GLOBALS['userdata']['groups'] ) )
    {
        return TRUE;
    }
}

function checkdomain ($fld_d88)
{
    if ( !$fld_d88 OR !o('redirect_to_base_href') )
    {
        return true;
    }

    if ( in_array( $GLOBALS['site']['ident'],  explode(' ',$fld_d88)   )  )
    {
        return TRUE;
    }
}


function umenu ($rod, $ur=false, $pref=false, $nadm=FALSE) {
    if (o('hmenu_umenu_new')) {
        return fn('menu2::umenu', $rod, $ur, $pref, $nadm );
    } else {
        return fn('umenu::main', $rod, $ur, $pref, $nadm);
    }
}


function url2arr ($tgset,$tovfields, $_baseurl) {
    return fullsearch::url2arr($tgset,$tovfields, $_baseurl);
}


function url_fromarr($tgset,$tovfields,$arr)
{
    //($arr);

    if ( sizeof($arr) > 3 ) $filter=true;

    foreach ($tovfields as $tovfield)
    {
        if ( $arr[$tovfield['d1']] )
        {
            //foreach( $tovfields )
            $ark=array_keys( $arr[$tovfield['d1']] );
            if ( sizeof($ark)>1 ) $filter=true;
            sort( $ark );

            unset($filters_val);


            foreach ( $ark as $k )
            {
                $chpu[]=($arr[$tovfield['d1']][$k]['d2']);
                $filters_val[]= $arr[$tovfield['d1']][$k]['d1'] ;
            }

            $filters[]=$tovfield['d1'].'-'.implode('-',$filters_val);

            //v($ark);

            //v($arr[$tovfield['d1']]);
        }

    }

    if ( $filter OR $chpu ) {

        $url = str_replace( '//', '/', ($filter ?  'filter/'.implode('/',$filters) : implode( '/', $chpu )).o('fullsearch_url_suf') ) ;

    }

    return vl( array ( $url , $filter ), 'url_fromarr return' );

}

function fullseach2 ($tgset)
{
    //return;
    return fn( 'fullsearch::filter', $tgset );

}

function num2str($integer,$num_char)
{
    $ost= $num_char-strlen( (string)$integer);
    if ($ost>=1)
    {
        return str_repeat('0',$ost).$integer;
    }
    else
    {
        return $integer;
    }
}

function tovgroupsets_get($ident_rod_d1,$val)
{
    $ident_rod=($ident_rod_d1);
    if  ( isset($GLOBALS['cache']['tovgr'.$ident_rod.$val]) ) return $GLOBALS['cache']['tovgr'.$ident_rod.$val];
    //v($val);
    //$GLOBALS['cache']['tovgr'.$ident_rod.$val];

    if ($ident_rod=='ident' )
    {
        $mm=qvar('ident', $val, 'rod');
        $rod=   ($mm['type']=='tov') ? $mm['rod'] : $val;
        $tovgroup=  par( (qvar('ident', ($rod ))), 'd6' )  ;
    }
    elseif ($ident_rod=='d1')
    {
        $tovgroup=$val;
    }
    else
    {
        $rod=  $val ;
        $tovgroup=  par( (qvar('ident', ($rod ))), 'd6' )  ;
    }


    list($tovgrsets)=(mydbGetSub('content',array('rod'=>'tov_subtype', 'd1'=>($tovgroup),'type'=>'vid_tovara' ) ));
    if (!$tovgrsets) list($tovgrsets)=(mydbGetSub('content',array('rod'=>'tov_subtype', 'd1'=>'' ) ));
    setsub($tovgrsets);
    $GLOBALS['cache']['tovgr'.$ident_rod.$val]= $tovgrsets;

    return $tovgrsets;
}


function tovsetsfields( $tovgrsets )
{
    if ( isset($GLOBALS['cache']['tovsetsfields'.$tovgrsets['d1']])) return $GLOBALS['cache']['tovsetsfields'.$tovgrsets['d1']];

    $or = $tovgrsets['d1'] ?  'OR rod='.xs($tovgrsets['ident']) : '';

    $prebase=my_mysql_query(( 'SELECT * FROM `'.pref_db.'content` WHERE type=\'tovset\'  AND ( rod=\'po-umolchaniyu\'  '.$or.')  ORDER BY `num` ASC ' )) ;
    while ($mm=mysqli_fetch_assoc($prebase) )
    {
        setsub($mm);
        $out[$mm['d1']]=$mm;

    }
    $GLOBALS['cache']['tovsetsfields'.$tovgrsets['d1']]=$out;
    return ($out);
}

function fetcher ($need_field_arr,$table, $field_find_name, $field_value_sec, $tpl, $_order_by='num', $_ASC=true,$_ok=false)
{
    $qu='SELECT `'.implode('`,`', $need_field_arr).'` FROM `'.pref_db.$table.'` WHERE `'.$field_find_name.'`='.xs($field_value_sec).($_ok?' AND `ok`<>\'\' ':'').'  ORDER BY `'.$_order_by.'` '.($_ASC?'ASC':'DESC') ;
    $pr=my_mysql_query($qu);
    while ($mm=mysqli_fetch_assoc($pr) )
    {
        foreach ($mm as $k=>$v)
        {
            $rep['['.$k.']']=$v;
        }
        $out.=strtr($tpl,$rep);
    }
    return $out;
}

function pages_numpage($x){
    if ($x>=1)
    {
        return '?p='.$x;
    }
}

function listalkaSmart( $cur_page,$all_tov,$left_right_pages, $centerpages,$numtovpage, $base_href, $list_pref )
{
    $alpages=ceil ( $all_tov / $numtovpage)-1  ;
    if ($alpages>=1)
    {
        $out.='<div id="nlistalka">';
        for ($x=0;$x<=$alpages;$x++)
        {
            $name=$x+1;
            if ($cur_page==$x)
            {
                $out.="<span>$name</span>";
            }
            else
            {
                if ($x>=1)
                {
                    $numpage='--'.$x;
                }
                if($_GET['tovgroupp'])
                {
                    $re=explode( '?' , $_SERVER['REQUEST_URI']);
                    $hr='href="'.hrpref.urlFromMnemo($GLOBALS['m']['ident']).lngsuf.$numpage.'--?'.$re[1].'"';
                }
                else
                {
                    $hr='href="'.hrpref.urlFromMnemo($GLOBALS['m']['ident']).lngsuf.$numpage.'"';
                }
                $out.="<a $hr>$name</a> ";
            }
        }
        $out.='</div>';
    }
    return $out;
}

function xss ($inxss)
{
    //$outxss= addslashes($inxss)  ;
    //v($inxss);
    $outxss= mysqli_real_escape_string($GLOBALS['db_link'] ,magic($inxss)) ;
    //v($outxss);
    return $outxss;
}

function file_theme_add($file_name) {
    $path=file_theme_name($file_name);
	if ($path) {
		$name=explode('.',$path);
		$ext=array_pop($name);
		$name=implode('.',$name);

		if(  o('is_browser_flush_via_end') ) {
            $file_full_path= $name.'.'.$ext.'?'.filemtime($path);
		} else {
            $file_full_path= $name.'.v'.filemtime($path).'.'.$ext;
		}

		if  ($ext === 'js' ) {
            return '<script src="/' . $file_full_path . '"></script>';

        } else if ( $ext === 'css' ) {
			return '<link href="/'.$file_full_path.'" rel="stylesheet" type="text/css"/>';

        }

	} else {
        return '<!-- файл "'.$file_name.'" не найден -->';
    }

}

function yaml_encode($array) {
	return Spyc::YAMLDump($array);
}

function yaml_decode ($text) {
	return Spyc::YAMLLoad( $text );
}

?>