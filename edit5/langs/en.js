tinyMCE.addI18n({en:{
common:{
edit_confirm:"Do you want to use the WYSIWYG mode for this textarea?",
apply:"Применить",
insert:"Вставить",
update:"Обновить",
cancel:"Отмена",
close:"Закрыть",
browse:"Обзор",
class_name:"CSS Стиль",
not_set:"-- Не задано --",
clipboard_msg:"Copy/Cut/Paste is not available in Mozilla and Firefox.\nDo you want more information about this issue?",
clipboard_no_support:"Currently not supported by your browser, use keyboard shortcuts instead.",
popup_blocked:"Sorry, but we have noticed that your popup-blocker has disabled a window that provides application functionality. You will need to disable popup blocking on this site in order to fully utilize this tool.",
invalid_data:"Error: Invalid values entered, these are marked in red.",
more_colors:"More colors"
},
contextmenu:{
align:"Выравнивание",
left:"Влево",
center:"По центру",
right:"Вправо",
full:"Полное"
},
insertdatetime:{
date_fmt:"%d.%m.%Y",
time_fmt:"%H:%M:%S",
insertdate_desc:"Вставить дату",
inserttime_desc:"Вставить время",
months_long:"Январь,Февраль,Март,Апрель,Май,Июнь,Июль,Август,Сентябрь,Октябрь,Ноябрь,Декабрь",
months_short:"Янв,Фев,Мар,Апр,Май,Июнь,Июль,Авг,Сен,Окт,Нояб,Дек",
day_long:"Воскресенье,Понедельник,Вторник,Среда,Четверг,Пятница,Субота,Воскресенье",
day_short:"ВС,ПН,ВТ,СР,ЧТ,ПТ,СБ,ВС"
},
print:{
print_desc:"Печать"
},
preview:{
preview_desc:"Препросмотр"
},
directionality:{
ltr_desc:"Направление слева на право",
rtl_desc:"Направление справа на лево"
},
layer:{
insertlayer_desc:"Вставить новый слой",
forward_desc:"Переместить на уровень вперёд",
backward_desc:"Переместить на уровень назад",
absolute_desc:"Toggle absolute positioning",
content:"Новй слой..."
},
save:{
save_desc:"Сохранить",
cancel_desc:"Отменить все изменения"
},
nonbreaking:{
nonbreaking_desc:"Вставить неразрывный пробел"
},
iespell:{
iespell_desc:"Run spell checking",
download:"ieSpell not detected. Do you want to install it now?"
},
advhr:{
advhr_desc:"Horizontal rule"
},
emotions:{
emotions_desc:"Смайлы"
},
searchreplace:{
search_desc:"Поиск",
replace_desc:"Поиск/замена"
},
advimage:{
image_desc:"Вставить/изменить картинку"
},
advlink:{
link_desc:"Вставить/изменить ссылку"
},
xhtmlxtras:{
cite_desc:"Citation",
abbr_desc:"Abbreviation",
acronym_desc:"Acronym",
del_desc:"Deletion",
ins_desc:"Insertion",
attribs_desc:"Insert/Edit Attributes"
},
style:{
desc:"Редактировать CSS стиль"
},
paste:{
paste_text_desc:"Вставить как неформатированый текст",
paste_word_desc:"Вставить из WORD-а",
selectall_desc:"Выделить всё"
},
paste_dlg:{
text_title:"Используйте CTRL+V чтобы вставить текст",
text_linebreaks:"Оставить разрывы линий",
word_title:"Используйте CTRL+V чтобы вставить текст"
},
table:{
desc:"Вставить новую таблицу",
row_before_desc:"Вставить ряд сверху",
row_after_desc:"Вставить ряд снизу",
delete_row_desc:"Удалить ряд",
col_before_desc:"Вставить колонку слева",
col_after_desc:"Вставить колонку справа",
delete_col_desc:"Удалить колонку",
split_cells_desc:"Разорвать объеденённые ячейки таблицы",
merge_cells_desc:"Объеденить яцейки в таблице",
row_desc:"Свойства ряда в таблице",
cell_desc:"Свойства ячейки таблицы",
props_desc:"Свойства таблицы",
paste_row_before_desc:"Вставить ряд в таблице сверху",
paste_row_after_desc:"Вставить ряд в таблице снизу",
cut_row_desc:"Вырезать ряд таблицы",
copy_row_desc:"Копировать ряд таблицы",
del:"Удалить таблицу",
row:"Ряд",
col:"Колонка",
cell:"Ячейка"
},
autosave:{
unload_msg:"The changes you made will be lost if you navigate away from this page."
},
fullscreen:{
desc:"Переключить в полноэкранный режим"
},
media:{
desc:"Вставить/править присоединённый flash",
edit:"Править присоединённый flash"
},
fullpage:{
desc:"Свойства документа"
},
template:{
desc:"Вставить шаблон"
},
visualchars:{
desc:"Визуальный контроль символов вкл/выкл"
},
spellchecker:{
desc:"Toggle spellchecker",
menu:"Spellchecker settings",
ignore_word:"Ignore word",
ignore_words:"Ignore all",
langs:"Languages",
wait:"Ждите...",
sug:"Suggestions",
no_sug:"No suggestions",
no_mpell:"No misspellings found."
},
pagebreak:{
desc:"Вставить разрыв страницы"
}}});