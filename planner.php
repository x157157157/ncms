<?php

/**
 * Created by PhpStorm.
 * User: Юрий
 * Date: 03.10.2016
 * Time: 22:42
 */
class planner {

	/** Добавляет обработчик, который будет срабатывать после foreach всех крон задач
	 * @param $fn
	 * @param $data
	 */
	public static function postcron_add( $fn, $data ) {
		$GLOBALS['post_cron'][]= [ $fn, $data ];
	}

	/**
	 * Добавляет новую задачу для отложенного выполнения
	 * @param $timestamp
	 * @param $fun
	 * @param bool $fun_data
	 * @param bool $tag1
	 * @param bool $tag2
	 * @param bool $tag3
	 * @return int|string
	 */
	public static function add( $timestamp, $fun, $fun_data=false, $tag1=false, $tag2=false, $tag3=false ) {
		return mydbAddLine('planner',[
			'datetime'=>date( 'Y-m-d H:i:s', $timestamp ),
			'fun'=>$fun,
			'fun_data'=> json_encode($fun_data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE ),
			'tag1'=> $tag1,
			'tag2'=> $tag2,
			'tag3'=> $tag3,
		]);
	}

	/** Генерирует datetime для планировщика
	 * Можно указать
	 * таймстамп 1234214
	 * относительный таймстамп +60 (через минуту)
	 * дату
	 *
	 * @param $text_or_timestamp
	 */
	public static function datetime_gen( $text_or_timestamp){

	}

	/**
	 * Главный кронер - ищет для каких задачь надо выполнить и выполняет их
	 * @param bool $_hoffset смещение по времени для тестирования
	 * @param bool  $_must_run если false, то запущено не будет, а просто выведет списток найденных заданий
	 * @return array
	 */
	public static function cron($_hoffset=false, $_must_run=false) {

		// определяем нужноли выполнять задание или просто искать
		$_must_run= $GLOBALS['_must_run'] ? : $_must_run;

		// ищем таймстамп и если надо смещаем по времени
		$timestamp= time() + $_hoffset*3600 + $GLOBALS['time_offset'];


		$qu='
			SELECT * 
			FROM '.pref_db.'planner
			WHERE
			datetime < '.xs( date( 'Y-m-d H:i:s', $timestamp ) ).'
				AND
			status NOT IN ( \'error_3\', \'done\', \'canceled\' ) 
			ORDER BY datetime ASC
		';

		$tasks=mydb_query_all($qu);

		//Выполняем задачи, если это не тестовый режим со смещённым временем
		if ( !$_hoffset or $_must_run ) {

			foreach ($tasks as $task ) {

				$results[]= planner::run_task($task);

			}
		}

		// запуск посткронера
		foreach ( $GLOBALS['post_cron'] as $task  ) {
			fn( $task[0], $task[1] );
		}

		return get_defined_vars();
	}


	/** запускает выполнение одной запланированной задачи
	 *
	 */
	public static function run_task($id_or_arr) {

		if ( is_array( $id_or_arr ) ) {
			$task =$id_or_arr;
		} else {
			$task = mydbget('planner', 'id', $id_or_arr);
		}

		if ($task['status']!=='ERROR_3') {

			// выполнение назначенного задания
			$result = fn($task['fun'], $task );


			$old_status=explode( '_', $task['status']);

			if ($result['status']!=='done' ) {

				$result['status']= $old_status[0]=='error' ? 'error_'.($old_status[1]+1) : 'error_1' ;
			}

			mydbUpdate('planner', $result, 'id', $task['id'] );
			loger(0,'planner_task',0,0,0,['task'=>$task,'result'=> $result['result'] ]);
		}

		return get_defined_vars();

	}

	public static function test_cron_fun($task) {

		$out_task['result']=serialize( json_decode( $task['fun_data'],1) );

		//$out_task['status']='done';
		return $out_task;

	}

}