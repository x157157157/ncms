<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"><html  xmlns="http://www.w3.org/1999/xhtml"><head>
<title>Лог:</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" charset="utf-8"></script>
<script src="https://code.jquery.com/jquery-migrate-1.2.1.js" charset="utf-8"></script>
<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js" charset="utf-8"></script>
<script src="<?=core; ?>/plug.js" charset="utf-8"></script>
<script src="<?=core; ?>/plugadmin.js" charset="utf-8"></script>
<script> adminpanel=0; </script>
<script src="<?=core; ?>/my.js?1438939969"  charset="UTF-8"></script>
<link rel="shortcut icon" type="image/png" href="/cont/files/favicon.png?1390766400" >
<style>
	html, body { height:100%; margin:0; padding:0; }
	._log ._log_data { display:none; }
	._log.toggled ._log_data { display:block; }
	._log_name { cursor:pointer; padding: 5px; }
</style>
<script>

	$(function(){

		$.cookie( 'show_all', 1 );

		$('._log_name').click(function() {
			$_log=$(this).parent();
			$_log.toggleClass('toggled');

			$.localStorage( $_log.attr( 'id' ), $_log.hasClass('toggled') ? 1 : null );

		} );

		$('._log').each(function() {
			if ( $.localStorage( $(this).attr('id') ) ) { $(this).addClass('toggled'); }
		});
	});
</script>
</head>
<body>
<?
?>
<table style="height: 100%; width:100%;">
	<tr>
		<td valign="top" >
			<?

			$sufix=debug_suf();

			$rep = [
					'</textarea>'=>'[/textarea]'
			];

			$ver=$_GET['ver'] ? '.'.intval($_GET['ver']): ''; // add version url
			foreach ( explode('[vl_logger_sep]', file_get_contents('cont/logs/debug_'.$sufix.$ver.'.txt') ) as $k=>$log ) {

				/*'date'=> date('H:m:s'),
				'context'=>trim($_context),
				'data'=> is_array($in)  ? print_r($in, 1) : $in */

				$log=unserialize($log);
				$log['context']=explode(' ', $log['context']);
				$log['context_arr']=unserialize( $log['context_arr']);

				    //


				if ( $k==5 ) { ?>
				<div style="position: fixed; top: 0; left: 50%; background: rgba(255,255,255,1); padding:5px; box-shadow:1px 1px 5px rgba(0,0,0,0.4);"><?=$log['date'] ?> (<?=debug_suf() ?>)</div>

				<? }

				//todo надо доделать вывод ( например избавлятся от слешей при выводе sql запроса )
				if ( 0 ) {
					$data = '!!!!'.$log['data'];
				} else {
					$data = strtr( $log['data'], $rep ) ;
				}


				$time_exec = round(   strval (  $log['context_arr']['time_exec']*1000 ), 3 );
			$time_exec = $time_exec ?  '(time_exec:<span class="time_exec">'.$time_exec.'</span>ms)': ''

				?>
				<div class="_log" id="id<?=md5($log['context'][0].' '.$log['context'][1]) ?>" style="padding: 0 0 0 <?=intval($log['bedug_ur'])*40 ?>px" >
					<div class="_log_name"><span style="font-weight: <?=$log['context'][1]=='params' ? 'bold ' : '' ?>;">
							<?=$log['context'][0] ?></span>
						<?=$log['line']? '('.$log['line'].')': '' ; ?>
						<?=$log['context'][1] ?>
						<?=$time_exec; ?>

					</div>
					<div class="_log_data">
						<textarea style="width: 100%; " rows="<?=substr_count(  $log['data'], "\n" )+1; ?>"><?=$data; ?></textarea>
					</div>
				</div>
			<? } ?>
		</td>
		<td style="width:20%; vertical-align: top;"  >
				<?


				// file_get_contents('cont/logs/bebug_all.txt');

				foreach ( explode( "\n", "show_all\n" ) as $c ) {

					$cv=preg_replace('/[^a-zа-яё]+/iu', '_', $c);

					if ($c AND !$was[$c] ) {
						?>
						<div>
							<label>
								<input rel="<?=$cv ?>" class="ch_context" <?=( $_COOKIE[$cv] ? 'checked="checked" ' : ''); ?>  type="checkbox" />
								<?=$c; ?>
							</label>

						</div>
						<?
						$was[$c]=1;
					} ?>
				<? } ?>
		</td>
	</tr>
</table>
<script type="">
	$('.time_exec').filter(function( index ) {
		return $( this ).html() > 15 ;
	}).parent().addClass('red')

</script>
<style>
	.red { background-color: red; }
</style>
<?
exit();
?>
</body>
</html>